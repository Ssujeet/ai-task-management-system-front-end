import React from 'react';
import PrivateRoutes from 'routes';
import { Switch } from 'react-router-dom';
import { routes } from 'routes/routesLists';

import './app.css';
import FileViewerListsWithModal from 'components/FileViewerListsWithModal';

function App() {
  return (
    <Switch>
      <PrivateRoutes routes={routes} />;
    </Switch>
  );
}

export default App;
