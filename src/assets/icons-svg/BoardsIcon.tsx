import React from 'react';

function BoardIcon({ color }) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="none" viewBox="0 0 16 16">
      <path
        fill={color}
        d="M12.167 1.665a2.167 2.167 0 012.164 2.044l.003.123v8.003A2.167 2.167 0 0112.167 14h-8a2.167 2.167 0 01-2.164-2.044L2 11.835V3.832a2.167 2.167 0 012.167-2.167h8zm-4.5 4.667H3v5.503l.005.106a1.166 1.166 0 001.162 1.06h3.5V6.332zm5.666 4H8.667v2.669h3.5a1.167 1.167 0 001.166-1.167v-1.502zm-1.166-7.667h-3.5v6.668h4.666l.001-5.5-.005-.107a1.167 1.167 0 00-1.162-1.06z"
      ></path>
      <path fill={color} d="M7.667 2.665h-3.5l-.096.004a1.167 1.167 0 00-1.07 1.163L3 5.333h4.667V2.664z"></path>
    </svg>
  );
}

export default BoardIcon;
