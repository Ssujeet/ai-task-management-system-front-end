import React from 'react';

function Calendar() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="none" viewBox="0 0 16 16">
      <path
        fill="#343434"
        d="M12.666 2.666h-1.333V2A.666.666 0 1010 2v.666H6V2a.667.667 0 10-1.334 0v.666H3.333a2 2 0 00-2 2v8a2 2 0 002 2h9.333a2 2 0 002-2v-8a2 2 0 00-2-2zm.667 10a.666.666 0 01-.667.667H3.333a.666.666 0 01-.667-.667V8h10.667v4.666zm0-6H2.666v-2A.667.667 0 013.333 4h1.333v.666a.667.667 0 001.334 0V4h4v.666a.667.667 0 001.333 0V4h1.333a.666.666 0 01.667.666v2z"
      ></path>
    </svg>
  );
}

export default Calendar;
