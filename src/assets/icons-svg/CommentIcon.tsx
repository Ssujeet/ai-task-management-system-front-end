import React from 'react';

function CommentIcon() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="none" viewBox="0 0 12 12">
      <g fill="#343434" clipPath="url(#clip0_361_33)">
        <path d="M10.5.75a.75.75 0 01.75.75v6a.75.75 0 01-.75.75H8.625a1.5 1.5 0 00-1.2.6L6 10.75l-1.425-1.9a1.5 1.5 0 00-1.2-.6H1.5a.75.75 0 01-.75-.75v-6A.75.75 0 011.5.75h9zM1.5 0A1.5 1.5 0 000 1.5v6A1.5 1.5 0 001.5 9h1.875a.75.75 0 01.6.3L5.4 11.2a.75.75 0 001.2 0l1.425-1.9a.75.75 0 01.6-.3H10.5A1.5 1.5 0 0012 7.5v-6A1.5 1.5 0 0010.5 0h-9z"></path>
        <circle cx="3" cy="5" r="1"></circle>
        <circle cx="6" cy="5" r="1"></circle>
        <circle cx="9" cy="5" r="1"></circle>
      </g>
      <defs>
        <clipPath id="clip0_361_33">
          <path fill="#fff" d="M0 0H12V12H0z"></path>
        </clipPath>
      </defs>
    </svg>
  );
}

export default CommentIcon;
