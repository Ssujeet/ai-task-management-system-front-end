import React from 'react';

function CrossIcon() {
  return (
    <svg className="m-auto" xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="none" viewBox="0 0 14 14">
      <path
        className="stroke-[#222] group-hover:stroke-[#F85365]"
        strokeLinecap="round"
        strokeWidth="1.333"
        d="M13 13L1 1m12 0L1 13"
      ></path>
    </svg>
  );
}

export default CrossIcon;
