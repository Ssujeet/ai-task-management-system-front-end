import React from 'react';

function DeadLineIcon() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="none" viewBox="0 0 14 14">
      <path
        fill="#000"
        d="M7 .333C3.335.333.335 3.333.335 7c0 3.666 3 6.666 6.667 6.666 3.666 0 6.666-3 6.666-6.666 0-3.667-3-6.667-6.666-6.667zm0 12A5.34 5.34 0 011.668 7a5.34 5.34 0 015.334-5.334A5.34 5.34 0 0112.334 7a5.34 5.34 0 01-5.333 5.333zm.334-8.667h-1v4L9.801 9.8l.533-.867-3-1.8V3.666z"
      ></path>
    </svg>
  );
}

export default DeadLineIcon;
