import React from 'react';

function DocumentIcon({ color }) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="none" viewBox="0 0 16 16">
      <path
        stroke={color}
        strokeLinejoin="round"
        d="M10.5 8.254v5.371c0 .76-.595 1.375-1.342 1.375H3.344C2.595 15 2 14.384 2 13.625v-8.25A1.383 1.383 0 013.375 4h2.941a.767.767 0 01.547.23l3.41 3.469a.794.794 0 01.227.555v0z"
      ></path>
      <path
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M6.25 4v3.375a.886.886 0 00.875.875H10.5"
      ></path>
      <path
        stroke={color}
        strokeLinejoin="round"
        d="M5.5 4V2.375A1.383 1.383 0 016.875 1h2.938a.776.776 0 01.55.23l3.41 3.469A.784.784 0 0114 5.25v5.375c0 .76-.595 1.375-1.342 1.375H10.75"
      ></path>
      <path
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M9.75 1v3.375a.886.886 0 00.875.875H14"
      ></path>
    </svg>
  );
}

export default DocumentIcon;
