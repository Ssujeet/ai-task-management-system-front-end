import React from 'react';

function DoubleTick() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="9" fill="none" viewBox="0 0 14 9">
      <path
        fill="#fff"
        d="M10.5 1.083L9.678.261 5.979 3.959l.823.823L10.5 1.083zm2.473-.822L6.802 6.432 4.363 4l-.822.822L6.8 8.083l7-7-.828-.822zM.24 4.822L3.5 8.083l.823-.822L1.068 4l-.829.822z"
      ></path>
    </svg>
  );
}

export default DoubleTick;
