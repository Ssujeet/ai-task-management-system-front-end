import React from 'react';

function DropDownIcon() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="none" viewBox="0 0 16 16">
      <rect width="16" height="16" fill="#E5E5E5" fillOpacity="0.43" rx="4"></rect>
      <path fill="#343434" d="M8 11L2.804 6.5h10.392L8 11z"></path>
    </svg>
  );
}

export default DropDownIcon;
