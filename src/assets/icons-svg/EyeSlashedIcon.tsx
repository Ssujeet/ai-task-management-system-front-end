import React from 'react';

function EyeSlashedIcon() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="none" viewBox="0 0 16 16">
      <path
        stroke="#343434"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeOpacity="0.6"
        strokeWidth="1.333"
        d="M4.582 11.42c-1.23-.874-2.203-2.01-2.753-2.727a1.129 1.129 0 010-1.385C2.824 6.008 5.212 3.333 8 3.333c1.251 0 2.42.538 3.42 1.25"
      ></path>
      <path
        stroke="#343434"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeOpacity="0.6"
        strokeWidth="1.333"
        d="M9.421 6.591A2 2 0 106.593 9.42m-3.925 3.914L13.335 2.667m-6.667 9.802c.433.13.882.196 1.333.198 2.788 0 5.176-2.676 6.172-3.975a1.13 1.13 0 00-.001-1.385c-.35-.457-.725-.893-1.124-1.307"
      ></path>
    </svg>
  );
}

export default EyeSlashedIcon;
