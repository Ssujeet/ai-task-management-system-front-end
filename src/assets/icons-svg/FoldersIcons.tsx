import React from 'react';

function FoldersIcon() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="22" fill="none" viewBox="0 0 24 22">
      <rect width="24" height="19.826" y="2.087" fill="#5486FC" rx="3"></rect>
      <rect width="13.043" height="7.304" fill="#5486FC" rx="3"></rect>
      <path stroke="#fff" d="M7.826 18.26h7.826"></path>
    </svg>
  );
}

export default FoldersIcon;
