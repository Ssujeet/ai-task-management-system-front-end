import React from 'react';

function LabelIcon() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="16" fill="none" viewBox="0 0 12 16">
      <path
        stroke="#222"
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M1 1.333h10v13.333l-5-3.523-5 3.523V1.333z"
      ></path>
      <path stroke="#222" strokeLinecap="round" strokeLinejoin="round" d="M1 1.333h10v4H1v-4z"></path>
    </svg>
  );
}

export default LabelIcon;
