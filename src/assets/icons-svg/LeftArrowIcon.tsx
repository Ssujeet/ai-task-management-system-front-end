import React from 'react';

function LeftArrowIcon() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="none" viewBox="0 0 16 16">
      <path
        stroke="#000"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeMiterlimit="10"
        strokeWidth="0.444"
        d="M5.734 12.398l4.309-4.324-4.31-4.324"
      ></path>
    </svg>
  );
}

export default LeftArrowIcon;
