import React from 'react';

function MeetingsIcon({ color }) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="none" viewBox="0 0 16 16">
      <path
        fill={color}
        d="M4.665 2c-.896 0-1.683.596-1.926 1.459l-1.66 5.898A2.08 2.08 0 003.083 12h9.834a2.08 2.08 0 002.004-2.643l-1.66-5.898A2 2 0 0011.335 2h-6.67zm-.963 1.73A1 1 0 014.665 3h6.67a1 1 0 01.963.73l1.66 5.898A1.08 1.08 0 0112.918 11H3.082a1.082 1.082 0 01-1.04-1.372l1.66-5.899v.001zM4.497 13a.5.5 0 000 1h7.005a.5.5 0 000-1H4.497z"
      ></path>
    </svg>
  );
}

export default MeetingsIcon;
