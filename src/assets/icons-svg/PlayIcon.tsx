import React from 'react';

function PlayIcon() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="none" viewBox="0 0 28 28">
      <path
        fill="#F85365"
        d="M14 0C6.269 0 0 6.269 0 14s6.269 14 14 14 14-6.269 14-14S21.731 0 14 0zm4.503 14.216l-6.825 4.965a.25.25 0 01-.397-.203V9.053a.25.25 0 01.397-.203l6.825 4.963a.246.246 0 01.077.314.246.246 0 01-.077.089z"
      ></path>
    </svg>
  );
}

export default PlayIcon;
