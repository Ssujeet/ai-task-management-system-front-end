import React from 'react';

function PlusIcon() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="19" height="18" fill="none" viewBox="0 0 19 18">
      <path
        className="stroke-[#F95868] group-hover:stroke-[#ffff]"
        strokeLinecap="round"
        strokeWidth="1.333"
        d="M9.5 17V9m0 0V1m0 8h8m-8 0h-8"
      ></path>
    </svg>
  );
}

export default PlusIcon;
