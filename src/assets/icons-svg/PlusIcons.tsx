import React from 'react';

function PlusIcon() {
  return (
    <svg className="m-auto" xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" viewBox="0 0 24 24">
      <path stroke="#fff" strokeLinecap="round" strokeWidth="1.333" d="M12 12H4m8 8v-8 8zm0-8V4v8zm0 0h8-8z"></path>
    </svg>
  );
}

export default PlusIcon;
