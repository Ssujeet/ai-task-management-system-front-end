import React from 'react';

function SearchIcon() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" fill="none" viewBox="0 0 16 17">
      <path
        stroke="#222"
        strokeLinecap="round"
        strokeOpacity="0.5"
        strokeWidth="1.333"
        d="M14 14.5l-2.991-2.996M12.666 7.5a5.667 5.667 0 11-11.333 0 5.667 5.667 0 0111.333 0v0z"
      ></path>
    </svg>
  );
}

export default SearchIcon;
