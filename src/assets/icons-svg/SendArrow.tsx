import React from 'react';

function SendArrow() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="none" viewBox="0 0 16 16">
      <path
        fill="url(#paint0_linear_594_1659)"
        d="M1.177 1.119a.5.5 0 01.547-.066l13 6.5a.5.5 0 010 .894l-13 6.5a.5.5 0 01-.702-.594L2.977 8 1.022 1.647a.5.5 0 01.155-.528zm2.692 7.38l-1.547 5.03L13.382 8 2.322 2.47 3.87 7.5H9.5a.5.5 0 010 1H3.87z"
      ></path>
      <defs>
        <linearGradient
          id="paint0_linear_594_1659"
          x1="0.527"
          x2="13.637"
          y1="1"
          y2="20.247"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#F85365"></stop>
          <stop offset="0.844" stopColor="#FC6767" stopOpacity="0.83"></stop>
        </linearGradient>
      </defs>
    </svg>
  );
}

export default SendArrow;
