import React from 'react';

function SpinLogo() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="28"
      height="28"
      fill="none"
      viewBox="0 0 28 28"
      className="animate-spin"
    >
      <path fill="#F95868" d="M21.5 0A6.5 6.5 0 0128 6.5V7a7 7 0 01-7 7h-6V6.5A6.5 6.5 0 0121.5 0z"></path>
      <path fill="#F95968" d="M14 14v7a7 7 0 11-7-7h7z"></path>
      <circle cx="7" cy="6" r="6" fill="#343434" fillOpacity="0.8"></circle>
      <circle cx="21.5" cy="21.5" r="6.5" fill="#343434" fillOpacity="0.8"></circle>
    </svg>
  );
}

export default SpinLogo;
