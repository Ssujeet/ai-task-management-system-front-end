import React from 'react';

function StatusIcon() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="none" viewBox="0 0 16 16">
      <path
        fill="#000"
        d="M1.6 4.8a1.6 1.6 0 011.6-1.6h9.6a1.6 1.6 0 011.6 1.6v6.4a1.6 1.6 0 01-1.6 1.6H3.2a1.6 1.6 0 01-1.6-1.6V4.8zM3.2 4a.8.8 0 00-.8.8v6.4a.8.8 0 00.8.8h4.4V4H3.2zm9.6 8a.8.8 0 00.8-.8V4.8a.8.8 0 00-.8-.8H8.4v8h4.4zM9.6 4.8a.4.4 0 100 .8h2.8a.4.4 0 000-.8H9.6z"
      ></path>
    </svg>
  );
}

export default StatusIcon;
