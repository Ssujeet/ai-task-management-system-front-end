import React from 'react';

function StopIcon() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="none" viewBox="0 0 32 32">
      <path
        fill="#F85365"
        d="M16 2C8.269 2 2 8.269 2 16s6.269 14 14 14 14-6.269 14-14S23.731 2 16 2zm-2.5 18.75a.25.25 0 01-.25.25h-1.5a.25.25 0 01-.25-.25v-9.5a.25.25 0 01.25-.25h1.5a.25.25 0 01.25.25v9.5zm7 0a.25.25 0 01-.25.25h-1.5a.25.25 0 01-.25-.25v-9.5a.25.25 0 01.25-.25h1.5a.25.25 0 01.25.25v9.5z"
      ></path>
    </svg>
  );
}

export default StopIcon;
