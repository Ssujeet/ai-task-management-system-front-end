import React from 'react';

function TasksPlusIcon() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" fill="none" viewBox="0 0 17 17">
      <rect width="16.667" height="16.667" className="fill-[#C4C4C4] group-hover:fill-[##F85365]" rx="8.333"></rect>
      <path
        stroke="#fff"
        strokeLinecap="round"
        strokeWidth="1.333"
        d="M8.333 8.333H3m5.333 5.334V8.333v5.334zm0-5.334V3v5.333zm0 0h5.334-5.334z"
      ></path>
    </svg>
  );
}

export default TasksPlusIcon;
