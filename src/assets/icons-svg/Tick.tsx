import React from 'react';

function Tick() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="8" fill="none" viewBox="0 0 11 8">
      <path stroke="#fff" d="M.958 4.01L3.57 6.869 10 1"></path>
    </svg>
  );
}

export default Tick;
