import React from 'react';

function UserAvatarIcon() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="none" viewBox="0 0 16 16">
      <path
        fill="#000"
        d="M8 7.555a3.111 3.111 0 100-6.222 3.111 3.111 0 000 6.222zm0-5.333a2.222 2.222 0 110 4.444 2.222 2.222 0 010-4.444zM13.543 10.831a7.628 7.628 0 00-11.08 0 .889.889 0 00-.24.61v2.337a.889.889 0 00.889.889h9.777a.889.889 0 00.89-.889V11.44a.889.889 0 00-.236-.609zm-.654 2.947H3.112v-2.342a6.742 6.742 0 019.777 0v2.342z"
      ></path>
    </svg>
  );
}

export default UserAvatarIcon;
