import React from 'react';

function VerticalThreeDotsIcon() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="6" height="24" fill="none" viewBox="0 0 6 24">
      <circle cx="3" cy="3" r="3" fill="#C4C4C4"></circle>
      <circle cx="3" cy="12" r="3" fill="#C4C4C4"></circle>
      <circle cx="3" cy="21" r="3" fill="#C4C4C4"></circle>
    </svg>
  );
}

export default VerticalThreeDotsIcon;
