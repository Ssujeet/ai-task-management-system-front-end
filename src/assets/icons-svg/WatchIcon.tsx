import React from 'react';

function WatchIcon() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" viewBox="0 0 20 20">
      <path fill="#343434" d="M9.375 11.25h2.5V10H10V6.875H8.75v3.75l.625.625z"></path>
      <path
        fill="#343434"
        fillRule="evenodd"
        d="M6.875 4.586a6.248 6.248 0 00-2.288 8.54 6.248 6.248 0 002.288 2.288v2.711l.625.625h5l.625-.625v-2.711a6.248 6.248 0 002.288-8.54 6.247 6.247 0 00-2.288-2.288V1.875L12.5 1.25h-5l-.625.625v2.711zM15 10a5 5 0 11-10 0 5 5 0 0110 0z"
        clipRule="evenodd"
      ></path>
    </svg>
  );
}

export default WatchIcon;
