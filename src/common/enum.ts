export enum ROLE {
  USER = 'USER',
  ADMIN = 'ADMIN',
  MEMBER = 'MEMBER',
}

export enum PROJECTMEMBERROLE {
  LEADER = 'LEADER',
  MEMBER = 'MEMBER',
}

export enum USERSTATUSINPROJECT {
  ACTIVE = 'ACTIVE',
  DEACTIVATE = 'DEACTIVATE',
}

export enum PROJECTSTATUS {
  ONPROGRESS = 'ONPROGRESS',
  COMPLETED = 'COMPLETED',
}

export enum TASKSPRIORITY {
  HIGH,
  MEDUIM,
  LOW,
}

export enum USERSTATUSINTASK {
  ACTIVE,
  DEACTIVE,
}

export enum STANDUPNOTIFICATIONREPEATSSCHUDULE {
  ONCEAWEEK = 'ONCEAWEEK',
  ONCEAYEAR = 'ONCEAYEAR',
  ONCEAMONTH = 'ONCEAMONTH',
}

export enum SCREENS {
  BOARDS = 'boards',
  DOCUMENTS = 'documents',
  CHAT = 'chat',
  STANDUP = 'stand-up',
  MEMBERS = 'members',
  ROLES = 'roles',
}

export enum SCREENS_ROUTES_ENUMS {
  DASHBOARD = '/',
  PROFILE = '/profile',
  BILLING = '/billing-and-payments',
  WORKSPACE = '/work-space',
  Chats = '/work-space/chat/:id',
  ROLES = '/work-space/roles/:id',
  BOARDS = '/work-space/boards/:id',
  SUPPORT_TICKET = '/support-ticket',
  SUPPORT_TICKET_DETAILS = '/support-ticket/:supportTicketId',
  MEMBERS = '/work-space/members/:id',
  STANDUP = '/work-space/stand-up/:id',
  PERMISSIONS = '/work-space/roles/:id/permissions/:rolesId',
  Documents = '/work-space/documents/:id/:projectFolderRootId',
  STANDUPDETAILS = '/work-space/stand-up/:projectId/details/:standUpId',
}

export enum TASKTIMETRACKER {
  STOP,
  START,
}

export enum PROJECTTYPENOTIFICATIONS {
  MEMBER,
  MEMBER_REMOVED,
  TASKS_RELATED,
  STAND_UPS,
  DOCUMENTS,
  CHATS,
  GENERAL,
  SUPPORT_TICKETS,
}

export enum NOTIFICATION_STATUS {
  SEEN = 'SEEN',
  UNSEEN = 'UNSEEN',
}
