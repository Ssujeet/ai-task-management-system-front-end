export interface BaseResponse<T> {
  meta: any;
  data: T | null;
  error: errorType | null;
}

export type errorType = {
  statusCode: number;
  message: string;
  localizedMessage: string;
  errorName: string;
  details: any;
  path: string;
  requestId: string;
  timestamp: string;
};

export interface AbstractInterface {
  id: number;

  createdAt: Date;

  updatedAt: Date;
}
