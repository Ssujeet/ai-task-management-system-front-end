import React from 'react';
import AddIcon from '@material-ui/icons/Add';
import PlusIcon from 'assets/icons-svg/PlusIcon';

interface IAddButton {
  name: string;
  onClick: (e) => void;
  className?: string;
}
const AddButton = (props: IAddButton) => {
  return (
    <div className="group">
      <button
        className={
          ' border-[#F95868] group-hover:bg-[#F95868] border px-[24px] py-[10px] text-[16px] flex justify-between rounded-[4px] text-white ' +
          props.className
        }
        onClick={props.onClick}
      >
        <div className="flex my-auto">
          <PlusIcon />
        </div>

        <span className="ml-4 group-hover:text-white text-[16px] text-[#F95868]">{props.name}</span>
      </button>
    </div>
  );
};

export default AddButton;
