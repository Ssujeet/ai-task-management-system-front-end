import React from 'react';

type buttonStye = {
  size?: 'sm' | 'lg';
  color?: 'primary' | 'secondary' | 'info' | 'danger' | 'success';
  textColor?: string;
  isOutline?: boolean;
};
interface Ibuttons {
  buttonText?: string;
  buttonStyle?: buttonStye;
  onClick?: () => void;
  type: 'button' | 'submit' | 'reset';
  bootstrapClasses?: string;
}
const Button: React.FC<Ibuttons> = (props): React.ReactElement => {
  const bootstrapClassName = `btn btn${props.buttonStyle?.isOutline ? '-outline-' : '-'}${
    props.buttonStyle?.color || 'primary'
  } btn-${props.buttonStyle?.size || 'sm'}`;

  return (
    <button
      type={props.type}
      onClick={() => (props.onClick ? props.onClick() : alert('Add a Call Back Function for OnClick in Button'))}
      className={bootstrapClassName}
    >
      {props.buttonText ? props.buttonText : 'Click Me'}
    </button>
  );
};

export default Button;
