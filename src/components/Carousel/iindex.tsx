import React, { useEffect, useState, FC } from 'react';
import { useSwipeable } from 'react-swipeable';

export const CarouselItem: FC<{ children: React.ReactNode; width?: number }> = ({ children, width }) => {
  return <div className="inline-flex items-center justify-center   w-full">{children}</div>;
};

const Carousel: FC<{
  children: React.ReactNode;
  showArrowButton?: boolean;
  hideDotNavigations?: boolean;
  autoScroll?: boolean;
  customActiveIndex?: number;
}> = ({ children, showArrowButton = false, hideDotNavigations = false, autoScroll = true, customActiveIndex = 0 }) => {
  const [activeIndex, setActiveIndex] = useState(customActiveIndex);
  const [paused, setPaused] = useState(false);

  const updateIndex = (newIndex: number) => {
    if (newIndex < 0) {
      newIndex = React.Children.count(children) - 1;
    } else if (newIndex >= React.Children.count(children)) {
      newIndex = 0;
    }

    setActiveIndex(newIndex);
  };

  useEffect(() => {
    if (autoScroll) {
      const interval = setInterval(() => {
        if (!paused) {
          updateIndex(activeIndex + 1);
        }
      }, 3000);

      return () => {
        if (interval) {
          clearInterval(interval);
        }
      };
    }
  });

  const handlers = useSwipeable({
    onSwipedLeft: () => updateIndex(activeIndex + 1),
    onSwipedRight: () => updateIndex(activeIndex - 1),
  });

  return (
    <div
      {...handlers}
      className="overflow-hidden "
      onMouseEnter={() => setPaused(true)}
      onMouseLeave={() => setPaused(false)}
    >
      <div
        className="whitespace-nowrap transition transform duration-[0.3s]"
        style={{ transform: `translateX(-${activeIndex * 100}%)` }}
      >
        {React.Children.map(children, (child: any, index) => {
          return React.cloneElement(child, { width: '100%' });
        })}
      </div>
      {!hideDotNavigations && (
        <div className="flex justify-center gap-[4px] mt-[31px]">
          {React.Children.map(children, (child, index) => {
            return (
              <button
                className={`${
                  index === activeIndex ? 'bg-white w-[18px] ' : 'w-[11px] '
                }border rounded-[20px]  h-[8px]`}
                onClick={() => {
                  updateIndex(index);
                }}
              ></button>
            );
          })}
        </div>
      )}
      {showArrowButton && React.Children.count(children) > 1 ? (
        <div className="hidden md:flex justify-center gap-[24px] mt-[10px]">
          <button
            className={`${
              activeIndex === 0 ? 'opacity-50' : ''
            } bg-white w-[32px] h-[32px] bg-white border rounded-[50%] grid place-content-center `}
            onClick={() => {
              activeIndex && updateIndex(activeIndex - 1);
            }}
          >
            <img src={'/left.svg'} alt="arrowLeft" width={6} height={12} />
          </button>

          <button
            className={`${
              activeIndex === React.Children.count(children) - 1 ? 'opacity-50' : ''
            } bg-white w-[32px] h-[32px] bg-white border rounded-[50%] grid place-content-center `}
            onClick={() => {
              activeIndex !== React.Children.count(children) - 1 && updateIndex(activeIndex + 1);
            }}
          >
            <img src={'/right.svg'} alt="arrowRIght" width={6} height={12} />
          </button>
        </div>
      ) : null}
    </div>
  );
};

export default Carousel;
