import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
      width: '100%',
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: '100%',
    },
  })
);

interface IDatePickersProps {
  label?: string;
  value?: string;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  defaultValue?: string;
  name?: string;
}
export default function DatePickers(props: IDatePickersProps) {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <TextField
        id="date"
        label={props.label}
        type="date"
        defaultValue={props.defaultValue}
        className={classes.textField}
        InputLabelProps={{
          shrink: true,
        }}
        name={props.name}
        value={props.value}
        onChange={props.onChange}
      />
    </div>
  );
}
