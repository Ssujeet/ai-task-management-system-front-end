import React from 'react';
import MaxWidthDialog, { IModal } from '../Modal';

interface DeleteModalI extends IModal {
  deleteMessage: string;
  onConfirmClicked: () => void;
  onDiscardClicked: () => void;
}

export const DeleteModal = (props: DeleteModalI) => {
  return (
    <MaxWidthDialog {...props}>
      <div className="p-2 px-5">
        <div>
          <h2 className=" text-xl">{props.deleteMessage}</h2>
        </div>

        <div className=" mt-5 flex flex-row justify-end pb-[21px]">
          <button
            type="button"
            className="cursor-pointer hover:text-red-700 py-1 rounded-lg text-gray-700"
            onClick={props.onDiscardClicked}
          >
            Cancel
          </button>
          <button
            type="submit"
            className="cursor-pointer ml-[24px] bg-red-400 px-[35px] py-[12px] text-white rounded-[4px]"
            onClick={props.onConfirmClicked}
          >
            Confirm
          </button>
        </div>
      </div>
    </MaxWidthDialog>
  );
};
