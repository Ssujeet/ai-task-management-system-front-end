import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper, { PaperProps } from '@material-ui/core/Paper';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme: Theme) => createStyles({}));

interface IDraggableDialog {
  isModalOpen: boolean;
  toggleModal: () => void;
  children: any;
  renderTitle: any;
  submitLabel: string;
  onSubmitClicked?: () => void;
  buttonType?: 'button' | 'submit';
}

export default function DraggableDialog(props: IDraggableDialog) {
  const {
    isModalOpen,
    toggleModal,
    children,
    renderTitle,
    submitLabel,
    onSubmitClicked,
    buttonType = 'button',
  } = props;

  const classes = useStyles();

  return (
    <Dialog
      open={isModalOpen}
      onClose={toggleModal}
      // PaperComponent={PaperComponent}
      maxWidth={'md'}
      aria-labelledby="draggable-dialog-title"
    >
      <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
        {renderTitle()}
      </DialogTitle>
      <DialogContent>
        <DialogContentText>{children}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={toggleModal} color="primary">
          Cancel
        </Button>
        <Button color="primary" type={buttonType}>
          {submitLabel}
        </Button>
      </DialogActions>
    </Dialog>
  );
}
