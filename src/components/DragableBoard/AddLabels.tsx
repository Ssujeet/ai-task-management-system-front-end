import { ClickAwayListener } from '@material-ui/core';
import TasksPlusIcon from 'assets/icons-svg/TaskPlusIcon';
import classNames from 'classnames';
import React, { FC } from 'react';
import { toast } from 'react-toastify';
import { useGetTaskAttachmentByTaskIdQuery } from 'redux/reducers/task-attachments';
import { useAddRemoveLabelsInTaskMutation, useGetTasksLabelListByProjectIdQuery } from 'redux/reducers/tasks';
import { ITasksLabel } from 'redux/reducers/tasks-label';

interface IAddLabels {
  labels?: ITasksLabel[];
  projectId?: string;
  taskId?: string;
  hideLists?: boolean;
}
const AddLabels: FC<IAddLabels> = ({ labels, projectId, taskId, hideLists = false }) => {
  const [showDownDrop, setShowDownDrop] = React.useState(false);
  const { data: labelsLists } = useGetTasksLabelListByProjectIdQuery(projectId || '', { skip: !projectId });
  //   const { data } = useGetTaskAttachmentByTaskIdQuery(taskId);

  const [title, setTitle] = React.useState('');
  const [addRemoveLabelsInTask] = useAddRemoveLabelsInTaskMutation();
  const labelsLength = labels?.length ?? 0;

  return (
    <ClickAwayListener onClickAway={() => setShowDownDrop(false)}>
      <div className="relative inline-block text-left ">
        <div>
          <button
            type="button"
            className="inline-flex justify-center w-full rounded-md bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 "
            id="menu-button"
            aria-expanded="true"
            aria-haspopup="true"
            onClick={() => setShowDownDrop(!showDownDrop)}
          >
            <div className="flex my-auto flex-col justify-center">
              <TasksPlusIcon />
            </div>
            <span className="ml-2  ">Add Labels</span>
          </button>
        </div>

        {showDownDrop && (
          <div
            className={classNames(
              'z-[400000] absolute left-0 mt-2 w-56 focus:border-none rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 divide-y divide-gray-100 focus:outline-none',
              { 'divide-y-0 shadow-none mt-0': hideLists }
            )}
            role="menu"
            aria-orientation="vertical"
            aria-labelledby="menu-button"
          >
            {labelsLength < 3 ? (
              <div className={classNames('py-1 px-2', { 'px-0 py-0': hideLists })} role="none">
                <input
                  value={title}
                  name="taskCheck"
                  className="border block px-2 py-[5px] text-[12px] w-full"
                  type="text"
                  placeholder="Add new label"
                  onChange={(e) => setTitle(e.target.value)}
                  onKeyPress={(e) => {
                    if (e.key === 'Enter') {
                      const postData = {
                        title,
                        projectId,
                      };

                      if (taskId) {
                        postData['taskId'] = taskId.toString();
                      }
                      if (labelsLists?.data?.some((item) => item.title === title)) {
                        toast.error('Label is already present');

                        return;
                      }
                      addRemoveLabelsInTask(postData)
                        .unwrap()
                        .then((item) => {
                          setTitle('');
                        });
                    }
                  }}
                />
              </div>
            ) : null}
            <div className={classNames('py-1', { hidden: hideLists })} role="none">
              {!hideLists &&
                labelsLists?.data?.map((item) => (
                  <div
                    className="flex justify-between hover:bg-red-200"
                    onClick={() => {
                      if (labelsLength < 3 || labels?.some((label) => label.id === item.id)) {
                        const postData = {
                          projectId,
                          taskId: taskId?.toString(),
                        };
                        if (labels?.some((label) => label.id === item.id)) {
                          postData['removedLabelId'] = item.id.toString();
                        } else {
                          postData['newLabelID'] = item.id.toString();
                        }

                        addRemoveLabelsInTask(postData)
                          .unwrap()
                          .then((item) => {
                            setTitle('');
                          });
                      }
                    }}
                  >
                    <div className="flex px-2">
                      <div
                        className={'w-[10px] h-[10px] rounded-[2px] my-auto'}
                        style={{ backgroundColor: item.color }}
                      ></div>

                      <span
                        className={`block px-2 py-2 text-sm hover:text-[${item.color}]`}
                        role="menuitem"
                        id="menu-item-0"
                      >
                        {item.title}
                      </span>
                    </div>
                    {!labels?.some((label) => label.id === item.id) && (
                      <div className="flex flex-col justify-center px-2">
                        <TasksPlusIcon />
                      </div>
                    )}
                  </div>
                ))}
            </div>
          </div>
        )}
      </div>
    </ClickAwayListener>
  );
};

export default AddLabels;
