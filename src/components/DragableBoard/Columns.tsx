import * as React from 'react';
import * as ReactDOM from 'react-dom';
import styled from 'styled-components';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import Task from './Task';
import { Typography, makeStyles, createStyles, Theme, Grid, Button, ClickAwayListener } from '@material-ui/core';
import { red } from '@material-ui/core/colors';
import AddIcon from '@material-ui/icons/Add';
import { inherits } from 'util';
import TaskCreationsFormsWithFormikAndModal from 'pages/protected/Boards/BoardsDetails/TaskCreationsForm/index';
import useToggle from 'hooks/useToggle';
import {
  TaskInterface,
  useAddTasksMutation,
  useGetTaskStatusByProjectIdQuery,
  useUpdateTasksByIdMutation,
  useUpdateTaskStatusMutation,
} from 'redux/reducers/tasks';
import AddButton from 'components/Buttons/AddButton';
import TasksPlusIcon from 'assets/icons-svg/TaskPlusIcon';
import { useRouteMatch } from 'react-router-dom';
import { SCREENS_ROUTES_ENUMS } from 'common/enum';
import { toast } from 'react-toastify';

const List = styled.div<{ isDraggingOver: boolean; hasData: boolean }>`
  padding: 8px;
  transition: background 0.1s;
  background-color: ${(props) =>
    props.isDraggingOver ? 'lightgrey' : props.hasData ? 'rgba(165, 165, 165, 0.1) ' : 'rgba(165, 165, 165, 0.1)'};
  flex-grow: 1;
  border-radius: 8px;
  // border: ${(props) => (props.hasData ? '' : props.isDraggingOver ? '' : '1px solid #A5A5A5 ')};
`;

const Container = styled.div<{ isDragging: boolean }>`
  margin: 8px;
  width: 280px;
  position: relative;
  flex-shrink: 0;
`;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      padding: '8px',
      // backgroundColor: 'red',
    },
  })
);

export interface Task extends TaskInterface {
  taskId: number;
  content: string;
  commentsCount: number;
  attachmentCounts: number;
}

export interface ColumnProps {
  tasks: Task[];
  index: number;
  column: Column;
  onTaskClicked: (event: any, taskId: number) => void;
  sprintPlanningId: string;
}
export interface Column {
  id: string;
  title: string;
  taskStatusId: number;
  taskIds?: [];
  canEdit: boolean;
}
const Column = ({ column, tasks, index, onTaskClicked, sprintPlanningId }: ColumnProps) => {
  const classes = useStyles();
  let match = useRouteMatch<{ id: string }>(SCREENS_ROUTES_ENUMS.BOARDS);
  const { data: taskStatusLists } = useGetTaskStatusByProjectIdQuery(match?.params?.id || '');

  const [taskAdditionFormModal, setTaskAdditionFormModal] = useToggle(false);
  const [updateTaskStatus] = useUpdateTaskStatusMutation();
  const [taskName, setTaskName] = React.useState<string | undefined>(undefined);

  const [addTasks, { isLoading }] = useAddTasksMutation();

  const [toDoId, setToDoId] = React.useState<number>();
  const onTaskCreate = (values) => {
    const postData = {
      ...values,
      priority: +values.priority.value,
      taskMemberId: values.taskMemberId.map((item) => +item.value),
      taskStatusId: toDoId,
      sprintId: +sprintPlanningId,
    };
    addTasks(postData)
      .unwrap()
      .then((response) => setTaskAdditionFormModal(false))
      .catch((error) => console.log(error));
  };

  return (
    <>
      <TaskCreationsFormsWithFormikAndModal
        taskAdditionFormModal={taskAdditionFormModal}
        toggleModal={setTaskAdditionFormModal}
        onSubmit={onTaskCreate}
        isLoading={isLoading}
      />

      <Draggable draggableId={column.id} index={index}>
        {(provided, snapshot) => (
          <Container
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            isDragging={snapshot.isDragging}
            ref={provided.innerRef}
            hasData={!!column.taskIds?.length}
          >
            <div className="flex justify-between align-center mt-auto p-[10px] sticky h-[40px]">
              <div>
                <ClickAwayListener onClickAway={() => setTaskName(undefined)}>
                  {taskName ? (
                    <input
                      value={taskName}
                      name="taskCheck"
                      className="border block px-2 py-[2px] text-[14px] w-full"
                      type="text"
                      placeholder="Add new label"
                      onChange={(e) => setTaskName(e.target.value)}
                      onKeyPress={(e) => {
                        if (e.key === 'Enter') {
                          if (taskName) {
                            if (taskName === column.title) {
                              setTaskName(undefined);

                              return;
                            }

                            if (
                              taskStatusLists?.data?.some(
                                (item) => item.taskName.toLowerCase() === taskName.toLowerCase()
                              )
                            ) {
                              toast(taskName + ' already taken');

                              return;
                            }
                            const postData = {
                              taskName,
                              id: +column.taskStatusId,
                              projectId: match?.params.id ? +match?.params.id : 0,
                            };
                            updateTaskStatus(postData)
                              .unwrap()
                              .then((item) => {
                                setTaskName(undefined);
                              });
                          }
                        }
                      }}
                    />
                  ) : (
                    <p
                      className={'font-[500] mt-auto tracking-widest text-[14px]'}
                      {...provided.dragHandleProps}
                      onDoubleClick={() => column.canEdit && setTaskName(column.title)}
                    >
                      {column.title}
                      <span className="ml-[8px]">{column.taskIds?.length}</span>
                    </p>
                  )}
                </ClickAwayListener>
              </div>
              <div
                className="cursor-pointer group"
                onClick={() => {
                  setToDoId(column.taskStatusId);
                  setTaskAdditionFormModal(true);
                }}
              >
                <TasksPlusIcon />
                {/* {column.title.toUpperCase() === 'TODO' && (
                  <AddButton
                    name="Add Task"
                    onClick={() => {
                      setToDoId(column.taskStatusId);
                      setTaskAdditionFormModal(true);
                    }}
                  />
                )} */}
              </div>
            </div>

            <Droppable droppableId={column.id} type="task">
              {(provided, snapshot) => (
                <div className="h-[65vh] flex flex-col overflow-auto ">
                  <List
                    ref={provided.innerRef}
                    {...provided.droppableProps}
                    isDraggingOver={snapshot.isDraggingOver}
                    hasData={!!column.taskIds?.length}
                  >
                    {tasks.map((t, i) => (
                      <Task key={t.id} task={t} index={i} onTaskClicked={onTaskClicked} />
                    ))}
                    {provided.placeholder}
                  </List>
                </div>
              )}
            </Droppable>
          </Container>
        )}
      </Draggable>
    </>
  );
};

export default React.memo(Column);
