import * as React from 'react';
import * as ReactDOM from 'react-dom';
import styled from 'styled-components';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { Task } from './Columns';
import TaskCard from './TaskCard';

interface TaskProps {
  task: Task;
  index: number;
  onTaskClicked: (event: any, taskId: number) => void;
}
const TaskContainer = styled.div<{ isDragging: boolean }>`
  margin-bottom: 8px;
  // background-color: ${(props) => (props.isDragging ? 'red' : 'white')};
  transition: background 0.1s;
`;
const Tasks = ({ task, index, onTaskClicked }: TaskProps) => {
  return (
    <Draggable draggableId={task.id} index={index}>
      {(provided, snapshot) => (
        <TaskContainer
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          ref={provided.innerRef}
          isDragging={snapshot.isDragging}
        >
          <TaskCard onTaskClicked={onTaskClicked} task={task} />
        </TaskContainer>
      )}
    </Draggable>
  );
};

export default Tasks;
