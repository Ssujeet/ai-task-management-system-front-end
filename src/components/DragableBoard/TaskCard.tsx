import React from 'react';
import { Task } from './Columns';
import classNames from 'classnames';
import { getFirstLetters, hexToRGBA, months } from 'utils';
import ChatIcon from 'assets/icons-svg/ChatIcon';
import { useRouteMatch } from 'react-router-dom';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { red, blue, purple } from '@material-ui/core/colors';
import AttachmentIcon from 'assets/icons-svg/AttachmentIcon';
import { SCREENS_ROUTES_ENUMS, TASKSPRIORITY } from 'common/enum';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import AddMembersToTasks from 'pages/protected/Boards/BoardsDetails/TaskDetails/AddMembersToTasks';
import CommentIcon from 'assets/icons-svg/CommentIcon';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: '100%',
    },
    regBg: {
      backgroundColor: red[500],
    },
    blueBg: {
      backgroundColor: blue[500],
    },
    greenBg: {
      backgroundColor: purple[500],
    },
  })
);

interface ITaskCard {
  task: Task;
  onTaskClicked: (event: any, taskId: number) => void;
}

export default function TaskCard(props: ITaskCard) {
  const classes = useStyles();
  let match = useRouteMatch<{ id: string }>(SCREENS_ROUTES_ENUMS.BOARDS);

  console.log(props.task, 'task');
  const date = new Date(props.task?.createdAt || '');
  const memberLength = props?.task?.taskMember?.length ?? 0;

  return (
    <div className="relative  group">
      {/* <button aria-label="settings" className="hidden group-hover:block absolute right-0 z-[300000] ">
        <MoreVertIcon />
      </button> */}
      <div className="shadow-lg bg-white p-2 rounded-[4px] ">
        <div className="" onClick={(e) => props.onTaskClicked(e, props.task.taskId)}>
          <p className="text-[16px] font-[400] w-[95%] text-ellipsis overflow-hidden ">{props.task.title}</p>
          <div className="flex mt-[14px] gap-1">
            {props?.task?.labels?.map((item) => (
              <p
                className="px-[6px] py-[3px] flex flex-col  rounded-[4px] text-[10px] tracking-[.03em] border "
                // style={{ borderColor: hexToRGBA(item.color, 0.5) }}
              >
                <span className="mx-auto w-[100%] h-[2px]" style={{ background: hexToRGBA(item.color, 0.5) }}></span>
                {item.title}
              </p>
            ))}
          </div>
        </div>
        <div className="flex justify-between mt-2">
          <div>
            <p
              className={classNames('px-[5px] text-[10px] text-white  py-[3px] rounded-[4px]', {
                'bg-red-300': props.task.priority === TASKSPRIORITY.HIGH,
                'bg-yellow-300': props.task.priority === TASKSPRIORITY.LOW,
                'bg-blue-300': props.task.priority === TASKSPRIORITY.MEDUIM,
              })}
            >
              {props.task.priority === TASKSPRIORITY.HIGH
                ? 'HIGH'
                : props.task.priority === TASKSPRIORITY.LOW
                ? 'LOW'
                : 'MID'}
            </p>
          </div>

          <div className="flex w-[50%] justify-between">
            <div className="flex ">
              <div className="flex flex-col justify-center">
                <CommentIcon />
              </div>

              <p className="flex flex-col justify-center ml-2">{props.task.commentsCount}</p>
            </div>
            <div className="flex">
              <div className="flex flex-col justify-center">
                <AttachmentIcon />
              </div>

              <p className="flex flex-col justify-center ml-2">{props.task.attachmentCounts}</p>
            </div>

            <div className="flex justify-end">
              <div className="flex self-center -space-x-1 overflow-hidden">
                {props?.task?.taskMember?.map((item, index) => {
                  if (index < 2) {
                    return (
                      <img
                        className="inline-block h-[16px] p-[1px] w-[16px] rounded-full text-[8px] ring-2 bg-red-200 grid place-content-center ring-white"
                        style={{ backgroundColor: hexToRGBA(item.user?.color, 0.5) }}
                        src={item?.user?.userProfile?.userPictureUrl}
                        alt={getFirstLetters(item.user?.userProfile?.fullName)}
                      />
                    );
                  }

                  return null;
                })}
              </div>
              {memberLength < 2 ? (
                <>
                  <div className="flex flex-col justify-center ml-2">
                    <AddMembersToTasks
                      hideText={true}
                      taskId={props?.task?.taskId.toString()}
                      members={props?.task?.taskMember}
                      projectId={match?.params?.id || ''}
                      position="right-0"
                    />
                  </div>
                </>
              ) : (
                <>
                  {memberLength > 2 ? (
                    <div className="font-medium text-[14px] my-auto ">
                      <a href="#" className="ml-[2px] text-[12px] text-[#F85365] ">
                        + {memberLength - 2}
                      </a>
                    </div>
                  ) : null}
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
