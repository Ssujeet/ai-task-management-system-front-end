import * as React from 'react';
import * as ReactDOM from 'react-dom';
import styled from 'styled-components';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

import './styles.css';
import Column from './Columns';
import { useChangeOrderOfTasksMutation, useUpdateTasksByIdMutation } from 'redux/reducers/tasks';

const { useState, memo } = React;

const Columns = styled.div`
  display: flex;
  position: relative;
`;

interface IDragableBaord {
  onTaskClicked: (event: any, taskId: number) => void;
  taskData: any;
  sprintPlanningId: string;
}
function DragableBoard(props: IDragableBaord) {
  const [state, setState] = useState<any>();
  const [updateTasksById] = useUpdateTasksByIdMutation();
  const [changeOrderOfTasks] = useChangeOrderOfTasksMutation();

  React.useEffect(() => {
    setState(props.taskData);
  }, [props.taskData]);

  if (!state) {
    return null;
  }

  return (
    <DragDropContext
      onDragEnd={({ destination, source, draggableId, type, ...rest }) => {
        if (destination.droppableId === source.droppableId) {
          if (destination.index === source.index) {
            return;
          }
        }
        const postData = {
          id: +draggableId.split('-')[1],
          taskStatusId: +destination.droppableId.split('-')[1],
        };
        if (!destination) {
          return;
        }
        if (destination.droppableId !== source.droppableId) {
          updateTasksById(postData);
        }

        const changeOrderPostData = {
          taskId: +draggableId.split('-')[1],
          order: destination.index,
          taskStatusId: +destination.droppableId.split('-')[1],
          previousOrder: +draggableId.split('-')[2],
        };

        changeOrderOfTasks(changeOrderPostData as any);

        if (type === 'column') {
          const newColOrd = Array.from(state.columnOrder);
          newColOrd.splice(source.index, 1);
          newColOrd.splice(destination.index, 0, draggableId);

          const newState = {
            ...state,
            columnOrder: newColOrd,
          };
          setState(newState);
        }

        const startcol = state.columns[source.droppableId];
        const endcol = state.columns[destination.droppableId];

        if (startcol === endcol) {
          const tasks = Array.from(startcol.taskIds);
          tasks.splice(source.index, 1);
          tasks.splice(destination.index, 0, draggableId);

          const newCol = {
            ...startcol,
            taskIds: tasks,
          };

          const newState = {
            ...state,
            columns: {
              ...state.columns,
              [newCol.id]: newCol,
            },
          };

          setState(newState);

          return;
        }

        const startTaskIds = Array.from(startcol.taskIds);
        startTaskIds.splice(source.index, 1);
        const newStart = {
          ...startcol,
          taskIds: startTaskIds,
        };
        const endTaskIds = Array.from(endcol.taskIds);
        endTaskIds.splice(destination.index, 0, draggableId);
        const newEnd = {
          ...endcol,
          taskIds: endTaskIds,
        };
        const newState = {
          ...state,
          columns: {
            ...state.columns,
            [newStart.id]: newStart,
            [newEnd.id]: newEnd,
          },
        };
        setState(newState);
      }}
    >
      <Droppable droppableId="columns" direction="horizontal" type="column">
        {(provided) => (
          <div
            className="flex relative overflow-x-auto flex-nowrap "
            style={{ scrollbarWidth: 'none' }}
            {...provided.droppableProps}
            ref={provided.innerRef}
          >
            {state.columnOrder.map((id, i) => {
              const col = state.columns[id];
              const tasks = col.taskIds.map((taskid) => state.tasks[taskid]);

              return (
                <Column
                  key={id}
                  column={col}
                  tasks={tasks}
                  index={i}
                  onTaskClicked={props.onTaskClicked}
                  sprintPlanningId={props.sprintPlanningId}
                />
              );
            })}
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  );
}
export default React.memo(DragableBoard);
