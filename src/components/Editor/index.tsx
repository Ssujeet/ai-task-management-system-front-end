import React, { Component } from 'react';
import { EditorState, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import '../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { convertFromHTML, convertToHTML } from 'draft-convert';
import draftToHtml from 'draftjs-to-html';

interface IEditorComponent {
  onChangeValue: (value: string) => void;
  editorValue: string;
}
const EditorComponent = (props: IEditorComponent) => {
  const { onChangeValue, editorValue } = props;
  const [editorState, setEditorState] = React.useState(() => EditorState.createEmpty());

  React.useEffect(() => {
    if (!editorValue) {
      setEditorState(() => EditorState.createEmpty());
    } else {
      setEditorState(editorState);
    }
  }, [editorValue]);

  const onEditorStateChange: Function = (editorState) => {
    setEditorState(editorState);

    onChangeValue(draftToHtml(convertToRaw(editorState.getCurrentContent())));
  };

  return (
    <div>
      <Editor
        editorState={editorState}
        wrapperClassName="demo-wrapper"
        editorClassName="demo-editor"
        onEditorStateChange={onEditorStateChange as any}
      />
    </div>
  );
};

export default EditorComponent;
