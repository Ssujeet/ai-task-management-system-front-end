import DropFiles from 'assets/icons-svg/DropFiles';
import React from 'react';
import { Accept, useDropzone } from 'react-dropzone';

interface IFileDrop {
  setFiles: (files: File[]) => void;
  message: string;
  files: File[];
  multiple?: boolean;
  accept?: Accept;
}

export function FileDrop(props: IFileDrop) {
  const { message, setFiles, multiple, accept } = props;
  const { acceptedFiles, getRootProps, getInputProps } = useDropzone({
    onDrop: (files) => setFiles(files),
    multiple,
    accept,
  });

  return (
    <section className="container  ">
      <div
        {...getRootProps({
          className: 'dropzone border p-[14px] p-2 border-dashed rounded-[10px] h-[100px] flex items-center',
        })}
      >
        <input {...getInputProps()} />
        <div className="flex">
          <DropFiles />
          <p className="text-center ml-[10px]">{message}</p>
        </div>
      </div>
    </section>
  );
}
