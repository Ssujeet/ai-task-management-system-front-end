import React, { FC } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';

export interface IFileViewer {
  mimeType: string;
  url: string;
  fileName: string;
}

const imageMimeType = ['image/jpg', 'image/jpeg', 'image/png'];
const pdfMimeType = ['application/pdf'];
const videoMimeType = ['video/mp4', 'video/quicktime'];
const audioMimeType = ['audio/mpeg'];
const FileViewer: FC<IFileViewer> = ({ mimeType, url, fileName }) => {
  if (imageMimeType.includes(mimeType)) {
    return (
      <>
        <img src={url} className="h-[50%] w-[50%]  " />
      </>
    );
  }

  if (videoMimeType.includes(mimeType)) {
    return (
      <video width="320" height="240" controls>
        <source src={url} type="video/mp4" />
        Your browser does not support the video tag.
      </video>
    );
  }

  if (audioMimeType.includes(mimeType)) {
    return (
      <audio controls>
        <source src={url} type="audio/mpeg" />
        Your browser does not support the audio element.
      </audio>
    );
  }

  return (
    <>
      {pdfMimeType.includes(mimeType) && (
        <Document file={{ url }} onLoadError={(error) => console.log('Inside Error', error)}></Document>
      )}
    </>
  );
};

export default FileViewer;
