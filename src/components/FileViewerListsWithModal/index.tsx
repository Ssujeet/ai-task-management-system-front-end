import { ClickAwayListener } from '@material-ui/core';
import CrossIcon from 'assets/icons-svg/CrosIcon';
import Carousel, { CarouselItem } from 'components/Carousel/iindex';
import FileViewer, { IFileViewer } from 'components/FileViewer';
import React, { FC } from 'react';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { closeFilesViewer } from 'redux/reducers/file-viewer';
import { RootState } from 'redux/store';

const FileViewerListsWithModal: FC = () => {
  const dispatch = useDispatch();
  const showFileViewer = useSelector((state: RootState) => state.fileViewer.showFileViewer);
  const activeIndex = useSelector((state: RootState) => state.fileViewer.currentActiveFileIndex);
  const fileLists = useSelector((state: RootState) => state.fileViewer.filesLists);

  if (!showFileViewer) {
    return null;
  }

  return (
    <div
      className="fixed  z-[1000000]  h-[100vh] w-[100vw] flex flex-col justify-center"
      style={{ background: 'rgba(52, 52, 52, 0.8)' }}
    >
      <ClickAwayListener onClickAway={() => dispatch(closeFilesViewer())}>
        <>
          <div className=" absolute top-0 right-0 m-[20px]">
            <div
              className="bg-[#E5E5E5] cursor-pointer w-[24px] h-[24px] flex justify-between group rounded-[4px]"
              onClick={() => dispatch(closeFilesViewer())}
            >
              <CrossIcon />
            </div>
          </div>
          <div className="flex justify-center my-auto">
            {fileLists?.length ? (
              <Carousel
                showArrowButton={true}
                hideDotNavigations={true}
                autoScroll={false}
                customActiveIndex={activeIndex}
              >
                {fileLists?.map((item, index) => {
                  return (
                    <CarouselItem>
                      <FileViewer {...item} />;
                    </CarouselItem>
                  );
                })}
              </Carousel>
            ) : null}
          </div>
        </>
      </ClickAwayListener>
    </div>
  );
};

export default FileViewerListsWithModal;
