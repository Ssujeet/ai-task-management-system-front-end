import { useFormikContext } from 'formik';
import React, { ReactElement } from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';
import ErrorIcon from '@material-ui/icons';
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    error: {
      color: red[900],
      display: 'flex',
    },
  })
);

interface Props {
  name: string;
  touched: { [key: string]: any | undefined };
  errors: { [key: string]: any | undefined };
  index?: number;
  keyName?: string;
  t?: any;
}

export function FormikFieldArrayValidationError(props: Props): ReactElement {
  const { name, touched, errors, index, keyName } = props;

  const classes = useStyles();
  if (`${keyName}` in errors && `${keyName}` in touched && errors[keyName!][index!]) {
    return (
      <span className={classes.error}>
        {' '}
        {errors[keyName!][index!][name] && <span></span>} {errors[keyName!][index!][name] as string}
      </span>
    );
  }

  return <span></span>;
}

function FormikValidationError(props: Props): ReactElement {
  const { name, touched, errors } = props;
  const classes = useStyles();

  return touched[name] && !!errors[name] ? (
    <span className={classes.error}> {errors[name] ? (errors[name] as string) : ''}</span>
  ) : (
    <></>
  );
}
export default FormikValidationError;
