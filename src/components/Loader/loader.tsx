import SpinLogo from 'assets/icons-svg/SpinLogo';
import React from 'react';

const Loader = () => {
  return (
    <div className="place-items-center  grid  my-auto h-[60vh]">
      <SpinLogo />
    </div>
  );
};

export default Loader;
