import React from 'react';

import Button from '@material-ui/core/Button';
import Dialog, { DialogProps } from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles, Theme, createStyles, useTheme } from '@material-ui/core/styles';
import { useMediaQuery } from '@material-ui/core';

export interface IModal {
  isModalOpen: boolean;
  toggleModal: () => void;
  children: any;
  renderTitle: any;
  submitLabel: string;
  onSubmitClicked?: () => void;
  buttonType?: 'button' | 'submit';
  modalSize?: DialogProps['maxWidth'];
}

export default function Modal(props: IModal) {
  const {
    isModalOpen,
    toggleModal,
    children,
    renderTitle,
    submitLabel,
    onSubmitClicked,
    buttonType = 'button',
    modalSize,
  } = props;

  const [fullWidth, setFullWidth] = React.useState(true);

  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <React.Fragment>
      <div className="rounded-[24px]">
        <Dialog
          PaperProps={{
            style: {
              borderRadius: '24px',
            },
          }}
          fullScreen={fullScreen}
          open={isModalOpen}
          onClose={toggleModal}
          fullWidth={fullWidth}
          maxWidth={modalSize ? modalSize : 'md'}
          aria-labelledby="max-width-dialog-title"
        >
          <DialogTitle id="max-width-dialog-title">{renderTitle()}</DialogTitle>
          {children}
        </Dialog>
      </div>
    </React.Fragment>
  );
}
