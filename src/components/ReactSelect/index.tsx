import React from 'react';
import Select from 'react-select';

interface IReactSelect {
  options: any;
}

export const ReactSelect = (props) => {
  return <Select {...props} />;
};
