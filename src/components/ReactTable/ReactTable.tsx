import React, { FC } from 'react';

import { Column, useGlobalFilter, useTable } from 'react-table';

interface IColumns {
  Header: string;
  accessor?: string;
  columns?: IColumns[];
}
interface IReactTable {
  columns: readonly Column<IColumns & { width?: number }>[];
  data: any;
  tableHeaders?: () => React.ReactNode;
  search?: boolean;
}
const ReactTable: FC<IReactTable> = ({ columns, data, tableHeaders, search = true }) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    // @ts-ignore
    setGlobalFilter,
    // @ts-ignore
    state: { globalFilter },
  } = useTable(
    {
      columns,
      data,
    },
    useGlobalFilter
  );

  return (
    <div className="shadow-md pb-[18px]">
      <div className="flex justify-between items-center mb-[3px] border-b px-[20px] py-[13px]">
        <div className="">{tableHeaders && tableHeaders()}</div>
        {search && (
          <div className="">
            <div className="flex justify-items-end">
              <div className="">
                <input
                  type="text"
                  className="border focus:outline-none rounded-[8px] px-[24px] py-[8px] "
                  placeholder="Search..."
                  name="key"
                  value={globalFilter}
                  onChange={(e) => setGlobalFilter(e.target.value)}
                />
                <i className="ri-search-line search-icon" />
              </div>
            </div>
          </div>
        )}
      </div>
      <div className="px-[20px] mt-[18px]">
        <table className=" w-full" {...getTableProps()}>
          <thead className=" ">
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <th
                    className=" text-left px-[2px] text-[16px] font-[500]"
                    data-sort="customer_name"
                    style={{ width: column.width ?? 80, color: 'rgba(34, 34, 34, 0.8)' }}
                    {...column.getHeaderProps()}
                  >
                    {column.render('Header')}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody className="" {...getTableBodyProps()}>
            {rows.map((row, i) => {
              prepareRow(row);

              return (
                <tr {...row.getRowProps()} className=" px-[2px]  border-b">
                  {row.cells.map((cell) => {
                    return (
                      <td {...cell.getCellProps()} className="py-[14px]">
                        {cell.render('Cell')}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="noresult" style={{ display: data.length ? 'none' : 'block' }}>
          <div className="text-center">
            <h5 className="mt-[25px]">Sorry! No Result Found</h5>
          </div>
        </div>
      </div>
    </div>
  );
};
export default ReactTable;
