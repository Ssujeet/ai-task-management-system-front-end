import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      // minWidth: 120,
      width: '100%',
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  })
);

interface ITypes {
  label: any;
  value: any;
  [key: string]: string;
}

interface INativeSelects {
  placeHolder?: string;
  options: Array<ITypes>;
  value: any;
  label: string;
  onChange: (value: any) => void;
  name: string;
}

export default function NativeSelects(props: INativeSelects) {
  const { placeHolder, options, value, label, onChange, name } = props;
  const classes = useStyles();

  return (
    <FormControl required className={classes.formControl}>
      <InputLabel htmlFor="age-native-required">{label}</InputLabel>
      <NativeSelect
        value={value}
        onChange={onChange}
        name={name}
        inputProps={{
          id: 'age-native-required',
        }}
        // placeholder={placeHolder}
      >
        {options.map((item) => {
          <option value={item.value}>{item.label}</option>;
        })}
      </NativeSelect>
      <FormHelperText>Required</FormHelperText>
    </FormControl>
  );
}
