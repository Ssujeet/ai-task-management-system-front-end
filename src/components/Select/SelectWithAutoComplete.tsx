/* eslint-disable no-use-before-define */
import React from 'react';
import Chip from '@material-ui/core/Chip';
import Autocomplete, { AutocompleteProps } from '@material-ui/lab/Autocomplete';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Avatar } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      '& > * + *': {
        marginTop: theme.spacing(2),
      },
    },
  })
);
interface ITypes {
  [key: string]: string;
}

interface ISelectWithMultiProps {
  placeHolder?: string;
  options: Array<ITypes>;
  value: any;
  label: string;
  onChange: (value: any) => void;
  name: string;
  isMulti?: boolean;
  size?: 'medium' | 'small';
  defaultValue?: any;
  chipSize?: 'medium' | 'small';
  loading?: boolean;
  visibleLabel?: string;
  getOptionSelected?: any;
}

export default function Sizes(props: ISelectWithMultiProps) {
  const {
    placeHolder,
    options,
    value,
    label,
    onChange,
    name,
    isMulti,
    size,
    defaultValue,
    chipSize,
    loading,
    visibleLabel,
    getOptionSelected,
  } = props;

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Autocomplete
        multiple={isMulti}
        id="size-small-filled-multi"
        size={size ? size : 'small'}
        options={options}
        getOptionLabel={(option: ITypes) => (options ? (visibleLabel ? option[visibleLabel] : option.label) : '')}
        defaultValue={defaultValue}
        onInputChange={(e) => console.log(e.target)}
        loading={loading}
        value={value}
        getOptionSelected={getOptionSelected}
        onChange={(e, value) => onChange(value)}
        renderTags={(value, getTagProps) =>
          value?.map((option, index) => (
            <Chip
              key={index.toString()}
              avatar={<Avatar alt="Natacha" src="/static/images/avatar/1.jpg" />}
              variant="outlined"
              label={option.label}
              size={chipSize ? chipSize : 'small'}
              {...getTagProps({ index })}
            />
          ))
        }
        renderInput={(params) => <TextField {...params} variant="filled" label={label} placeholder={placeHolder} />}
      />
    </div>
  );
}
