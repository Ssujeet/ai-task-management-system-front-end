import React from 'react';

const Spinner = () => {
  return (
    <div>
      <div
        style={{ borderTopColor: 'transparent' }}
        className="w-[32px] h-[32px] border-4 border-white border-dashed rounded-full animate-spin"
      ></div>
    </div>
  );
};

export default Spinner;
