import Profile from 'assets/images/profile.svg';
import Message from 'assets/images/message.svg';
import LogOutIcon from 'assets/images/logout.svg';
import TwitterIcon from 'assets/images/twitter.svg';
import LinkedInIcon from 'assets/images/linkedIn.svg';
import TasCaidLogo from 'assets/images/tascaidLogo.svg';
import OnlyBubblesLogo from 'assets/images/tascaidOnlyLogo.svg';

import Notification from 'assets/images/notifications.svg';
import SideArrowIcon from 'assets/images/leftArrowIcon.svg';
import EmptyWorkSpace from 'assets/images/EmptyWorkSpace.png';

export const profileIcon = Profile;
export const messageIcon = Message;
export const logOutIcon = LogOutIcon;
export const tasCaidLogo = TasCaidLogo;
export const twitterIcon = TwitterIcon;
export const linkedInIcon = LinkedInIcon;
export const sideArrowIcon = SideArrowIcon;
export const notificationIcon = Notification;
export const emptyWorkSpace = EmptyWorkSpace;
export const onlyBubblesLogo = OnlyBubblesLogo;
