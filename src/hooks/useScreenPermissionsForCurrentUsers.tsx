import { SCREENS, SCREENS_ROUTES_ENUMS } from 'common/enum';
import { BaseResponse } from 'common/types';
import { useState } from 'react';
import { useRouteMatch } from 'react-router-dom';
import { useGetCurrentUserDetailsInProjectQuery } from 'redux/reducers/projectMembers';
import { ProjectOutputInterface } from 'redux/reducers/projects';
import { UserRolesScreenResponse } from 'redux/reducers/user-roles-screens';

export default function useScreenPermissionsForCurrentUsers(
  routeMatch: SCREENS_ROUTES_ENUMS,
  screenKey: SCREENS
): [UserRolesScreenResponse | undefined] {
  let match = useRouteMatch<{ id: string; projectFolderRootId: string }>(routeMatch);

  const { data: userDetails } = useGetCurrentUserDetailsInProjectQuery(match?.params.id || '', {
    skip: !match?.params.id,
  });
  const userDetailsData = userDetails;

  const screenRoles = userDetailsData?.data?.projectMemberRole.screenRole.find(
    (item) => item.screen.name === screenKey
  );

  return [screenRoles];
}
