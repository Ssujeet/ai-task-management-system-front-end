import { useCallback, useState, useEffect } from 'react';

export function useLocalStorage<T>(key: string, defaultValue?: T) {
  return useStorage<T>(key, window.localStorage, defaultValue);
}

export function useSessionStorage<T>(key: string, defaultValue?: T) {
  return useStorage<T>(key, window.sessionStorage, defaultValue);
}

function useStorage<T>(key: string, storageObject: Storage, defaultValue?: T) {
  const [value, setValue] = useState(() => {
    const jsonValue = storageObject.getItem(key);
    if (jsonValue !== null) return JSON.parse(jsonValue);

    if (typeof defaultValue === 'function') {
      return defaultValue();
    } else {
      return defaultValue;
    }
  });

  useEffect(() => {
    if (value === undefined) return storageObject.removeItem(key);
    storageObject.setItem(key, JSON.stringify(value));
  }, [key, value, storageObject]);

  const remove = useCallback(() => {
    setValue(undefined);
  }, []);

  return [value, setValue, remove];
}
