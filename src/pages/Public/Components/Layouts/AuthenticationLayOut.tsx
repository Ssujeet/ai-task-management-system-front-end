import React from 'react';
import { Link } from 'react-router-dom';
import GoogleIcon from '@material-ui/icons/Facebook';
import LoginImage from 'assets/icons-svg/LoginImage';
import Carousel, { CarouselItem } from 'components/Carousel/iindex';
import LoginScheduleImage from 'assets/icons-svg/LoginScheduleImage';
import LoginRoleImage from 'assets/icons-svg/LoginRoleImage';

const AuthenticationLayout = ({ children }) => {
  return (
    <div className="flex h-screen ">
      <div
        className="w-1/2 flex h-screen justify-center "
        style={{ background: 'linear-gradient(145.74deg, #F85365 -1.37%, rgba(252, 103, 103, 0.83) 99.65%)' }}
      >
        <div className=" w-full flex flex-col justify-center my-auto  h-screen">
          <Carousel>
            <CarouselItem>
              <div className="flex justify-center my-auto">
                <div className="flex flex-col">
                  <div className="mx-auto flex">
                    <LoginImage />
                  </div>

                  <p className="text-white text-[24px] text-center font-[600] mt-[55px]">Welcome to TascAid</p>

                  <p className="text-white text-[16px] text-center font-[400] mt-[10px]">
                    A complete task management solution for your next project.
                  </p>
                </div>
              </div>
            </CarouselItem>

            <CarouselItem>
              <div className="flex justify-center my-auto">
                <div className="flex flex-col">
                  <div className="mx-auto flex">
                    <LoginScheduleImage />
                  </div>
                  <p className="text-white text-[24px] text-center font-[600] mt-[55px]">
                    Interactive board and standup features
                  </p>

                  <p className="text-white  text-[16px] text-center font-[400] mt-[10px]">
                    A complete task management solution for your next project.
                  </p>
                </div>
              </div>
            </CarouselItem>

            <CarouselItem>
              <div className="flex justify-center my-auto">
                <div className="flex flex-col">
                  <div className="mx-auto flex">
                    <LoginRoleImage />
                  </div>
                  <p className="text-white text-[24px] text-center font-[600] mt-[55px]">
                    Fully customizable user roles and permissions
                  </p>

                  <p className="text-white text-[16px] text-center font-[400] mt-[10px]">
                    A complete task management solution for your next project.
                  </p>
                </div>
              </div>
            </CarouselItem>
          </Carousel>
        </div>
      </div>
      <div className="w-1/2">
        <div className="flex h-screen justify-center">{children}</div>
      </div>
    </div>
  );
};

export default AuthenticationLayout;
