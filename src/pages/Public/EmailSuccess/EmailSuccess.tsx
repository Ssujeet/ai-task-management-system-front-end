import React from 'react';

import AuthenticationLayout from '../Components/Layouts/AuthenticationLayOut';
import { Helmet } from 'react-helmet';

import AuthenticationPageLogo from 'assets/icons-svg/AuthenticationPageLogo';
import { useHistory } from 'react-router-dom';

const EmailSuccess = () => {
  const history = useHistory();

  return (
    <div>
      <Helmet>
        <title>Forgot Password | Tascaid</title>
      </Helmet>
      <AuthenticationLayout>
        <div className="my-auto w-1/3">
          <AuthenticationPageLogo />
          <div className="mt-[9px]">
            <p className="font-[500] text-[24px] leading-[32px] ">Success Message</p>
            <p className="text-[14px] font-[400] text-[rgba(34, 34, 34, 0.8)]">
              Reset Password link has been sent successfully.
            </p>
          </div>

          <div>
            <button onClick={() => history.replace('/forgot-password')}>Go Back</button>
          </div>
        </div>
      </AuthenticationLayout>
    </div>
  );
};

export default EmailSuccess;
