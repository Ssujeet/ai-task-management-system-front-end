import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import GoogleIcon from '@material-ui/icons/Facebook';
import AuthenticationLayout from '../Components/Layouts/AuthenticationLayOut';
import { Helmet } from 'react-helmet';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useLocalStorage } from 'hooks/useStorage';
import { Checkbox } from '@material-ui/core';
import AuthenticationPageLogo from 'assets/icons-svg/AuthenticationPageLogo';
import Spinner from 'components/Spinner/spinner';
import { useForgotPasswordMutation } from 'redux/reducers/auth';
import { toast } from 'react-toastify';

const ForgotPassword = () => {
  const history = useHistory();
  const [token, setToken] = useLocalStorage('token');
  const [forgotPassword, { isLoading, isError }] = useForgotPasswordMutation();

  return (
    <>
      <Helmet>
        <title>Forgot Password | Tascaid</title>
      </Helmet>
      <AuthenticationLayout>
        <div className="my-auto w-1/3">
          <AuthenticationPageLogo />
          <div className="mt-[9px]">
            <p className="font-[500] text-[24px] leading-[32px] ">Forgot Password</p>
            <p className="text-[14px] font-[400] text-[rgba(34, 34, 34, 0.8)]">
              Enter your email below and we will send you a reset link.
            </p>
          </div>
          <Formik
            initialValues={{
              email: '',
            }}
            validationSchema={Yup.object().shape({
              email: Yup.string().email('Must be a valid email').max(255).required('Email is required'),
            })}
            onSubmit={(data) => {
              forgotPassword(data)
                .unwrap()
                .then(() => history.push('/success-email-sent'))
                .catch(() => toast.error('Email not found'));
              console.log(data, 'data');
              // setToken('token');
              // window.location.href = '/';
            }}
          >
            {({ errors, handleBlur, handleChange, handleSubmit, isSubmitting, touched, values }) => (
              <form className="mt-10" onSubmit={handleSubmit}>
                <div className="flex flex-col">
                  <label className="font-[500] text-[16px] leading-[21px]">Email</label>
                  <input
                    type="text"
                    name="email"
                    placeholder="Someone@gmail.com"
                    className="px-3 py-3 rounded-[4px] border focus:outline-none text-[rgba(34, 34, 34, 0.8)] w-full mt-2"
                    onBlur={handleBlur}
                    value={values.email}
                    onChange={handleChange}
                  />

                  {Boolean(touched.email && errors.email) && (
                    <span className="text-sm text-red-500">{touched.email && errors.email}</span>
                  )}
                </div>
                <div className="mt-8 flex">
                  <button
                    type="submit"
                    className="flex justify-center bg-[#F85365] w-full  rounded-[4px] text-white px-8 py-[8px] rounded-[4px] "
                    disabled={isLoading}
                  >
                    {isLoading ? <Spinner /> : 'Submit'}
                  </button>
                  <button
                    disabled={isLoading}
                    type="button"
                    className="border w-full px-8 py-[8px] rounded-md ml-5 "
                    onClick={() => history.push('/')}
                  >
                    Cancel
                  </button>
                </div>
              </form>
            )}
          </Formik>
        </div>
      </AuthenticationLayout>
    </>
  );
};

export default ForgotPassword;
