import React from 'react';
import { Link as RouterLink, useHistory, useLocation, useRouteMatch } from 'react-router-dom';
import AuthenticationLayout from '../Components/Layouts/AuthenticationLayOut';
import { Helmet } from 'react-helmet';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useLocalStorage } from 'hooks/useStorage';
import { Checkbox } from '@material-ui/core';
import AuthenticationPageLogo from 'assets/icons-svg/AuthenticationPageLogo';
import EyeSlashedIcon from 'assets/icons-svg/EyeSlashedIcon';
import EyeOutlined from 'assets/icons-svg/EyeOutLined';
import { useResetPasswordMutation } from 'redux/reducers/auth';
import { toast } from 'react-toastify';
import { useQueryParams } from 'utils';
import Spinner from 'components/Spinner/spinner';

const ResetPassword = () => {
  const history = useHistory();
  const [token, setToken] = useLocalStorage('token');
  const [showPass, setShowPass] = React.useState(true);
  const [confirmPass, setConfirmPass] = React.useState(true);

  const query = useQueryParams();

  const accessToken = query.get('token');

  if (!accessToken) {
    history.push('/login');
  }

  const [resetPassword, { isLoading }] = useResetPasswordMutation();

  return (
    <>
      <Helmet>
        <title>Reset Password | Taskey</title>
      </Helmet>
      <AuthenticationLayout>
        <div className="my-auto w-2/5">
          <AuthenticationPageLogo />
          <div className="mt-[9px]">
            <p className="font-[500] text-[24px] leading-[32px] ">Reset Password</p>
            <p className="text-[14px] font-[400] text-[rgba(34, 34, 34, 0.8)]">
              Reset your password before token expires
            </p>
          </div>
          <Formik
            initialValues={{
              password: '',
              confirmPassword: '',
            }}
            validationSchema={Yup.object().shape({
              password: Yup.string().max(255).required('Password is required'),
              confirmPassword: Yup.string()
                .oneOf([Yup.ref('password'), null], "Password doesn't Match")
                .required('This is Required'),
            })}
            onSubmit={(data) => {
              accessToken &&
                resetPassword({ password: data.password, token: accessToken })
                  .unwrap()
                  .then(() => {
                    history.push('/login');
                    toast.success('Password reset successfully');
                  })
                  .catch(() => toast.error('Password link expired'));
              // setToken('token');
              // window.location.href = '/';
            }}
          >
            {({ errors, handleBlur, handleChange, handleSubmit, isSubmitting, touched, values }) => (
              <form className="mt-10" onSubmit={handleSubmit}>
                <div className="flex flex-col mt-5">
                  <div className="flex justify-between">
                    <label className="text-xl">Password</label>
                  </div>

                  <div className="flex border rounded-[4px] mt-2">
                    <input
                      type={showPass ? 'password' : 'text'}
                      name="password"
                      value={values.password}
                      placeholder="**************"
                      className="px-3 py-[8px]  focus:outline-none rounded-[4px] w-full "
                      onBlur={handleBlur}
                      onChange={handleChange}
                    />
                    <div
                      className="pointer-cursor flex flex-col justify-center mr-[18px]"
                      onClick={() => setShowPass(!showPass)}
                    >
                      {showPass ? <EyeSlashedIcon /> : <EyeOutlined />}
                    </div>
                  </div>
                  {Boolean(touched.password && errors.password) && (
                    <span className="text-sm text-red-500">{touched.password && errors.password}</span>
                  )}
                </div>
                <div className="flex flex-col mt-5">
                  <div className="flex justify-between">
                    <label className="text-xl">Confirm Password</label>
                  </div>

                  <div className="flex border rounded-[4px] mt-2">
                    <input
                      type={confirmPass ? 'password' : 'text'}
                      name="confirmPassword"
                      value={values.confirmPassword}
                      placeholder="**************"
                      className="px-3 py-[8px]  focus:outline-none rounded-[4px] w-full "
                      onBlur={handleBlur}
                      onChange={handleChange}
                    />
                    <div
                      className="pointer-cursor flex flex-col justify-center mr-[18px]"
                      onClick={() => setConfirmPass(!confirmPass)}
                    >
                      {confirmPass ? <EyeSlashedIcon /> : <EyeOutlined />}
                    </div>
                  </div>
                  {Boolean(touched.confirmPassword && errors.confirmPassword) && (
                    <span className="text-sm text-red-500">{touched.confirmPassword && errors.confirmPassword}</span>
                  )}
                </div>

                <div className="mt-8 flex">
                  <button
                    type="submit"
                    className="flex justify-center bg-[#F85365] w-full  rounded-[4px] text-white px-8 py-[8px] rounded-[4px] "
                    disabled={isLoading}
                  >
                    {isLoading ? <Spinner /> : 'Submit'}
                  </button>
                  <button
                    disabled={isLoading}
                    type="button"
                    className="border  px-8 py-[8px] w-full rounded-md ml-5 "
                    onClick={() => history.push('/signin')}
                  >
                    Cancel
                  </button>
                </div>
              </form>
            )}
          </Formik>
        </div>
      </AuthenticationLayout>
    </>
  );
};

export default ResetPassword;
