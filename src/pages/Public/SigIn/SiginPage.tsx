import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import GoogleIcon from '@material-ui/icons/Facebook';
import AuthenticationLayout from '../Components/Layouts/AuthenticationLayOut';
import { Helmet } from 'react-helmet';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useLocalStorage } from 'hooks/useStorage';
import { Checkbox, CircularProgress } from '@material-ui/core';
import { useLoginMutation, useLoginWithGoogleMutation } from 'redux/reducers/auth';
import GoogleLogin from 'react-google-login';
import AuthenticationPageLogo from 'assets/icons-svg/AuthenticationPageLogo';
import EyeOutlined from 'assets/icons-svg/EyeOutLined';
import EyeSlashedIcon from 'assets/icons-svg/EyeSlashedIcon';
import Spinner from 'components/Spinner/spinner';

const SignInPage = () => {
  const history = useHistory();
  const [token, setToken] = useLocalStorage('token');
  const [refreshToken, setRefreshToken] = useLocalStorage('refreshToken');
  const [login, { isLoading, error }] = useLoginMutation();
  const [showPass, setShowPass] = React.useState(true);

  const [loginWithGoogle, { isLoading: isLoadingGoogle, error: googleAuthError }] = useLoginWithGoogleMutation();

  const onGoogleAuthSuccess = (data) => {
    const postData = {
      token: data.accessToken,
    };
    loginWithGoogle(postData)
      .unwrap()
      .then((res) => {
        setRefreshToken(res.refreshToken);
        setToken(res.accessToken);
        window.location.href = '/';
        console.log(res);
      });
  };

  return (
    <>
      <Helmet>
        <title>Login | Tascaid</title>
      </Helmet>
      <AuthenticationLayout>
        <div className="my-auto w-4/12  ">
          <AuthenticationPageLogo />
          <div className="mt-[9px]">
            <p className="font-[500] text-[24px] leading-[32px]">Hello There !</p>
            <p className="text-[rgba(34, 34, 34, 0.8)] text-[14px] font-[400] ">Let’s sign in with your credentials</p>
          </div>
          <Formik
            initialValues={{
              email: '',
              password: '',
            }}
            validationSchema={Yup.object().shape({
              email: Yup.string().email().required('Email is required'),
              password: Yup.string().max(255).required('Password is required'),
            })}
            onSubmit={(values) => {
              login(values as any)
                .unwrap()
                .then((payload) => {
                  setRefreshToken(payload?.data?.refreshToken);
                  setToken(payload?.data?.accessToken);
                  window.location.href = '/';
                })
                .catch((error) => console.error('fulfilled', error));
            }}
          >
            {({ errors, handleBlur, handleChange, handleSubmit, isSubmitting, touched, values }) => (
              <form className="mt-[47px]" onSubmit={handleSubmit}>
                <div className="flex flex-col">
                  <label className="text-[16px] font-[500]">Email</label>
                  <input
                    type="text"
                    name="email"
                    placeholder="Someone@gmail.com"
                    className="px-3 py-[8px] border focus:outline-none  rounded-[4px] w-full mt-2"
                    onBlur={handleBlur}
                    value={values.email}
                    onChange={handleChange}
                  />

                  {Boolean(touched.email && errors.email) && (
                    <span className="text-sm text-red-500">{touched.email && errors.email}</span>
                  )}
                </div>
                <div className="flex flex-col mt-5">
                  <div className="flex justify-between">
                    <label className="text-[16px] font-[500]">Password</label>
                    <Link className="text-[12px] font-[500] focus:outline-none" to="/forgot-password">
                      Forgot Password?
                    </Link>
                  </div>
                  <div className="flex border rounded-[4px] mt-2">
                    <input
                      type={showPass ? 'password' : 'text'}
                      name="password"
                      value={values.password}
                      placeholder="**************"
                      className="px-3 py-[8px]  focus:outline-none rounded-[4px] w-full "
                      onBlur={handleBlur}
                      onChange={handleChange}
                    />
                    <div
                      className="pointer-cursor flex flex-col justify-center mr-[18px]"
                      onClick={() => setShowPass(!showPass)}
                    >
                      {showPass ? <EyeSlashedIcon /> : <EyeOutlined />}
                    </div>
                  </div>

                  {Boolean(touched.password && errors.password) && (
                    <span className="text-sm text-red-500">{touched.password && errors.password}</span>
                  )}
                </div>

                <div className="mt-8">
                  <button
                    type="submit"
                    className="flex justify-center bg-[#F85365] w-full  rounded-[4px] text-white px-8 py-[8px] rounded-[4px] "
                    disabled={isLoading}
                  >
                    {isLoading ? <Spinner /> : 'Login'}
                  </button>

                  <Link to="/signup">
                    <button
                      type="button"
                      className="border-[#F85365] border text-[#F85365] w-full mt-[10px] rounded-[4px] text-white px-8 py-[8px] rounded-[4px] "
                      disabled={isLoading}
                    >
                      Register
                    </button>
                  </Link>

                  <GoogleLogin
                    clientId={process.env.REACT_APP_GOOGLE_AUTH_CLIENT_ID as string}
                    buttonText="Log in with Google"
                    className="w-full text-center google-login-btn border flex flex-row justify-center bg-transparent mt-[24px]"
                    onSuccess={(data) => {
                      console.log(data);
                      onGoogleAuthSuccess(data);
                    }}
                  />
                </div>
                <div className="mt-4">
                  {/* {error?.data?.error && (
                    <span className="text-sm text-red-500">
                      {error?.data?.error?.statusCode === 401
                        ? 'Please make sure email and password are correct.'
                        : error?.data?.error?.message}
                    </span>
                  )} */}
                </div>
              </form>
            )}
          </Formik>
        </div>
      </AuthenticationLayout>
    </>
  );
};

export default SignInPage;
