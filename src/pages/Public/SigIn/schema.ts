import { string as YupString, object as YupObject } from 'yup';

export const loginValidationSchema = YupObject().shape({
  username: YupString().required('Required'),
  password: YupString().required('Required'),
});
