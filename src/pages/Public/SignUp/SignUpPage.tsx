import React from 'react';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import AuthenticationLayout from '../Components/Layouts/AuthenticationLayOut';
import { Helmet } from 'react-helmet';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { useLocalStorage } from 'hooks/useStorage';
import { Checkbox, CircularProgress } from '@material-ui/core';
import { useSignUpMutation } from 'redux/reducers/auth';
import AuthenticationPageLogo from 'assets/icons-svg/AuthenticationPageLogo';
import EyeSlashedIcon from 'assets/icons-svg/EyeSlashedIcon';
import EyeOutlined from 'assets/icons-svg/EyeOutLined';
import Spinner from 'components/Spinner/spinner';
const SignUpPage = () => {
  const history = useHistory();
  const [token, setToken] = useLocalStorage('token');
  const [showPass, setShowPass] = React.useState(true);
  const [confirmPass, setConfirmPass] = React.useState(true);

  const [signUp, { isLoading, error }] = useSignUpMutation();

  return (
    <>
      <Helmet>
        <title>Register | Tascaid</title>
      </Helmet>
      <AuthenticationLayout>
        <div className="my-auto w-2/5  ">
          <AuthenticationPageLogo />
          <div className="mt-[9px]">
            <p className="font-[500] text-[24px] leading-[32px]">Let’s create an account for you</p>
            <p className="text-[rgba(34, 34, 34, 0.8)] text-[14px] font-[400] ">
              Enter your details below to get you started using tascaid
            </p>
          </div>
          <Formik
            initialValues={{
              email: '',
              fullName: '',
              password: '',
              confirmPassword: '',
              policy: false,
            }}
            validationSchema={Yup.object().shape({
              email: Yup.string().email('Must be a valid email').max(255).required('Email is required'),
              fullName: Yup.string().max(255).required('First name is required'),
              password: Yup.string().max(255).required('Password is required'),
              confirmPassword: Yup.string()
                .oneOf([Yup.ref('password'), null], "Password doesn't Match")
                .required('This is Required'),
              policy: Yup.boolean().oneOf([true], 'Please read terms and conditions and accept it'),
            })}
            onSubmit={(values) => {
              signUp({ ...values, fullName: values.fullName } as any)
                .unwrap()
                .then((payload) => {
                  window.location.href = '/';
                })
                .catch((error) => console.error('fulfilled', error));
            }}
          >
            {({ errors, handleBlur, handleChange, handleSubmit, isSubmitting, touched, values }) => (
              <form className="mt-10" onSubmit={handleSubmit}>
                <div className="flex flex-col">
                  <label className="text-[16px] font-[500]">Full Name</label>
                  <input
                    type="text"
                    name="fullName"
                    placeholder="John"
                    className="px-3 py-[8px] focus:outline-none border  rounded-md w-full mt-2"
                    onBlur={handleBlur}
                    value={values.fullName}
                    onChange={handleChange}
                  />

                  {Boolean(touched.fullName && errors.fullName) && (
                    <span className="text-sm text-red-500">{touched.fullName && errors.fullName}</span>
                  )}
                </div>
                <div className="mt-[9px] flex flex-col">
                  <label className="text-[16px] font-[500]">Email</label>
                  <input
                    type="text"
                    name="email"
                    placeholder="someone@gmail.com"
                    className="px-3 py-[8px] border focus:outline-none  rounded-md w-full mt-2"
                    onBlur={handleBlur}
                    value={values.email}
                    onChange={handleChange}
                  />

                  {Boolean(touched.email && errors.email) && (
                    <span className="text-sm text-red-500">{touched.email && errors.email}</span>
                  )}
                </div>
                <div className="flex flex-col mt-[9px]">
                  <div className="flex justify-between">
                    <label className="text-[16px] font-[500]">Password</label>
                  </div>

                  <div className="flex border rounded-[4px] mt-2">
                    <input
                      type={showPass ? 'password' : 'text'}
                      name="password"
                      value={values.password}
                      placeholder="**************"
                      className="px-3 py-[8px]  focus:outline-none rounded-[4px] w-full "
                      onBlur={handleBlur}
                      onChange={handleChange}
                    />
                    <div
                      className="pointer-cursor flex flex-col justify-center mr-[18px]"
                      onClick={() => setShowPass(!showPass)}
                    >
                      {showPass ? <EyeSlashedIcon /> : <EyeOutlined />}
                    </div>
                  </div>
                  {Boolean(touched.password && errors.password) && (
                    <span className="text-sm text-red-500">{touched.password && errors.password}</span>
                  )}
                </div>
                <div className="flex flex-col mt-[9px]">
                  <div className="flex justify-between">
                    <label className="text-[16px] font-[500]">Confirm Password</label>
                  </div>

                  <div className="flex border rounded-[4px] mt-2">
                    <input
                      type={confirmPass ? 'password' : 'text'}
                      name="confirmPassword"
                      value={values.confirmPassword}
                      placeholder="**************"
                      className="px-3 py-[8px]  focus:outline-none rounded-[4px] w-full "
                      onBlur={handleBlur}
                      onChange={handleChange}
                    />
                    <div
                      className="pointer-cursor flex flex-col justify-center mr-[18px]"
                      onClick={() => setConfirmPass(!confirmPass)}
                    >
                      {confirmPass ? <EyeSlashedIcon /> : <EyeOutlined />}
                    </div>
                  </div>
                  {Boolean(touched.confirmPassword && errors.confirmPassword) && (
                    <span className="text-sm text-red-500">{touched.confirmPassword && errors.confirmPassword}</span>
                  )}
                </div>
                <div className="flex mt-2">
                  <Checkbox checked={values.policy} name="policy" onChange={handleChange} />
                  <span className="align-bottom text-gray-600 pt-2">
                    I have read the
                    <RouterLink className="text-blue-400 ml-1" to="/terms-and-condition">
                      Terms and Conditions
                    </RouterLink>
                  </span>
                </div>
                {Boolean(touched.policy && errors.policy) && (
                  <span className="text-sm text-red-500">{touched.policy && errors.policy}</span>
                )}

                <div className="mt-8 flex">
                  <button type="submit" className="bg-[#F85365] w-1/2 text-white px-8 py-3 rounded-md ">
                    {isLoading ? <Spinner /> : '  Submit'}
                  </button>
                  <button
                    type="button"
                    className="border w-1/2 px-8 py-3 rounded-md ml-5 "
                    onClick={() => history.push('/signin')}
                  >
                    Cancel
                  </button>
                </div>
              </form>
            )}
          </Formik>
        </div>
      </AuthenticationLayout>
    </>
  );
};

export default SignUpPage;
