import Loader from 'components/Loader/loader';
import { useLocalStorage } from 'hooks/useStorage';
import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useVerifyEmailMutation } from 'redux/reducers/auth';
import { useQueryParams } from 'utils';

const VerifyEmail = () => {
  const history = useHistory();
  const [verifyEmail] = useVerifyEmailMutation();
  const query = useQueryParams();

  const [token, setToken, removeToken] = useLocalStorage('token');

  removeToken();

  const accessToken = query.get('token');

  useEffect(() => {
    accessToken &&
      verifyEmail({ token: accessToken })
        .unwrap()
        .then(() => {
          toast.success('Email Verified');
          history.replace('/login');
        })
        .catch(() => toast.error('Token Expired'));
  }, []);

  return <Loader />;
};

export default VerifyEmail;
