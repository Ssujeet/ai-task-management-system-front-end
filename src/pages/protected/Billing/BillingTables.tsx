import React from 'react';
import ReactTable from 'components/ReactTable/ReactTable';
import { FcDownload } from 'react-icons/fc';

const BillingTables = () => {
  const columns: any = [
    {
      Header: 'S.N',
      accessor: 'sn',
      width: 40,
      Cell: (data: any) => {
        return <>{data.row.index + 1}</>;
      },
    },
    {
      Header: 'Invoices',
      accessor: 'Invoices',
      width: 230,
    },
    {
      Header: 'Amount',
      accessor: 'amount',
    },
    {
      Header: 'Date',
      accessor: 'date',
    },
    {
      Header: 'Status',
      Cell: (data: any) => {
        return <>{data.row.original.user?.email}</>;
      },
    },
    {
      Header: 'Action',
      Cell: (data: any) => {
        return (
          <>
            <FcDownload />
          </>
        );
      },
    },
  ];

  return (
    <div className="mt-[31px]">
      <ReactTable
        columns={columns}
        data={[]}
        tableHeaders={() => (
          <div className="hstack gap-2 justify-content-end">
            <p className="font-[700] text-[16px] leading-[21px]">Billings History</p>
          </div>
        )}
      />
    </div>
  );
};

export default BillingTables;
