import React from 'react';
import BillingTables from './BillingTables';

const Billing = () => {
  return (
    <div className="mt-[47px]">
      <div>
        <h4 className="font-[700] text-[18px] leading-[24px]">Payment Method</h4>
        <h5 className="font-[400] text-[16px] leading-[21px]">Add your billing details & payment method</h5>
      </div>

      <div className="flex gap-[20px] mt-[32px]">
        <div className="w-1/2 shadow">
          <div className="flex justify-between px-[16px] py-[11px] border-b ">
            <p className="text-[16px] font-[500] py-[8px]">Next Payment</p>
          </div>
        </div>

        <div className="w-1/2 shadow">
          <div className="flex justify-between px-[16px] py-[11px] border-b ">
            <p className="text-[16px] font-[500]">Card details</p>

            <button className="px-[10px] py-[5px] border border-[#F95868] hover:bg-[#F95868] hover:text-white text-[#F95868] rounded-[4px] ">
              Add Card
            </button>
          </div>

          <div className="px-[16px] py-[11px]">
            <div
              className="flex justify-between items-center px-[25px] py-[28px] rounded-[8px]"
              style={{ background: 'rgba(20, 90, 255, 0.1)' }}
            >
              <div className="text-[16px] font-[500]">
                <p>Visa ****4565</p>

                <p className="cursor-pointer">Edit</p>
              </div>

              <div className="w-[14px] h-[14px] flex items-center border border-[#F95868] justify-center rounded-[50%]">
                <div className="w-[6px] h-[6px] bg-[#F95868] rounded-[50%]"></div>
              </div>
            </div>
            <div
              className="flex justify-between items-center px-[25px] py-[28px] rounded-[8px] mt-[10px]"
              style={{ background: '#F7F7F7' }}
            >
              <div className="text-[16px] font-[500]">
                <p>Master Card ****4565</p>

                <p className="cursor-pointer">Set as default</p>
              </div>

              <div className="w-[14px] h-[14px] flex items-center border border-[#F95868] justify-center rounded-[50%]">
                {/* <div className="w-[6px] h-[6px] bg-[#F95868] rounded-[50%]"></div> */}
              </div>
            </div>
          </div>
        </div>
      </div>
      <BillingTables />
    </div>
  );
};

export default Billing;
