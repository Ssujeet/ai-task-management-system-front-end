import React from 'react';
import { useFormik } from 'formik';
import { FormikProps } from 'formik';
import { SCREENS_ROUTES_ENUMS, TASKSPRIORITY } from 'common/enum';
import MaxWidthDialog from 'components/Modal';
import { useRouteMatch } from 'react-router-dom';
import { TaskInterface } from 'redux/reducers/tasks';
import FormikValidationError from 'components/FormikErrors';
import DatePickers from 'components/DatePickers/DatePickers';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from 'components/Select/SelectWithAutoComplete';
import { taskFormValidationSchema } from '../TaskDetails/schema';
import { useGetProjectMembersListsQuery } from 'redux/reducers/projectMembers';
import { makeStyles, createStyles, Theme, CircularProgress } from '@material-ui/core';
import { Grid, Typography, DialogContent, DialogActions, Button } from '@material-ui/core';
import CrossIcon from 'assets/icons-svg/CrosIcon';
import { ReactSelect } from 'components/ReactSelect';
import Spinner from 'components/Spinner/spinner';

export interface ITaskCreationsForms extends TaskInterface {}

interface ITaskCreationsFormsProps extends FormikProps<ITaskCreationsForms> {}

export function TaskCreationsForms(props: ITaskCreationsFormsProps) {
  const onInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    props.setFieldValue(event.target.name, event.target.value);
  };
  let match = useRouteMatch<{ id: string }>(SCREENS_ROUTES_ENUMS.BOARDS);

  const { data } = useGetProjectMembersListsQuery(match?.params?.id || '', { skip: !match?.params?.id });

  const userLists =
    data?.data?.map((item) => {
      return {
        label: item?.user?.userProfile?.fullName || '',
        value: +item?.user?.id || 0,
      };
    }) || [];

  return (
    <div>
      <Grid container direction="row" justifyContent="flex-start" alignItems="flex-start">
        <div className="w-full">
          <div className="flex flex-col w-full">
            <label htmlFor="component-filled" className="text-[rgba(52, 52, 52, 0.6)]">
              Task Name *
            </label>
            <input
              id="component-filled"
              className="border rounded-[8px] px-[24px] py-[12px] mt-[7px]"
              value={props.values.title}
              onChange={onInputChange}
              aria-describedby="component-error-text"
              name="title"
              type="text"
            />

            <FormikValidationError name="title" errors={props.errors} touched={props.touched} />
          </div>
        </div>
        <div className="mt-3 w-full">
          <div className="flex flex-col w-full">
            <label htmlFor="component-filled">Descriptions</label>
            <textarea
              id="component-filled"
              className="border rounded-[8px] px-[24px] py-[12px] mt-[7px]"
              value={props.values.descriptions}
              onChange={(event) => {
                console.log(event.target.value, 'value');
                props.setFieldValue('descriptions', event.target.value);
              }}
              aria-describedby="component-error-text"
              name="descriptions"
              rows={3}
            />

            <FormikValidationError name="descriptions" errors={props.errors} touched={props.touched} />
          </div>
        </div>

        <Grid xs={6} className="mt-3">
          <div className="flex flex-col ">
            <label className="text-[14px]">End Date</label>
            <input
              type="date"
              name="deadline"
              className="border focus:outline-none rounded-[8px] px-[24px] py-[12px] mt-[7px]"
              onBlur={props.handleBlur}
              value={props.values.deadline}
              onChange={props.handleChange}
              min={new Date().toISOString().split('T')[0]}
            />
          </div>
          <FormikValidationError name="deadline" errors={props.errors} touched={props.touched} />
        </Grid>

        <Grid xs={6} className="mt-3">
          <div className="ml-2">
            <label className="text-[14px]">Select Priority</label>
            <div className="w-full mt-[7px] ">
              <ReactSelect
                options={[
                  { label: 'High', value: TASKSPRIORITY.HIGH.toString() },
                  { label: 'Meduim', value: TASKSPRIORITY.MEDUIM.toString() },
                  { label: 'Low', value: TASKSPRIORITY.LOW.toString() },
                ]}
                placeholder={''}
                value={props.values.priority}
                onChange={(value) => {
                  props.setFieldValue('priority', value);
                }}
                menuPlacement="top"
                // menuPortalTarget={document.body}

                styles={{
                  control: (base) => ({
                    ...base,
                    height: 49,
                    minHeight: 49,
                    borderRadius: 8,
                    marginTop: 2,
                  }),
                  menu: (base) => ({ ...base, position: 'absolute' }),
                }}
              />
            </div>
          </div>
          <FormikValidationError name="priority" errors={props.errors} touched={props.touched} />
        </Grid>

        <Grid xs={12} className="mt-3">
          <label className="text-[14px]">Select Members to Assign</label>
          <div className="w-full mt-[7px] ">
            <ReactSelect
              isMulti
              options={userLists}
              placeholder={'Select User'}
              value={props.values.taskMemberId}
              menuPlacement="top"
              // menuPortalTarget={document.body}
              onChange={(value) => props.setFieldValue('taskMemberId', value)}
              styles={{
                input: () => ({
                  padding: '8px',
                }),
                placeholder: () => ({
                  position: 'absolute',
                  paddingLeft: '20px',
                  fontSize: '17px',
                  fontWeight: '400',
                  color: 'rgba(0, 0, 0, 0.75)',
                }),
                menu: (base) => ({ ...base, position: 'absolute' }),
              }}
            />
          </div>

          <FormikValidationError name="taskMemberId" errors={props.errors} touched={props.touched} />
        </Grid>
      </Grid>
    </div>
  );
}

interface ITaskCreationsFormsWithFormikProps {
  formData?: ITaskCreationsForms;
  toggleModal: () => void;
  onSubmit: (data) => void;
  isLoading?: boolean;
}

export const TaskCreationsFormsWithFormik = (props: ITaskCreationsFormsWithFormikProps) => {
  const { formData, toggleModal, onSubmit, isLoading } = props;
  const initialValues: ITaskCreationsForms = {
    descriptions: '',
    title: '',
    priority: { label: 'High', value: TASKSPRIORITY.HIGH } as any,
    deadline: '',
    taskMemberId: [],
  };
  const formik = useFormik({
    initialValues: formData ? formData : initialValues,
    onSubmit: (values) => {
      onSubmit(values);
    },
    validationSchema: taskFormValidationSchema,
  });

  return (
    <>
      <form onSubmit={formik.handleSubmit}>
        <DialogContent>
          <TaskCreationsForms {...formik} />
        </DialogContent>

        <DialogActions>
          <div className="flex justify-between w-full mt-[58px] mb-[35px] px-[10px]">
            <button
              color="primary"
              type={'submit'}
              className="bg-[#F85365] text-white px-[29px] py-[12px] rounded-[4px]"
              disabled={isLoading}
            >
              {isLoading ? <Spinner /> : 'Submit'}
            </button>
            <button
              onClick={toggleModal}
              type={'button'}
              color="secondary"
              style={{ textTransform: 'none' }}
              disabled={isLoading}
            >
              Close
            </button>
          </div>
        </DialogActions>
      </form>
    </>
  );
};

interface ITaskCreationsFormsWithFormikAndModalProps {
  taskAdditionFormModal: boolean;
  // setTaskAdditionFormModal: any;
  modalTitle?: string;
  formData?: ITaskCreationsForms;
  toggleModal: () => void;
  onSubmit: (data) => void;
  isLoading?: boolean;
}

const TaskCreationsFormsWithFormikAndModal = (props: ITaskCreationsFormsWithFormikAndModalProps) => {
  const { taskAdditionFormModal, modalTitle, formData, toggleModal, onSubmit, isLoading } = props;

  return (
    <MaxWidthDialog
      isModalOpen={taskAdditionFormModal}
      toggleModal={toggleModal}
      renderTitle={() => {
        return (
          <div className="flex justify-between">
            <p className="text-[18px] font-[600]">{'Add Tasks'}</p>
            <div
              className="bg-[#E5E5E5] cursor-pointer w-[24px] h-[24px] flex justify-between group rounded-[4px]"
              onClick={() => toggleModal()}
            >
              <CrossIcon />
            </div>
          </div>
        );
      }}
      modalSize="sm"
      submitLabel={'Save'}
      buttonType={'submit'}
    >
      <TaskCreationsFormsWithFormik
        formData={formData}
        toggleModal={toggleModal}
        onSubmit={onSubmit}
        isLoading={isLoading}
      />
    </MaxWidthDialog>
  );
};

export default TaskCreationsFormsWithFormikAndModal;
