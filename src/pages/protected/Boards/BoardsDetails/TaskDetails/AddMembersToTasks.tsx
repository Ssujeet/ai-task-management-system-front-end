import TasksPlusIcon from 'assets/icons-svg/TaskPlusIcon';
import React, { FC } from 'react';
import {
  TaskMemberInterface,
  useAddMembersInTasksMutation,
  useDeleteTaskMemberByIdMutation,
} from 'redux/reducers/tasks';
import { useGetProjectMembersListsQuery } from 'redux/reducers/projectMembers';
import { getFirstLetters } from 'utils';
import { ClickAwayListener, Tooltip } from '@material-ui/core';

interface AddMembersToTasks {
  members?: TaskMemberInterface[];
  projectId: string;
  taskId: string;
  hideText?: boolean;
  position?: string;
}
const AddMembersToTasks: FC<AddMembersToTasks> = (props) => {
  const [showDownDrop, setShowDownDrop] = React.useState(false);

  return (
    <ClickAwayListener onClickAway={() => setShowDownDrop(false)}>
      <div className="relative  inline-block text-left">
        <div className=" flex mt-auto  flex-col justify-center">
          <Tooltip title={'Assign tasks'}>
            <button
              type="button"
              className="inline-flex justify-center w-full rounded-md bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 "
              id="menu-button"
              aria-expanded="true"
              aria-haspopup="true"
              onClick={() => setShowDownDrop(!showDownDrop)}
            >
              <div className="flex flex-col justify-center ">
                <TasksPlusIcon />
              </div>
              {!props.hideText && <span className="ml-2 mb-auto">Assign Tasks</span>}
            </button>
          </Tooltip>
        </div>

        {showDownDrop && <MemberRemoveAndAdd {...props} />}
      </div>
    </ClickAwayListener>
  );
};

export default AddMembersToTasks;

const MemberRemoveAndAdd: FC<AddMembersToTasks> = ({ members, projectId, taskId, position = 'left-0' }) => {
  const [addMembersInTasks] = useAddMembersInTasksMutation();
  const { data: projectMember } = useGetProjectMembersListsQuery(projectId);

  const [projectMembersLists, setProjectMembersLists] = React.useState<any>([]);
  const [deleteTaskMemberById] = useDeleteTaskMemberByIdMutation();

  const [searchKey, setSearchKey] = React.useState('');

  const labelsLength = members?.length ?? 0;

  React.useEffect(() => {
    if (projectMember?.data) {
      setProjectMembersLists(projectMember?.data);
    }
  }, [projectMember?.data]);

  React.useEffect(() => {
    if (searchKey) {
      const filterProjectMember = projectMembersLists?.filter((item) =>
        item?.user?.userProfile?.fullName?.includes(searchKey)
      );
      setProjectMembersLists(filterProjectMember || []);
    } else {
      setProjectMembersLists(projectMember?.data || []);
    }
  }, [searchKey]);

  return (
    <div
      className={
        'z-[400000] absolute mt-2 w-56 focus:border-none rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5  focus:outline-none ' +
        position
      }
      role="menu"
      aria-orientation="vertical"
      aria-labelledby="menu-button"
    >
      {labelsLength < 3 ? (
        <div className="py-2 px-2" role="none">
          <input
            value={searchKey}
            name="taskCheck"
            className="border block px-2 py-[5px] text-[12px] w-full"
            type="text"
            placeholder="Search User"
            onChange={(e) => setSearchKey(e.target.value)}
          />
        </div>
      ) : null}
      <div className="py-1" role="none">
        {projectMembersLists?.map((item) => (
          <div
            className="flex justify-between hover:bg-red-200 cursor-pointer"
            onClick={() => {
              if (members?.some((label) => label?.user?.id === item.user.id)) {
                const taskMember = members.find((member) => member?.user?.id === item.user.id);
                taskMember && deleteTaskMemberById(taskMember?.id.toString());
              } else {
                const postData = {
                  projectId: +projectId,
                  taskId: +taskId,
                  userId: +item.user.id,
                };
                addMembersInTasks(postData);
              }
            }}
          >
            <div className="flex px-2">
              <img
                className="inline-block border my-auto h-[20px] w-[20px] rounded-full text-[8px] ring-2 bg-red-200 grid place-content-center ring-white cursor-pointer"
                style={{ backgroundColor: item.user?.color }}
                src={item.user.userProfile.userPictureUrl}
                alt={getFirstLetters(item.user?.userProfile?.fullName)}
              />
              <span className={`block px-2 py-2 text-sm hover:text-[${item.color}]`} role="menuitem" id="menu-item-0">
                {item.user.userProfile.fullName}
              </span>
            </div>
            {!members?.some((label) => label?.user?.id === item.user.id) ? (
              <div className="flex flex-col justify-center px-2">
                <TasksPlusIcon />
              </div>
            ) : (
              <div className="flex flex-col justify-center px-2 text-[10px]">
                <span>Remove</span>
              </div>
            )}
          </div>
        ))}
      </div>
    </div>
  );
};
