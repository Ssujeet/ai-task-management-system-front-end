import TasksPlusIcon from 'assets/icons-svg/TaskPlusIcon';
import React, { FC } from 'react';
import {
  TaskMemberInterface,
  useAddMembersInTasksMutation,
  useDeleteTaskMemberByIdMutation,
  TaskStatusInterface,
  useGetTaskStatusByProjectIdQuery,
  useUpdateTasksByIdMutation,
} from 'redux/reducers/tasks';
import { useGetProjectMembersListsQuery } from 'redux/reducers/projectMembers';
import { getFirstLetters } from 'utils';
import { ClickAwayListener, Tooltip } from '@material-ui/core';
import DropDownIcon from 'assets/icons-svg/DropDownIcon';

interface ChangeTaskStatusOfTask {
  taskStatus?: TaskStatusInterface;
  projectId: string;
  taskId: string;
  position?: string;
}
const ChangeTaskStatusOfTask: FC<ChangeTaskStatusOfTask> = (props) => {
  const [showDownDrop, setShowDownDrop] = React.useState(false);

  return (
    <ClickAwayListener onClickAway={() => setShowDownDrop(false)}>
      <div className="relative  inline-block text-left">
        <div className=" flex flex-col justify-center">
          <Tooltip title={'Assign tasks'}>
            <button
              type="button"
              className="inline-flex justify-center w-full rounded-md bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 "
              id="menu-button"
              aria-expanded="true"
              aria-haspopup="true"
              onClick={() => setShowDownDrop(!showDownDrop)}
            >
              <span
                className="ml-2 mb-auto px-4 rounded-[4px]"
                style={{ backgroundColor: 'rgba(128, 244, 212, 0.57)' }}
              >
                {props.taskStatus?.taskName}
              </span>
              <div className="flex flex-col justify-center ml-2 ">
                <DropDownIcon />
              </div>
            </button>
          </Tooltip>
        </div>

        {showDownDrop && <ChangeTaskStatus {...props} />}
      </div>
    </ClickAwayListener>
  );
};

export default ChangeTaskStatusOfTask;

const ChangeTaskStatus: FC<ChangeTaskStatusOfTask> = ({ taskStatus, projectId, taskId, position = 'left-0' }) => {
  const [updateTasksById] = useUpdateTasksByIdMutation();

  const { data: taskStatusLists } = useGetTaskStatusByProjectIdQuery(projectId);

  return (
    <div
      className={
        'z-[400000] absolute mt-2 w-56 focus:border-none rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5  focus:outline-none ' +
        position
      }
      role="menu"
      aria-orientation="vertical"
      aria-labelledby="menu-button"
    >
      <div className="py-1" role="none">
        {taskStatusLists?.data?.map((item) => (
          <div
            className="flex justify-between hover:bg-red-200 cursor-pointer"
            onClick={() => {
              if (item?.id && taskStatus?.id !== item.id) {
                const postData = {
                  id: +taskId,
                  taskStatusId: +item?.id,
                };
                updateTasksById(postData);
              }
            }}
          >
            <div className="flex px-2">
              <span className={`block px-2 py-2 text-sm`} role="menuitem" id="menu-item-0">
                {item.taskName}
              </span>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
