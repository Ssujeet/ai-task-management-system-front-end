import CrossIcon from 'assets/icons-svg/CrosIcon';
import FormikValidationError from 'components/FormikErrors';
import Modal from 'components/Modal';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import React, { FC } from 'react';
import { useAddSprintMutation, useUpdateSprintMutation } from 'redux/reducers/sprint-planning';
import * as Yup from 'yup';

export const initialValues = {
  title: '',
  descriptions: '',
  startDate: '',
  endDate: '',
};
interface ISprintFormModal {
  formState: any;
  formModal: boolean;
  toggleFormModal: () => void;
  projectId: string;
  columns: any;
}
const SprintFormModal: FC<ISprintFormModal> = ({ formState, formModal, toggleFormModal, projectId, columns }) => {
  const [addSprint] = useAddSprintMutation();

  const [taskModalMessage, toggleModalMessage] = useToggle(false);

  const [updateSprint] = useUpdateSprintMutation();

  const updatingTaskId = [] as any;

  const taskIdsToMoveToNextSprint = columns
    ? Object.keys(columns)?.forEach((item: any) => {
        if (!columns[item].canEdit && columns[item].taskIds.length) {
          columns[item].taskIds.map((item) => updatingTaskId.push(+item.split('-')[1]));
        }
      })
    : [];
  console.log('taskIdsToMoveToNextSprint', updatingTaskId, columns, formState, 'taskIdsToMoveToNextSprint');

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: formState,
    onSubmit: (values, { resetForm }) => {
      let execFunc = addSprint;

      const postData = {
        ...values,
        projectId: +projectId,
      };
      if (formState.id) {
        execFunc = updateSprint;
      } else {
        postData['taskId'] = updatingTaskId;
      }

      execFunc(postData)
        .unwrap()
        .then((res) => {
          toggleFormModal();
          updatingTaskId.length && toggleModalMessage();
          resetForm({ values: initialValues });
        })
        .catch((err) => console.log(err));
    },
    validationSchema: Yup.object().shape({
      title: Yup.string().max(255).required('Title is required'),
    }),
  });

  return (
    <>
      <Modal
        modalSize="xs"
        isModalOpen={taskModalMessage}
        toggleModal={() => toggleModalMessage()}
        renderTitle={() => {
          return (
            <div className="flex justify-between">
              <p className="text-[18px] font-[600]">{'Sprint Message'}</p>
              <div
                className="bg-[#E5E5E5] cursor-pointer w-[24px] h-[24px] flex justify-between group rounded-[4px]"
                onClick={() => toggleModalMessage()}
              >
                <CrossIcon />
              </div>
            </div>
          );
        }}
        submitLabel={'Save'}
        onSubmitClicked={() => console.log()}
      >
        <p className="p-2 px-6 text-center font-[500] text-[18px]">
          {updatingTaskId.length} task has been moved to next sprint.
        </p>
      </Modal>
      <Modal
        modalSize="xs"
        isModalOpen={formModal}
        toggleModal={() => toggleFormModal()}
        renderTitle={() => {
          return (
            <div className="flex justify-between">
              <p className="text-[18px] font-[600]">{formState.id ? 'Update Sprint' : 'Start New Sprint'}</p>
              <div
                className="bg-[#E5E5E5] cursor-pointer w-[24px] h-[24px] flex justify-between group rounded-[4px]"
                onClick={() => toggleFormModal()}
              >
                <CrossIcon />
              </div>
            </div>
          );
        }}
        submitLabel={'Save'}
        onSubmitClicked={() => console.log()}
      >
        <form className="py-[15px] px-6" onSubmit={formik.handleSubmit}>
          <div className="flex flex-col">
            <label className="text-[14px]">Title*</label>
            <input
              type="text"
              name="title"
              className="border rounded-[8px] px-[24px] py-[12px] mt-[7px]"
              onBlur={formik.handleBlur}
              value={formik.values.title}
              onChange={formik.handleChange}
            />

            {Boolean(formik.touched.title && formik.errors.title) && (
              <span className="text-sm text-red-500">{formik.touched.title && formik.errors.title}</span>
            )}
          </div>

          <div className="mt-3">
            <div className="flex flex-col w-full">
              <label htmlFor="component-filled text-[14px]">Descriptions</label>
              <textarea
                className="border rounded-[8px] px-[14px] py-[12px] mt-[7px]"
                value={formik.values.descriptions}
                onChange={(event) => {
                  console.log(event.target.value, 'value');
                  formik.setFieldValue('descriptions', event.target.value);
                }}
                aria-describedby="component-error-text"
                name="descriptions"
                rows={3}
              />

              <FormikValidationError name="descriptions" errors={formik.errors} touched={formik.touched} />
            </div>
          </div>
          <div className="flex flex-col mt-[16px]">
            <label className="text-[14px]">Start Date</label>
            <input
              type="date"
              name="startDate"
              className="border rounded-[8px] px-[24px] py-[12px] mt-[7px]"
              onBlur={formik.handleBlur}
              value={formik.values.startDate}
              onChange={formik.handleChange}
            />
          </div>

          <div className="flex flex-col mt-[16px]">
            <label className="text-[14px]">End Date</label>
            <input
              type="date"
              name="endDate"
              className="border rounded-[8px] px-[24px] py-[12px] mt-[7px]"
              onBlur={formik.handleBlur}
              value={formik.values.endDate}
              onChange={formik.handleChange}
              min={formik.values.startDate}
            />
          </div>
          <p className="mt-2 font-[500] text-[12px] italic">
            Note: When starting new sprint, tasks from Backlog, TODO, Progress are moved to new sprint.
          </p>
          <div className=" mt-5 flex flex-row justify-end pt-4">
            <button
              type="button"
              className="cursor-pointer hover:text-red-700 px-4 py-1 rounded-lg text-gray-700"
              onClick={() => {
                formik.resetForm({ values: initialValues });
                toggleFormModal();
              }}
            >
              Discard
            </button>
            <button
              type="submit"
              className="cursor-pointer ml-2  bg-[#F85365] px-[29px] py-[12px] rounded-[4px] text-white"
            >
              Submit
            </button>
          </div>
        </form>
      </Modal>
    </>
  );
};

export default SprintFormModal;
