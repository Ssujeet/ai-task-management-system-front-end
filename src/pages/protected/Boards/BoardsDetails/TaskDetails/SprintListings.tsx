import { TiTick } from 'react-icons/ti';
import useToggle from 'hooks/useToggle';
import React, { FC, useState } from 'react';
import SprintFormModal from './SprintFormModal';
import EditIcon from 'assets/icons-svg/EditIcon';
import { initialValues } from './SprintFormModal';
import DropDownIcon from 'assets/icons-svg/DropDownIcon';
import { ClickAwayListener, Tooltip } from '@material-ui/core';
import { useGetSprintListsOfProjectByIdQuery } from 'redux/reducers/sprint-planning';
import Spinner from 'components/Spinner/spinner';

interface SprintListings {
  projectId: string;
  position?: string;
  columns: any;
  setSelectedSprint: (data: any) => void;
  selectedSprint: any;
}
const SprintListings: FC<SprintListings> = (props) => {
  const [showDownDrop, setShowDownDrop] = React.useState(false);
  const { data: sprintLists } = useGetSprintListsOfProjectByIdQuery(props.projectId);
  const activeSprint = sprintLists?.data?.find((item) => item.isActive);

  return (
    <ClickAwayListener onClickAway={() => setShowDownDrop(false)}>
      <div className="relative inline-block text-left my-auto mr-[19px]">
        <div className="flex flex-col justify-center border w-[177px] rounded-[8px]">
          <Tooltip title={'Sprint Planning'}>
            <button
              type="button"
              className="inline-flex justify-center w-full rounded-md bg-white text-sm font-medium text-gray-700 hover:bg-gray-50  my-auto py-[10px] px-[20px] "
              id="menu-button"
              aria-expanded="true"
              aria-haspopup="true"
              onClick={() => setShowDownDrop(!showDownDrop)}
            >
              <span className=" mb-auto  w-full  truncate ">{props?.selectedSprint?.title}</span>
              <div className="flex flex-col justify-center ml-2 ">
                <DropDownIcon />
              </div>
            </button>
          </Tooltip>
        </div>

        {<Sprints {...props} activeSprint={activeSprint} showDownDrop={showDownDrop} />}
      </div>
    </ClickAwayListener>
  );
};

export default SprintListings;

const Sprints: FC<SprintListings & { activeSprint: any; showDownDrop: boolean }> = ({
  projectId,
  activeSprint,
  showDownDrop,
  columns,
  setSelectedSprint,
}) => {
  const { data: sprintLists } = useGetSprintListsOfProjectByIdQuery(projectId);
  const [sprintPlanningModal, toggleSprintPlanningModal] = useToggle(false);
  const [sprintFormState, setSprintFormState] = useState(initialValues);
  if (sprintLists?.data?.length) {
    return (
      <>
        <SprintFormModal
          formModal={sprintPlanningModal}
          toggleFormModal={toggleSprintPlanningModal}
          formState={sprintFormState}
          projectId={projectId}
          columns={columns}
        />
        {showDownDrop && (
          <div
            className={
              'z-[10] absolute mt-2 w-[177px] focus:border-none rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5  focus:outline-none '
            }
            role="menu"
            aria-orientation="vertical"
            aria-labelledby="menu-button"
          >
            <div className="py-1 max-h-[100px]  overflow-y-auto" role="none">
              {[...sprintLists?.data]?.reverse()?.map((item) => (
                <div className="flex  justify-between hover:bg-red-200 cursor-pointer" onClick={() => {}}>
                  <div className="flex justify-between px-2">
                    <span
                      className={`block px-2 py-2 text-sm`}
                      role="menuitem"
                      id="menu-item-0"
                      onClick={() => {
                        setSelectedSprint(item);
                      }}
                    >
                      {item?.title}
                    </span>
                  </div>
                  {item.isActive && (
                    <div className="flex">
                      <div className="flex flex-col justify-center ml-[5px]">
                        <TiTick color="#F85365" />
                      </div>
                      <div
                        className="flex flex-col  justify-center pl-[10px]"
                        onClick={() => {
                          setSprintFormState(item as any);
                          toggleSprintPlanningModal();
                        }}
                      >
                        <EditIcon />
                      </div>
                    </div>
                  )}
                </div>
              ))}
            </div>
            <div className="px-[8px] py-[12px]">
              <button
                type="button"
                className="inline-flex bg-[#F85365] text-[13px] text-white font-[500] px-[10px] py-[5px] mx-auto justify-center w-full rounded-md  "
                id="menu-button"
                aria-expanded="true"
                aria-haspopup="true"
                onClick={() => {
                  toggleSprintPlanningModal();
                  setSprintFormState(initialValues);
                }}
              >
                Start New Sprint
              </button>
            </div>
          </div>
        )}
      </>
    );
  }

  return <Spinner />;
};
