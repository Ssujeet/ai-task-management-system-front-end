import { FileDrop } from 'components/FileDrop/fileDrop';
import React from 'react';
import { toast } from 'react-toastify';
import {
  useAddTaskCommentsMutation,
  useGetTaskCommentsListByTaskIdQuery,
  useDeleteCommentByIdMutation,
  useUpdateCommentMutation,
  useDeleteFilesMutation,
  ITaskComments,
} from 'redux/reducers/tasks-comments';
import { useGetCurrentUserProfileQuery } from 'redux/reducers/user-profile';
import { getFirstLetters } from 'utils';
import Moment from 'react-moment';

const TaskComments: React.FC<{ taskId: string }> = ({ taskId }) => {
  const [comment, setComment] = React.useState('');

  const [files, setFiles] = React.useState<File[]>();

  const { data } = useGetTaskCommentsListByTaskIdQuery(taskId);
  const [addTaskComments] = useAddTaskCommentsMutation();
  const [deleteCommentById] = useDeleteCommentByIdMutation();
  const [updateComment] = useUpdateCommentMutation();
  const [deleteFiles] = useDeleteFilesMutation();
  const { data: userProfile } = useGetCurrentUserProfileQuery();

  const onCommentSend = () => {
    let id;
    if (files) {
      id = toast.loading('Comment file is being uploaded');
    }

    addTaskComments({
      taskId: +taskId,
      message: comment,
      files,
    })
      .unwrap()
      .then(() => {
        files &&
          toast.update(id, {
            render: 'Comment file upload success',
            type: 'success',
            isLoading: false,
            autoClose: 800,
          });
        setFiles([]);
      })
      .catch((e) => {
        files &&
          toast.update(id, { render: 'Comment file upload error', type: 'success', isLoading: false, autoClose: 800 });
      });

    setComment('');
  };

  const comments = data?.data;

  return (
    <div>
      {/* <FileDrop
        setFiles={(file) => {
          console.log(file, 'setFieldValue');
          setFiles(file);
        }}
        message={'Upload Comment Files'}
        files={files ?? []}
      /> */}
      <div className="relative flex  mt-[20px] ">
        <div className="border rounded-[4px] flex w-full">
          <img
            className="inline-block my-auto  ml-[10px] h-[35px] w-[35px] rounded-full text-[8px] ring-2 bg-red-200 grid place-content-center ring-white cursor-pointer"
            src={userProfile?.data?.userPictureUrl}
            alt={getFirstLetters(userProfile?.data?.fullName)}
          />
          <input
            value={comment}
            name="taskCheck"
            className="border-0 block focus:outline-none  ml-[15px] w-full rounded-[4px] py-[14px]"
            type="text"
            placeholder="Write a comment"
            onChange={(e) => setComment(e.target.value)}
          />
        </div>

        <button
          type="button"
          onClick={onCommentSend}
          className="bg-[#F85365] ml-[13px] rounded-[4px]  px-[27px] py-[12px] text-white right-0"
        >
          Send
        </button>
      </div>
      <div className="mt-[10px]">
        {comments?.map((item) => {
          return (
            <div className="flex mt-[10px]">
              <img
                className="inline-block  border  ml-[10px] h-[35px] w-[35px] rounded-full text-[8px] ring-2 bg-red-200 grid place-content-center ring-white cursor-pointer"
                src={item.commenter?.userProfile?.userPictureUrl}
                alt={getFirstLetters(item.commenter?.userProfile.fullName)}
              />
              <div className="ml-[10px]">
                <div className="flex">
                  <p className="font-[600]">{item.commenter?.userProfile.fullName}</p>

                  {/* {
                      item.createdAt && <Moment fromNow>  <p>
                        
                         {item.createdAt as any}
                         </p>
                         </Moment>
                    } */}
                  <p></p>
                </div>
                <p>{item.message}</p>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default TaskComments;
