import React from 'react';
import {
  useAddTaskCheckListsMutation,
  useGetTaskChecklistByTaskIdQuery,
  useUpdateTaskChecklistMutation,
  useDeleteTaskCheckListsMutation,
} from 'redux/reducers/tasks-checklists';
import { Checkbox, CircularProgress } from '@material-ui/core';
import useToggle from 'hooks/useToggle';
import TasksPlusIcon from 'assets/icons-svg/TaskPlusIcon';
import { AiFillDelete } from 'react-icons/ai';

import MaxWidthDialog from 'components/Modal';
import CrossIcon from 'assets/icons-svg/CrosIcon';
interface ITaskPoint {
  taskId: string;
}
const TaskPoint = (props: ITaskPoint) => {
  const { taskId } = props;

  const [taskCheckList, setTaskCheckLists] = React.useState('');
  const [showInputItem, toggleShowInputItem] = useToggle(false);
  const [editValue, setEditValue] = React.useState<any>('');

  const { data: taskChecklistsData } = useGetTaskChecklistByTaskIdQuery(taskId);
  const [addTaskCheckLists] = useAddTaskCheckListsMutation();
  const [updateTaskChecklist] = useUpdateTaskChecklistMutation();
  const [deleteTaskCheckLists] = useDeleteTaskCheckListsMutation();

  const [deletingAttachment, setDeletingAttachment] = React.useState({ id: '', note: '' });
  const [deleteConfirmation, toggleConfirmation] = useToggle(false);

  const submitTaskCheckLists = () => {
    if (editValue?.id) {
      updateTaskChecklist({
        id: editValue.id,
        note: taskCheckList,
      })
        .unwrap()
        .then(() => {
          setTaskCheckLists('');
          setEditValue('');
        });

      return;
    }
    taskCheckList &&
      addTaskCheckLists({
        note: taskCheckList,
        taskId: +taskId,
      })
        .unwrap()
        .then(() => {
          setTaskCheckLists('');
          toggleShowInputItem();
        });
  };

  const onCheckBoxHandled = (item) => {
    updateTaskChecklist({
      ...item,
      isCompleted: !item.isCompleted,
    });
  };

  const onDeleteTaskCheck = (item) => {
    toggleConfirmation();
    setDeletingAttachment(item);
  };

  const onDeleteConfirm = () => {
    deleteTaskCheckLists(deletingAttachment.id);
    toggleConfirmation();
  };

  return (
    <>
      <MaxWidthDialog
        isModalOpen={deleteConfirmation}
        toggleModal={toggleConfirmation}
        renderTitle={() => {
          return (
            <div className="flex justify-between">
              <p className="text-[18px] font-[600]">Confirmation</p>
              <div
                className="bg-[#E5E5E5] cursor-pointer w-[24px] h-[24px] flex justify-between group rounded-[4px]"
                onClick={() => toggleConfirmation(false)}
              >
                <CrossIcon />
              </div>
            </div>
          );
        }}
        submitLabel={'Save'}
        buttonType={'submit'}
        modalSize={'xs'}
      >
        <div className="px-[28px]">
          <div>
            <h2 className="text-[16px] font-[400]">
              Are you sure you want to delete: {deletingAttachment.note} checklist.
            </h2>
          </div>

          <div className=" mt-5 flex flex-row pb-[21px]">
            <button
              type="button"
              className="cursor-pointer hover:text-red-700 py-1 rounded-lg text-gray-700"
              onClick={() => {
                toggleConfirmation();
                setDeletingAttachment({ id: '', note: '' });
              }}
            >
              Cancel
            </button>
            <button
              type="button"
              className="cursor-pointer ml-[24px] bg-red-400 px-[35px] py-[12px] text-white rounded-[4px]"
              onClick={onDeleteConfirm}
            >
              Confirm
            </button>
          </div>
        </div>
      </MaxWidthDialog>

      <div className="mt-[12px]">
        <p className="text-[16px] font-[600]">Tasks Criteria Checklists</p>
        {taskChecklistsData?.data?.map((item) => {
          return (
            <div className="flex mt-2">
              <Checkbox
                style={{ padding: 0 }}
                checked={item.isCompleted}
                name="policy"
                onChange={() => onCheckBoxHandled(item)}
              />{' '}
              {editValue?.id === item.id ? (
                <input
                  value={taskCheckList}
                  name="taskCheck"
                  className="border block px-2 text-[16px]"
                  type="text"
                  onChange={(e) => setTaskCheckLists(e.target.value)}
                  onKeyPress={(e) => {
                    if (e.key === 'Enter') {
                      submitTaskCheckLists();
                    }
                  }}
                />
              ) : (
                <p
                  className="my-auto text-[16px] ml-2 "
                  onDoubleClick={() => {
                    setEditValue(item);
                    setTaskCheckLists(item.note);
                    toggleShowInputItem(false);
                  }}
                >
                  {item.note}
                </p>
              )}
              <p className="my-auto ml-3 cursor-pointer" onClick={() => onDeleteTaskCheck(item)}>
                <AiFillDelete size={15} color="#f85365" />
              </p>
            </div>
          );
        })}

        {showInputItem && (
          <input
            value={taskCheckList}
            name="taskCheck"
            className="border block px-2 text-[16px] mt-2"
            type="text"
            onChange={(e) => setTaskCheckLists(e.target.value)}
            onKeyPress={(e) => {
              if (e.key === 'Enter') {
                submitTaskCheckLists();
              }
            }}
          />
        )}
        {!editValue && (
          <button type="button" className="flex  mt-2" onClick={() => toggleShowInputItem()}>
            <div className=" flex flex-col justify-center">
              <TasksPlusIcon />
            </div>
            <span className="ml-3">Add Item</span>
          </button>
        )}
      </div>
    </>
  );
};

export default TaskPoint;
