import clsx from 'clsx';
import React from 'react';
import { getFirstLetters, hexToRGBA, months } from 'utils';
import Add from '@material-ui/icons/Add';

import { USERSTATUSINTASK } from 'common/enum';
import { makeStyles } from '@material-ui/core/styles';
import TaskCreationsFormsWithFormikAndModal from '../TaskCreationsForm';
import { Grid, Typography, Container, Avatar, IconButton, Tooltip, ClickAwayListener } from '@material-ui/core';
import { ITaskInterfaceInput, useGetTaskDetailsByIdQuery, useUpdateTasksByIdMutation } from 'redux/reducers/tasks';
import TaskPoint from './TaskPoint';
import TaskTimeTracker from './TaskTimeTracker';
import TasksAttachments from './TasksAttachments';
import TaskComments from './TaskComments';
import LeftArrowIcon from 'assets/icons-svg/LeftArrowIcon';
import BadgeAvatars from 'components/BadgeAvatars';
import StatusIcon from 'assets/icons-svg/StatusIcon';
import UserAvatarIcon from 'assets/icons-svg/UserAvatarIcon';
import LabelIcon from 'assets/icons-svg/LabelIcon';

import AddLabels from 'components/DragableBoard/AddLabels';
import AddMembersToTasks from './AddMembersToTasks';
import ChangeTaskStatusOfTask from './ChangeTaskStatusOfTask';
import classNames from 'classnames';
import { useGetCurrentUserTimeTrackerQuery } from 'redux/reducers/task-time-tracker';
import { useDispatch } from 'react-redux';
import DeadLineIcon from 'assets/icons-svg/DeadlineIcon';
import Moment from 'react-moment';

const useStyles = makeStyles({
  list: {
    width: '50vw',
  },
  fullList: {
    width: 'auto',
  },
});

const TaskSideBarDetails = (props) => {
  const classes = useStyles();
  const fileRef = React.useRef<any>();

  const { setaddMemberToTaskModal, toggleTaskEditFormModal, anchor, taskId, projectId, toggleDrawer } = props;
  const [updateTasksById] = useUpdateTasksByIdMutation();

  const { data, isLoading } = useGetTaskDetailsByIdQuery(taskId);
  const [taskTitle, setTitle] = React.useState<string | undefined>(undefined);
  const [descriptions, setDescriptions] = React.useState<string | undefined>(undefined);
  const [activeTabIndex, setActiveTabIndex] = React.useState(0);
  const { data: currentUserTimeTrackers } = useGetCurrentUserTimeTrackerQuery();

  const createdDate = new Date(data?.data?.createdAt || '');

  if (isLoading) {
    return null;
  }

  const daysDiffFromToday = (date) => {
    let difference = date.getTime() - new Date().getTime();
    let TotalDays = difference > 0 ? Math.ceil(difference / (1000 * 3600 * 24)) : 0;

    return TotalDays;
  };

  return (
    <div className="my-[41px] ">
      <div
        className={clsx(classes.list, {
          [classes.fullList]: anchor === 'top' || anchor === 'bottom',
        })}
        role="presentation"
      >
        <div className="">
          <div className="mt-2 px-[31px] flex justify-between border-b pb-4">
            <ClickAwayListener onClickAway={() => setTitle(undefined)}>
              {taskTitle ? (
                <input
                  value={taskTitle}
                  name="taskCheck"
                  className="border block px-2 py-[10px] text-[16px] w-[50%]"
                  type="text"
                  placeholder="Add new label"
                  onChange={(e) => setTitle(e.target.value)}
                  onKeyPress={(e) => {
                    if (e.key === 'Enter') {
                      const postData: ITaskInterfaceInput = {
                        title: taskTitle,
                        id: +taskId,
                      };
                      updateTasksById(postData)
                        .unwrap()
                        .then((item) => {
                          setTitle(undefined);
                        });
                    }
                  }}
                />
              ) : (
                <h1
                  className="text-[24px] font-[600] leading-[32px]"
                  onDoubleClick={() => {
                    setTitle(data?.data?.title);
                  }}
                >
                  {' '}
                  {data?.data?.title}
                </h1>
              )}
            </ClickAwayListener>
            <div>
              <button
                className="flex justify-end ml-auto px-[7px] py-[7px] mb-[17px]"
                style={{ background: 'rgba(229, 229, 229, 0.43)' }}
                onClick={toggleDrawer('right', false)}
              >
                <LeftArrowIcon />
              </button>
              <p> {`${months[createdDate.getMonth()]} ${createdDate.getDate()}, ${createdDate.getFullYear()}`}</p>
            </div>
          </div>
          <div className="px-[31px]">
            <div className="flex items-center mt-[14px]">
              <p className="my-auto text-[16px] font-[600]"> Descriptions</p>
              {/* <Tooltip title="Click to Edit Task Details" aria-label="expand">
                <div className="ml-[10px] cursor-pointer">
                  <EditIcon />
                </div>
              </Tooltip> */}
            </div>
            <ClickAwayListener onClickAway={() => setDescriptions(undefined)}>
              {descriptions ? (
                <textarea
                  className="border rounded-[8px] w-[80%] px-[24px] py-[12px] mt-[7px]"
                  value={descriptions}
                  onChange={(event) => {
                    setDescriptions(event.target.value);
                  }}
                  aria-describedby="component-error-text"
                  name="descriptions"
                  rows={4}
                  onKeyPress={(e) => {
                    if (e.key === 'Enter') {
                      const postData: ITaskInterfaceInput = {
                        descriptions: descriptions,
                        id: +taskId,
                      };
                      updateTasksById(postData)
                        .unwrap()
                        .then((item) => {
                          setDescriptions(undefined);
                        });
                    }
                  }}
                />
              ) : (
                <p
                  onDoubleClick={() => {
                    setDescriptions(data?.data?.descriptions);
                  }}
                >
                  {data?.data?.descriptions}
                </p>
              )}
            </ClickAwayListener>

            <div>
              <div className="flex mt-[15px] items-center">
                <div className="flex justify-center flex-col">
                  <StatusIcon />
                </div>
                <p className="ml-[8px] my-auto flex text-center font-[500] text-[14px]">Status</p>
                <div className="ml-[15px]">
                  <ChangeTaskStatusOfTask projectId={projectId} taskStatus={data?.data?.taskStatus} taskId={taskId} />
                </div>
              </div>
              <div className="flex mt-[15px] items-center">
                <div className="flex justify-center flex-col">
                  <UserAvatarIcon />
                </div>
                <p className="ml-[8px]  my-auto flex  text-center font-[500] text-[14px]">Assigned to</p>
                <div className="flex">
                  <div className="flex self-center -space-x-1 overflow-hidden ml-2 mr-2">
                    {data?.data?.taskMember?.map((item, index) => {
                      return (
                        <Tooltip title={data?.data?.createdUser?.userProfile?.fullName || ''}>
                          <img
                            className="inline-block h-[30px] p-[1px] w-[30px] rounded-full text-[8px] ring-2 bg-red-200 grid place-content-center ring-white cursor-pointer"
                            style={{ backgroundColor: hexToRGBA(item.user?.color, 0.5) }}
                            src={item?.user?.userProfile?.userPictureUrl}
                            alt={getFirstLetters(item.user?.userProfile?.fullName)}
                          />
                        </Tooltip>
                      );
                    })}
                  </div>
                  <div className=" flex items-center">
                    <AddMembersToTasks projectId={projectId} members={data?.data?.taskMember} taskId={taskId} />
                  </div>
                </div>
              </div>
              <div className="flex mt-[15px] ">
                <div className="flex justify-center flex-col ">
                  <LabelIcon />
                </div>
                <p className="ml-[8px] my-auto flex text-center font-[500] text-[14px] ">Labels</p>
                {data?.data?.labels?.map((item) => (
                  <p
                    className="ml-[8px]  flex text-center text-white font-[500] text-[14px] bg-red-100 px-[10px] rounded-[4px]"
                    style={{ backgroundColor: hexToRGBA(item.color, 0.5) }}
                  >
                    {item?.title}
                  </p>
                ))}

                <div className="ml-[8px]  flex my-auto flex-col items-center  text-center font-[500] text-[14px] cursor-pointer hover:opacity-3">
                  <AddLabels projectId={projectId} labels={data?.data?.labels || []} taskId={taskId} />
                </div>
              </div>

              <div className="flex mt-[15px]">
                <div className="flex justify-center flex-col">
                  <DeadLineIcon />
                </div>
                <p className="ml-[8px] mb-auto flex text-center font-[500] text-[14px]">Deadline</p>
                <p className="ml-[8px] mb-auto flex text-center font-[500] text-[14px]">
                  {data?.data?.deadline ? <p> in {daysDiffFromToday(new Date(data.data.deadline))} days</p> : <></>}
                </p>
                <>
                  <button className=" relative cursor-pointer ml-[8px]" onClick={() => fileRef?.current?.click()}>
                    <p className="cursor-pointer absolute z-[-10] text-[14px] ">
                      {data?.data?.deadline ? 'Change deadline' : ' Set Deadline'}{' '}
                    </p>
                    <input
                      ref={fileRef}
                      type="date"
                      id="dateInput"
                      name="deadline"
                      style={{ color: 'transparent' }}
                      className="bg-transparent w-full cursor-pointer focus:outline-none"
                      placeholder="Set Deadline"
                      value="Set Deadline"
                      onChange={(e) => {
                        // alert(e.target.value);
                        const postData: ITaskInterfaceInput = {
                          deadline: e.target.value,
                          id: +taskId,
                        };
                        updateTasksById(postData);
                      }}
                      min={new Date().toISOString().split('T')[0]}
                    />
                  </button>
                </>
              </div>
            </div>
          </div>

          <div className="flex mt-[20px] border-b px-[31px] pb-[10px]">
            <p className="font-[600] text-[14px] ml-auto"> Created By</p>
            <Tooltip title={data?.data?.createdUser?.userProfile?.fullName || ''}>
              <>
                <BadgeAvatars
                  alt={`Avatar n°${1}`}
                  src={data?.data?.createdUser?.userProfile.userPictureUrl}
                  sx={{
                    width: 16,
                    height: 16,
                    marginTop: 'auto',
                    marginBottom: 'auto',
                    marginLeft: '8px',
                    marginRight: '8px',
                  }}
                >
                  <p className="text-[9px]">{getFirstLetters(data?.data?.createdUser?.userProfile?.fullName)}</p>
                </BadgeAvatars>
                <p className="my-auto font-[500]">{data?.data?.createdUser?.userProfile?.fullName}</p>
              </>
            </Tooltip>
          </div>
          <div className="px-[31px]">
            <TaskPoint taskId={taskId} />
          </div>
          <div className="flex px-[31px] mt-[28px] border-y">
            <p
              className={classNames('px-[16px] py-[10px] text-[16px] border-[#343434] cursor-pointer', {
                'font-[600] border-b-[1px] ': activeTabIndex === 0,
              })}
              onClick={() => setActiveTabIndex(0)}
            >
              Comments
            </p>

            <p
              className={classNames('px-[16px] py-[10px] text-[16px] border-[#343434] cursor-pointer', {
                'font-[600] border-b-[1px] ': activeTabIndex === 1,
              })}
              onClick={() => setActiveTabIndex(1)}
            >
              Attachments
            </p>
            <p
              className={classNames('px-[16px] py-[10px] text-[16px] border-[#343434] cursor-pointer', {
                'font-[600] border-b-[1px] ': activeTabIndex === 2,
              })}
              onClick={() => setActiveTabIndex(2)}
            >
              Time Tracker
            </p>
          </div>
          <div className="px-[31px]">
            {activeTabIndex === 1 && <TasksAttachments taskId={taskId} />}
            {activeTabIndex === 2 && currentUserTimeTrackers && (
              <TaskTimeTracker taskId={taskId} currentUserTimeTrackers={currentUserTimeTrackers.data} />
            )}

            {activeTabIndex === 0 && <TaskComments taskId={taskId} />}
          </div>
        </div>
      </div>
    </div>
  );
};

export default TaskSideBarDetails;
