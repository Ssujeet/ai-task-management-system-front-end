import { TASKTIMETRACKER } from 'common/enum';
import React from 'react';
import {
  useGetListsOfTaskTrackerTimeByTaskIdQuery,
  useAddTaskTimeTrackersMutation,
  ITaskTimeTrackerResponse,
  useAddManualTimeMutation,
  useDeleteTaskTimeTrackersMutation,
} from 'redux/reducers/task-time-tracker';
import { useStopwatch } from 'react-timer-hook';
import PlayIcon from 'assets/icons-svg/PlayIcon';
import StopIcon from 'assets/icons-svg/StopIcon';
import { AiFillDelete } from 'react-icons/ai';
import { Tooltip } from '@material-ui/core';
import { getFirstLetters } from 'utils';
import { toast } from 'react-toastify';
import MaxWidthDialog from 'components/Modal';
import useToggle from 'hooks/useToggle';
import CrossIcon from 'assets/icons-svg/CrosIcon';
import { useGetCurrentUserProfileQuery } from 'redux/reducers/user-profile';

interface ITaskTimeTracker {
  taskId: string;
  currentUserTimeTrackers: ITaskTimeTrackerResponse | null;
}
const TaskTimeTracker = (props: ITaskTimeTracker) => {
  const { taskId, currentUserTimeTrackers } = props;

  const [deleteConfirmation, toggleConfirmation] = useToggle(false);

  const [manualTime, setManualTime] = React.useState('');

  const { data } = useGetListsOfTaskTrackerTimeByTaskIdQuery(taskId);
  const { data: currentUserDetails } = useGetCurrentUserProfileQuery();
  const [deletingTimeTracker, setDeletingTimeTracker] = React.useState('');
  const [addTaskTimeTrackers] = useAddTaskTimeTrackersMutation();
  const [addManualTime] = useAddManualTimeMutation();
  const [deleteTaskTimeTrackers] = useDeleteTaskTimeTrackersMutation();
  const [stoppedTimers, setStoppedTimers] = React.useState<ITaskTimeTrackerResponse[]>([]);
  const [currentRunningTimer, setCurrentRunningTimer] = React.useState<ITaskTimeTrackerResponse | null>(null);

  const todayDate = new Date();
  const createdTime = currentUserTimeTrackers?.createdAt
    ? new Date(currentUserTimeTrackers?.createdAt).toISOString()
    : new Date().toISOString();

  const differenceTime =
    currentUserTimeTrackers?.task?.id === +taskId
      ? currentUserTimeTrackers?.createdAt
        ? (new Date(
            Date.UTC(
              todayDate.getFullYear(),
              todayDate.getMonth(),
              todayDate.getDate(),
              todayDate.getHours(),
              todayDate.getMinutes(),
              todayDate.getSeconds()
            )
          ).getTime() -
            new Date(createdTime).getTime()) /
          1000
        : 0
      : 0;
  const stopwatchOffset = new Date();
  const offsetTimestamp: any = stopwatchOffset.setSeconds(stopwatchOffset.getSeconds() + differenceTime);
  const { seconds, minutes, hours, days, isRunning, start, pause, reset } = useStopwatch({
    autoStart: false,
    offsetTimestamp,
  });

  React.useEffect(() => {
    if (currentUserTimeTrackers?.task?.id === +taskId) {
      start();
    } else {
      reset();
      pause();
    }
  }, [currentUserTimeTrackers]);
  React.useEffect(() => {
    if (data?.data) {
      const runningTimer = data.data.find((item) => !item.isStopped);
      setCurrentRunningTimer(runningTimer as any);
      const stoppedTimerLists = data.data.filter((item) => {
        return item.isStopped;
      });
      setStoppedTimers(stoppedTimerLists);
    }
  }, [data]);

  const startStopTimer = () => {
    if (currentUserTimeTrackers && currentUserTimeTrackers?.task?.id !== +taskId) {
      toast('You have timer running in task ' + currentUserTimeTrackers?.task.title);

      return;
    }
    addTaskTimeTrackers({
      taskId: +taskId,
      taskStatus: currentRunningTimer ? TASKTIMETRACKER.STOP : TASKTIMETRACKER.START,
    });
    if (isRunning) {
      reset();
      pause();
    } else {
      start();
    }
  };

  const onManualTimeSubmit = () => {
    manualTime &&
      addManualTime({
        taskId: +taskId,
        timeInHours: manualTime,
      })
        .unwrap()
        .then(() => {
          setManualTime('');
        });
  };

  function diff_hours(dt2, dt1) {
    var diff = (dt2.getTime() - dt1.getTime()) / 1000;
    if (diff > 3600) {
      return { diff: (diff /= 60 * 60), unit: ' hrs' };
    }
    if (diff > 59) {
      return { diff: (diff /= 60), unit: ' min' };
    }

    return { diff, unit: ' sec' };
  }

  const onRemoveTimer = (item) => {
    if (item.user.id !== currentUserDetails?.data?.user?.id) {
      toast('You cannot delete this time tracker item.');

      return;
    }
    toggleConfirmation();
    setDeletingTimeTracker(item.id);
  };

  const getTotalHoursWorked = () => {
    let sum = 0;
    stoppedTimers.map((item) => {
      const time = item.manualTime
        ? +item.manualTime
        : (new Date(item.updatedAt).getTime() - new Date(item.createdAt).getTime()) / (1000 * 60 * 60);

      sum += time;
    });

    return sum.toFixed(2);
  };

  const onDeleteConfirm = () => {
    deleteTaskTimeTrackers(deletingTimeTracker);
    toggleConfirmation();
  };

  return (
    <>
      <MaxWidthDialog
        isModalOpen={deleteConfirmation}
        toggleModal={toggleConfirmation}
        renderTitle={() => {
          return (
            <div className="flex justify-between">
              <p className="text-[18px] font-[600]">Confirmation</p>
              <div
                className="bg-[#E5E5E5] cursor-pointer w-[24px] h-[24px] flex justify-between group rounded-[4px]"
                onClick={() => toggleConfirmation(false)}
              >
                <CrossIcon />
              </div>
            </div>
          );
        }}
        submitLabel={'Save'}
        buttonType={'submit'}
        modalSize={'xs'}
      >
        <div className="px-[28px]">
          <div>
            <h2 className="text-[16px] font-[400]">Are you sure you want to delete single time tracker?</h2>
          </div>

          <div className=" mt-5 flex flex-row pb-[21px]">
            <button
              type="button"
              className="cursor-pointer hover:text-red-700 py-1 rounded-lg text-gray-700"
              onClick={() => {
                toggleConfirmation();
                setDeletingTimeTracker('');
              }}
            >
              Cancel
            </button>
            <button
              type="button"
              className="cursor-pointer ml-[24px] bg-red-400 px-[35px] py-[12px] text-white rounded-[4px]"
              onClick={onDeleteConfirm}
            >
              Confirm
            </button>
          </div>
        </div>
      </MaxWidthDialog>
      <div className="mt-[20px]">
        <div className="flex w-full justify-between mb-[20px] text-[16px] font-[600]">
          <h2>Total</h2>

          <p className="text-[16px] font-[600]">{getTotalHoursWorked()} hrs</p>
        </div>
        <div className="flex w-full">
          <input
            value={manualTime}
            name="timeInHours"
            className="border block px-2 py-[12px] w-full"
            type="number"
            placeholder={'Add manual time in hours'}
            onChange={(e) => {
              var regex = /^[0-9]+$/;
              if (e.target.value.match(regex)) {
                setManualTime(e.target.value);
              }
            }}
            onKeyPress={(e) => {
              if (e.key === 'Enter') {
                onManualTimeSubmit();
              }
            }}
          />
          <div className="flex ml-[101px]">
            <div className="text-[16px] flex items-center">
              {days !== 0 && (
                <>
                  <span>{days}</span> :{' '}
                </>
              )}
              {hours !== 0 && (
                <>
                  <span>{hours}</span> :{' '}
                </>
              )}
              <span>{minutes !== 0 ? (minutes < 10 ? '0' + minutes : minutes) : '00'}</span>:
              <span>{seconds !== 0 ? (seconds < 10 ? '0' + seconds : seconds) : '00'}</span>
            </div>
            <button className="ml-[12px]" onClick={startStopTimer}>
              {isRunning ? <StopIcon /> : <PlayIcon />}
            </button>
          </div>
        </div>

        {stoppedTimers.map((item) => {
          const timeDiff = diff_hours(new Date(item.updatedAt), new Date(item.createdAt));

          return (
            <div className="flex mt-[10px]">
              <div className="mr-[10px]">
                <Tooltip title={item.user?.userProfile?.fullName}>
                  <img
                    className="inline-block h-[20px] w-[20px] rounded-full text-[8px] ring-2 bg-red-200 grid place-content-center ring-white cursor-pointer"
                    style={{ backgroundColor: item.user?.color }}
                    src={item?.user?.userProfile?.userPictureUrl}
                    alt={getFirstLetters(item.user?.userProfile?.fullName)}
                  />
                </Tooltip>
              </div>

              <p>{item.manualTime ? item.manualTime + ' hrs' : timeDiff.diff.toFixed(0) + timeDiff.unit}</p>
              <p className="ml-2 cursor-pointer rounded-[50%] " onClick={() => onRemoveTimer(item)}>
                <AiFillDelete size={18} color="#f85365" />
              </p>
            </div>
          );
        })}
      </div>
    </>
  );
};

export default TaskTimeTracker;
