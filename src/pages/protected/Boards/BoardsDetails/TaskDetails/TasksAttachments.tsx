import { FileDrop } from 'components/FileDrop/fileDrop';
import FileViewerListsWithModal from 'components/FileViewerListsWithModal';
import React from 'react';
import { toast } from 'react-toastify';
import {
  useGetTaskAttachmentByTaskIdQuery,
  useAddTaskAttachmentsMutation,
  useDeleteFilesMutation,
} from 'redux/reducers/task-attachments';
import { FcDownload } from 'react-icons/fc';
import { AiFillDelete } from 'react-icons/ai';
import { useGetCurrentUserProfileQuery } from 'redux/reducers/user-profile';
import MaxWidthDialog from 'components/Modal';
import useToggle from 'hooks/useToggle';
import CrossIcon from 'assets/icons-svg/CrosIcon';
import { useDispatch } from 'react-redux';
import { addFilesInViewer } from 'redux/reducers/file-viewer';

const TasksAttachments: React.FC<{ taskId: string }> = ({ taskId }) => {
  const dispatch = useDispatch();
  const [files, setFiles] = React.useState<File[]>();
  const [deleteConfirmation, toggleConfirmation] = useToggle(false);
  const { data } = useGetTaskAttachmentByTaskIdQuery(taskId);
  const [addTaskAttachments] = useAddTaskAttachmentsMutation();
  const [deleteFiles] = useDeleteFilesMutation();
  const { data: currentUserDetails } = useGetCurrentUserProfileQuery();
  const [deletingAttachment, setDeletingAttachment] = React.useState({ id: '', fileName: '' });

  const onDeleteClick = (data) => {
    if (currentUserDetails?.data?.user?.id !== data?.uploadedBy?.id) {
      toast('You cannot delete ' + data.fileName + ' attachment.');

      return;
    }
    toggleConfirmation();
    setDeletingAttachment(data);
  };

  const onDeleteConfirm = () => {
    deleteFiles(deletingAttachment.id);
    toggleConfirmation();
  };

  return (
    <>
      <MaxWidthDialog
        isModalOpen={deleteConfirmation}
        toggleModal={toggleConfirmation}
        renderTitle={() => {
          return (
            <div className="flex justify-between">
              <p className="text-[18px] font-[600]">Confirmation</p>
              <div
                className="bg-[#E5E5E5] cursor-pointer w-[24px] h-[24px] flex justify-between group rounded-[4px]"
                onClick={() => toggleConfirmation(false)}
              >
                <CrossIcon />
              </div>
            </div>
          );
        }}
        submitLabel={'Save'}
        buttonType={'submit'}
        modalSize={'xs'}
      >
        <div className="px-[28px]">
          <div>
            <h2 className="text-[16px] font-[400]">
              Are you sure you want to delete: {deletingAttachment.fileName} attachment.
            </h2>
          </div>

          <div className=" mt-5 flex flex-row pb-[21px]">
            <button
              type="button"
              className="cursor-pointer hover:text-red-700 py-1 rounded-lg text-gray-700"
              onClick={() => {
                toggleConfirmation();
                setDeletingAttachment({ id: '', fileName: '' });
              }}
            >
              Cancel
            </button>
            <button
              type="button"
              className="cursor-pointer ml-[24px] bg-red-400 px-[35px] py-[12px] text-white rounded-[4px]"
              onClick={onDeleteConfirm}
            >
              Confirm
            </button>
          </div>
        </div>
      </MaxWidthDialog>
      <div className="mt-[20px]">
        <FileDrop
          setFiles={(file) => {
            console.log(file, 'setFieldValue');
            const id = toast.loading('Attachments are being uploaded');
            addTaskAttachments({ files: file, taskId: +taskId })
              .unwrap()
              .then(() => {
                toast.update(id, { render: 'Upload Success', type: 'success', isLoading: false, autoClose: 800 });
              })
              .catch((e) => {
                toast.update(id, { render: 'Upload Error', type: 'error', isLoading: false, autoClose: 800 });
              });
            setFiles(file);
          }}
          accept={{
            'image/png': ['.png', '.jgp', '.jpeg', '.gif'],
            audio: ['.mp3'],
            video: ['.mp4'],
            applications: ['.pdf', '.docx'],
          }}
          message={'Upload Attachments here '}
          files={files ?? []}
        />

        {data?.data?.length ? (
          <div className="mt-[29px]">
            <p className="font-[600] text-[16px]">All Attachments</p>
            {data?.data?.map((item, index: number) => (
              <div className="flex my-2">
                <button className=" ml-2 rounded-[50%] px-2 pl-0 cursor-pointer" onClick={() => onDeleteClick(item)}>
                  <AiFillDelete size={20} color="#f85365" />
                </button>
                <a href={`https://s3.us-east-2.amazonaws.com/taskey/${item.key}`} className="px-2">
                  <FcDownload size={20} />
                </a>
                <p
                  className="cursor-pointer hover:text-[#f85365]"
                  onClick={() => {
                    dispatch(addFilesInViewer({ activeIndex: index, fileLists: data?.data }));
                  }}
                >
                  {item.fileName}
                </p>
              </div>
            ))}
          </div>
        ) : null}
      </div>
    </>
  );
};

export default TasksAttachments;
