import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import DragableBoard from 'components/DragableBoard';
import Modal from 'components/Modal';
import PeopleLists from '../../Components/PeopleLists';
import { Tooltip, Typography } from '@material-ui/core';
import TaskCreationsFormsWithFormikAndModal from '../TaskCreationsForm';
import useToggle from 'hooks/useToggle';
import TaskSideBarDetails from './TaskSideBarDetails';

import * as Yup from 'yup';
import {
  useGetTaskListsByProjectIdQuery,
  useGetTaskStatusByProjectIdQuery,
  useAddTasksMutation,
  useAddTaskStatusMutation,
  useAddMembersInTasksMutation,
  useGetTasksLabelListByProjectIdQuery,
} from 'redux/reducers/tasks';
import { useRouteMatch } from 'react-router-dom';
import { taskFormInitialData } from './schema';
import { useFormik } from 'formik';
import { useGetProjectMembersListsQuery } from 'redux/reducers/projectMembers';
import { SCREENS_ROUTES_ENUMS } from 'common/enum';
import AddButton from 'components/Buttons/AddButton';
import CrossIcon from 'assets/icons-svg/CrosIcon';
import { useGetSprintListsOfProjectByIdQuery } from 'redux/reducers/sprint-planning';
import SprintListings from './SprintListings';
import Loader from 'components/Loader/loader';
import { useGetProjectsDetailByIdQuery } from 'redux/reducers/projects';
import { getFirstLetters, hexToRGBA } from 'utils';
import classNames from 'classnames';
import AddLabels from 'components/DragableBoard/AddLabels';
import Spinner from 'components/Spinner/spinner';
// import { useGetTasksLabelListByProjectIdQuery } from 'redux/reducers/tasks-label';

type Anchor = 'top' | 'left' | 'bottom' | 'right';
const anchor = 'right';

export default function TaskDetails() {
  let match = useRouteMatch<{ id: string }>(SCREENS_ROUTES_ENUMS.BOARDS);

  const [state, setState] = React.useState({
    right: false,
  });
  const [taskEditFormModal, toggleTaskEditFormModal] = useToggle(false);
  const [selectedMembers, setSelectedMembers] = React.useState<Array<string>>([]);
  const [selectedLabels, setSelectedLabels] = React.useState<Array<string>>([]);

  const [taskStatusFormModal, setTaskStatusFormModal] = useToggle(false);
  const [addMemberToTaskModal, setaddMemberToTaskModal] = React.useState<boolean>(false);
  const [selectedTaskId, setSelectedTaskId] = React.useState<number>();
  const [taskData, setTaskData] = React.useState<any>(null);
  const [selectedSprint, setSelectedSprint] = React.useState<any>();
  const [searchKeyWord, setSearchKeyWord] = React.useState('');

  const [taskFormData, setTaskFormData] = React.useState(taskFormInitialData);

  const { data: tasksLabelLists } = useGetTasksLabelListByProjectIdQuery(match?.params?.id || '');

  const { data: sprintPlanning } = useGetSprintListsOfProjectByIdQuery(match?.params?.id || '');
  const { data: projectDetails } = useGetProjectsDetailByIdQuery(match?.params?.id || '', {
    skip: !match?.params?.id,
  });
  const activeSprint = sprintPlanning?.data?.find((item) => item.isActive);

  React.useEffect(() => {
    if (activeSprint) {
      setSelectedSprint(activeSprint);
    }
  }, [activeSprint]);

  const { data: tasksLists, isLoading: fetchingTaskLists } = useGetTaskListsByProjectIdQuery(
    { projectId: match?.params?.id || '', sprintId: selectedSprint?.id || '' },
    { skip: !selectedSprint }
  );
  const { data: taskStatusLists, isLoading: fetchingTaskStatus } = useGetTaskStatusByProjectIdQuery(
    match?.params?.id || ''
  );
  const [addTasks] = useAddTasksMutation();
  const [addTaskStatus, { isLoading: isAddingTaskStatus }] = useAddTaskStatusMutation();
  const [addMembersInTasks] = useAddMembersInTasksMutation();
  const { data: projectMember } = useGetProjectMembersListsQuery(match?.params?.id || '');

  React.useEffect(() => {
    if (tasksLists && taskStatusLists) {
      var taskListsData = tasksLists?.data;

      if (taskListsData?.length) {
        if (searchKeyWord) {
          taskListsData = [
            ...taskListsData?.filter((item) => item?.title?.toLowerCase().includes(searchKeyWord.toLowerCase())),
          ];
        }

        if (selectedMembers.length) {
          taskListsData = [
            ...taskListsData.filter((item) =>
              item?.taskMember?.some(
                (member) => member?.user?.id && selectedMembers.includes(member?.user?.id.toString())
              )
            ),
          ];
        }

        if (selectedLabels.length) {
          taskListsData = [
            ...taskListsData.filter((item) =>
              item?.labels?.some((label) => selectedLabels.includes(label.id.toString()))
            ),
          ];
        }
      }

      const columns = taskStatusLists?.data?.reduce((prev, next) => {
        return {
          ...prev,
          ['col-' + next?.id?.toString()]: {
            id: 'col-' + next?.id?.toString(),
            taskStatusId: next?.id,
            title: next?.taskName,
            taskIds: taskListsData
              ?.filter((task) => task?.taskStatus?.id === next.id)
              .map((task) => 'tasks-' + task?.id?.toString() + '-' + task?.order?.toString()),

            canEdit: next.canEdit,
          },
        };
      }, {});
      const tasks = taskListsData?.reduce((prev, current) => {
        return {
          ...prev,
          ['tasks-' + current?.id?.toString() + '-' + current?.order?.toString()]: {
            ...current,
            taskId: current?.id,
            id: 'tasks-' + current.id + '-' + current?.order?.toString(),
            content: current.descriptions,
            commentsCount: current?.comments?.length,
            attachmentCounts: current?.attachments?.length,
            taskMember: current.taskMember,
            labels: current.labels,
          },
        };
      }, {});

      const taskData = { tasks: tasks || {}, columns: columns || {}, columnOrder: columns ? Object.keys(columns) : [] };
      setTaskData(taskData);
    }
  }, [taskStatusLists, tasksLists, searchKeyWord, selectedMembers, selectedLabels]);

  const toggleDrawer = (anchor: Anchor, open: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => {
    if (
      event.type === 'keydown' &&
      ((event as React.KeyboardEvent).key === 'Tab' || (event as React.KeyboardEvent).key === 'Shift')
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const initialValues = {
    taskName: '',
  };

  const formik = useFormik({
    initialValues: initialValues,
    onSubmit: (values, { resetForm }) => {
      if (match?.params?.id) {
        const postData = {
          taskName: values.taskName,
          projectId: +match?.params?.id,
        };
        addTaskStatus(postData as any)
          .unwrap()
          .then((res) => {
            setTaskStatusFormModal();
            resetForm({ values: initialValues });
          })
          .catch((err) => console.log(err));
      }
      // onSubmit(values);
    },
    validationSchema: Yup.object().shape({
      taskName: Yup.string().max(255).required('Task Status name is required'),
    }),
  });

  const handleAddMemberInTasks = (user) => {
    if (match?.params.id && selectedTaskId) {
      const postData = {
        taskId: +selectedTaskId,
        projectId: +match?.params.id,
        userId: +user.value,
      };
      addMembersInTasks(postData);
    }
  };
  const onTaskClicked = React.useCallback((event, taskId) => {
    setSelectedTaskId(taskId);
    toggleDrawer(anchor, true)(event);
  }, []);

  if (fetchingTaskStatus) {
    return <Loader />;
  }

  return (
    <div className="bg-[#FCFCFC] relative w-full">
      <div className="flex justify-between mt-5 mb-2 ">
        <div className="flex  items-center">
          <h2 className="font-[600] text-[24px]">{projectDetails?.data?.projectName}</h2>
          <div className="flex ml-[42px] ">
            <div className="flex -space-x-2 overflow-hidden">
              {projectMember?.data?.map((item, index) => {
                return (
                  <Tooltip title={item.user.userProfile.fullName}>
                    <img
                      className={classNames(
                        'inline-block h-[32px] cursor-pointer p-[1px] w-[32px] text-[8px] grid place-content-center rounded-full opacity-90',
                        { ' border border-[#F85365] ': selectedMembers?.includes(item?.user?.id.toString()) }
                      )}
                      style={{ backgroundColor: hexToRGBA(item.user?.color, 0.5) }}
                      src={item.user.userProfile.userPictureUrl}
                      alt={getFirstLetters(item.user?.userProfile?.fullName)}
                      onClick={() => {
                        let preSelectedMembers = [...selectedMembers];
                        if (preSelectedMembers.includes(item.user.id.toString())) {
                          preSelectedMembers = preSelectedMembers.filter(
                            (mem) => mem.toString() !== item.user.id.toString()
                          );
                        } else {
                          preSelectedMembers.push(item?.user?.id.toString());
                        }

                        setSelectedMembers(preSelectedMembers);
                      }}
                    />
                  </Tooltip>
                );
              })}
            </div>
          </div>
        </div>
        <div className="flex items-center">
          <div className="mr-[15px]">
            <input
              type="text"
              name="taskName"
              className="border focus:outline-none rounded-[8px] px-[24px] py-[8px] "
              onChange={(e) => setSearchKeyWord(e.target.value)}
              value={searchKeyWord}
              placeholder={'Search by task keyword'}
            />
          </div>
          <SprintListings
            projectId={match?.params?.id || ''}
            columns={taskData?.columns}
            setSelectedSprint={setSelectedSprint}
            selectedSprint={selectedSprint}
          />

          <div className="mr-[19px]">
            <AddButton name="Add Task Status" onClick={() => setTaskStatusFormModal()} />
          </div>
        </div>
      </div>
      <div className="flex justify-between  items-center">
        <div className=" w-[70%]">
          {tasksLabelLists?.data?.length ? (
            <div className="flex items-center w-full " style={{ scrollbarWidth: 'none' }}>
              {tasksLabelLists?.data.map((label) => {
                return (
                  <div>
                    <span
                      className={classNames(
                        'px-[31px] ml-[4px] py-[6px] text-[14px] rounded-[15px] cursor-pointer hover:text-[#F85365] hover: hover:border-[#F85365] ',
                        { 'text-[#F85365] border  border-[#F85365]': selectedLabels.includes(label.id.toString()) }
                      )}
                      style={{
                        backgroundColor: selectedLabels.includes(label.id.toString()) ? '' : 'rgba(165, 165, 165, 0.1)',
                      }}
                      onClick={() => {
                        let preSelectedLabels = [...selectedLabels];
                        if (preSelectedLabels.includes(label.id.toString())) {
                          preSelectedLabels = preSelectedLabels.filter((mem) => mem.toString() !== label.id.toString());
                        } else {
                          preSelectedLabels.push(label?.id?.toString());
                        }

                        setSelectedLabels(preSelectedLabels);
                      }}
                    >
                      {label.title}
                    </span>
                  </div>
                );
              })}
              {tasksLabelLists?.data?.length < 8 && (
                <div className="ml-[24px] flex flex-col justify-center">
                  <AddLabels projectId={match?.params.id} hideLists={true} />
                </div>
              )}
            </div>
          ) : (
            <AddLabels projectId={match?.params.id} />
          )}
        </div>
      </div>
      <TaskCreationsFormsWithFormikAndModal
        taskAdditionFormModal={taskEditFormModal}
        modalTitle={'Edit Tasks Details'}
        onSubmit={(value) => console.log(value, 'onSubmit')}
        toggleModal={toggleTaskEditFormModal}
        formData={taskFormData}
      />
      <Modal
        modalSize="xs"
        isModalOpen={taskStatusFormModal}
        toggleModal={() => setTaskStatusFormModal()}
        renderTitle={() => {
          return (
            <div className="flex justify-between">
              <p className="text-[18px] font-[600]">Add Task Status</p>
              <div
                className="bg-[#E5E5E5] cursor-pointer w-[24px] h-[24px] flex justify-between group rounded-[4px]"
                onClick={() => setTaskStatusFormModal()}
              >
                <CrossIcon />
              </div>
            </div>
          );
        }}
        submitLabel={'Save'}
        onSubmitClicked={() => console.log()}
      >
        <form className="p-2 px-6" onSubmit={formik.handleSubmit}>
          <div className="flex flex-col">
            <label className="text-xl">Task Status</label>
            <input
              type="text"
              name="taskName"
              className="border rounded-[8px] px-[24px] py-[12px] mt-[7px]"
              onBlur={formik.handleBlur}
              value={formik.values.taskName}
              onChange={formik.handleChange}
            />

            {Boolean(formik.touched.taskName && formik.errors.taskName) && (
              <span className="text-sm text-red-500">{formik.touched.taskName && formik.errors.taskName}</span>
            )}
          </div>

          <div className=" mt-5 flex flex-row justify-end pt-4">
            <button
              type="button"
              className="cursor-pointer hover:text-[#F85365] px-[29px] py-[12px] rounded-[4px] text-gray-700"
              onClick={() => {
                formik.resetForm({ values: initialValues });
                setTaskStatusFormModal();
              }}
              disabled={isAddingTaskStatus}
            >
              Discard
            </button>
            <button
              type="submit"
              className="cursor-pointer ml-2 bg-[#F85365] px-[29px] py-[12px] rounded-[4px] rounded-lg text-white"
              disabled={isAddingTaskStatus}
            >
              {isAddingTaskStatus ? <Spinner /> : 'Submit'}
            </button>
          </div>
        </form>
      </Modal>
      <Modal
        modalSize="xs"
        isModalOpen={addMemberToTaskModal}
        toggleModal={() => setaddMemberToTaskModal(false)}
        renderTitle={() => {
          return <Typography>Tasks Assigned To</Typography>;
        }}
        submitLabel={'Save'}
        onSubmitClicked={() => console.log()}
      >
        <PeopleLists
          taskId={selectedTaskId}
          projectMembers={projectMember?.data || []}
          handleAdd={handleAddMemberInTasks}
        />
      </Modal>
      <React.Fragment key={anchor}>
        <div className=" pt-[17px] w-full">
          <div className="bg-[#FCFCFC]">
            {taskData && (
              <DragableBoard onTaskClicked={onTaskClicked} taskData={taskData} sprintPlanningId={selectedSprint?.id} />
            )}
          </div>
        </div>

        <Drawer anchor={anchor} open={state[anchor]} onClose={toggleDrawer(anchor, false)}>
          <TaskSideBarDetails
            taskId={selectedTaskId}
            setaddMemberToTaskModal={setaddMemberToTaskModal}
            toggleTaskEditFormModal={toggleTaskEditFormModal}
            anchor={anchor}
            projectId={match?.params?.id}
            toggleDrawer={toggleDrawer}
          />
        </Drawer>
      </React.Fragment>
    </div>
  );
}
