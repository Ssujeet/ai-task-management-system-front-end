import { TASKSPRIORITY } from 'common/enum';
import { ITaskCreationsForms } from '../TaskCreationsForm';
import { string as YupString, object as YupObject } from 'yup';

export const taskFormInitialData: ITaskCreationsForms = {
  descriptions: '',
  title: '',
  priority: TASKSPRIORITY.HIGH,
  deadline: '',
  taskMemberId: [],
};

export const taskFormValidationSchema = YupObject().shape({
  descriptions: YupString(),
  title: YupString().required('Required'),
  deadline: YupString(),
  priority: YupObject().required('Required'),
});
