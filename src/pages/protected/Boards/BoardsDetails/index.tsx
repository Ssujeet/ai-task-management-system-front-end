import React from 'react';
import TaskDetails from './TaskDetails';
import { makeStyles } from '@material-ui/core/styles';
import { RouteComponentProps } from 'react-router-dom';

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
});

interface IBoardsDetails extends RouteComponentProps {}

export default function CenteredTabs(props: IBoardsDetails) {
  const classes = useStyles();

  return (
    <>
      <TaskDetails />
    </>
  );
}
