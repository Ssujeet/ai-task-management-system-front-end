import React, { useRef } from 'react';
import classNames from 'classnames';
import BadgeAvatars from 'components/BadgeAvatars';
import {
  useGetMessagesListBetweenUserMutation,
  IChatMessageInput,
  ChatMessagesResponse,
  useMakeMessageSeenForGivenUserIdMutation,
} from 'redux/reducers/chat-messages';
import { useQueryParams } from 'utils';
import { useRouteMatch } from 'react-router-dom';
import { useGetCurrentUserProfileByUserIdQuery } from 'redux/reducers/user-profile';
import { Socket, io } from 'socket.io-client';
import { useGetCurrentUserQuery } from 'redux/reducers/user';
import FallBackLoader from 'components/FallBackLoader';
import { socketEnvironment } from 'utils/socket-request';
import { SCREENS_ROUTES_ENUMS } from 'common/enum';
import { useSelector } from 'react-redux';
import { RootState } from 'redux/store';
import { useGetProjectMembersListsQuery } from 'redux/reducers/projectMembers';
import { TiTick } from 'react-icons/ti';
import { useGetProjectsDetailByIdQuery } from 'redux/reducers/projects';
import SendArrow from 'assets/icons-svg/SendArrow';
import DoubleTick from 'assets/icons-svg/DoubleTick';
import Tick from 'assets/icons-svg/Tick';
import Spinner from 'components/Spinner/spinner';

interface IChatMesage {
  userName: string;
  message: string;
  isSender: boolean;
  sameUserasAboveTask?: boolean;
}

const ChatBox = () => {
  const messagesEndRef = useRef<any>(null);
  const [socket, setSocket] = React.useState<Socket>();
  const [message, setMessage] = React.useState('');
  const { data: currentUser } = useGetCurrentUserQuery();
  const connectedUsers = useSelector((state: RootState) => state.socket.connectUserId);

  const [makeMessageSeenForGivenUserId] = useMakeMessageSeenForGivenUserIdMutation();

  const [messageLists, setMessageLists] = React.useState<ChatMessagesResponse[]>([] as any);
  const [getMessagesListBetweenUser, { isLoading }] = useGetMessagesListBetweenUserMutation();

  const query = useQueryParams();
  let match = useRouteMatch<{ id: string }>(SCREENS_ROUTES_ENUMS.Chats);

  const { data: projectDetails } = useGetProjectsDetailByIdQuery(match?.params?.id || '', {
    skip: !match?.params?.id,
  });

  const { data: selectedUserProfile } = useGetCurrentUserProfileByUserIdQuery(query.get('key') || '', {
    skip: query.get('key') === '-1',
  });

  React.useEffect(() => {
    if (query.get('key') !== '-1') {
      makeMessageSeenForGivenUserId(query.get('key') || '');
    }
  }, [query.get('key')]);

  React.useEffect(() => {
    const receiverId = query?.get('key');
    setMessage('');

    const socket = io(process.env.REACT_APP_SOCKET_URL as string, socketEnvironment);
    setSocket(socket);

    if (match?.params.id && receiverId && currentUser?.data?.id) {
      const postData: IChatMessageInput = {
        projectId: match?.params.id ? parseInt(match?.params.id) : 0,
        receiverId: +receiverId,
        isGroupChat: parseInt(receiverId) < 0,
      };
      getMessagesListBetweenUser(postData)
        .unwrap()
        .then((res) => {
          res?.data && setMessageLists(res.data);
          scrollToBottom();
        })
        .catch((err) => console.log(err));
      socket?.on('chat' + match.params.id.toString(), (data) => {
        if (data.isGroupChat) {
          if (query?.get('key') === '-1') {
            setMessageLists((prevMessageLists) => [...prevMessageLists, { ...data }]);
            scrollToBottom();
          }
        }
      });
      socket?.on('chat' + match.params.id.toString() + currentUser?.data?.id?.toString(), (data) => {
        if (query?.get('key') === data.receiver.id.toString() || query?.get('key') === data.sender.id.toString()) {
          if (!data.isGroupChat) {
            setMessageLists((prevMessageLists) => [...prevMessageLists, { ...data }]);
            scrollToBottom();
          }
        }
      });
    }

    return () => {
      socket.disconnect();
    };
  }, [match?.params.id, query?.get('key'), currentUser?.data?.id]);

  const onSendClicked = () => {
    const receiverId = query?.get('key');
    if (match?.params?.id && receiverId) {
      const messageData = {
        projectId: match?.params.id ? parseInt(match?.params.id) : 0,
        message: message,
        receiverId: parseInt(receiverId) < 0 ? null : +receiverId,
        isGroupChat: parseInt(receiverId) < 0,
      };
      setMessage('');
      socket?.emit('createMessageBox', messageData);
    }
  };

  const scrollToBottom = () => {
    messagesEndRef.current?.scrollIntoView();
  };
  const isUserActive =
    selectedUserProfile?.data?.user?.id && connectedUsers.includes(selectedUserProfile?.data?.user?.id);

  const isWorkSpaceMessageBox = query?.get('key') === '-1';

  return (
    <>
      <div className="border-b pb-[22px]  ">
        <div className="p-4 pb-0 flex">
          <BadgeAvatars
            src={!isWorkSpaceMessageBox ? selectedUserProfile?.data?.userPictureUrl : projectDetails?.data?.avatarUrl}
            sx={{ width: 50, height: 50 }}
          >
            {selectedUserProfile?.data?.fullName?.split('')[0]?.toUpperCase()}
          </BadgeAvatars>
          <div className="ml-[10px]">
            <h1 className="font-[400]">
              {!isWorkSpaceMessageBox
                ? selectedUserProfile?.data?.fullName ?? selectedUserProfile?.data?.user?.name ?? 'Workspace group chat'
                : 'Workspace group chat'}
            </h1>
            {
              <div className="flex">
                <div className="flex flex-col justify-center mr-2">
                  <span
                    className={'w-[10px] h-[10px] rounded-[50%] bg-green-200'}
                    style={{
                      backgroundColor: isWorkSpaceMessageBox ? 'green' : isUserActive ? 'green' : 'gray',
                    }}
                  ></span>
                </div>

                {<span>{!isWorkSpaceMessageBox ? (isUserActive ? 'Active' : 'In Active') : 'Active'}</span>}
              </div>
            }
          </div>
        </div>
      </div>
      <div className="relative   ">
        <div
          className=" absolute bg-white px-[24px]     "
          style={{ left: 0, bottom: 0, right: 0, zIndex: 10000, scrollbarWidth: 'none' }}
        >
          <div className="w-full border bg-[#FCFCFC] rounded-[8px] flex mb-[30px]    bg-red-200">
            <textarea
              className=" focus:outline-none px-[24px] py-[16px] w-full rounded-[8px] text-[16px] font-[400] bg-[#FCFCFC] resize-none  "
              value={message}
              placeholder="Enter a message to send"
              onChange={(e) => setMessage(e.target.value)}
              rows={2}
            />
            <button disabled={!message || /^\s*$/.test(message)} className="px-[29px]" onClick={onSendClicked}>
              <SendArrow />
            </button>
          </div>
        </div>

        <div className="p-4 overflow-scroll hide-scrollbar  h-[75vh] pb-[170px] " style={{ scrollbarWidth: 'none' }}>
          {messageLists.length ? (
            messageLists?.map((item, index) => (
              <MessageBox
                chatMessages={item}
                message={item.message}
                userName={item?.sender?.userProfile?.fullName}
                sameUserasAboveTask={index === 0 ? false : item.sender.id === messageLists[index - 1].sender.id}
                isSender={item.sender.id.toString() === currentUser?.data?.id?.toString()}
                index={index}
                totalCount={messageLists.length}
              />
            ))
          ) : (
            <p className="mx-auto flex my-auto w-1/2 mt-10 text-2xl font-[400] text-center">
              {!isLoading ? "You don't have any conversations in the message box till now." : <Spinner />}
            </p>
          )}
          <div ref={messagesEndRef} />
        </div>
      </div>
    </>
  );
};

export default ChatBox;

interface IMessageBox {
  chatMessages: ChatMessagesResponse;
  userName: string;
  message: string;
  isSender: boolean;
  sameUserasAboveTask?: boolean;
  index: number;
  totalCount: number;
}

const MessageBox = (props: IMessageBox) => {
  const { chatMessages, userName, message, sameUserasAboveTask, isSender, index, totalCount } = props;
  let match = useRouteMatch<{ id: string }>(SCREENS_ROUTES_ENUMS.Chats);

  const { data: projectMembers } = useGetProjectMembersListsQuery(match?.params?.id || '', {
    skip: !match?.params?.id,
  });

  const src = projectMembers?.data?.length
    ? [...projectMembers?.data]?.find((item) => item.user.id === chatMessages?.sender?.id)?.user?.userProfile
        .userPictureUrl
    : '';
  const messageTime = new Date(chatMessages.createdAt);
  var time = messageTime.getHours() + ':' + messageTime.getMinutes();

  return (
    <div className={classNames('mt-[5px] w-1/2', { 'ml-auto': isSender })}>
      {!sameUserasAboveTask && (
        <div className={classNames('flex flex-row mb-[10px]', { 'flex-row-reverse': isSender })}>
          <BadgeAvatars src={src} sx={{ width: 40, height: 40 }}>
            {props?.userName?.split('')[0]?.toUpperCase()}
          </BadgeAvatars>
          <div className={classNames({ 'ml-2 mt-2': !isSender, 'mr-2 mt-2': isSender })}>
            <p className="font-[500] text-[18px]">{isSender ? 'You' : userName}</p>
          </div>
        </div>
      )}

      <div
        className={classNames(' p-4  py-2   rounded-[10px] mt-1', {
          ' bg-gray-200 flex': !isSender,
          'bg-[#f85365] ml-auto text-white': isSender,
        })}
        style={{ width: 'fit-content' }}
      >
        <p>{message}</p>
        <div className={classNames('flex justify-end', { 'ml-2': !isSender })}>
          <p
            className={classNames('text-[12px] font-[500] flex flex-col justify-end  ', { 'text-gray-600': !isSender })}
          >
            {time}
          </p>
          {index === totalCount - 1 && isSender && !chatMessages.isGroupChat && (
            <div className="flex">
              <p className="text-[12px]  ml-[9px]">
                {chatMessages.isMessageSeen ? 'Seen' : chatMessages.isDelivered ? 'Delivered' : 'Sent'}
              </p>
              <div className="flex flex-col ml-[1px] justify-center">
                {chatMessages.isMessageSeen || chatMessages.isDelivered ? <DoubleTick /> : <Tick />}
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
