import React from 'react';
import classNames from 'classnames';
import { useRouteMatch, useHistory } from 'react-router-dom';
import { useGetProjectMembersListsQuery } from 'redux/reducers/projectMembers';
import { ProjectOutputInterface } from 'redux/reducers/projects';

import { useQueryParams, getFirstLetters } from 'utils';
import { useGetCurrentUserQuery } from 'redux/reducers/user';
import BadgeAvatars from 'components/BadgeAvatars';
import { SCREENS_ROUTES_ENUMS } from 'common/enum';
import { useGetProjectsDetailByIdQuery } from 'redux/reducers/projects';
import { useSelector } from 'react-redux';
import { RootState } from 'redux/store';
import { useGetCurrentUserUnSeenMessageListsQuery } from 'redux/reducers/chat-messages';
import SearchIcon from 'assets/icons-svg/SearchIcon';

const UserLists = () => {
  const [searchKey, setSearchKey] = React.useState('');
  const [filteredUserLists, setFilteredUserLists] = React.useState<any>();
  const { data: currentUser } = useGetCurrentUserQuery();

  const query = useQueryParams();
  const history = useHistory();
  const connectedUsers = useSelector((state: RootState) => state.socket.connectUserId);

  let match = useRouteMatch<{ id: string }>(SCREENS_ROUTES_ENUMS.Chats);
  const { data: membersLists } = useGetProjectMembersListsQuery(match?.params?.id || '');

  const { data: projectDetails } = useGetProjectsDetailByIdQuery(match?.params?.id || '', {
    skip: !match?.params?.id,
  });

  const onUserCardClicked = (id: number) => {
    if (match) {
      history.replace(match.url + '?key=' + id);
    }
  };

  React.useEffect(() => {
    if (membersLists && membersLists?.data?.length) {
      const filteredUserLists = searchKey
        ? membersLists?.data?.filter(
            (item) =>
              item?.user?.userProfile?.fullName?.toLowerCase()?.includes(searchKey) ||
              item?.user?.email?.toLowerCase()?.includes(searchKey)
          )
        : [...membersLists?.data];
      setFilteredUserLists(filteredUserLists);
    }
  }, [membersLists, searchKey]);

  return (
    <div className=" ">
      <div className="flex justify-between rounded-[8px]  border  w-full">
        <input
          type="text"
          name="search"
          className="  w-full py-[16px] pl-[12px] focus:outline-none font-sm text-gray-600"
          placeholder="Search by username or email "
          value={searchKey}
          onChange={(e) => setSearchKey(e.target.value.toLowerCase())}
        />
        <div className="flex flex-col justify-center px-[20px]">
          <SearchIcon />
        </div>
      </div>
      <div className="overflow-scroll hide-scrollbar h-[75vh] py-[12px] bg-white px-[15px] shadow-sm mt-[22px]">
        <p className="px-2 text-[18px] font-[500] mt-[5px] py-2 pb-0 mb-[21px]">Group Chat</p>

        <UserCard
          item={{
            user: {
              userProfile: {
                fullName: 'Work Space',
                userPictureUrl: projectDetails?.data?.avatarUrl,
              },
              color: projectDetails?.data?.color,
              id: -1,
            },
          }}
          activeUser={query?.get('key')}
          onClick={onUserCardClicked}
          projectId={match?.params?.id}
        />

        <p className="px-2 text-[18px] mt-[12px] font-[500] py-2 pb-0 mb-[2px]">Private Chat</p>
        <div className="divide ">
          {filteredUserLists?.map((item) => {
            if (item.user.id.toString() === currentUser?.data?.id.toString()) {
              return null;
            }

            return (
              <UserCard
                item={item}
                activeUser={query?.get('key')}
                onClick={onUserCardClicked}
                isOnline={connectedUsers.includes(item.user.id)}
                projectId={match?.params?.id}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default UserLists;

interface IUserCard {
  item?: ProjectOutputInterface | any;
  activeUser: string | null;
  onClick: (id: number) => void;
  isOnline?: boolean;
  projectId: string | undefined;
}

const UserCard = (props: IUserCard) => {
  const { item, isOnline, projectId } = props;
  const { data: unSeenMessageMessageLists } = useGetCurrentUserUnSeenMessageListsQuery();

  const currentProjectUserUnMessage = unSeenMessageMessageLists?.data?.filter(
    (message) =>
      message.sender.id.toString() === item.user.id.toString() && message?.project?.id?.toString() === projectId
  );

  return (
    <div
      className={classNames('flex  px-2 py-[16px] border-b cursor-pointer   mt-3 hover:bg-gray-200', {
        'bg-gray-200': props.activeUser === props.item?.user.id.toString(),
      })}
      onClick={() => props.onClick(props.item?.user.id || -1)}
    >
      <BadgeAvatars
        src={item?.user?.userProfile?.userPictureUrl}
        sx={{ width: 45, height: 45, backgroundColor: item?.user?.color }}
        isActive={isOnline}
      >
        {getFirstLetters(item?.user?.name)?.toUpperCase()}
      </BadgeAvatars>
      <div className="ml-2 my-auto">
        <p className="font-[200] text-[18px]">{props.item?.user?.userProfile?.fullName}</p>

        {/* <p className="text-gray-500">latest message</p> */}
      </div>
      {props.item?.user.id !== -1 && currentProjectUserUnMessage?.length !== 0 && (
        <div className="ml-auto my-auto">
          {/* <p className="text-gray-500">5 hours ago</p> */}
          <p className="text-[10px] text-right">
            <span className="rounded-3xl bg-yellow-100 px-1 text-yellow-500">
              {currentProjectUserUnMessage?.length}
            </span>
          </p>
        </div>
      )}
    </div>
  );
};
