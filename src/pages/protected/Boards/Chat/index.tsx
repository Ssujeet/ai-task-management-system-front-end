import React from 'react';
import UserLists from './components/user-lists/user-lists';
import ChatBox from './components/chat-box/chat-box';
import { useRouteMatch, useHistory, useLocation } from 'react-router-dom';
import { useGetProjectMembersListsQuery } from 'redux/reducers/projectMembers';
import { useQueryParams } from 'utils';
import { useGetCurrentUserUnSeenMessageListsQuery } from 'redux/reducers/chat-messages';
import { SCREENS_ROUTES_ENUMS } from 'common/enum';
import Loader from 'components/Loader/loader';

const Chat = () => {
  let match = useRouteMatch<{ id: string }>(SCREENS_ROUTES_ENUMS.Chats);
  const { data: membersLists, isLoading: fetchingUserLists } = useGetProjectMembersListsQuery(match?.params?.id || '');
  const history = useHistory();
  const query = useQueryParams();

  React.useEffect(() => {
    if (match && membersLists && membersLists?.data?.length && !query.get('key')) {
      history.replace(match.url + `?` + 'key=' + '-1');
    }
  }, [membersLists, match?.url]);

  if (fetchingUserLists) {
    return <Loader />;
  }

  return (
    <div className="mt-[41px]">
      <div className="flex justify-between flex-row">
        <div className=" w-4/12 ">
          <UserLists />
        </div>

        <div className=" bg-white w-10/12 ml-4 shadow-sm ">
          <ChatBox />
        </div>
      </div>
    </div>
  );
};

export default Chat;

{
  /* <div>
        <div className="mt-3 flex -space-x-2 overflow-hidden">
          <img
            className="inline-block h-12 w-12 rounded-full ring-2 ring-white"
            src="https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
            alt=""
          />
          <img
            className="inline-block h-12 w-12 rounded-full ring-2 ring-white"
            src="https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
            alt=""
          />
          <img
            className="inline-block h-12 w-12 rounded-full ring-2 ring-white"
            src="https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2.25&w=256&h=256&q=80"
            alt=""
          />
          <img
            className="inline-block h-12 w-12 rounded-full ring-2 ring-white"
            src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
            alt=""
          />
          <img
            className="inline-block h-12 w-12 rounded-full ring-2 ring-white"
            src="https://images.unsplash.com/photo-1517365830460-955ce3ccd263?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
            alt=""
          />
        </div>

        <div className="h-[50vh] mt-4 overflow-scroll">
          <p>Hello</p>
        </div>
      </div> */
}
