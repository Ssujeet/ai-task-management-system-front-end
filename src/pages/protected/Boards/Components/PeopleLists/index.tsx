import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import PersonIcon from '@material-ui/icons/Person';
import AddIcon from '@material-ui/icons/Add';
import Typography from '@material-ui/core/Typography';
import { blue } from '@material-ui/core/colors';
import Select from 'components/Select/SelectWithAutoComplete';
import { ListItemSecondaryAction, IconButton, useMediaQuery, Tooltip } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import { ProjectMemberInput } from 'redux/reducers/projectMembers';
import { ProjectOutputInterface } from 'redux/reducers/projects';
import { useGetTaskDetailsByIdQuery, useDeleteTaskMemberByIdMutation } from 'redux/reducers/tasks';
import { USERSTATUSINTASK } from 'common/enum';
const emails = [
  'username@gmail.com',
  'username@gmail.com',
  'username@gmail.com',
  'username@gmail.com',
  'username@gmail.com',
  'username@gmail.com',
  'username@gmail.com',
  'username@gmail.com',
  'username@gmail.com',
  'username@gmail.com',
  'username@gmail.com',
  'username@gmail.com',
  'username@gmail.com',
  'username@gmail.com',
  'username@gmail.com',
];
const useStyles = makeStyles({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
});

export interface SimpleDialogProps {
  projectMembers: ProjectOutputInterface[];
  handleAdd?: (data) => void;
  taskId?: number;
}

export default function PeopleLists(props: SimpleDialogProps) {
  const { projectMembers, handleAdd, taskId } = props;
  const classes = useStyles();
  const [addedUser, setAssignedUser] = React.useState();
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const { data: taskDetails, isLoading } = useGetTaskDetailsByIdQuery(taskId as any, { skip: !taskId });
  const [deleteTaskMemberById] = useDeleteTaskMemberByIdMutation();

  const userLists = projectMembers?.map((item) => {
    return {
      id: item.user.id,
      label: item?.user?.userProfile?.fullName,
      value: item.user.id.toString(),
    };
  });

  const onAddClicked = () => {
    handleAdd && addedUser && handleAdd(addedUser);
    setAssignedUser(undefined);
  };

  const onDeleteUser = (id: string) => {
    deleteTaskMemberById(id);
  };

  return (
    <List>
      <div style={{ maxHeight: fullScreen ? '70vh' : '30vh', overflowY: 'auto' }}>
        {taskDetails?.data?.taskMember?.map(
          (item, index) =>
            item.userStatus === USERSTATUSINTASK.ACTIVE && (
              <ListItem button onClick={() => console.log(item)} key={index}>
                <ListItemAvatar>
                  <Avatar className={classes.avatar} src={item?.user?.userProfile?.userPictureUrl}>
                    <PersonIcon />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary={item?.user?.name} />
                <ListItemSecondaryAction>
                  <Tooltip title={'Clicking on Icon will delete member from tasks.'}>
                    <IconButton>
                      <DeleteIcon color="error" onClick={() => onDeleteUser(item?.id?.toString())} />
                    </IconButton>
                  </Tooltip>
                </ListItemSecondaryAction>
              </ListItem>
            )
        )}
      </div>
      <ListItem button onClick={() => console.log('addAccount')}>
        <Select
          isMulti={false}
          label="Add Members to Assign"
          name="assignedUser"
          value={addedUser}
          onChange={(value) => setAssignedUser(value)}
          options={
            userLists?.filter((item) => !taskDetails?.data?.taskMember?.some((tm) => tm?.user?.id === item?.id)) as any
          }
        />
        <ListItemAvatar className="ml-2">
          <Avatar>
            <AddIcon onClick={onAddClicked} />
          </Avatar>
        </ListItemAvatar>
      </ListItem>
    </List>
  );
}
