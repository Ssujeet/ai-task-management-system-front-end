import React from 'react';
import Button from '@material-ui/core/Button';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { IconButton } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { ProjectAddForm } from '../ProjectForms/schema';
import { useChangeProjectAvatarMutation } from 'redux/reducers/projects';
import { toast } from 'react-toastify';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    paper: {
      marginRight: theme.spacing(2),
    },
  })
);

interface IProjectCardMenu {
  onEditClicked: (data: ProjectAddForm) => void;
}
export default function ProjectCardMenu({ onEditClicked, projectId }) {
  const fileRef = React.useRef<any>();
  const [changeProjectAvatar] = useChangeProjectAvatarMutation();

  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef<HTMLButtonElement>(null);
  const history = useHistory();

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event: any) => {
    if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) {
      return;
    }
    if (event.target.id) {
      history.push(`/work-space/${event.target.id}/123123`);
    }

    setOpen(false);
  };

  function handleListKeyDown(event: React.KeyboardEvent) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setOpen(false);
    }
  }

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current!.focus();
    }

    prevOpen.current = open;
  }, [open]);

  const onFileUpload = (e) => {
    const [file] = e.target.files;
    const id = toast.loading('Workspace avatar is updating.');
    changeProjectAvatar({ projectId: +projectId, file: file })
      .unwrap()
      .then(() => toast.update(id, { render: 'Upload Success', type: 'success', isLoading: false, autoClose: 800 }))
      .catch((err) => {
        toast.update(id, { render: 'Error while uploading avatar', type: 'error', isLoading: false, autoClose: 800 });
      });
  };

  return (
    <div className={classes.root}>
      <IconButton
        ref={anchorRef}
        aria-controls={open ? 'menu-list-grow' : undefined}
        aria-haspopup="true"
        style={{ textTransform: 'none' }}
        onClick={handleToggle}
      >
        <MoreVertIcon />
      </IconButton>
      <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{
              transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
            }}
          >
            <Paper>
              <ClickAwayListener onClickAway={handleClose}>
                <MenuList autoFocusItem={open} onKeyDown={handleListKeyDown}>
                  {/* <MenuItem id="tasks">Delete</MenuItem> */}
                  <MenuItem id="documents" onClick={onEditClicked}>
                    Edit
                  </MenuItem>
                  <MenuItem id="documents">
                    <button onClick={() => fileRef?.current?.click()}>
                      <input
                        accept="image/png, image/gif, image/jpeg image/jpg"
                        ref={fileRef}
                        max-file-size="1024"
                        className="hidden ml-2"
                        onChange={onFileUpload}
                        multiple={true}
                        type="file"
                        hidden
                      />
                      <span className="text-[16px] my-auto">Change Avatar</span>
                    </button>
                  </MenuItem>
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </div>
  );
}
