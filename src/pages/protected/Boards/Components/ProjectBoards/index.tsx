import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import { useHistory, Link } from 'react-router-dom';
import { ProjectInterface, ProjectOutputInterface } from 'redux/reducers/projects';

import ProjectCardMenu from './ProjectCardMenu';
import FilesIcon from 'assets/icons-svg/FilesIcon';
import { getFirstLetters, getRandomColor, hexToRGBA, months } from 'utils';
import { ProjectAddForm } from '../ProjectForms/schema';
interface IProjectBoardCards {
  project: ProjectInterface;
  onEditClicked: (data: ProjectAddForm) => void;
}
export default function ProjectsBoardCards(props: IProjectBoardCards) {
  const { project } = props;
  const history = useHistory();

  const onProjectCardClicked = () => {
    history.push('/work-space/boards/' + project.id);
  };

  const date = new Date(project.createdAt || '');

  return (
    <div className="p-[19px] group relative rounded-[16px] w-[325px] border cursor-pointer  shadow transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-105  duration-300 ...">
      <div>
        <div className="flex justify-between">
          <div className="flex ">
            <Avatar
              aria-label="recipe"
              src={project.avatarUrl}
              className="w-[52px] h-[52px]"
              style={{ backgroundColor: project.color }}
            >
              {getFirstLetters(project.projectName)}
            </Avatar>
            <div className="ml-[9px]">
              <p className="text-[18px] font-[600] text-[#145AFF]">{project.projectName}</p>
              <p className="text-[12px] font-[700]">
                {`${months[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()}`}
              </p>
            </div>
          </div>
          <div className="hidden group-hover:block absolute right-0 px-2 z-[900]">
            <ProjectCardMenu onEditClicked={() => props.onEditClicked(project)} projectId={project.id} />
          </div>
        </div>

        <Link to={`/work-space/boards/${project.id}`}>
          <div className="cursor-pointer mt-[17px] text-[14px] font-[400]" onClick={onProjectCardClicked}>
            <p className="text-[14px] font-[400]">{project.descriptions}</p>
            <div className="flex mt-[6px]">
              <FilesIcon />
              <p className="text-[14px] font-[400] ml-[5px] text-center">
                {project?.folders?.length} number of folders
              </p>
            </div>
          </div>
          <div className="flex mt-[31px] justify-end">
            <div className="flex -space-x-2 overflow-hidden">
              {project.projectMember.map((item, index) => {
                if (index < 2) {
                  return (
                    <img
                      className="inline-block h-[32px] px-[1px] w-[32px] text-[8px] grid place-content-center rounded-full "
                      style={{ backgroundColor: hexToRGBA(item.user?.color, 0.5) }}
                      src={item.user.userProfile.userPictureUrl}
                      alt={getFirstLetters(item.user?.userProfile?.fullName)}
                    />
                  );
                }

                return null;
              })}
            </div>
            {project.projectMember.length > 2 ? (
              <div className="font-medium text-[14px] my-auto ">
                <a href="#" className="ml-[2px] ">
                  + {project.projectMember.length - 2}
                </a>
              </div>
            ) : null}
          </div>
        </Link>
      </div>
    </div>
  );
}
