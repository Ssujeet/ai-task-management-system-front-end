import React from 'react';
import { useFormik } from 'formik';
import { FormikProps } from 'formik';
import { DialogContent, DialogActions, CircularProgress } from '@material-ui/core';

import FormikValidationError from 'components/FormikErrors';
import MaxWidthDialog from 'components/Modal';
import { initialValues, ProjectAddForm, projectSchema } from './schema';
import { FileDrop } from 'components/FileDrop/fileDrop';
import CrossIcon from 'assets/icons-svg/CrosIcon';
import Spinner from 'components/Spinner/spinner';

interface IProjectForms extends FormikProps<ProjectAddForm> {}

export function ProjectForms(props: IProjectForms) {
  const onInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    props.setFieldValue(event.target.name, event.target.value);
  };

  return (
    <div>
      <div>
        <div>
          <div className="flex flex-col w-full">
            <label htmlFor="component-filled" className="text-[rgba(52, 52, 52, 0.6)]">
              Workspace name *
            </label>
            <input
              className="border rounded-[8px] px-[14px] py-[12px] mt-[7px]"
              value={props.values.projectName}
              onChange={onInputChange}
              aria-describedby="component-error-text"
              name="projectName"
              type="text"
            />

            <FormikValidationError name="projectName" errors={props.errors} touched={props.touched} />
          </div>
        </div>

        <div className="mt-3">
          <div className="flex flex-col w-full">
            <label htmlFor="component-filled">Descriptions *</label>
            <textarea
              className="border rounded-[8px] px-[14px] py-[12px] mt-[7px]"
              value={props.values.descriptions}
              onChange={(event) => {
                console.log(event.target.value, 'value');
                props.setFieldValue('descriptions', event.target.value);
              }}
              aria-describedby="component-error-text"
              name="descriptions"
              rows={3}
            />

            <FormikValidationError name="descriptions" errors={props.errors} touched={props.touched} />
          </div>
        </div>
        {!props.values.id && (
          <div className="mt-3">
            <div className="flex flex-col w-full">
              <label htmlFor="component-filled">Avatar</label>
              <div className="mt-3">
                <FileDrop
                  setFiles={(file) => {
                    props.setFieldValue('file', file[0]);
                  }}
                  message={'Upload or Drag avatar in JPEG or PNG format '}
                  files={props.values.file ? [props.values.file] : []}
                  accept={{ 'image/png': ['.png', '.jgp', '.jpeg', '.gif'] }}
                />
                {props?.values?.file?.name && (
                  <div className="flex mt-[10px]">
                    <p
                      className="cursor-pointer p-[5px] py-[0px] text-white rounded-[50%]  ml-[10px] hover:bg-red-400 bg-[#f85365] text-[12px] align-bottom  mt-auto inline-block"
                      onClick={() => {
                        props.setFieldValue('file', null);
                      }}
                    >
                      -
                    </p>
                    <p className="text-[14px] ml-[10px]">{props?.values?.file?.name}</p>
                  </div>
                )}
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

interface IProjectFormWithFormiks {
  formData?: ProjectAddForm;
  toggleModal: () => void;
  onSubmit: (data) => void;
  isLoading?: boolean;
}
export const ProjectFormWithFormiks = (props: IProjectFormWithFormiks) => {
  const { formData, toggleModal, onSubmit, isLoading } = props;

  const formik = useFormik({
    initialValues: formData ? formData : initialValues,
    onSubmit: (values) => {
      onSubmit(values);
    },
    validationSchema: projectSchema,
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <DialogContent>
        <ProjectForms {...formik} />
      </DialogContent>
      <DialogActions>
        <div className="flex justify-between w-full mt-[58px] mb-[35px] px-[10px]">
          <button
            color="primary"
            type={'submit'}
            className="bg-[#F85365] text-white px-[39px] py-[12px] rounded-[4px]"
            disabled={isLoading}
          >
            {isLoading ? <Spinner /> : 'Submit'}
          </button>
          <button
            onClick={toggleModal}
            type={'button'}
            color="secondary"
            style={{ textTransform: 'none' }}
            disabled={isLoading}
          >
            Close
          </button>
        </div>
      </DialogActions>
    </form>
  );
};

interface IProjectFormWithFormiksWithModalProps {
  projectAdditionFormModal: boolean;
  toggleprojectAdditionFormModal: (data: boolean) => void;
  modalTitle: string;
  formData?: ProjectAddForm;
  toggleModal: () => void;
  onSubmit: (data) => void;
  isLoading?: boolean;
  setProjectFormData?: (data: ProjectAddForm) => void;
}
const ProjectFormWithFormiksWithModal = (props: IProjectFormWithFormiksWithModalProps) => {
  const {
    projectAdditionFormModal,
    toggleprojectAdditionFormModal,
    modalTitle,
    formData,
    setProjectFormData,
    ...rest
  } = props;

  const onToggleAndResetValues = () => {
    toggleprojectAdditionFormModal(!projectAdditionFormModal);
    setProjectFormData && setProjectFormData(initialValues);
  };

  return (
    <MaxWidthDialog
      modalSize="xs"
      isModalOpen={projectAdditionFormModal}
      toggleModal={() => toggleprojectAdditionFormModal(!projectAdditionFormModal)}
      renderTitle={() => {
        return (
          <div className="flex justify-between">
            <h1 className="text-[18px] font-[600]">{modalTitle}</h1>
            <div
              className="bg-[#E5E5E5] cursor-pointer w-[24px] h-[24px] flex justify-between group rounded-[4px]"
              onClick={rest.toggleModal}
            >
              <CrossIcon />
            </div>
          </div>
        );
      }}
      submitLabel={'Save'}
      buttonType={'submit'}
    >
      <ProjectFormWithFormiks formData={formData} {...rest} toggleModal={onToggleAndResetValues} />
    </MaxWidthDialog>
  );
};
export default ProjectFormWithFormiksWithModal;
