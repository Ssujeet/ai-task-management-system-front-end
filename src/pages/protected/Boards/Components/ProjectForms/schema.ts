import { string as YupString, object as YupObject } from 'yup';

export const projectSchema = YupObject().shape({
  projectName: YupString().required('Project Name is required'),
  descriptions: YupString().required('Desriptions in required'),
  // startDate: YupString().required('Start Date is required'),
  // endDate: YupString().required('End Date is required'),
});

export interface ProjectAddForm {
  projectName: string;
  descriptions: string;
  file?: File | null;
  id?: number;
}

export const initialValues: ProjectAddForm = {
  projectName: '',
  descriptions: '',
  file: null,
};
