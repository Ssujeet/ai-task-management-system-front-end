import React from 'react';
import { useFormik } from 'formik';
import { ProjectMemberInput } from 'redux/reducers/projectMembers';
import { PROJECTMEMBERROLE } from 'common/enum';
import { ReactSelect } from 'components/ReactSelect';
import { useGetUsersListsQuery } from 'redux/reducers/user';

const initialValues: ProjectMemberInput = {
  userId: 0,
  projectId: 0,
  role: PROJECTMEMBERROLE.MEMBER,
  userRoleIdInProject: 0,
};

interface IStandUpForm {
  formData?: typeof initialValues;
  onSubmit: (data: any) => void;
  setAddMemberForm: () => void;
}

const AddMemberForm = (props: IStandUpForm) => {
  const { data: userLists } = useGetUsersListsQuery();
  const { formData, onSubmit, setAddMemberForm } = props;
  const formik = useFormik({
    initialValues: formData ? formData : initialValues,
    onSubmit: (values) => {
      onSubmit(values);
    },
    // validationSchema: standUpValidationSchema,
  });

  const options = userLists?.data?.map((item) => {
    return {
      label: item.username,
      value: item.id,
    };
  });

  return (
    <div className="px-8 py-8">
      <label className="text-xl">Select User to Invite</label>
      <div className="mb-10">
        <ReactSelect options={options} placeholder="Search user by username or email to invite" />
      </div>

      <div></div>
    </div>
  );
};

export default AddMemberForm;
