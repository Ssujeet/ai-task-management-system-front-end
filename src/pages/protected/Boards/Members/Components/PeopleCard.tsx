import React from 'react';
import BadgeAvatars from 'components/BadgeAvatars';

import { ProjectOutputInterface } from 'redux/reducers/projects';
import { SCREENS_ROUTES_ENUMS, USERSTATUSINPROJECT } from 'common/enum';

import { getFirstLetters, getRandomColor } from 'utils';
import { Link, useHistory, useRouteMatch } from 'react-router-dom';
import VerticalThreeDotsIcon from 'assets/icons-svg/VerticalThreeDotsIcon';
import PeopleCardMenu from './PeopleCardsMenu';

interface IPeopleCard {
  people: ProjectOutputInterface;
  onRemove: (people: ProjectOutputInterface) => void;
  onRoleChangeClicked: (people: ProjectOutputInterface) => void;
  connectedUsers: Array<number>;
}
const PeopleCard = (props: IPeopleCard) => {
  const { people, onRemove, onRoleChangeClicked, connectedUsers } = props;
  let match = useRouteMatch<{ id: string }>(SCREENS_ROUTES_ENUMS.MEMBERS);

  const src = people?.user?.userProfile?.userPictureUrl;
  const history = useHistory();
  console.log(connectedUsers, 'connectedUsers');

  return (
    <>
      <div className="bg-white shadow-md flex flex-col relative   rounded-[16px] w-[20%]  my-2">
        <div className="absolute px-[16px] py-[10px] flex justify-between w-full">
          <p
            className="py-[4px] text-[12px] px-[8px] rounded-[16px]"
            style={{ backgroundColor: 'rgba(20, 90, 255, 0.1)' }}
          >
            <Link
              to={
                people?.projectMemberRole?.systemGenerated
                  ? ''
                  : `/work-space/roles/${match?.params.id}/permissions/${people?.projectMemberRole?.id}`
              }
            >
              {people?.projectMemberRole?.name}
            </Link>
          </p>
          <div>
            <PeopleCardMenu
              onSendAMessageClicked={() => {
                history.push(`/work-space/chat/${match?.params?.id}?key=${people.user.id}`);
              }}
              onRemoveClicked={() => onRemove(people)}
              onChangeRoleClicked={() => onRoleChangeClicked(people)}
              isDeactive={people.userStatus === USERSTATUSINPROJECT.DEACTIVATE}
              isUserAdmin={people?.projectMemberRole?.systemGenerated}
              isOnline={connectedUsers.includes(people.user.id)}
            />
          </div>
        </div>
        <div className="text-center py-[42px]">
          <div>
            <BadgeAvatars
              src={src}
              sx={{ width: 86, height: 86, backgroundColor: people?.user?.color }}
              isActive={connectedUsers.includes(people.user.id)}
            >
              {getFirstLetters(people?.user?.userProfile?.fullName)}
            </BadgeAvatars>{' '}
          </div>
          <div className="mt-1">
            <p className="font-[500] text-[16px]">{people?.user?.userProfile?.fullName}</p>
            <p className="font-[400] text-[12px]">{people?.user?.email}</p>
            {/* <div className="flex justify-center flex-row w-full ">
      
            <p
              className="font-bold w-[80px] text-[10px] cursor-pointer hover:text-gray-500  text-gray-400 mt-2"
              onClick={() => onRoleChangeClicked(people)}
            >
              Change Role{' '}
            </p>
          </div> */}

            {/* <p className="text-gray-400">Art Director</p> */}
          </div>

          {/* <div className="mt-5 mx-auto flex justify-between">
          <button
            className="bg-gray-400 text-white w-[120px] px-4 py-1 mr-2 rounded-md hover:bg-gray-5n00"
            onClick={() => {
              console.log(people, 'people');
              history.push(`/work-space/chat/${match?.params?.id}?key=${people.user.id}`);
            }}
          >
            Chat
          </button>
          <button
            className={`text-white px-5 w-[120px]  text-center py-1 rounded-md  flex text-md ${
              people.userStatus !== USERSTATUSINPROJECT.DEACTIVATE
                ? 'bg-red-300 hover:bg-red-400'
                : 'bg-green-400 hover:bg-green-700'
            }`}
            onClick={() => onRemove(people)}
          >
           
            <span style={{ lineHeight: 2 }} className=" w-full">
              {people.userStatus !== USERSTATUSINPROJECT.DEACTIVATE ? 'Remove' : 'Add again'}
            </span>
          </button>
          </div> */}
        </div>
      </div>
    </>
  );
};

export default PeopleCard;
