import React from 'react';
import PeopleCard from './Components/PeopleCard';
import MaxWidthDialog from 'components/Modal';
import {
  useGetProjectMembersListsQuery,
  useAddProjectsMembersMutation,
  useToogleProjectMemberStatusMutation,
  ProjectMemberInput,
  useUpdateProjectMembersDetailsMutation,
} from 'redux/reducers/projectMembers';
import { toast } from 'react-toastify';
import useToggle from 'hooks/useToggle';
import { useRouteMatch } from 'react-router-dom';
import CrossIcon from 'assets/icons-svg/CrosIcon';
import { ReactSelect } from 'components/ReactSelect';
import AddButton from 'components/Buttons/AddButton';
import { useGetUsersListsQuery, useSearchUsersByEmailOrNameMutation } from 'redux/reducers/user';
import { ProjectOutputInterface } from 'redux/reducers/projects';
import { useGetUserRolesInProjectsQuery } from 'redux/reducers/user-roles-screens';
import { PROJECTMEMBERROLE, SCREENS_ROUTES_ENUMS, USERSTATUSINPROJECT } from 'common/enum';

import { useSelector } from 'react-redux';
import { RootState } from 'redux/store';
import Loader from 'components/Loader/loader';

import AsyncSelect from 'react-select/async';

export interface IOptions {
  value: string;
  label: string;
}
const Members = () => {
  const [selectedUser, setSelectedUser] = React.useState<IOptions | undefined>(undefined);
  const [selectedRole, setSelectedRole] = React.useState<IOptions | undefined>(undefined);
  const connectedUsers = useSelector((state: RootState) => state.socket.connectUserId);
  const [searchUsersByEmailOrName] = useSearchUsersByEmailOrNameMutation();

  const [confirmationBox, setConfirmationBox] = useToggle(false);
  const [rolesModal, setRoleModal] = useToggle(false);

  const [selectedMember, setSelectedMember] = React.useState<ProjectOutputInterface>();

  let match = useRouteMatch<{ id: string }>(SCREENS_ROUTES_ENUMS.MEMBERS);

  const { data: rolesLists, isLoading: fetchingRoles } = useGetUserRolesInProjectsQuery(match?.params.id || '', {
    skip: !match?.params.id,
  });

  const { data: projectMembers, isLoading: fetchingProjectMembers } = useGetProjectMembersListsQuery(
    match?.params?.id || ''
  );

  const [addProjectsMembers, { error, isLoading }] = useAddProjectsMembersMutation();
  const [updateProjectMembersDetails] = useUpdateProjectMembersDetailsMutation();

  const [toogleProjectMemberStatus] = useToogleProjectMemberStatusMutation();

  const roleOptions = rolesLists?.data?.map((item) => {
    return {
      id: item?.id,
      value: item?.id,
      label: item?.name,
    };
  });

  const onAddMemberClicked = () => {
    if (selectedUser && match?.params?.id && selectedRole) {
      const postData: ProjectMemberInput = {
        userId: +selectedUser.value,
        projectId: +match?.params?.id,
        role: PROJECTMEMBERROLE.MEMBER,
        userStatus: USERSTATUSINPROJECT.ACTIVE,
        userRoleIdInProject: +selectedRole.value,
      };
      addProjectsMembers(postData)
        .unwrap()
        .then((payload) => {
          toast('User Added Successful');
          setRoleModal();
          setSelectedUser(undefined);
        })
        .catch((error) => console.error('fulfilled', error));
    }
  };

  const onUpdateProjectMemberRole = () => {
    if (selectedMember && match?.params?.id && selectedRole) {
      const postData: Partial<ProjectMemberInput> = {
        id: selectedMember?.id,
        userRoleIdInProject: +selectedRole.value,
        userStatus: selectedMember?.userStatus,
      };
      updateProjectMembersDetails(postData)
        .unwrap()
        .then((payload) => {
          setRoleModal();
          setSelectedMember(undefined);
          toast('Role Update Successful');
        })
        .catch((error) => console.error('fulfilled', error));
    }
  };

  const onMemberTotRemoveOrAddAgainConfirmedClicked = () => {
    if (selectedMember) {
      const postData = {
        id: selectedMember.id,
        userStatus:
          selectedMember.userStatus !== USERSTATUSINPROJECT.DEACTIVATE
            ? USERSTATUSINPROJECT.DEACTIVATE
            : USERSTATUSINPROJECT.ACTIVE,
      };
      toogleProjectMemberStatus(postData)
        .unwrap()
        .then((response) => setConfirmationBox())
        .catch((error) => console.log(error));
    }
  };

  const onMemberToRemoveOrAddAgain = (people) => {
    setSelectedMember(people);
    setConfirmationBox();
  };

  const onRoleChangeClicked = (people) => {
    setSelectedMember(people);
    setRoleModal();
  };

  if (fetchingProjectMembers || fetchingRoles) {
    return <Loader />;
  }

  // console.log(searchedUserLists, options, 'searchUserLists');

  const loadUserListOptions = (inputValue) => {
    return new Promise((resolve, reject) => {
      // using setTimeout to emulate a call to server
      setTimeout(() => {
        inputValue &&
          searchUsersByEmailOrName(inputValue)
            .unwrap()
            .then((searchedUserLists) => {
              const userLists = [...(searchedUserLists?.data ?? [])]
                ?.map((item) => {
                  return {
                    label: item?.userProfile?.fullName,
                    value: item.id,
                  };
                })
                .filter((item) => !projectMembers?.data?.some((allLists) => +allLists.user.id === +item.value));
              resolve(userLists);
            });
      }, 2000);
    });
  };

  return (
    <div className="w-full">
      <MaxWidthDialog
        isModalOpen={rolesModal}
        toggleModal={setRoleModal}
        renderTitle={() => {
          return (
            <div className="flex justify-between">
              <p className="text-[18px] font-[600]">Add role to user</p>
              <div
                className="bg-[#E5E5E5] cursor-pointer w-[24px] h-[24px] flex justify-between group rounded-[4px]"
                onClick={() => setRoleModal()}
              >
                <CrossIcon />
              </div>
            </div>
          );
        }}
        submitLabel={'Save'}
        buttonType={'submit'}
        modalSize={'sm'}
      >
        <div className="px-[25px]">
          <div>
            <h2 className="text-[18px] font-[600] mb-[10px]">
              Select Role for {selectedUser?.label ?? selectedMember?.user?.userProfile?.fullName}
            </h2>
            <div className="w-10/12">
              <ReactSelect
                options={roleOptions}
                placeholder={'Search RoleName by name'}
                value={selectedRole}
                onChange={(value) => setSelectedRole(value)}
              />
            </div>
          </div>

          <div className=" mt-[30px] flex flex-row pb-[21px]">
            <button
              type="button"
              className="cursor-pointer hover:text-red-700 py-1 rounded-lg text-gray-700"
              onClick={() => {
                setRoleModal();
                setSelectedMember(undefined);
              }}
            >
              Cancel
            </button>
            <button
              type="button"
              className="cursor-pointer ml-[24px] bg-red-400 px-[35px] py-[12px] text-white rounded-[4px]"
              onClick={selectedUser ? onAddMemberClicked : onUpdateProjectMemberRole}
            >
              Confirm
            </button>
          </div>
        </div>
      </MaxWidthDialog>
      <MaxWidthDialog
        isModalOpen={confirmationBox}
        toggleModal={setConfirmationBox}
        renderTitle={() => {
          return (
            <div className="flex justify-between">
              <p className="text-[18px] font-[600]">Confirmation</p>
              <div
                className="bg-[#E5E5E5] cursor-pointer w-[24px] h-[24px] flex justify-between group rounded-[4px]"
                onClick={() => setConfirmationBox(false)}
              >
                <CrossIcon />
              </div>
            </div>
          );
        }}
        submitLabel={'Save'}
        buttonType={'submit'}
        modalSize={'xs'}
      >
        <div className="px-[28px]">
          <div>
            <h2 className="text-[16px] font-[400]">
              Are you sure want to{' '}
              {selectedMember?.userStatus !== USERSTATUSINPROJECT.DEACTIVATE
                ? `delete ${selectedMember?.user?.userProfile?.fullName} user from project?`
                : `add ${selectedMember?.user?.userProfile?.fullName} user in the project?`}{' '}
            </h2>
          </div>

          <div className=" mt-5 flex flex-row pb-[21px]">
            <button
              type="button"
              className="cursor-pointer hover:text-red-700 py-1 rounded-lg text-gray-700"
              onClick={() => {
                setConfirmationBox();
                setSelectedMember(undefined);
              }}
            >
              Cancel
            </button>
            <button
              type="button"
              className="cursor-pointer ml-[24px] bg-red-400 px-[35px] py-[12px] text-white rounded-[4px]"
              onClick={onMemberTotRemoveOrAddAgainConfirmedClicked}
            >
              Confirm
            </button>
          </div>
        </div>
      </MaxWidthDialog>
      <div className="flex justify-between w-full mt-[45px]">
        <div className="flex">
          <h2 className="text-xl text-[24px] font-[600]">Members</h2>
          <div className="font-medium text-[24px] ml-[9px] text-[#F85365] h-[32px] p-[1px] w-[32px] rounded-[50%] flex items-center justify-between   bg-[#ffff]  ">
            <a href="#" className="flex mx-auto  ">
              {projectMembers?.data?.length}
            </a>
          </div>
        </div>
        <div className="flex  w-1/2 justify-end">
          <div className="w-8/12  mr-[25px] my-auto">
            <AsyncSelect
              defaultOptions
              cacheOptions
              loadOptions={loadUserListOptions}
              placeholder="Search by user email"
              value={selectedUser}
              onChange={(value) => value && setSelectedUser(value)}
            />
          </div>
          <AddButton name="Add Member" onClick={() => selectedUser && setRoleModal()} />
        </div>
      </div>
      <div className="mt-[59px]">
        <div className="flex gap-4 flex-wrap">
          {projectMembers?.data?.map((item, index) => (
            <PeopleCard
              connectedUsers={connectedUsers}
              key={index}
              people={item}
              onRemove={onMemberToRemoveOrAddAgain}
              onRoleChangeClicked={onRoleChangeClicked}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Members;
