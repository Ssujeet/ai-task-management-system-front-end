import { Checkbox } from '@material-ui/core';
import { SCREENS_ROUTES_ENUMS } from 'common/enum';
import AddButton from 'components/Buttons/AddButton';
import DataTable from 'components/DataTable';
import { HeadCell } from 'components/DataTable/EnhancedTableHead';
import FallBackLoader from 'components/FallBackLoader';
import { ReactSelect } from 'components/ReactSelect';
import Spinner from 'components/Spinner/spinner';
import { FieldArray, Form, Formik, FormikProps } from 'formik';
import React from 'react';
import { useRouteMatch } from 'react-router-dom';
import { toast } from 'react-toastify';
import {
  useGetScreensQuery,
  useGetUserRolesScreenByRoleIdQuery,
  useCreateUserRolesScreensMutation,
} from 'redux/reducers/user-roles-screens';

export interface IOptions {
  value: string;
  label: string;
}
const Permissions = () => {
  let match = useRouteMatch<{ id: string; rolesId: string }>(SCREENS_ROUTES_ENUMS.PERMISSIONS);
  const [initialFormData, setInitialFormData] = React.useState<Array<any>>([]);

  const [selectedScreens, setSelectedScreens] = React.useState<IOptions>();

  const { data } = useGetScreensQuery();
  const { data: userRolesScreen } = useGetUserRolesScreenByRoleIdQuery(match?.params.rolesId || '', {
    skip: !match?.params.rolesId,
  });
  const [createUserRolesScreens, { isLoading: isCreatingRolesScreen }] = useCreateUserRolesScreensMutation();

  React.useEffect(() => {
    if (userRolesScreen?.data) {
      const formikData = userRolesScreen?.data?.map((item) => {
        return {
          ...item,
          screeName: item.screen.name,
          screenId: item.screen.id,
        };
      });

      setInitialFormData(formikData);
    }
  }, [userRolesScreen]);

  const headCells = (formikProps: FormikProps<any>) => {
    return [
      {
        id: 'SN.',
        numeric: false,
        disablePadding: true,
        label: 'Sn.',
        renderCell: (data, index) => {
          return <>{index + 1}</>;
        },
      },
      {
        id: 'screeName',
        numeric: false,
        disablePadding: true,
        label: 'Name',
        renderCell: (data, index) => {
          return <>{data.screeName.charAt(0).toUpperCase() + data.screeName.slice(1)} </>;
        },
      },
      {
        id: 'All',
        numeric: false,
        disablePadding: false,
        label: 'All',
        renderCell: (data, index) => {
          return (
            <>
              <Checkbox
                name={`accessWithScreens.${index}.canRead`}
                checked={
                  formikProps?.values?.accessWithScreens[index]?.canRead &&
                  formikProps?.values?.accessWithScreens[index]?.canUpdate &&
                  formikProps?.values?.accessWithScreens[index]?.canDelete &&
                  formikProps?.values?.accessWithScreens[index]?.canCreate
                }
                onChange={() => {
                  let isActive = true;
                  if (
                    formikProps?.values?.accessWithScreens[index]?.canRead &&
                    formikProps?.values?.accessWithScreens[index]?.canUpdate &&
                    formikProps?.values?.accessWithScreens[index]?.canDelete &&
                    formikProps?.values?.accessWithScreens[index]?.canCreate
                  ) {
                    isActive = false;
                  }
                  formikProps.setFieldValue(`accessWithScreens.${index}.canRead`, isActive);
                  formikProps.setFieldValue(`accessWithScreens.${index}.canUpdate`, isActive);
                  formikProps.setFieldValue(`accessWithScreens.${index}.canDelete`, isActive);
                  formikProps.setFieldValue(`accessWithScreens.${index}.canCreate`, isActive);
                }}
              />
            </>
          );
        },
      },
      {
        id: 'Read',
        numeric: false,
        disablePadding: false,
        label: 'Read',
        renderCell: (data, index) => {
          return (
            <>
              <Checkbox
                name={`accessWithScreens.${index}.canRead`}
                checked={!!formikProps?.values?.accessWithScreens[index]?.canRead}
                onChange={formikProps.handleChange}
              />
            </>
          );
        },
      },
      {
        id: 'Create',
        numeric: false,
        disablePadding: false,
        label: 'Create',
        renderCell: (data, index) => {
          return (
            <>
              <Checkbox
                name={`accessWithScreens.${index}.canCreate`}
                checked={!!formikProps?.values?.accessWithScreens[index]?.canCreate}
                onChange={formikProps.handleChange}
              />
            </>
          );
        },
      },
      {
        id: 'Update',
        numeric: false,
        disablePadding: false,
        label: 'Update',
        renderCell: (data, index) => {
          return (
            <>
              <Checkbox
                name={`accessWithScreens.${index}.canUpdate`}
                checked={!!formikProps?.values?.accessWithScreens[index]?.canUpdate}
                onChange={formikProps.handleChange}
              />
            </>
          );
        },
      },
      {
        id: 'Delete',
        numeric: false,
        disablePadding: false,
        label: 'Delete',
        renderCell: (data, index) => {
          return (
            <>
              <Checkbox
                name={`accessWithScreens.${index}.canDelete`}
                checked={!!formikProps?.values?.accessWithScreens[index]?.canDelete}
                onChange={formikProps.handleChange}
              />
            </>
          );
        },
      },
    ];
  };

  const onSubmit = (values) => {
    if (match?.params?.rolesId) {
      const postData = {
        userRoleId: +match?.params?.rolesId,
        ...values,
      };

      createUserRolesScreens(postData)
        .unwrap()
        .then((res) => toast('Save Successful'));
    }
  };

  const options = data
    ?.map((item) => {
      return {
        label: item.name.charAt(0).toUpperCase() + item.name.slice(1),
        value: item.id,
      };
    })
    ?.filter(
      (item) =>
        !initialFormData?.some((allLists) => +allLists?.screenId === +item?.value) && !(item.label === 'Stand-up')
    );

  const onScreenAdditions = (formik: FormikProps<any>) => {
    if (selectedScreens) {
      const screenData = formik.dirty ? [...formik.values.accessWithScreens] : [...initialFormData];
      screenData.push({
        screeName: selectedScreens?.label,
        screenId: selectedScreens?.value ? +selectedScreens?.value : 0,
        canRead: true,
        canCreate: false,
        canUpdate: false,
        canDelete: false,
      });

      setInitialFormData(screenData);
      setSelectedScreens(undefined);
    }
  };

  if (initialFormData) {
    return (
      <Formik
        enableReinitialize={true}
        initialValues={{ accessWithScreens: [...initialFormData] }}
        onSubmit={(values) => {
          onSubmit(values);
        }}
        render={(formik) => (
          <div>
            <div className="flex justify-between w-full mt-[51px]">
              <h2 className="text-[24px] font-[400]">Roles Permissions</h2>
              <div className="flex w-1/2  justify-end">
                <div className="w-1/2">
                  <ReactSelect
                    options={options}
                    placeholder={'Search by Name'}
                    value={selectedScreens}
                    onChange={(value) => setSelectedScreens(value)}
                  />
                </div>
                {/* {selectedScreens && (
                  <button
                    className="ml-2 bg-red-500 rounded-md px-4 py-2 text-white text-md"
                    onClick={() => setSelectedScreens(undefined)}
                  >
                    Reset
                  </button>
                )} */}

                <AddButton
                  name="Add Screen"
                  className="ml-[18px] px-4 py-2 text-white text-md"
                  onClick={() => onScreenAdditions(formik)}
                />
              </div>
            </div>

            <div className="mt-5">
              <Form>
                <DataTable
                  headCells={headCells(formik)}
                  rows={initialFormData || []}
                  tableTitle={'All Screens with Permissions'}
                />

                <button
                  disabled={!formik.values.accessWithScreens.length || !formik.dirty || isCreatingRolesScreen}
                  type="submit"
                  className=" border-[#F95868] mt-[20px] hover:bg-[#F95868] hover:text-white border px-[24px] py-[10px] text-[16px] flex ml-auto rounded-[4px]  text-[#F95868]"
                >
                  {isCreatingRolesScreen ? <Spinner /> : 'Save'}
                </button>
              </Form>
            </div>
          </div>
        )}
      />
    );
  }

  return <FallBackLoader />;
};

export default Permissions;
