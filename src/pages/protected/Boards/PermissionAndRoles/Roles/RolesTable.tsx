import { Tooltip } from '@material-ui/core';
import EditIcon from 'assets/icons-svg/EditIcon';
import RolesIcon from 'assets/icons-svg/RolesIcon';
import { SCREENS_ROUTES_ENUMS } from 'common/enum';
import DataTable from 'components/DataTable';
import { HeadCell } from 'components/DataTable/EnhancedTableHead';
import React from 'react';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { useGetUserRolesInProjectsQuery } from 'redux/reducers/user-roles-screens';

const RolesTable = ({ setInitialForm }) => {
  let match = useRouteMatch<{ id: string }>(SCREENS_ROUTES_ENUMS.ROLES);

  const { data } = useGetUserRolesInProjectsQuery(match?.params.id || '', { skip: !match?.params.id });
  const history = useHistory();

  const headCells: HeadCell[] = [
    {
      id: 'SN.',
      numeric: false,
      disablePadding: true,
      label: 'Sn.',
      renderCell: (data, index) => {
        return <>{index + 1}</>;
      },
    },
    {
      id: 'name',
      numeric: false,
      disablePadding: true,
      label: 'Name',
      renderCell: (data, index) => {
        return <>{data.name}</>;
      },
    },
    {
      id: 'Created By',
      numeric: false,
      disablePadding: true,
      label: 'Created By',
      renderCell: (data, index) => {
        return <>{data?.createdBy?.userProfile?.fullName ?? '-'}</>;
      },
    },
    {
      id: 'CreatedTime',
      numeric: false,
      disablePadding: true,
      label: 'Created Time',
      renderCell: (data, index) => {
        return <>{data.createdAt.split('T')[0]}</>;
      },
    },

    {
      id: 'updatedAt',
      numeric: false,
      disablePadding: true,
      label: 'Updated Time',
      renderCell: (data, index) => {
        return <>{data.updatedAt.split('T')[0]}</>;
      },
    },

    // {
    //   id: 'status',
    //   numeric: false,
    //   disablePadding: false,
    //   label: 'Active',
    //   renderCell: (data, index) => {
    //     return <>{data.status ? 'true' : 'false'}</>;
    //   },
    // },
    {
      id: 'Actions',
      numeric: false,
      disablePadding: false,
      label: 'Actions',
      renderCell: (data, index) => {
        return (
          <>
            <div className="flex">
              <Tooltip title="Edit Role">
                <button onClick={() => setInitialForm(data)}>
                  <EditIcon />
                </button>
              </Tooltip>

              <Tooltip title="Edit Permission">
                <button
                  className="ml-[10px]"
                  onClick={() => history.push(`/work-space/roles/${match?.params.id}/permissions/${data.id}`)}
                >
                  <RolesIcon color="#F95868" />
                </button>
              </Tooltip>
            </div>
          </>
        );
      },
    },
  ];

  return (
    <div>
      <DataTable
        headCells={headCells}
        rows={data?.data || []}
        tableTitle={'All Roles'}
        pagination={true}
        onCheckBoxClicked={() => true}
        // onDeleteIconClicked={toggleFilesDeletingModal}
      />
    </div>
  );
};

export default RolesTable;
