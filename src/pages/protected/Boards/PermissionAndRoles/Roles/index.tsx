import { useFormik } from 'formik';
import React from 'react';
import { RouteComponentProps, useRouteMatch } from 'react-router-dom';
import PrivateRoutes from 'routes';
import { IRoutes } from 'routes/routesLists';
import RolesTable from './RolesTable';

import { string as YupString, object as YupObject } from 'yup';
import {
  useAddUserRolesInProjectsMutation,
  useGetUserRolesInProjectsQuery,
  useUpdateUserRolesInProjectsMutation,
} from 'redux/reducers/user-roles-screens';
import { toast } from 'react-toastify';
import useScreenPermissionsForCurrentUsers from 'hooks/useScreenPermissionsForCurrentUsers';
import { SCREENS, SCREENS_ROUTES_ENUMS } from 'common/enum';
import Loader from 'components/Loader/loader';
import AddButton from 'components/Buttons/AddButton';
import PlusIcon from 'assets/icons-svg/PlusIcon';

export const rolesSchema = YupObject().shape({
  name: YupString().required('Role name is required'),
});

interface IRoles extends RouteComponentProps {
  childrenRoutes: IRoutes[];
  computedMatch: any;
}

const rolesRoute = '/work-space/roles/:id';

const Roles = (props: IRoles) => {
  let match = useRouteMatch<{ id: string }>(SCREENS_ROUTES_ENUMS.ROLES);

  const { childrenRoutes } = props;
  const [userScreenRoles] = useScreenPermissionsForCurrentUsers(SCREENS_ROUTES_ENUMS.ROLES, SCREENS.ROLES);

  const { data, isLoading: fetchingUserRoles } = useGetUserRolesInProjectsQuery(match?.params.id || '', {
    skip: !match?.params.id,
  });

  const initialFormData = { id: '', name: '' };
  const [initialForm, setInitialForm] = React.useState(initialFormData);
  const [addUserRolesInProjects] = useAddUserRolesInProjectsMutation();
  const [updateUserRolesInProjects] = useUpdateUserRolesInProjectsMutation();

  console.log(userScreenRoles, 'userScreenRoles');

  const formik = useFormik({
    initialValues: initialForm,
    enableReinitialize: true,
    onSubmit: (values, { resetForm }) => {
      if (match?.params.id) {
        const postData = {
          projectId: +match?.params.id,
          name: values.name,
        };
        if (data?.data?.find((item) => item?.name?.toLowerCase() === values.name.toLowerCase())) {
          if (initialForm.id) {
            resetForm();
            setInitialForm(initialFormData);

            return;
          }
          toast('Name Already Taken');

          return;
        }
        if (initialForm.id) {
          postData['id'] = +initialForm.id;
          updateUserRolesInProjects(postData)
            .unwrap()
            .then((res) => {
              toast('Update Successful');
              resetForm();
              setInitialForm(initialFormData);
            });

          return;
        }
        addUserRolesInProjects(postData)
          .unwrap()
          .then((res) => {
            toast('Add Successful');
            resetForm();
            setInitialForm(initialFormData);
          });
      }
    },
    validationSchema: rolesSchema,
  });

  if (fetchingUserRoles) {
    return <Loader />;
  }

  if (props.computedMatch.isExact) {
    return (
      <div>
        {/* <div className="  flex justify-between">
          <h1 className="">User Roles</h1>
        </div> */}

        <div className="mt-[45px]">
          <form onSubmit={formik.handleSubmit}>
            <div className="flex w-full items-center">
              <div className="w-1/3">
                <input
                  type="text"
                  name="name"
                  className="border focus:outline-none rounded-[8px] px-[24px]  w-full  py-[12px] mt-[7px]"
                  placeholder="Enter Role Name"
                  onBlur={formik.handleBlur}
                  value={formik.values.name}
                  onChange={formik.handleChange}
                />

                <div className="h-[10px]">
                  {Boolean(formik.touched.name && formik.errors.name) && (
                    <span className="text-sm text-red-500">{formik.touched.name && formik.errors.name}</span>
                  )}
                </div>
              </div>
              {formik.values.name && (
                <button
                  type="button"
                  className="cursor-pointer ml-[24px] bg-gray-400  px-[24px] py-[10px]  my-auto  rounded-[4px] px-10 py-0 block  text-white"
                  onClick={() => {
                    formik.resetForm();
                    setInitialForm(initialFormData);
                  }}
                >
                  Reset
                </button>
              )}

              <div className="group ml-[10px]">
                <button
                  type="submit"
                  className="border-[#F95868] group-hover:bg-[#F95868] border my-auto   px-[24px] py-[10px] text-[16px] flex justify-between rounded-[4px] text-white"
                >
                  <div className="flex my-auto">
                    <PlusIcon />
                  </div>
                  <span className="ml-4 group-hover:text-white text-[16px] text-[#F95868]">
                    {' '}
                    {formik.values.id ? 'Edit Role' : 'Add New Role'}
                  </span>
                </button>
              </div>
            </div>
          </form>
        </div>
        <div className=" mt-[40px]">
          <RolesTable setInitialForm={setInitialForm} />
        </div>
      </div>
    );
  }

  return <PrivateRoutes routes={childrenRoutes} />;
};

export default Roles;
