import React from 'react';
import { InputLabel } from '@material-ui/core';
import { FormikProps, useFormik } from 'formik';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import FormikValidationError from 'components/FormikErrors';
import { Grid, Typography, DialogContent, DialogActions, Button } from '@material-ui/core';
import Spinner from 'components/Spinner/spinner';
export interface IFolderAddForm {
  folderName: string;
}

interface IFolderCreationsForm extends FormikProps<IFolderAddForm> {}
export function FolderCreationsForm(props: IFolderCreationsForm) {
  const onInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    props.setFieldValue(event.target.name, event.target.value);
  };

  return (
    <>
      <FormControl variant="filled" style={{ width: '100%' }}>
        <div className="flex flex-col w-full">
          <label htmlFor="component-filled" className="text-[rgba(52, 52, 52, 0.6)]">
            Folder Name *
          </label>
          <input
            id="component-filled"
            className="border rounded-[8px] px-[24px] py-[12px] mt-[7px]"
            value={props.values.folderName}
            onChange={onInputChange}
            aria-describedby="component-error-text"
            name="folderName"
            type="text"
          />
          <FormikValidationError name="folderName" errors={props.errors} touched={props.touched} />
        </div>
      </FormControl>
    </>
  );
}

interface IFolderCreationsFormWithFormik {
  onSubmit: (value) => void;
  toggleModal: () => void;
  initialValues: IFolderAddForm;
  isLoading: boolean;
}

export const FolderCreationsFormWithFormik = (props: IFolderCreationsFormWithFormik) => {
  const { onSubmit, toggleModal, initialValues, isLoading } = props;

  const formik = useFormik({
    enableReinitialize: true,
    initialValues,
    onSubmit: (values) => {
      onSubmit(values);
    },
  });

  return (
    <>
      <form onSubmit={formik.handleSubmit}>
        <DialogContent>
          <FolderCreationsForm {...formik} />
        </DialogContent>
        <DialogActions>
          <div className=" mt-5 flex flex-row pb-[21px]">
            <button
              type="button"
              className="cursor-pointer hover:text-red-700 py-1 rounded-lg text-gray-700"
              onClick={toggleModal}
              disabled={isLoading}
            >
              Cancel
            </button>
            <button
              type="submit"
              className="cursor-pointer ml-[24px] bg-[#F85365] px-[29px] py-[12px] rounded-[4px] text-white "
              disabled={isLoading}
            >
              {isLoading ? <Spinner /> : 'Confirm'}
            </button>
          </div>
        </DialogActions>
      </form>
    </>
  );
};
