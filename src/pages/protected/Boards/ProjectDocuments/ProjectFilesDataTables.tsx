import * as React from 'react';
import DataTable from 'components/DataTable';
import { HeadCell } from 'components/DataTable/EnhancedTableHead';
import { useDeleteFilesMutation } from 'redux/reducers/folders-and-files';
import { DeleteModal } from 'components/DeleteModal';
import useToggle from 'hooks/useToggle';
import { toast } from 'react-toastify';
import { FcDownload } from 'react-icons/fc';
import { Tooltip } from '@material-ui/core';
import EyeOutlined from 'assets/icons-svg/EyeOutLined';
import { addFilesInViewer } from 'redux/reducers/file-viewer';
import { useDispatch } from 'react-redux';

interface IProjectFiles {
  filesLists: any;
  renderHeader: () => React.ReactNode;
}
export default function ProjectFilesTable(props: IProjectFiles) {
  const { filesLists, renderHeader } = props;
  const [deleteFiles] = useDeleteFilesMutation();
  const dispatch = useDispatch();
  const [selectedData, setSelectedData] = React.useState<{ id: string }[] | null>(null);
  const [filesDeletingModal, toggleFilesDeletingModal] = useToggle(false);

  const onDeleteDiscardClicked = () => {
    toggleFilesDeletingModal();
  };

  const onConfirmDeleteClicked = () => {
    if (selectedData?.length) {
      const postData = {
        id: selectedData?.map((item) => +item.id),
      };
      deleteFiles(postData)
        .unwrap()
        .then((res) => {
          onDeleteDiscardClicked();
          toast('Files Deleted Successfully');
        })
        .catch((err) => console.log(err));
    }
  };

  const headCells: HeadCell[] = [
    {
      id: 'SN.',
      numeric: false,
      disablePadding: true,
      label: 'Sn.',

      renderCell: (data, index) => {
        return <>{index + 1}</>;
      },
    },
    {
      id: 'fileName',
      numeric: false,
      disablePadding: true,
      label: 'File Name',
      renderCell: (data, index) => {
        return <>{data.fileName}</>;
      },
    },
    {
      id: 'mimeType',
      numeric: false,
      disablePadding: false,
      label: 'Mime type',
    },
    {
      id: 'size',
      numeric: false,
      disablePadding: false,
      label: 'Size in Kb',
    },
    {
      id: 'uploadedBy.name',
      numeric: false,
      disablePadding: false,
      label: 'Uploaded By',
      renderCell: (data, index) => {
        return <>{data?.uploadedBy?.userProfile?.fullName}</>;
      },
    },
    {
      id: 'action',
      numeric: false,
      disablePadding: false,
      label: 'Action',
      renderCell: (data, index) => {
        return (
          <div className="flex">
            <Tooltip title="Download">
              <a href={data.url.split('?')[0]} download>
                <FcDownload />
              </a>
            </Tooltip>

            <Tooltip title="View">
              <button
                className="ml-[10px]"
                onClick={() => dispatch(addFilesInViewer({ activeIndex: index, fileLists: filesLists }))}
              >
                <EyeOutlined />
              </button>
            </Tooltip>
          </div>
        );
      },
    },
  ];

  return (
    <>
      <DeleteModal
        isModalOpen={filesDeletingModal}
        toggleModal={toggleFilesDeletingModal}
        renderTitle={() => {
          return <p className="font-bold text-xl">Delete Confirmation</p>;
        }}
        submitLabel={'Save'}
        modalSize={'sm'}
        buttonType={'submit'}
        deleteMessage={`Are you sure you want to delete selected ${selectedData?.length} files?`}
        onConfirmClicked={onConfirmDeleteClicked}
        onDiscardClicked={onDeleteDiscardClicked}
      >
        <div></div>
      </DeleteModal>

      <DataTable
        renderEnhancedToolBar={renderHeader}
        headCells={headCells}
        rows={filesLists || []}
        tableTitle={'All Your Files'}
        pagination={true}
        onCheckBoxClicked={setSelectedData}
        onDeleteIconClicked={toggleFilesDeletingModal}
      />
    </>
  );
}
