import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import FolderRounded from '@material-ui/icons/FolderRounded';
import { Menu, MenuItem, Typography } from '@material-ui/core';

import { IFolderInput, useDeleteFolderByIdMutation } from 'redux/reducers/folders-and-files';
import classNames from 'classnames';

import { DeleteModal } from 'components/DeleteModal';
import useToggle from 'hooks/useToggle';
import { toast } from 'react-toastify';
import AddButton from 'components/Buttons/AddButton';
import CrossIcon from 'assets/icons-svg/CrosIcon';
import FoldersIcon from 'assets/icons-svg/FoldersIcons';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      borderWidth: 1,
      padding: 10,
      borderRadius: 4,
      cursor: 'pointer',
      marginTop: 10,
    },
    folder: {
      marginLeft: 12,
    },
    listRoot: {
      flexGrow: 1,
    },
    gridActive: {
      backgroundColor: '#e8f0fe',
    },
  })
);

interface IProjectFolderCards {
  item: IFolderInput;
  activeFolder;
  setActiveFolder;
  onFolderClicked: (folder: IFolderInput) => void;
  onUpdateMenuClicked: (folder: IFolderInput) => void;
}

function ProjectFolderCards(props: IProjectFolderCards) {
  const { activeFolder, setActiveFolder, onFolderClicked, item } = props;
  const [folderDeletingModal, toggleFolderDeletingModal] = useToggle(false);
  const [deletingFolder, setDeletingFolder] = React.useState<IFolderInput | null>(null);
  const [contextMenu, setContextMenu] = React.useState<any>(null);
  const [deleteFolderById] = useDeleteFolderByIdMutation();
  const classes = useStyles();

  const handleContextMenu = (event) => {
    event.preventDefault();
    setContextMenu(
      contextMenu === null
        ? {
            mouseX: event.clientX - 2,
            mouseY: event.clientY - 4,
          }
        : null
    );
  };

  const handleClose = () => {
    setContextMenu(null);
  };

  const onFolderOpen = () => {
    setActiveFolder(props.item.folderName);
    onFolderClicked(item);
    handleClose();
  };

  const onConfirmDeleteClicked = () => {
    if (deletingFolder) {
      deleteFolderById(deletingFolder.id || '')
        .unwrap()
        .then(() => {
          toast('Folder Deleted Successfully');
          onDeleteDiscardClicked();
        })
        .catch((err) => {
          toast('You must first empty folder to delete folder');
        });
    }
  };

  const onDeleteDiscardClicked = () => {
    toggleFolderDeletingModal();
    setDeletingFolder(null);
  };

  return (
    <div>
      <DeleteModal
        isModalOpen={folderDeletingModal}
        toggleModal={toggleFolderDeletingModal}
        renderTitle={() => {
          return (
            <div className="flex justify-between">
              <p className="text-[18px] font-[600]">Delete Confirmation</p>
              <div
                className="bg-[#E5E5E5] cursor-pointer w-[24px] h-[24px] flex justify-between group rounded-[4px]"
                onClick={() => toggleFolderDeletingModal()}
              >
                <CrossIcon />
              </div>
            </div>
          );
        }}
        submitLabel={'Save'}
        modalSize={'sm'}
        buttonType={'submit'}
        deleteMessage={`Are you sure you want to delete folder:${deletingFolder?.folderName}?`}
        onConfirmClicked={onConfirmDeleteClicked}
        onDiscardClicked={onDeleteDiscardClicked}
      >
        <div></div>
      </DeleteModal>
      <div onContextMenu={handleContextMenu} style={{ cursor: 'context-menu' }}>
        <div
          className={classNames('w-[220px] flex border py-3 px-[16px]  rounded-lg  cursor-pointer hover:opacity-80', {
            'bg-red-100': false,
          })}
          onClick={onFolderOpen}
        >
          <FoldersIcon />
          <p className={'ml-2 text-center text-[16px]'}>{props.item.folderName}</p>
        </div>
        <Menu
          open={contextMenu !== null}
          onClose={handleClose}
          anchorReference="anchorPosition"
          anchorPosition={contextMenu !== null ? { top: contextMenu?.mouseY, left: contextMenu?.mouseX } : undefined}
        >
          <MenuItem onClick={onFolderOpen}>Open</MenuItem>
          <MenuItem
            onClick={() => {
              props.onUpdateMenuClicked(item);
              handleClose();
            }}
          >
            Update
          </MenuItem>

          <MenuItem
            onClick={() => {
              setDeletingFolder(item);
              toggleFolderDeletingModal();
              handleClose();
            }}
          >
            Delete
          </MenuItem>
          <MenuItem onClick={handleClose}>Close</MenuItem>
        </Menu>
      </div>
    </div>
  );
}

interface IProjectFoldersLists {
  folderLists: IFolderInput[];
  onFolderClicked: (folder: IFolderInput) => void;
  onFolderAddClicked: () => void;
  onUpdateMenuClicked: (folder: IFolderInput) => void;
}
const ProjectFoldersLists = (props: IProjectFoldersLists) => {
  const classes = useStyles();
  const [activeFolder, setActiveFolder] = React.useState('');

  return (
    <div className={'flex flex-wrap'}>
      {props.folderLists.length ? (
        props.folderLists.map((item, index) => {
          return (
            <div className="ml-2 mt-2">
              <ProjectFolderCards
                onFolderClicked={props.onFolderClicked}
                activeFolder={activeFolder}
                onUpdateMenuClicked={props.onUpdateMenuClicked}
                setActiveFolder={setActiveFolder}
                item={item}
                key={`${item}${index}`}
              />
            </div>
          );
        })
      ) : (
        <div className="mx-auto mt-[10px] ">
          <p className="text-[16px] font-[400] mb-3 text-center">You have no folders</p>
        </div>
      )}
    </div>
  );
};

export default ProjectFoldersLists;
