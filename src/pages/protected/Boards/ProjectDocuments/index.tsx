import React from 'react';
import useToggle from 'hooks/useToggle';
import AddIcon from '@material-ui/icons/Add';
import MaxWidthDialog from 'components/Modal';
import ProjectListsCard from './ProjectFolderCards';
import ProjectFilesDataTables from './ProjectFilesDataTables';
import { useRouteMatch, useHistory } from 'react-router-dom';

import { Typography, makeStyles, Theme, createStyles, IconButton } from '@material-ui/core';
import { MdOutlineArrowForwardIos } from 'react-icons/md';

import {
  useGetRootFoldersOfProjectByIdQuery,
  useGetFoldersListsByParentIdQuery,
  useAddFolderMutation,
  useGetFilesListsByParentIdQuery,
  useAddFilesMutation,
  useGetCurrentFoldersParentListsQuery,
  IFolderInput,
  useUpdateFolderMutation,
} from 'redux/reducers/folders-and-files';

import { FolderCreationsFormWithFormik, IFolderAddForm } from './FolderCreationsForm';
import { toast } from 'react-toastify';
import classNames from 'classnames';
import { SCREENS_ROUTES_ENUMS } from 'common/enum';
import CrossIcon from 'assets/icons-svg/CrosIcon';
import PlusIcon from 'assets/icons-svg/PlusIcon';
import Loader from 'components/Loader/loader';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
  })
);
const initialValues: IFolderAddForm = {
  folderName: '',
};

function ProjectsDocuments() {
  const fileRef = React.useRef<any>();

  const classes = useStyles();
  const [folderFormInitialValue, setFolderFormInitialValue] = React.useState<IFolderAddForm>(initialValues);
  const [folderAdditionFormModal, toggleFolderAdditionFormModal] = useToggle(false);
  const [updateFolderDetails, setUpdateFolderDetails] = React.useState<IFolderInput | null>(null);
  const history = useHistory();
  let match = useRouteMatch<{ id: string; projectFolderRootId: string }>(SCREENS_ROUTES_ENUMS.Documents);
  const { data } = useGetFoldersListsByParentIdQuery(match?.params?.projectFolderRootId || '');
  const { data: filesLists, isLoading: fetchingFiles } = useGetFilesListsByParentIdQuery(
    match?.params?.projectFolderRootId || ''
  );
  const { data: parentFolderLists, isLoading: fetchingFolders } = useGetCurrentFoldersParentListsQuery(
    match?.params?.projectFolderRootId || ''
  );

  const [addFolder, { isLoading: isCreatingFolder }] = useAddFolderMutation();
  const [addFiles] = useAddFilesMutation();
  const [updateFolder, { isLoading: isUpdatingFolder }] = useUpdateFolderMutation();

  const onFolderCreateSubmit = (values) => {
    if (match) {
      const postData = {
        folderName: values.folderName,
        parentFolderId: +match?.params?.projectFolderRootId,
        projectId: +match?.params?.id,
      };
      addFolder(postData)
        .unwrap()
        .then((res) => {
          toast('Folder Created Successfully');
          toggleFolderAdditionFormModal();
        })
        .catch((err) => console.log(err));
    }
  };

  const onFolderUpdate = (values) => {
    if (values.folderName !== updateFolderDetails?.folderName) {
      if (updateFolderDetails?.id) {
        const postData = {
          id: +updateFolderDetails?.id,
          folderName: values.folderName,
        };
        updateFolder(postData)
          .unwrap()
          .then((res) => {
            toast('Folder Updated Successfully');
            toggleFolderAdditionFormModal();
          })
          .catch((err) => console.log(err));
      } else {
        toggleFolderAdditionFormModal();
      }
    }
  };
  const onFolderClicked = (folder: any) => {
    history.push(`/work-space/documents/${match?.params?.id}/${folder.id}`);
  };

  const handleChange = (e) => {
    const [file] = e.target.files;
    if (match) {
      const id = toast.loading(e.target.files.length + ' file is uploading.');
      addFiles({ parentFolderId: +match?.params?.projectFolderRootId, files: e.target.files })
        .unwrap()
        .then(() =>
          toast.update(id, { render: 'File uploaded successfully', type: 'success', isLoading: false, autoClose: 800 })
        )
        .catch((err) => {
          toast.update(id, { render: 'Error while uploading file', type: 'error', isLoading: false, autoClose: 800 });
        });
    }
  };

  const onUpdateMenuClicked = (data: IFolderInput) => {
    setUpdateFolderDetails(data);
    setFolderFormInitialValue({ folderName: data.folderName });
    toggleFolderAdditionFormModal();
  };

  if (fetchingFiles || fetchingFolders) {
    return <Loader />;
  }

  return (
    <div className="mt-[41px]">
      {/* <ProjectDetailsHeader /> */}
      <MaxWidthDialog
        isModalOpen={folderAdditionFormModal}
        toggleModal={toggleFolderAdditionFormModal}
        renderTitle={() => {
          return (
            <div className="flex justify-between">
              <p className="text-[18px] font-[600]">{updateFolderDetails ? 'Change Folder Name' : 'Add New Folder'}</p>
              <div
                className="bg-[#E5E5E5] cursor-pointer w-[24px] h-[24px] flex justify-between group rounded-[4px]"
                onClick={() => toggleFolderAdditionFormModal()}
              >
                <CrossIcon />
              </div>
            </div>
          );
        }}
        submitLabel={'Save'}
        modalSize={'xs'}
        buttonType={'submit'}
      >
        <FolderCreationsFormWithFormik
          initialValues={folderFormInitialValue}
          toggleModal={toggleFolderAdditionFormModal}
          onSubmit={updateFolderDetails ? onFolderUpdate : onFolderCreateSubmit}
          isLoading={isCreatingFolder || isUpdatingFolder}
        />
      </MaxWidthDialog>
      <div className="ml-2">
        <div className="flex justify-between">
          <div className="flex rounded-md pb-2 mb-2">
            {parentFolderLists?.data?.map((item, index) => {
              return (
                <div className="flex" onClick={() => onFolderClicked(item)}>
                  <p
                    className={classNames('text-[16px] my-auto', {
                      'font-[500] ': parentFolderLists?.data?.length === index + 1,
                      'cursor-pointer  hover:opacity-50': parentFolderLists?.data?.length !== index + 1,
                    })}
                  >
                    {item.folderName}
                  </p>
                  {parentFolderLists?.data?.length !== index + 1 && (
                    <div className="flex flex-col justify-center mx-3">/</div>
                  )}
                </div>
              );
            })}
          </div>
          <div className="group">
            <button
              aria-label="add"
              className="border border-[#F85365] hover:bg-[#F85365] block rounded-[50%] w-[52px] h-[52px]   "
              onClick={() => {
                setUpdateFolderDetails(null);
                setFolderFormInitialValue(initialValues);
                toggleFolderAdditionFormModal(true);
              }}
            >
              <div className=" flex justify-center">
                <PlusIcon />
              </div>
            </button>
          </div>
        </div>
      </div>
      {data?.data && (
        <ProjectListsCard
          onFolderClicked={onFolderClicked}
          folderLists={data?.data}
          onFolderAddClicked={() => toggleFolderAdditionFormModal(true)}
          onUpdateMenuClicked={onUpdateMenuClicked}
        />
      )}
      <div className="mt-10 ml-2">
        <div className="flex justify-between p-[10px] py-[12px] pb-0">
          <p className="text-[24px] font-[400] mt-auto">Files</p>
          <div className="group">
            <button
              onClick={() => fileRef?.current?.click()}
              className={
                ' border-[#F95868] group-hover:bg-[#F95868] border  px-[24px] py-[10px] text-[16px] flex justify-between rounded-[4px] text-white'
              }
            >
              <div className="flex my-auto">
                <PlusIcon />
              </div>
              <input
                ref={fileRef}
                max-file-size="1024"
                className="hidden ml-2"
                onChange={(e) => {
                  console.log(e, 'asd');
                  const files = e.target.files;
                  if (files && files[0].size > 2000000) {
                    toast.error('File size too big');

                    return;
                  }
                  files && handleChange(e);
                }}
                multiple={false}
                type="file"
                hidden
                accept=".jpg, .jpeg,.csv, .png, .mp3, .mp4, .pdf, .doc, .docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
              />
              <span className="ml-4 group-hover:text-white text-[16px] text-[#F95868]">Upload File</span>
            </button>
          </div>
        </div>
        <div className="mt-8">
          <ProjectFilesDataTables
            filesLists={filesLists?.data}
            renderHeader={() => {
              return <></>;
            }}
          />
        </div>
      </div>
    </div>
  );
}

export default ProjectsDocuments;
