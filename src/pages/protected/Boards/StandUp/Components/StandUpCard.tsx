import React from 'react';
import Calendar from 'assets/icons-svg/Calendar';
import EditIcon from 'assets/icons-svg/EditIcon';
import WatchIcon from 'assets/icons-svg/WatchIcon';
import ClockHistory from 'assets/icons-svg/ClockHistory';
import { STANDUPNOTIFICATIONREPEATSSCHUDULE } from 'common/enum';

const StandUpCard = ({ setTaskAdditionFormModal, onClick, standUpItem, onEditClicked }) => {
  return (
    <div className="cursor-pointer mt-4 w-[45%] rounded-2xl">
      <div className="flex ">
        <p className="mt-2 font-[600] text-[24px] " onClick={onClick}>
          {standUpItem.title}
        </p>
        <div
          className="flex flex-col justify-end ml-[9px] cursor-pointer "
          onClick={() => {
            console.log(standUpItem, 'editData');
            const editData = {
              ...standUpItem,
              alertDays: standUpItem.alertDays.split(','),
            };
            onEditClicked(editData);
            setTaskAdditionFormModal();
          }}
        >
          <p className="rounded-[4px] p-[2px] opacity-20 ">
            <EditIcon />
          </p>
        </div>
      </div>
      <div onClick={onClick} className="bg-white shadow-sm rounded-[12px] py-[19px] px-[21px] mt-[17px]">
        <p className="mt-2 font-[600] text-[18px] leading-[24px]">{standUpItem.descriptions}</p>
        <div className="mt-2">
          <div className="flex mt-2">
            <div className="flex flex-col justify-center">
              <Calendar />
            </div>
            <div className="flex flex-wrap">
              {standUpItem.alertDays.split(',').map((item) => (
                <p className="mt-1 w-14 text-center ml-2 border px-[12px] py-[2px]  rounded-[4px]">{item}</p>
              ))}
            </div>
          </div>
          <div className="flex mt-[14px]">
            <WatchIcon />
            <p className="ml-[8px]">{standUpItem.alertTime}</p>
          </div>
          <div className=" mt-[14px] flex">
            <div className=" flex flex-col justify-center">
              <ClockHistory />
            </div>

            <p className="ml-[8px] ">
              {standUpItem.notificationAlertSchdule === STANDUPNOTIFICATIONREPEATSSCHUDULE.ONCEAWEEK
                ? 'Once a week'
                : standUpItem.notificationAlertSchdule === STANDUPNOTIFICATIONREPEATSSCHUDULE.ONCEAYEAR
                ? 'Once a year'
                : 'Once a month'}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default StandUpCard;
