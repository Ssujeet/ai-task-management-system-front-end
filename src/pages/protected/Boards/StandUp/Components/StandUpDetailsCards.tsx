import React from 'react';
import { StandUpDetailsOutput } from 'redux/reducers/standup';
import draftToHtml from 'draftjs-to-html';
import BadgeAvatars from 'components/BadgeAvatars';
import EditIcon from '@material-ui/icons/Edit';
import { IconButton, Tooltip } from '@material-ui/core';

interface IStandUpDetailsCards {
  item: StandUpDetailsOutput;
}
const StandUpDetailsCards = (props: IStandUpDetailsCards) => {
  const { item } = props;
  const src = item?.user?.userProfile?.userPictureUrl;

  return (
    <div>
      <div className="mt-5">
        <div className="flex">
          <div className="flex">
            <BadgeAvatars src={src} sx={{ width: 56, height: 56 }}>
              <p className="text-4xl">{src ? '' : item?.user?.userProfile?.fullName?.split('')[0].toUpperCase()}</p>
            </BadgeAvatars>
            <div className="ml-4">
              <h2 className="text-2xl">{item?.user?.userProfile?.fullName}</h2>
              <div dangerouslySetInnerHTML={{ __html: draftToHtml(JSON.parse(item?.details)) }}></div>
            </div>
          </div>
          <div className="ml-8">
            <Tooltip title="Click to Edit your answers" aria-label="expand">
              <IconButton onClick={() => console.log(true)} aria-label="edit">
                {/* <EditIcon /> */}
              </IconButton>
            </Tooltip>
          </div>
        </div>
      </div>
    </div>
  );
};

export default StandUpDetailsCards;
