import React from 'react';
import { useFormik } from 'formik';
import classNames from 'classnames';
import { StandUpInput } from 'redux/reducers/standup';
import { STANDUPNOTIFICATIONREPEATSSCHUDULE } from 'common/enum';
import TextField from '@mui/material/TextField';
// import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
// import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
// import { StaticTimePicker } from '@mui/x-date-pickers/StaticTimePicker';

const initialValues = {
  title: '',
  descriptions: '',

  alertTime: '',
  alertDays: ['Sun'],
  notificationAlertSchdule: STANDUPNOTIFICATIONREPEATSSCHUDULE.ONCEAWEEK,
};

const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
// const standUpValidationSchema = {};

interface IStandUpForm {
  formData?: typeof initialValues;
  onSubmit: (data: any) => void;
  setTaskAdditionFormModal: () => void;
}
const StandUpForm = (props: IStandUpForm) => {
  const { formData, onSubmit, setTaskAdditionFormModal } = props;
  console.log(formData, 'formData');
  const formik = useFormik({
    initialValues: formData ? formData : initialValues,
    onSubmit: (values) => {
      onSubmit(values);
    },
    // validationSchema: standUpValidationSchema,
  });

  return (
    <div>
      <form className="p-4 px-8" onSubmit={formik.handleSubmit}>
        <div className="flex flex-col">
          <label className="text-[14px] font-[500] leading-[19px] text-[rgba(52, 52, 52, 0.6)]">Title</label>
          <input
            type="text"
            name="title"
            className="border rounded-[8px] px-[24px] py-[12px] mt-[7px]"
            onBlur={formik.handleBlur}
            value={formik.values.title}
            onChange={formik.handleChange}
          />

          {Boolean(formik.touched.title && formik.errors.title) && (
            <span className="text-sm text-red-500">{formik.touched.title && formik.errors.title}</span>
          )}
        </div>
        <div className="flex flex-col mt-2">
          <label className="text-[14px] font-[500] leading-[19px] text-[rgba(52, 52, 52, 0.6)]">Description</label>
          <textarea
            cols={4}
            name="descriptions"
            className="border rounded-[8px] px-[24px] py-[12px] mt-[7px]"
            onBlur={formik.handleBlur}
            value={formik.values.descriptions}
            onChange={formik.handleChange}
          />

          {Boolean(formik.touched.descriptions && formik.errors.descriptions) && (
            <span className="text-sm text-red-500">{formik.touched.descriptions && formik.errors.descriptions}</span>
          )}
        </div>

        <div className="flex mt-4 flex-col w-full ">
          <label className="text-[14px] font-[500] leading-[19px] text-[rgba(52, 52, 52, 0.6)]">Repeats at</label>
          <div className="flex justify-between  mt-2">
            <div>
              <input
                type="radio"
                name="notificationAlertSchdule"
                checked={formik.values.notificationAlertSchdule === STANDUPNOTIFICATIONREPEATSSCHUDULE.ONCEAWEEK}
                onBlur={formik.handleBlur}
                value={STANDUPNOTIFICATIONREPEATSSCHUDULE.ONCEAWEEK}
                onChange={formik.handleChange}
              />
              <label className="ml-2">Once a week</label>
            </div>

            <div>
              <input
                type="radio"
                name="notificationAlertSchdule"
                checked={formik.values.notificationAlertSchdule === STANDUPNOTIFICATIONREPEATSSCHUDULE.ONCEAMONTH}
                onBlur={formik.handleBlur}
                value={STANDUPNOTIFICATIONREPEATSSCHUDULE.ONCEAMONTH}
                onChange={formik.handleChange}
              />

              <label className="ml-2">Once a month</label>
            </div>
            <div>
              <input
                type="radio"
                name="notificationAlertSchdule"
                checked={formik.values.notificationAlertSchdule === STANDUPNOTIFICATIONREPEATSSCHUDULE.ONCEAYEAR}
                onBlur={formik.handleBlur}
                value={STANDUPNOTIFICATIONREPEATSSCHUDULE.ONCEAYEAR}
                onChange={formik.handleChange}
              />
              <label className="ml-2">Once a year</label>
            </div>
          </div>

          {Boolean(formik.touched.notificationAlertSchdule && formik.errors.notificationAlertSchdule) && (
            <span className="text-sm text-red-500">
              {formik.touched.notificationAlertSchdule && formik.errors.notificationAlertSchdule}
            </span>
          )}
        </div>
        <div className="flex flex-row mt-4">
          <div className="flex flex-col w-full">
            <label className="text-[14px] font-[500] leading-[19px] text-[rgba(52, 52, 52, 0.6)]">Time</label>
            <input
              type="time"
              name="alertTime"
              className="border rounded-[8px] px-[24px] py-[12px] mt-[7px]"
              onBlur={formik.handleBlur}
              value={formik.values.alertTime}
              onChange={formik.handleChange}
            />
            {/* <LocalizationProvider dateAdapter={AdapterDateFns}>
      <StaticTimePicker
        ampm
        orientation="landscape"
        openTo="minutes"
        value={formik.values.alertTime}
        onChange={(newValue) => {
          formik.setFieldValue("alertTime",newValue);
        }}
        renderInput={(params) => <TextField {...params} />}
      />
    </LocalizationProvider> */}

            {Boolean(formik.touched.alertTime && formik.errors.alertTime) && (
              <span className="text-sm text-red-500">{formik.touched.alertTime && formik.errors.alertTime}</span>
            )}
          </div>
        </div>

        <div className="flex flex-col mt-2">
          <label className="text-[14px] font-[500] leading-[19px] text-[rgba(52, 52, 52, 0.6)]">Days</label>
          <div className="flex flex-wrap gap-[10px]">
            {days.map((item, index) => {
              return (
                <p
                  onClick={() => {
                    if (formik.values.alertDays.includes(item)) {
                      const newValue = formik.values.alertDays.filter((d) => d !== item);
                      formik.setFieldValue('alertDays', newValue);
                    } else {
                      const newValue = [...formik.values.alertDays, item];
                      console.log(newValue, 'newValue');
                      formik.setFieldValue('alertDays', newValue);
                    }
                  }}
                  key={index}
                  className={classNames(
                    'mt-1 w-20 cursor-pointer text-center border-2 border-red-100 px-2 py-1  rounded-xl',
                    { 'bg-red-600 text-white ': formik?.values?.alertDays?.includes(item) },

                    { 'hover:bg-red-400 hover:text-white ': !formik?.values?.alertDays?.includes(item) }
                  )}
                >
                  {item}
                </p>
              );
            })}
          </div>

          {Boolean(formik.touched.title && formik.errors.title) && (
            <span className="text-sm text-red-500">{formik.touched.title && formik.errors.title}</span>
          )}
        </div>

        <div className=" mt-5 flex justify-between mt-[66px] pb-[21px]">
          <button type="submit" className="cursor-pointer  bg-[#F85365] px-[35px] py-[12px] text-white rounded-[4px]">
            Submit
          </button>
          <button
            type="button"
            className="cursor-pointer hover:text-red-700 py-1 rounded-lg text-gray-700"
            onClick={() => {
              formik.resetForm({ values: initialValues });
              setTaskAdditionFormModal();
            }}
          >
            Cancel
          </button>
        </div>
      </form>
    </div>
  );
};

export default StandUpForm;
