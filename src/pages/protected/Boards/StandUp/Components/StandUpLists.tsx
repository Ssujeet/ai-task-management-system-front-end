import React from 'react';
import { StandUpDetailsOutput } from 'redux/reducers/standup';
import StandUpDetailsCards from './StandUpDetailsCards';

interface IStandUpLists {
  standUpLists: StandUpDetailsOutput[];
}

const StandUpLists = (props: IStandUpLists) => {
  const { standUpLists } = props;

  return (
    <>
      <div>
        <h1 className="text-5xl">Today</h1>
      </div>
      <div className="mt-8">
        {standUpLists?.map((item) => {
          return <StandUpDetailsCards item={item} />;
        })}
      </div>
    </>
  );
};

export default StandUpLists;
