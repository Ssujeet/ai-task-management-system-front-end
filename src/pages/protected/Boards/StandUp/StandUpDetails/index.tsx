import React from 'react';
import MyEditor from 'components/Editor';
import { useRouteMatch } from 'react-router-dom';
import {
  useGetStandUpDetailsListByStandUpIdQuery,
  useGetStandUpDetailsByStandUpIdQuery,
  useAddStandUpDetailsMutation,
  StandUpDetailsInput,
} from 'redux/reducers/standup';
import DatePickers from 'components/DatePickers/DatePickers';
import StandUpLists from '../Components/StandUpLists';
import { SCREENS_ROUTES_ENUMS } from 'common/enum';
import MaxWidthDialog from 'components/Modal';
import CrossIcon from 'assets/icons-svg/CrosIcon';
import useToggle from 'hooks/useToggle';
import { CircularProgress, DialogActions, DialogContent } from '@material-ui/core';
import AddButton from 'components/Buttons/AddButton';

const StandUpDetails = () => {
  const [editorValue, setEditorValue] = React.useState('');
  const [dateSelected, setDateSelected] = React.useState(new Date().toISOString().split('T')[0]);
  let match = useRouteMatch<{ id: string; standUpId: string }>(SCREENS_ROUTES_ENUMS.STANDUPDETAILS);

  const { data: standUpDetailsLists } = useGetStandUpDetailsListByStandUpIdQuery(match?.params?.standUpId || '');
  const [addStandUpDetails, { isLoading }] = useAddStandUpDetailsMutation();
  const [checkInModal, toggleCheckInModal] = useToggle(false);

  const { data } = useGetStandUpDetailsByStandUpIdQuery(match?.params?.standUpId || '');

  const onSaveHandled = () => {
    if (match?.params?.standUpId) {
      const postData: StandUpDetailsInput = {
        details: editorValue,
        standUpId: +match?.params?.standUpId,
      };
      addStandUpDetails(postData)
        .unwrap()
        .then((res) => {
          setEditorValue('');
          toggleCheckInModal();
        });
    }
  };

  return (
    <div className="mt-[41px]">
      <div className="flex justify-between">
        <div>
          <h1>{data?.data?.title}</h1>
          <p>{data?.data?.descriptions}</p>
        </div>
        <AddButton name="Add Update" onClick={toggleCheckInModal} />
        <div className="w-2/12">
          <DatePickers
            name="date"
            label="Select Date"
            value={dateSelected}
            onChange={(e) => setDateSelected(e.target.value)}
          />
        </div>
      </div>
      <MaxWidthDialog
        isModalOpen={checkInModal}
        toggleModal={toggleCheckInModal}
        renderTitle={() => {
          return (
            <div className="flex justify-between">
              <p className="text-[18px] font-[600]">Check In</p>
              <div
                className="bg-[#E5E5E5] cursor-pointer w-[24px] h-[24px] flex justify-between group rounded-[4px]"
                onClick={() => toggleCheckInModal()}
              >
                <CrossIcon />
              </div>
            </div>
          );
        }}
        submitLabel={'Save'}
        modalSize={'sm'}
        buttonType={'submit'}
      >
        <div className="">
          <DialogContent>
            <MyEditor onChangeValue={setEditorValue} editorValue={editorValue} />
          </DialogContent>

          <DialogActions>
            <div className="flex justify-between w-full mb-[35px] px-[10px]">
              <button
                color="primary"
                type={'button'}
                className="bg-[#F85365] text-white px-[57px] py-[12px] rounded-[4px]"
                onClick={onSaveHandled}
              >
                {isLoading ? <CircularProgress /> : 'Submit'}
              </button>
              <button
                onClick={() => toggleCheckInModal()}
                type={'button'}
                color="secondary"
                style={{ textTransform: 'none' }}
              >
                Close
              </button>
            </div>
          </DialogActions>
        </div>
      </MaxWidthDialog>
      {standUpDetailsLists?.data && (
        <div>
          <StandUpLists standUpLists={standUpDetailsLists?.data} />
        </div>
      )}
    </div>
  );
};

export default StandUpDetails;
