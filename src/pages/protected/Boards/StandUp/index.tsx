import React from 'react';
import StandUpCard from './Components/StandUpCard';
import MaxWidthDialog from 'components/Modal';
import useToggle from 'hooks/useToggle';
import StandUpForm from './Components/StandUpForm';
import { IRoutes } from 'routes/routesLists';
import PrivateRoutes from 'routes';
import { RouteComponentProps, useHistory, useRouteMatch } from 'react-router-dom';
import {
  useAddStandUpMutation,
  useGetStandUpListOfProjectByIdQuery,
  useUpdateStandUpMutation,
} from 'redux/reducers/standup';
import { SCREENS_ROUTES_ENUMS } from 'common/enum';
import AddButton from 'components/Buttons/AddButton';
import CrossIcon from 'assets/icons-svg/CrosIcon';
import StandUpImage from 'assets/icons-svg/StandUpImage';
import Loader from 'components/Loader/loader';

interface IStandUp extends RouteComponentProps {
  childrenRoutes: IRoutes[];
  computedMatch: any;
}
const StandUp = (props: IStandUp) => {
  let match = useRouteMatch<{ id: string }>(SCREENS_ROUTES_ENUMS.STANDUP);
  const { childrenRoutes } = props;
  const [taskAdditionFormModal, setTaskAdditionFormModal] = useToggle(false);
  const [addStandUp, { isLoading, error }] = useAddStandUpMutation();
  const [updateStandUp] = useUpdateStandUpMutation();
  const [standUpInitialFormData, setStandUpInitialFormData] = React.useState();

  const { data: standUpLists, isLoading: fetchingLists } = useGetStandUpListOfProjectByIdQuery(match?.params?.id || '');
  const history = useHistory();

  const onStandUpClicked = (id: string) => {
    history.push(props.location.pathname + '/details/' + id);
  };

  const OnStandUpFormSubmit = (values) => {
    if (match) {
      const postData = { ...values, alertDays: values.alertDays.toString(), projectId: +match?.params?.id };

      if (postData.id) {
        updateStandUp(postData)
          .unwrap()
          .then((res) => setTaskAdditionFormModal())
          .catch((err) => console.log(err));

        return;
      }
      addStandUp(postData)
        .unwrap()
        .then((res) => setTaskAdditionFormModal())
        .catch((err) => console.log(err));
    }
  };

  if (fetchingLists) {
    return <Loader />;
  }

  if (props.computedMatch.isExact) {
    return (
      <div className="">
        {standUpLists?.data?.length ? (
          <div className="flex justify-between mt-[45px]">
            <h2 className="text-xl text-[36px] font-[600]">Stand up</h2>
            <AddButton
              name="Add Stand up"
              onClick={() => {
                setTaskAdditionFormModal();
                setStandUpInitialFormData(undefined);
              }}
            />
          </div>
        ) : null}

        <MaxWidthDialog
          isModalOpen={taskAdditionFormModal}
          toggleModal={setTaskAdditionFormModal}
          renderTitle={() => {
            return (
              <div className="flex justify-between">
                <p className="text-[18px] font-[600]">{'Stand Up'}</p>
                <div
                  className="bg-[#E5E5E5] cursor-pointer w-[24px] h-[24px] flex justify-between group rounded-[4px]"
                  onClick={() => setTaskAdditionFormModal()}
                >
                  <CrossIcon />
                </div>
              </div>
            );
          }}
          submitLabel={'Save'}
          buttonType={'submit'}
          modalSize="sm"
        >
          <StandUpForm
            onSubmit={OnStandUpFormSubmit}
            setTaskAdditionFormModal={setTaskAdditionFormModal}
            formData={standUpInitialFormData}
          />
        </MaxWidthDialog>

        {standUpLists?.data?.length ? (
          <div className="flex flex-wrap gap-[53px]">
            {standUpLists?.data?.map((item, key) => {
              return (
                <StandUpCard
                  key={key}
                  standUpItem={item}
                  setTaskAdditionFormModal={setTaskAdditionFormModal}
                  onClick={() => onStandUpClicked(item?.id || '')}
                  onEditClicked={setStandUpInitialFormData}
                />
              );
            })}
          </div>
        ) : (
          <div className="h-[80vh] flex my-auto">
            <div className="my-auto mx-auto ">
              <StandUpImage />
              <div className="mt-[22px]">
                <h2 className="text-[24px] font-[600] mb-[35px]">Let’s add a standup for your project</h2>
                <div className="flex flex-row justify-center">
                  <AddButton
                    name="Add Stand up"
                    onClick={() => {
                      setTaskAdditionFormModal();
                      setStandUpInitialFormData(undefined);
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }

  return <PrivateRoutes routes={childrenRoutes} />;
};

export default StandUp;
