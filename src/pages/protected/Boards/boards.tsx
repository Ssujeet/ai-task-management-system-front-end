import React from 'react';
import { Helmet } from 'react-helmet';

import {
  useGetCurrentUserProjectListsQuery,
  useAddProjectsMutation,
  ProjectOutputInterface,
  useUpdateProjectMutation,
} from 'redux/reducers/projects';
import useToggle from 'hooks/useToggle';
import { PROJECTSTATUS } from 'common/enum';
import AddButton from 'components/Buttons/AddButton';
import { useGetCurrentUserQuery } from 'redux/reducers/user';

import ProjectBoard from './Components/ProjectBoards';
import ProjectFormWithFormiksWithModal from './Components/ProjectForms';
import { getRandomColor } from 'utils';
import { initialValues, ProjectAddForm } from './Components/ProjectForms/schema';
import { emptyWorkSpace } from 'constant/images';
import Loader from 'components/Loader/loader';

function Boards(props) {
  const [projectAdditionFormModal, toggleprojectAdditionFormModal] = useToggle(false);
  const [projectFormData, setProjectFormData] = React.useState<ProjectAddForm>(initialValues);

  const [addProjects, { isLoading: createLoading, error }] = useAddProjectsMutation();
  const [updateProject, { isLoading: updateLoading }] = useUpdateProjectMutation();
  const { data } = useGetCurrentUserQuery();
  const { data: projectLists, isLoading: isFetchingLists } = useGetCurrentUserProjectListsQuery();

  const onProgressProjectLists = projectLists?.data?.filter(
    (item) => item.project.projectStatus === PROJECTSTATUS.ONPROGRESS
  );
  const completedProjectLists = projectLists?.data?.filter(
    (item) => item.project.projectStatus === PROJECTSTATUS.COMPLETED
  );

  const isTrue = true;

  const isLoading = createLoading || updateLoading;

  if (isFetchingLists) {
    return <Loader />;
  }

  return (
    <>
      <Helmet>
        <title>WorkSpaces | Tascaid</title>
      </Helmet>
      <ProjectFormWithFormiksWithModal
        projectAdditionFormModal={projectAdditionFormModal}
        toggleprojectAdditionFormModal={toggleprojectAdditionFormModal}
        modalTitle="Add Workspace"
        toggleModal={toggleprojectAdditionFormModal}
        onSubmit={(values) => {
          const createPostData = { ...values, color: values.color ?? getRandomColor() };
          const updatePostData = {
            id: +values.id,
            projectName: values.projectName,
            descriptions: values.descriptions,
          };
          // debugger;
          const projectAPIFunction = values.id ? updateProject : addProjects;
          const postData = values.id ? updatePostData : createPostData;

          projectAPIFunction(postData)
            .unwrap()
            .then((payload) => {
              toggleprojectAdditionFormModal(false);
            })
            .catch((error) => console.error('fulfilled', error));
        }}
        isLoading={isLoading}
        formData={projectFormData}
        setProjectFormData={setProjectFormData}
      />
      {!onProgressProjectLists?.length ? (
        <div className="flex flex-col justify-center h-[70vh]">
          <img src={emptyWorkSpace} className="mx-auto" />
          <p className="text-[24px] font-[600] text-center my-[22px]">Let’s add your first Workspace</p>
          <div className="flex justify-center">
            <AddButton name="Add WorkSpace" onClick={() => toggleprojectAdditionFormModal(true)} />
          </div>
        </div>
      ) : (
        <>
          <ProjectListWithDifferentBoards
            onAddNewProjectClicked={() => {
              toggleprojectAdditionFormModal(true);
              setProjectFormData(initialValues);
            }}
            projectTypeList={'Your Workspaces'}
            projectLists={onProgressProjectLists}
            onEditClicked={(data) => {
              setProjectFormData(data);
              toggleprojectAdditionFormModal();
            }}
          />
        </>
      )}
    </>
  );
}

export default Boards;

interface IProjectListWithDifferentBoards {
  projectTypeList: string;
  onAddNewProjectClicked?: () => void;
  projectLists: ProjectOutputInterface[];
  onEditClicked: (data: ProjectAddForm) => void;
}

const ProjectListWithDifferentBoards = (props: IProjectListWithDifferentBoards) => {
  return (
    <div className="mt-[45px]">
      <div className="flex justify-between ">
        <p className="text-[24px] font-[500]">
          {props.projectTypeList}

          <span className="text-[#F85365] text-[22px] ml-[24px] bg-[#ffff]  py-[2px] px-[12px] rounded-[50%]">
            {props?.projectLists?.length}
          </span>
        </p>

        {props.onAddNewProjectClicked && <AddButton name="Add WorkSpace" onClick={props.onAddNewProjectClicked} />}
      </div>

      <div className="flex flex-wrap gap-[27px] mt-[51px]">
        {props?.projectLists?.map((item) => (
          <ProjectBoard project={item.project} onEditClicked={props.onEditClicked} />
        ))}
      </div>
    </div>
  );
};
