import React from 'react';
import { IRoutes } from 'routes/routesLists';
import PrivateRoutes from 'routes';
import { Helmet } from 'react-helmet';
import { useLocation, RouteComponentProps, useRouteMatch } from 'react-router-dom';
import Projects from './boards';
import { useAppDispatch } from 'hooks';
import { addSideBarNav } from 'redux/reducers/sidebar';
import { useGetRootFoldersOfProjectByIdQuery } from 'redux/reducers/folders-and-files';
import { useGetCurrentUserDetailsInProjectQuery } from 'redux/reducers/projectMembers';
import { ProjectOutputInterface } from 'redux/reducers/projects';
import { BaseResponse } from 'common/types';
import { socketEnvironment } from 'utils/socket-request';
import { io } from 'socket.io-client';
import { changeConnectedUser } from 'redux/reducers/socket';

interface IBoardsIndexPage extends RouteComponentProps {
  childrenRoutes: IRoutes[];
}
const BoardsIndexPage = (props: IBoardsIndexPage) => {
  const { childrenRoutes } = props;
  const dispatcher = useAppDispatch();
  const [route, setRoutes] = React.useState<IRoutes[]>();
  const { data } = useGetRootFoldersOfProjectByIdQuery(props.location.pathname.split('/')[3], {
    skip: !props.location.pathname.split('/')[3],
  });
  const { data: userDetails } = useGetCurrentUserDetailsInProjectQuery(props.location.pathname.split('/')[3], {
    skip: !props.location.pathname.split('/')[3],
  });
  const userDetailsData = userDetails;

  React.useEffect(() => {
    if (props.location.pathname.split('/')[3]) {
      const socket = io(process.env.REACT_APP_SOCKET_URL as string, socketEnvironment);
      socket.on('ConnectedUser' + props.location.pathname.split('/')[3].toString(), (data) => {
        dispatcher(changeConnectedUser(data.onLineUser));
      });

      return () => {
        socket.disconnect();
      };
    }
  }, [props.location.pathname.split('/')[3]]);

  React.useEffect(() => {
    if (props.location?.pathname === '/work-space') {
      dispatcher(addSideBarNav([]));
    }
    if (props.location?.pathname && props.location?.pathname !== '/work-space' && userDetailsData) {
      const filteredRoutes = [...childrenRoutes].filter((item) =>
        userDetailsData?.data?.projectMemberRole?.screenRole?.some((screen) => screen.screen.name === item?.screenkey)
      );
      const routesLists = [...filteredRoutes]?.map((route) => {
        return {
          ...route,
          path: route?.path
            ?.replace(':id', props.location.pathname.split('/')[3])
            .replace(':projectFolderRootId', data?.data?.id || ''),
        };
      });
      setRoutes(filteredRoutes);
      dispatcher(addSideBarNav(routesLists));
    }
  }, [props.location?.pathname, data, userDetailsData]);

  if (props.location?.pathname === '/work-space') {
    return <Projects />;
  }
  if (route) {
    return <PrivateRoutes routes={route} />;
  }

  return null;
};

export default BoardsIndexPage;
