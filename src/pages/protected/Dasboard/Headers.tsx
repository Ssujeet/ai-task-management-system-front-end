import React from 'react';
import { sideArrowIcon } from 'constant';
import { useAppDispatch } from 'hooks';

import { useHistory, useLocation } from 'react-router-dom';
import { useGetProjectsDetailByIdQuery } from 'redux/reducers/projects';
import { addSideBarNav } from 'redux/reducers/sidebar';

import Notifications from './Notifications';
import Messages from './Messages';
import Profile from './Profile';
import { useGetCurrentUserProfileQuery } from 'redux/reducers/user-profile';
import Timer from './Timer';
import { useGetCurrentUserTimeTrackerQuery } from 'redux/reducers/task-time-tracker';

const Headers = () => {
  const history = useHistory();
  const dispatcher = useAppDispatch();
  const location = useLocation();
  const currentPath = location?.pathname?.split('/')[1] ? location.pathname.split('/')[1] : 'Dashboard';
  const { data: currentUserTimeTrackers } = useGetCurrentUserTimeTrackerQuery();

  const { data: projectDetails } = useGetProjectsDetailByIdQuery(location.pathname.split('/')[3], {
    skip: !location.pathname.split('/')[3],
  });

  return (
    <div className="py-[34px] bg-[#FFFFFF]  shadow-sm rounded-[12px] px-[24px]  flex justify-between ">
      <div className="fixed bottom-0 right-0 p-[50px] z-[1000000000000]">
        {currentUserTimeTrackers?.data?.task && <Timer />}
      </div>
      <div className="flex">
        <>
          {location?.pathname?.split('/')[3] && projectDetails && (
            <>
              <p
                className={`text-[18px] capitalize cursor-pointer text-gray-800 hover:text-gray-400 flex flex-col justify-center`}
                onClick={() => {
                  history.push('/');
                  dispatcher(addSideBarNav([]));
                }}
              >
                Work Space
              </p>
              <div className="px-1 flex flex-col justify-center">
                <img src={sideArrowIcon} className="mx-[4px] my-auto" />
              </div>
              <p className={`text-[18px] font-normal capitalize flex flex-col justify-center `}>
                {projectDetails?.data?.projectName}
              </p>
              <div className="px-1 flex flex-col justify-center">
                <img src={sideArrowIcon} className="mx-[4px]" />
              </div>
            </>
          )}

          <>
            <p className={`text-[18px] capitalize flex flex-col justify-center `}>
              {location.pathname.split('/')[2] ?? currentPath.replace('-', '')}
            </p>
          </>
        </>
      </div>

      <div className="flex">
        {/* <div className="ml-[18px] relative cursor-pointer">
          <Messages />
        </div> */}

        <div className="ml-[18px] relative cursor-pointer">
          <Notifications />
        </div>
        <div className="cursor-pointer ml-[18px]">
          <Profile />
        </div>
      </div>
    </div>
  );
};

export default Headers;
