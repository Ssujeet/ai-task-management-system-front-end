import { ClickAwayListener, Tooltip } from '@material-ui/core';
import React, { FC } from 'react';
import { useGetCurrentUserNotificationsListsCountQuery } from 'redux/reducers/notifications';
import { messageIcon } from 'constant';
import { ChatMessagesResponse, useGetCurrentUserUnSeenMessageListsQuery } from 'redux/reducers/chat-messages';
import NotificationBox from 'assets/icons-svg/NotificationBox';

export default function Messages() {
  const [showDownDrop, setShowDownDrop] = React.useState(false);
  const { data: unSeenMessageMessageLists } = useGetCurrentUserUnSeenMessageListsQuery();

  return (
    <ClickAwayListener onClickAway={() => setShowDownDrop(false)}>
      <div className="relative  inline-block text-left">
        <div className=" flex flex-col justify-center">
          <Tooltip title={'Messages'}>
            <button
              type="button"
              className="inline-flex justify-center w-full rounded-md bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 "
              id="menu-button"
              aria-expanded="true"
              aria-haspopup="true"
              onClick={() => setShowDownDrop(!showDownDrop)}
            >
              <div className="flex flex-col justify-center ml-2 ">
                <img src={messageIcon} />
                {unSeenMessageMessageLists?.data?.length ? (
                  <p className="absolute top-[-15px] w-[23px] h-[23px] z-[1000] left-[15px]  bg-[#F85365] flex justify-center flex-col text-white text-center rounded-[50%] font-bold text-[12px]">
                    {unSeenMessageMessageLists?.data.length}
                  </p>
                ) : null}
              </div>
            </button>
          </Tooltip>
        </div>

        {showDownDrop && <MessageLists unSeenMessageMessageLists={unSeenMessageMessageLists?.data} />}
      </div>
    </ClickAwayListener>
  );
}

const MessageLists: FC<{ unSeenMessageMessageLists: ChatMessagesResponse[] | undefined | null }> = ({
  unSeenMessageMessageLists,
}) => {
  return (
    <div
      className={
        'z-[400000] absolute mt-2  py-2  right-0 w-[617px] focus:border-none rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5  focus:outline-none '
      }
      role="menu"
      aria-orientation="vertical"
      aria-labelledby="menu-button"
    >
      <div className="px-[24px] flex justify-between pb-[12px]">
        <h2 className="font-[600] text-[24px]">Messages</h2>
      </div>
      <div
        className={` divide divide-y h-max-[550px] overflow-auto ${
          unSeenMessageMessageLists?.length ? 'h-min-[350px]' : ' h-[350px]'
        }`}
      >
        {unSeenMessageMessageLists?.length ? (
          unSeenMessageMessageLists?.map((item) => {
            return (
              <div className="py-1 hover:bg-red-100 px-2">
                <p>{item.message}</p>
              </div>
            );
          })
        ) : (
          <div className="grid place-items-center h-full">
            <div className="">
              <div className="flex justify-center">
                <NotificationBox />
              </div>
              <p className="font-[500] text-[14px] text-[#343434] mt-[30px]">All your Messages will appear here !</p>
            </div>
          </div>
        )}
      </div>
      {/* <div>
        <button className="mx-auto flex">Load more</button>
      </div> */}
    </div>
  );
};
