import React from 'react';
import { useAppSelector } from 'hooks';
import { IRoutes } from 'routes/routesLists';
import { Tooltip } from '@material-ui/core';
import { BrowserRouter as Router, Switch, Route, Link, NavLink, useLocation, useHistory } from 'react-router-dom';
import classNames from 'classnames';

const NavLists = ({ open, routes }) => {
  const routesLists = useAppSelector((state) => state.sideBar.routes);
  const finalLists = routesLists.length ? routesLists : routes;
  const location = useLocation();
  const history = useHistory();

  return (
    <ul className="mx-[37px] mt-[52px]">
      {finalLists.map((item: IRoutes, index: number) => {
        if (!item.notShowToNavBar)
          return (
            <li className="mt-[12px] shadow-lg rounded-[10px] h-[72px] ">
              <LinkToRoute item={item} open={open} />
            </li>
          );
      })}
    </ul>
  );
};

export default NavLists;

const LinkToRoute = ({ item, open }) => {
  const [isActiveRoute, setIsActiveRoute] = React.useState(false);

  return (
    <NavLink
      to={item.path}
      key={item.title}
      className="px-[24px]  rounded-[10px] h-full"
      exact
      style={(isActive) => {
        setIsActiveRoute(isActive);

        return {
          backgroundColor: isActive ? 'rgba(52, 52, 52, 0.8)' : '#FFFFFF',
          width: '100%',
          display: 'flex',
          color: isActive ? 'white' : '',
          opacity: 20,
        };
      }}
    >
      <Tooltip title={item.title}>
        <div className="flex items-center">{item.icon && item.icon(isActiveRoute ? 'white' : 'black')}</div>
      </Tooltip>
      {
        <span className={classNames({ 'text-[18px] flex my-auto ml-[24px] overflow-hidden': true, hidden: !open })}>
          {item.title}
        </span>
      }
    </NavLink>
  );
};
