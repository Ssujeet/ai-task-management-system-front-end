import { ClickAwayListener, Tooltip } from '@material-ui/core';
import React from 'react';
import {
  useGetCurrentUserNotificationsListsQuery,
  useGetCurrentUserNotificationsListsCountQuery,
  INotificationNotifier,
  INotifications,
  useMarkAsReadMutation,
  useUpdateNotificationWithIdMutation,
} from 'redux/reducers/notifications';
import { notificationIcon, sideArrowIcon } from 'constant';
import { useGetCurrentUserQuery } from 'redux/reducers/user';
import { io, Socket } from 'socket.io-client';
import { toast } from 'react-toastify';
import { socketEnvironment } from 'utils/socket-request';
import Avatar from '@material-ui/core/Avatar';
import { getFirstLetters } from 'utils';
import Moment from 'react-moment';
import { useHistory } from 'react-router-dom';
import NotificationBox from 'assets/icons-svg/NotificationBox';
import classNames from 'classnames';
import { NOTIFICATION_STATUS } from 'common/enum';

export default function Notifications() {
  const [showDownDrop, setShowDownDrop] = React.useState(false);
  const { data: unseenNotificationsCount } = useGetCurrentUserNotificationsListsCountQuery();
  const [socket, setSocket] = React.useState<Socket>();
  const { data: currentUser } = useGetCurrentUserQuery();
  const [notificationCount, setNotificationCount] = React.useState(0);
  const { data: notificationsLists } = useGetCurrentUserNotificationsListsQuery();

  const [notification, setNotifications] = React.useState<any>([]);

  React.useEffect(() => {
    if (notificationsLists) {
      setNotifications(notificationsLists.data);
    }
  }, [notificationsLists]);

  console.log(notificationsLists, 'notificationsLists');

  React.useEffect(() => {
    let socket;
    if (currentUser?.data?.id) {
      socket = io(process.env.REACT_APP_SOCKET_NOTIFICATION_URL_ as string, socketEnvironment);
      setSocket(socket);
      socket?.on(currentUser?.data?.id?.toString(), (data: INotifications) => {
        setNotifications((notification) => [{ notification: data }, ...notification]);
        setNotificationCount((count) => count + 1);
        toast(data.message, { position: 'top-center' });
      });
    }

    return () => {
      socket?.disconnect();
    };
  }, [currentUser?.data?.id]);

  React.useEffect(() => {
    unseenNotificationsCount?.data && setNotificationCount(unseenNotificationsCount?.data as any);
  }, [unseenNotificationsCount?.data]);

  return (
    <ClickAwayListener onClickAway={() => setShowDownDrop(false)}>
      <div className="relative  inline-block text-left">
        <div className=" flex flex-col justify-center">
          <Tooltip title={'Notifications'}>
            <button
              type="button"
              className="inline-flex justify-center w-full rounded-md bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 "
              id="menu-button"
              aria-expanded="true"
              aria-haspopup="true"
              onClick={() => setShowDownDrop(!showDownDrop)}
            >
              <div className="flex relative flex-col justify-center ml-2 ">
                <img src={notificationIcon} />
                {unseenNotificationsCount?.data ? (
                  <p className="absolute top-[-15px] w-[23px] h-[23px] z-[1000] left-[15px]  bg-[#F85365] flex justify-center flex-col text-white text-center rounded-[50%] font-bold text-[12px]">
                    {notificationCount}
                  </p>
                ) : null}
              </div>
            </button>
          </Tooltip>
        </div>

        {showDownDrop && <NotificationLists notification={notification} />}
      </div>
    </ClickAwayListener>
  );
}

const NotificationLists = ({ notification }) => {
  const [markAsRead] = useMarkAsReadMutation();
  const [updateNotificationWithId] = useUpdateNotificationWithIdMutation();
  const history = useHistory();

  const onNotificationClicked = (item) => (e) => {
    debugger;
    history.push(`/work-space/boards/${item.notification.project.id}`);
    updateNotificationWithId(item.id);
  };

  return (
    <div
      className={
        'z-[40000000000] absolute py-2 divide divide-y  mt-2 w-[617px]  right-0 focus:border-none rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5  focus:outline-none '
      }
      role="menu"
      aria-orientation="vertical"
      aria-labelledby="menu-button"
    >
      <div className="px-[24px] flex justify-between pb-[12px]">
        <h2 className="font-[600] text-[24px]">Notifications</h2>
        {notification.length ? (
          <span className="text-[12px] font-600 underline my-auto " onClick={() => markAsRead()}>
            Mark all as read
          </span>
        ) : null}
      </div>
      <div
        className={` divide divide-y h-[350px] overflow-auto ${notification.length ? ' h-min-[350px]' : ' h-[350px]'}`}
      >
        {notification.length ? (
          <>
            {[...notification]?.reverse()?.map((item) => {
              return (
                <div
                  className={classNames('py-1 px-[20px] py-[12px] flex items-center', {
                    'bg-notification-unseen hover:bg-notification-hover-unseen':
                      item.notificationStatus === NOTIFICATION_STATUS.UNSEEN,
                    'hover:bg-gray-light': item.notificationStatus !== NOTIFICATION_STATUS.UNSEEN,
                  })}
                  onClick={onNotificationClicked(item)}
                >
                  <div
                    className={classNames('w-[8px] h-[8px] rounded-[50%]  mr-[10px]', {
                      'bg-[#F85365]': item.notificationStatus === NOTIFICATION_STATUS.UNSEEN,
                    })}
                  ></div>
                  <Avatar
                    aria-label="recipe"
                    src={item.notification.actionBy.userProfile.userPictureUrl}
                    className="w-[32px] h-[32px]"
                    style={{ backgroundColor: item.notification.actionBy.color }}
                  >
                    {getFirstLetters(item.notification.actionBy.userProfile.fullName)}
                  </Avatar>
                  <div
                    className={classNames('ml-[8px] my-auto', {
                      // 'text-white': item.notificationStatus === NOTIFICATION_STATUS.UNSEEN,
                    })}
                  >
                    <p>
                      {' '}
                      <span className="font-[600]">{item.notification.actionBy.userProfile.fullName} </span>
                      {item.notification.message}.
                    </p>
                    <p
                      className={classNames('text-[12px] text-[rgba(52, 52, 52)] opacity-50', {
                        // 'text-white': item.notificationStatus === NOTIFICATION_STATUS.UNSEEN,
                      })}
                      style={{ color: '' }}
                    >
                      {' '}
                      <Moment fromNow>{item.createdAt}</Moment>
                    </p>
                  </div>
                </div>
              );
            })}
          </>
        ) : (
          <div className="grid place-items-center h-full">
            <div className="">
              <div className="flex justify-center">
                <NotificationBox />
              </div>
              <p className="font-[500] text-[14px] text-[#343434] mt-[30px]">
                All your Notificiations will appear here !
              </p>
            </div>
          </div>
        )}
      </div>

      {/* <div>
        <button className="mx-auto flex">Load more</button>
      </div> */}
    </div>
  );
};
