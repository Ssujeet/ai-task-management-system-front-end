import { ClickAwayListener, Tooltip } from '@material-ui/core';
import React from 'react';

import { profileIcon } from 'constant';
import { Link } from 'react-router-dom';
import { useLocalStorage } from 'hooks/useStorage';
import { useGetCurrentUserProfileQuery } from 'redux/reducers/user-profile';
import { getFirstLetters } from 'utils';

export default function Profile() {
  const [showDownDrop, setShowDownDrop] = React.useState(false);
  const { data: userProfile } = useGetCurrentUserProfileQuery();

  console.log(userProfile, 'userProfile');

  return (
    <ClickAwayListener onClickAway={() => setShowDownDrop(false)}>
      <div className="relative  inline-block text-left">
        <div className=" flex flex-col justify-center">
          <Tooltip title={'Profile'}>
            <button
              type="button"
              className="inline-flex justify-center w-full rounded-md bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 "
              id="menu-button"
              aria-expanded="true"
              aria-haspopup="true"
              onClick={() => setShowDownDrop(!showDownDrop)}
            >
              <div className="flex flex-col justify-center ml-2 ">
                <img
                  className="inline-block h-[24px] w-[24px] text-[8px] grid place-content-center rounded-full ring-2 ring-white"
                  style={{ backgroundColor: userProfile?.data?.user?.color }}
                  src={userProfile?.data?.userPictureUrl}
                  alt={getFirstLetters(userProfile?.data?.fullName)}
                />
              </div>
            </button>
          </Tooltip>
        </div>

        {showDownDrop && <ProfileMenu />}
      </div>
    </ClickAwayListener>
  );
}

const ProfileMenu = () => {
  const [token, setToken, removeToken] = useLocalStorage('token');
  const onLogOut = () => {
    removeToken();
    window.location.href = '/';
  };

  return (
    <div
      className={
        'z-[400000] absolute  py-2 mt-2 w-[120px] right-0 focus:border-none rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5  focus:outline-none '
      }
      role="menu"
      aria-orientation="vertical"
      aria-labelledby="menu-button"
    >
      <div className="py-1 px-2 hover:bg-gray-light" role="none">
        <Link to="/profile">
          <p>View Profile</p>
        </Link>
      </div>
      <div className="py-1 px-2 hover:bg-gray-light" role="none" onClick={onLogOut}>
        <p>Log Out</p>
      </div>
    </div>
  );
};
