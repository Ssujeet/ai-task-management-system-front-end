import React from 'react';
import { TASKTIMETRACKER } from 'common/enum';
import { useStopwatch } from 'react-timer-hook';
import PlayIcon from 'assets/icons-svg/PlayIcon';
import StopIcon from 'assets/icons-svg/StopIcon';
import { useAddTaskTimeTrackersMutation, useGetCurrentUserTimeTrackerQuery } from 'redux/reducers/task-time-tracker';

const Timer = () => {
  const { data: currentUserTimeTrackers } = useGetCurrentUserTimeTrackerQuery();

  const [addTaskTimeTrackers] = useAddTaskTimeTrackersMutation();

  const todayDate = new Date();
  const createdTime = currentUserTimeTrackers?.data?.createdAt
    ? new Date(currentUserTimeTrackers?.data?.createdAt).toISOString()
    : new Date().toISOString();

  const differenceTime = currentUserTimeTrackers?.data?.createdAt
    ? (new Date(
        Date.UTC(
          todayDate.getFullYear(),
          todayDate.getMonth(),
          todayDate.getDate(),
          todayDate.getHours(),
          todayDate.getMinutes(),
          todayDate.getSeconds()
        )
      ).getTime() -
        new Date(createdTime).getTime()) /
      1000
    : 0;

  const stopwatchOffset = new Date();
  const offsetTimestamp: any = stopwatchOffset.setSeconds(stopwatchOffset.getSeconds() + differenceTime);
  const { seconds, minutes, hours, days, isRunning, start, pause, reset } = useStopwatch({
    autoStart: false,
    offsetTimestamp,
  });

  React.useEffect(() => {
    if (currentUserTimeTrackers?.data?.task) {
      start();
    } else {
      reset();
      pause();
    }
  }, [currentUserTimeTrackers]);
  const startStopTimer = () => {
    currentUserTimeTrackers?.data?.task?.id &&
      addTaskTimeTrackers({
        taskId: currentUserTimeTrackers?.data?.task?.id,
        taskStatus: TASKTIMETRACKER.STOP,
      });
    if (isRunning) {
      reset();
      pause();
    } else {
      start();
    }
  };

  if (!currentUserTimeTrackers?.data?.id) {
    return null;
  }

  return (
    <div>
      <div className="flex ml-[101px] shadow-lg px-[10px] py-[5px]">
        {/* <p>
                  {currentUserTimeTrackers.data.task.title}
                  </p> */}
        <div className="text-[16px] flex items-center">
          {days !== 0 && (
            <>
              <span>{days}</span> :{' '}
            </>
          )}
          {hours !== 0 && (
            <>
              <span>{hours}</span> :{' '}
            </>
          )}
          <span>{minutes !== 0 ? (minutes < 10 ? '0' + minutes : minutes) : '00'}</span>:
          <span>{seconds !== 0 ? (seconds < 10 ? '0' + seconds : seconds) : '00'}</span>
        </div>
        <button className="ml-[12px]" onClick={startStopTimer}>
          {isRunning ? <StopIcon /> : <PlayIcon />}
        </button>
      </div>
    </div>
  );
};

export default Timer;
