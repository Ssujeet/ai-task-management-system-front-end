import React from 'react';

import { useTheme, Theme } from '@material-ui/core/styles';

import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import Headers from './Headers';
import NavLists from './NavLists';
import PrivateRoutes from 'routes';
import { useAppDispatch } from 'hooks';
import { IRoutes } from 'routes/routesLists';
import { useGetCurrentUserQuery } from 'redux/reducers/user';
import { linkedInIcon, logOutIcon, onlyBubblesLogo, tasCaidLogo, twitterIcon } from 'constant';
import { useLocalStorage } from 'hooks/useStorage';
import classNames from 'classnames';
import { AiOutlineLeft } from 'react-icons/ai';
interface IMiniDrawer {
  routes: IRoutes[];
}

export default function MiniDrawer(props: IMiniDrawer) {
  const { routes } = props;

  const theme = useTheme();
  const [open, setOpen] = React.useState(true);

  const { data } = useGetCurrentUserQuery();
  React.useEffect(() => {}, []);

  const [token, setToken, removeToken] = useLocalStorage('token');

  const onLogOut = () => {
    removeToken();
    window.location.href = '/';
  };

  return (
    <Router>
      <div className="bg-[#FCFCFC] h-screen">
        <div
          className={classNames({
            'fixed transition-all ease-in-out delay-150  delay-150  h-full bg-[#FEFEFE] border-r z-[20] duration-300 ':
              true,
            'w-[140px]': !open,
            'w-[384px]': open,
          })}
        >
          <div className={`flex  align-center cursor-pointer  mt-[54px]  justify-between px-5 `}>
            <img
              alt="logo"
              src={!open ? onlyBubblesLogo : tasCaidLogo}
              className="mx-auto cursor-pointer  "
              onClick={() => setOpen(true)}
            />
            {open && (
              <button className="my-auto cursor-pointer flex flex-col justify-center" onClick={() => setOpen(!open)}>
                <AiOutlineLeft size="20" />
              </button>
            )}
          </div>
          <div className="h-full ">
            <NavLists routes={routes} open={open} />
            {!open && (
              <div className="flex cursor-pointer justify-center mt-[30px]" onClick={onLogOut}>
                <img src={logOutIcon} className="w-[24px] h-[24px] my-auto" />
              </div>
            )}
            <div className="fixed bottom-0 flex justify-between w-[384px] px-[24px] pb-[49px]">
              <div className="flex">
                <img src={twitterIcon} className="cursor-pointer" />
                <img src={linkedInIcon} className="cursor-pointer ml-[6px]" />
              </div>

              {open && (
                <div className="flex cursor-pointer" onClick={onLogOut}>
                  <p className="w-[60px] my-auto text-[15px] ">Log Out</p>
                  <img src={logOutIcon} className="w-[24px] h-[24px] my-auto" />
                </div>
              )}
            </div>
          </div>
        </div>
        <main
          className={classNames('relative transition-all ease-in-out delay-150 duration-300 ', {
            'ml-[180px]': !open,
            'ml-[424px]': open,
          })}
        >
          <div
            className={classNames(
              'fixed w-[68%] bg-[#FEFEFE] my-[24px] z-[2] transition-all ease-in-out delay-150 duration-300',
              { 'w-[88%]': !open }
            )}
          >
            <Headers></Headers>
          </div>
          <div
            className={classNames({
              'pt-[125px] transition-all ease-in-out delay-150 duration-300': true,
              'w-[98%]': !open,
              'w-[90%]': open,
            })}
          >
            <PrivateRoutes routes={routes} />
          </div>
        </main>
      </div>
    </Router>
  );
}
