import * as React from 'react';
import DataTable from 'components/DataTable';
import { HeadCell } from 'components/DataTable/EnhancedTableHead';

interface Data {
  name: string;
  calories: number;
  fat: number;
  carbs: number;
  protein: number;
}
function createData(name: string, calories: number, fat: number, carbs: number, protein: number): Data {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Donut', 452, 25.0, 51, 4.9),
  createData('Eclair', 262, 16.0, 24, 6.0),
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
  createData('Honeycomb', 408, 3.2, 87, 6.5),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createData('Jelly Bean', 375, 0.0, 94, 0.0),
  createData('KitKat', 518, 26.0, 65, 7.0),
  createData('Lollipop', 392, 0.2, 98, 0.0),
  createData('Marshmallow', 318, 0, 81, 2.0),
  createData('Nougat', 360, 19.0, 9, 37.0),
  createData('Oreo', 437, 18.0, 63, 4.0),
];

const renderProtein = (data, index) => {
  return (
    <p>
      asdasd{index} {data.protein}
    </p>
  );
};

const headCells: HeadCell[] = [
  {
    id: 'name',
    numeric: false,
    disablePadding: true,
    label: 'Task Name',
    // align: 'right',
  },
  {
    id: 'calories',
    numeric: true,
    disablePadding: false,
    label: 'Deadline',
    align: 'center',
  },
  {
    id: 'fat',
    numeric: true,
    disablePadding: false,
    label: 'Projects',
    align: 'center',
  },
  {
    id: 'protein',
    numeric: true,
    disablePadding: false,
    label: 'Protein (g)',
    align: 'center',
    renderCell: renderProtein,
  },
];
export default function PendingTasks() {
  return (
    <DataTable
      headCells={headCells}
      rows={rows}
      tableTitle={'All Your Pending Tasks'}
      pagination={true}
      onCheckBoxClicked={(data) => console.log(data)}
    />
  );
}
