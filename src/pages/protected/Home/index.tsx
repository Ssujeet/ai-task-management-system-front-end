import React from 'react';
import { connect } from 'react-redux';
import PendingTasks from './PendingTasks';
import { Redirect } from 'react-router-dom';
const Home = (props) => {
  // return <PendingTasks />;
  return <Redirect to="/work-space" />;
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
