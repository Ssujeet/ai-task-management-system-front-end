import React from 'react';
import classNames from 'classnames';

export const Accordian = ({ title, children }) => {
  const [isActive, setIsActive] = React.useState(true);

  return (
    <>
      <div className="cursor-pointer" onClick={() => setIsActive(!isActive)}>
        <h2 className={classNames('text-xl  ', { 'border-b-2 pb-4': isActive })}>{title}</h2>
      </div>
      <div className={classNames({ hidden: !isActive })}>{children}</div>
    </>
  );
};
