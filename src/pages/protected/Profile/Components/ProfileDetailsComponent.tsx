import React from 'react';
import ProfileTabs from './ProfileTabs';

function ProfileDetailsComponent() {
  return (
    <div>
      <ProfileTabs />
    </div>
  );
}

export default ProfileDetailsComponent;
