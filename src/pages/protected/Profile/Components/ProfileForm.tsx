import React from 'react';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';
import { useFormik } from 'formik';
import { FormikProps } from 'formik';
import { Grid, makeStyles, createStyles, Theme } from '@material-ui/core';
import FormikValidationError from 'components/FormikErrors';
import Select from 'components/Select/SelectWithAutoComplete';
interface IProfileForms {
  firstName: string;
  lastName: string;
  address: string;
  experience: number;
  expertiseOn: Array<any>;
}

interface IProfileFormsProps extends FormikProps<IProfileForms> {}

export function ProfileForm(props: IProfileFormsProps) {
  const onInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    props.setFieldValue(event.target.name, event.target.value);
  };

  return (
    <div>
      <Grid
        container
        direction="row"
        // justifyContent="space-between"
        spacing={4}
        //   alignItems="flex-start"
      >
        <Grid item xs={5} className="mt-3">
          <FormControl variant="filled" style={{ width: '100%' }}>
            <InputLabel htmlFor="component-filled">First Name</InputLabel>
            <FilledInput
              id="component-filled"
              value={props.values.firstName}
              onChange={onInputChange}
              aria-describedby="component-error-text"
              name="firstName"
            />

            <FormikValidationError name="firstName" errors={props.errors} touched={props.touched} />
          </FormControl>
        </Grid>
        <Grid item xs={5} className="mt-3">
          <FormControl variant="filled" style={{ width: '100%' }}>
            <InputLabel htmlFor="component-filled">Last Name</InputLabel>
            <FilledInput
              id="component-filled"
              value={props.values.lastName}
              onChange={onInputChange}
              aria-describedby="component-error-text"
              name="lastName"
            />

            <FormikValidationError name="lastName" errors={props.errors} touched={props.touched} />
          </FormControl>
        </Grid>
        <Grid item xs={5} className="mt-3">
          <FormControl variant="filled" style={{ width: '100%' }}>
            <InputLabel htmlFor="component-filled">Address</InputLabel>
            <FilledInput
              id="component-filled"
              value={props.values.address}
              onChange={onInputChange}
              aria-describedby="component-error-text"
              name="address"
            />

            <FormikValidationError name="address" errors={props.errors} touched={props.touched} />
          </FormControl>
        </Grid>
        <Grid item xs={5} className="mt-3">
          <FormControl variant="filled" style={{ width: '100%' }}>
            <InputLabel htmlFor="component-filled">Working Experience (in Years)</InputLabel>
            <FilledInput
              id="component-filled"
              value={props.values.experience}
              onChange={onInputChange}
              aria-describedby="component-error-text"
              name="experience"
              type="number"
            />

            <FormikValidationError name="experience" errors={props.errors} touched={props.touched} />
          </FormControl>
        </Grid>

        <Grid item xs={12} className="mt-3">
          <Select
            isMulti={true}
            label="Select Skills"
            name="expertiseOn"
            value={props.values.expertiseOn}
            onChange={(value) => props.setFieldValue('expertiseOn', value)}
            options={[{ label: 'High', value: '2' }]}
          />
          <FormikValidationError name="expertiseOn" errors={props.errors} touched={props.touched} />
        </Grid>
      </Grid>
    </div>
  );
}

interface IProfileFormProps {
  formData?: IProfileForms;
}

export const ProfileFormWithFormik = (props: IProfileFormProps) => {
  const { formData } = props;
  const initialValues: IProfileForms = {
    firstName: 'Sujeet',
    lastName: 'Singh',
    address: 'Kalanki, Kathmandu',
    experience: 0,
    expertiseOn: [],
  };
  const formik = useFormik({
    initialValues: formData ? formData : initialValues,
    onSubmit: (values) => {
      alert(JSON.stringify(values, null, 2));
    },
  });

  return (
    <>
      <ProfileForm {...formik} />
    </>
  );
};
