import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import Switch from '@material-ui/core/Switch';

const settingsLists = [
  {
    title: 'Notification',
    name: 'isNotificationsOn',
  },
  {
    title: 'Profile Private',
    name: 'profilePrivate',
  },
  {
    title: 'Status',
    name: 'activeStatus',
  },
];
function ProfileSetting() {
  const [state, setState] = React.useState({
    isNotificationsOn: true,
    profilePrivate: true,
    activeStatus: true,
  });

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  return (
    <div>
      <Grid container direction="column">
        {settingsLists.map((item) => {
          return (
            <Grid
              item
              container
              key={item.name}
              xs={6}
              alignContent="center"
              justifyContent="space-between"
              style={{ backgroundColor: '#fafafa', padding: 10, marginTop: 4 }}
            >
              <Grid item xs={8}>
                <Typography variant="body1">{item.title}</Typography>
              </Grid>
              <Grid item>
                <Switch
                  checked={state[item.name]}
                  onChange={handleChange}
                  color="primary"
                  name={item.name}
                  inputProps={{ 'aria-label': 'primary checkbox' }}
                />
              </Grid>
            </Grid>
          );
        })}
      </Grid>
    </div>
  );
}

export default ProfileSetting;
