import React from 'react';
import { makeStyles, Theme, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import SwipeableViews from 'react-swipeable-views';
import { ProfileFormWithFormik } from './ProfileForm';
import ProfileSetting from './ProfileSetting';
import { BorderColor } from '@material-ui/icons';
import { Grid, Divider } from '@material-ui/core';
import Button from '@material-ui/core/Button';
interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: '#ffffff',
    borderWidth: 1,
    BorderColor: theme.palette.common.black,
    // backgroundColor: theme.palette.background.paper,
  },
  tabTitle: {
    textTransform: 'none',
  },
}));

export default function ProfileTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const theme = useTheme();

  const handleChange = (event: React.ChangeEvent<any>, newValue: number) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs
          value={value}
          indicatorColor="primary"
          textColor="primary"
          onChange={handleChange}
          style={{ backgroundColor: '#ffffff' }}
          aria-label="simple tabs example"
        >
          <Tab className={classes.tabTitle} label="Profile" {...a11yProps(0)} />
          <Tab className={classes.tabTitle} label="Settings" {...a11yProps(1)} />
        </Tabs>
      </AppBar>
      <SwipeableViews axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'} index={value} onChangeIndex={handleChange}>
        <TabPanel value={value} index={0}>
          <div>
            <ProfileFormWithFormik />
            <Divider className="mt-4" />
            <Grid container justifyContent="flex-end" className="mt-4">
              <Button
                variant="contained"
                onClick={() => console.log()}
                color="primary"
                style={{ textTransform: 'none' }}
              >
                Update
              </Button>
            </Grid>
          </div>
        </TabPanel>
        <TabPanel value={value} index={1}>
          <ProfileSetting />
        </TabPanel>
      </SwipeableViews>
    </div>
  );
}
