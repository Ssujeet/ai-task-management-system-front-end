import React from 'react';
import { Grid, Avatar } from '@material-ui/core';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    large: {
      width: theme.spacing(15),
      height: theme.spacing(15),
    },
  })
);
function ProfilesComponent() {
  const classes = useStyles();

  return (
    <div>
      <Grid container direction="column">
        <Grid>
          <Avatar
            alt={`Avatar n°${1}`}
            src={`https://material-ui.com//static/images/avatar/${1}.jpg`}
            className={classes.large}
          />
        </Grid>
      </Grid>
    </div>
  );
}

export default ProfilesComponent;
