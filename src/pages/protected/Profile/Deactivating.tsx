import React from 'react';
import { Accordian } from './Components/Accordian';
import { SettingChildComponentCommonInterface } from './Settings';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Checkbox } from '@material-ui/core';
interface IDeactivating extends SettingChildComponentCommonInterface {}

const Deactivating = (props: IDeactivating) => {
  const initialValues = {
    isAccountDeactivated: false,
  };

  const validationSchema = Yup.object().shape({
    isAccountDeactivated: Yup.boolean().oneOf([true], 'Please confirm the deactivation'),
  });

  const formik = useFormik({
    initialValues,
    enableReinitialize: true,
    validationSchema,
    onSubmit: (values) => {
      const { isAccountDeactivated } = values;
      props.onSubmit({ isAccountDeactivated });
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <Accordian title="Deactive Account">
        <div className="bg-yellow-50 mt-4 p-4">
          {/* <FiAlertCircle color="red"/> */}
          <p className="font-bold text-lg">You are deactivating your account</p>
          <p>
            For extra security, this requires you to confirm your email or phone number when you reset yousignr
            password.
          </p>
        </div>

        <div className="flex mt-4">
          {/* <input className="px-3 py-3 bg-gray-200 rounded-md" type="checkbox" checked={false} /> */}

          <Checkbox
            checked={formik.values.isAccountDeactivated}
            name="isAccountDeactivated"
            onChange={formik.handleChange}
          />

          <span className="ml-4 mt-auto mt-2 flex flex-col justify-end">I confirm my account deactivation</span>
        </div>
        <div className="flex flex-row justify-end w-full mt-8 border-t-2 pt-5">
          <button className="ml-4 w-40 cursor-pointer px-4 py-2 bg-red-400 text-white rounded-lg" type="submit">
            Deactivate Account
          </button>
        </div>
      </Accordian>
    </form>
  );
};

export default Deactivating;
