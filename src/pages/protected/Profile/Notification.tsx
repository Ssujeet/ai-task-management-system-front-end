import React from 'react';
import { Accordian } from './Components/Accordian';
import { SettingChildComponentCommonInterface } from './Settings';
import { useFormik } from 'formik';
import { useGetCurrentUserProfileQuery } from 'redux/reducers/user-profile';
import { Checkbox } from '@material-ui/core';

interface INotification extends SettingChildComponentCommonInterface {}
const Notification = (props: INotification) => {
  const initialValues = {
    isEmailNotificationOn: false,
    showAsActive: false,
    newsLaterNotification: false,
  };
  const { data: userProfile } = useGetCurrentUserProfileQuery();

  const { onSubmit } = props;
  const formik = useFormik({
    initialValues: userProfile?.data ? userProfile?.data : initialValues,
    enableReinitialize: true,
    onSubmit: (values) => {
      const { isEmailNotificationOn, newsLaterNotification, showAsActive } = values;
      onSubmit({ isEmailNotificationOn, newsLaterNotification, showAsActive });
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <Accordian title="Notification">
        <div className="flex mt-4 justify-between">
          <p className="w-3/12 text-sm my-auto">Email</p>
          {/* <input className="px-3 py-3 bg-gray-200 rounded-md w-1/2" type="checkbox" checked={false} /> */}
          <Checkbox
            checked={formik.values.isEmailNotificationOn}
            name="isEmailNotificationOn"
            onChange={formik.handleChange}
          />
        </div>
        <div className="flex mt-4 justify-between">
          <p className="w-3/12 text-sm my-auto">Show Active</p>
          <Checkbox checked={formik.values.showAsActive} name="showAsActive" onChange={formik.handleChange} />

          {/* <input className="px-3 py-3 bg-gray-200 rounded-md w-1/2" type="checkbox" checked={true} /> */}
        </div>
        <div className="flex mt-4 justify-between">
          <p className="w-3/12 text-sm my-auto">Newslater</p>
          {/* <input className="px-3 py-3 bg-gray-200 rounded-md w-1/2" type="checkbox" checked={true} /> */}
          <Checkbox
            checked={formik.values.newsLaterNotification}
            name="newsLaterNotification"
            onChange={formik.handleChange}
          />
        </div>

        <div className="flex flex-row justify-end w-full mt-8 border-t-2 pt-5">
          <button
            className="w-40 cursor-pointer px-4 py-2 text-gray-700 hover:text-blue-700 hover:border-2 hover:border-blue-700 rounded-lg"
            type="button"
            onClick={() => formik.resetForm()}
          >
            Reset
          </button>
          <button className="ml-4 w-40 cursor-pointer px-4 py-2 bg-red-400 text-white rounded-lg" type="submit">
            Save Changes
          </button>
        </div>
      </Accordian>
    </form>
  );
};

export default Notification;
