import React from 'react';
import { useGetCurrentUserProfileQuery } from 'redux/reducers/user-profile';
import { userProfileName } from './UserProfileForms';

const Overview = () => {
  const { data: userProfile } = useGetCurrentUserProfileQuery();

  return (
    <div className="bg-white p-4">
      <div className="flex justify-between  w-full border-b-2 pb-4">
        <h1 className="font-bold text-xl">Profile Details</h1>
        <div>
          <button className="bg-blue-600 text-white px-8 py-2 rounded-md text-lg">Edit Profile</button>
        </div>
      </div>
      {userProfile?.data ? (
        <div className="mt-10">
          {userProfileName.map((item, index) => {
            return (
              <>
                <div className="flex mt-6" key={index}>
                  <p className="w-3/12 text-sm">{item?.title}</p>
                  {userProfile?.data && <p className="font-bold text-sm">{userProfile?.data[item?.name]}</p>}
                </div>
              </>
            );
          })}
        </div>
      ) : (
        <>You don't have setup profile</>
      )}
    </div>
  );
};

export default Overview;
