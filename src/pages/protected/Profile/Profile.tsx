import { FileDrop } from 'components/FileDrop/fileDrop';
import { useFormik } from 'formik';
import React from 'react';
import { toast } from 'react-toastify';
import {
  useAddUserProfileDetailsMutation,
  useAddUserProfilePictureMutation,
  useGetCurrentUserProfileQuery,
} from 'redux/reducers/user-profile';
import * as Yup from 'yup';
import MyEditor from 'components/Editor';
import { EditorState, convertFromHTML, ContentState, convertFromRaw } from 'draft-js';

import htmlToDraft from 'html-to-draftjs';

const initialValues = {
  firstName: '',
  company: '',
  lastName: '',
  country: '',
  email: '',
  phoneNumber: '',
};
const Profile = () => {
  const fileRef = React.useRef<any>();

  const [formState, setFormState] = React.useState(initialValues);
  const [editorValue, setEditorValue] = React.useState<any>('');

  const { data: userProfile } = useGetCurrentUserProfileQuery();
  const [addUserProfilePicture] = useAddUserProfilePictureMutation();

  const [addUserProfileDetails] = useAddUserProfileDetailsMutation();

  const onSubmit = (value: any) => {
    addUserProfileDetails(value);
  };

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: formState,
    onSubmit: (values, { resetForm }) => {
      onSubmit({
        ...values,
        fullName: values.firstName + ' ' + values.lastName,
        phoneNumber: values.phoneNumber.toString(),
        bio: editorValue,
      });
    },
    validationSchema: Yup.object().shape({
      firstName: Yup.string().max(255).required('First Name is required'),
      lastName: Yup.string().max(255).required('Last Name is required'),
      email: Yup.string().email().required('Email is required'),
    }),
  });

  React.useEffect(() => {
    if (userProfile?.data) {
      const userDetails = {
        firstName: userProfile.data?.fullName.split(' ')[0] || '',
        lastName: userProfile.data?.fullName.split(' ')[1] || '',
        company: userProfile.data?.company,
        country: userProfile.data?.country,
        email: userProfile.data?.user?.email || '',
        phoneNumber: userProfile.data?.phoneNumber,
      };
      if (userProfile?.data?.bio) {
        const contentDataState = ContentState.createFromBlockArray(convertFromHTML(userProfile?.data?.bio) as any);
        const editorDataState = EditorState.createWithContent(contentDataState);
        setEditorValue(editorDataState);
      }

      setFormState(userDetails);
    }
  }, [userProfile]);

  const handleChange = (e) => {
    console.log(e, 'e');
    const [file] = e.target.files;
    const id = toast.loading('Your profile is being uploaded');

    addUserProfilePicture({ file: file })
      .unwrap()
      .then(() => {
        toast.update(id, {
          render: 'Profile uploaded successfully',
          type: 'success',
          isLoading: false,
          autoClose: 800,
        });
      })
      .catch((e) => {
        toast.update(id, { render: 'Profile upload failed', type: 'error', isLoading: false, autoClose: 800 });
      });
  };

  return (
    <form onSubmit={formik.handleSubmit}>
      <div className="mt-[47px]">
        <div className="flex  items-center justify-between">
          <div>
            <h4 className="font-[600] text-[24px]">General Details</h4>

            <p className="text-gray-700">Update your photo and personal details here</p>
          </div>

          <div>
            <button
              type="submit"
              className=" border-[#F95868]  hover:bg-[#F95868] hover:text-white border px-[44px] py-[10px] text-[16px] flex ml-auto rounded-[4px]  text-[#F95868]"
            >
              Save
            </button>
          </div>
        </div>

        <div className="flex mt-[20px] gap-[40px]">
          <div className="w-8/12 shadow-md  rounded-[8px]">
            <h4 className="font-[500] text-[22px] border-b pb-[12px] p-[20px]">Personal Information</h4>

            <div className="p-[20px]">
              <div className="flex flex-col mt-[12px]">
                <label className="text-[16px] font-[500]">Full Name*</label>
                <div className="flex gap-[12px]">
                  <div className="w-full">
                    <input
                      type="text"
                      name="firstName"
                      placeholder="First Name"
                      className="border w-full focus:outline-none  rounded-[8px] px-[24px] py-[12px] mt-[7px]"
                      onBlur={formik.handleBlur}
                      value={formik.values.firstName}
                      onChange={formik.handleChange}
                    />

                    {Boolean(formik.touched.firstName && formik.errors.firstName) && (
                      <span className="text-sm text-red-500">
                        {formik.touched.firstName && formik.errors.firstName}
                      </span>
                    )}
                  </div>

                  <div className="w-full">
                    <input
                      type="text"
                      name="lastName"
                      placeholder="Last Name"
                      className="border rounded-[8px] focus:outline-none w-full px-[24px] py-[12px] mt-[7px]"
                      onBlur={formik.handleBlur}
                      value={formik.values.lastName}
                      onChange={formik.handleChange}
                    />

                    {Boolean(formik.touched.lastName && formik.errors.lastName) && (
                      <span className="text-sm text-red-500">{formik.touched.lastName && formik.errors.lastName}</span>
                    )}
                  </div>
                </div>
              </div>
              <div className="flex gap-[12px]">
                <div className="flex w-full flex-col mt-[18px]">
                  <label className="text-[16px] font-[500]">Email Address</label>
                  <input
                    type="text"
                    name="email"
                    className="border w-full focus:outline-none rounded-[8px] px-[24px] py-[12px] mt-[7px]"
                    onBlur={formik.handleBlur}
                    value={formik.values.email}
                    onChange={formik.handleChange}
                  />

                  {Boolean(formik.touched.email && formik.errors.email) && (
                    <span className="text-sm text-red-500">{formik.touched.email && formik.errors.email}</span>
                  )}
                </div>
                <div className="flex  w-full flex-col mt-[18px]">
                  <label className="text-[16px] font-[500]">Company*</label>
                  <input
                    type="text"
                    name="company"
                    className="border w-full focus:outline-none rounded-[8px] px-[24px] py-[12px] mt-[7px]"
                    onBlur={formik.handleBlur}
                    value={formik.values.company}
                    onChange={formik.handleChange}
                  />

                  {Boolean(formik.touched.company && formik.errors.company) && (
                    <span className="text-sm text-red-500">{formik.touched.company && formik.errors.company}</span>
                  )}
                </div>
              </div>

              <div className="flex flex-col mt-[18px]">
                <label className="text-[16px] font-[500]  mb-[7px]">Bio*</label>
                <MyEditor onChangeValue={setEditorValue} editorValue={editorValue} />
              </div>
              <div className="flex gap-[12px]">
                <div className="flex w-full flex-col mt-[18px]">
                  <label className="text-[16px] font-[500]">Country*</label>
                  <input
                    type="text"
                    name="country"
                    className="border w-full focus:outline-none rounded-[8px] px-[24px] py-[12px] mt-[7px]"
                    onBlur={formik.handleBlur}
                    value={formik.values.country}
                    onChange={formik.handleChange}
                  />

                  {Boolean(formik.touched.country && formik.errors.country) && (
                    <span className="text-sm text-red-500">{formik.touched.country && formik.errors.country}</span>
                  )}
                </div>

                <div className="flex w-full flex-col mt-[18px]">
                  <label className="text-[16px] font-[500]">Phone Number*</label>
                  <input
                    type="number"
                    name="phoneNumber"
                    className="border w-full focus:outline-none rounded-[8px] px-[24px] py-[12px] mt-[7px]"
                    onBlur={formik.handleBlur}
                    value={formik.values.phoneNumber}
                    onChange={formik.handleChange}
                  />

                  {Boolean(formik.touched.phoneNumber && formik.errors.phoneNumber) && (
                    <span className="text-sm text-red-500">
                      {formik.touched.phoneNumber && formik.errors.phoneNumber}
                    </span>
                  )}
                </div>
              </div>
            </div>
          </div>

          <div className="w-4/12">
            <div className="w-full shadow-md rounded-[8px]">
              <p className="text-[22px] font-[500] border-b  p-[20px] ">Your photo</p>
              <div className=" p-[20px] ">
                <div className="flex mt-[20px]">
                  <img
                    src={
                      userProfile?.data?.userPictureUrl ??
                      'https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/User-avatar.svg/1024px-User-avatar.svg.png'
                    }
                    className="w-[80px] h-[80px] rounded-[50%]"
                  />

                  <div className=" flex w-full items-center ml-[14px]">
                    <div>
                      <p>Edit your photo</p>

                      <div className="flex gap-[10px]">
                        {/* <p>Delete</p> */}
                        <p>
                          <button
                            className="text-[13px] opacity-50 hover:opacity-90"
                            onClick={() => fileRef?.current?.click()}
                          >
                            <input
                              ref={fileRef}
                              className="hidden "
                              onChange={handleChange}
                              multiple={false}
                              type="file"
                              hidden
                              accept="image/png, image/gif, image/jpeg"
                            />
                            Update
                          </button>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="mt-[20px]">
                  <FileDrop
                    setFiles={(file) => {
                      console.log(file, 'file');
                      handleChange({
                        target: { files: file },
                      });
                    }}
                    message={'Click to upload or Drag avatar in JPEG or PNG format '}
                    files={[]}
                    accept={{ 'image/png': ['.png', '.jgp', '.jpeg', '.gif'] }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  );
};

export default Profile;
