import React from 'react';
import { Accordian } from './Components/Accordian';
import UserProfileForms from './UserProfileForms';
import SignMethod from './SignMethod';
import Notification from './Notification';
import Deactivating from './Deactivating';
import { useAddUserProfileDetailsMutation } from 'redux/reducers/user-profile';

export interface SettingChildComponentCommonInterface {
  onSubmit: (value) => void;
}

const Settings = () => {
  const [addUserProfileDetails] = useAddUserProfileDetailsMutation();

  const onSubmit = (value: any) => {
    addUserProfileDetails(value);
  };

  return (
    <div>
      <div className="bg-white p-4">
        <UserProfileForms onSubmit={onSubmit} />
      </div>

      {/* <div className="bg-white p-4 mt-4">
        <SignMethod onSubmit={onSubmit} />
      </div> */}

      <div className="bg-white p-4 mt-4">
        <Notification onSubmit={onSubmit} />
      </div>

      {/* <div className="bg-white p-4 mt-4">
        <Deactivating onSubmit={onSubmit} />
      </div> */}
    </div>
  );
};

export default Settings;
