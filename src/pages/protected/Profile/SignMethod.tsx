import React from 'react';
import { Accordian } from './Components/Accordian';
import { SettingChildComponentCommonInterface } from './Settings';

interface ISignMethod extends SettingChildComponentCommonInterface {}

const SignMethod = (props: ISignMethod) => {
  return (
    <div>
      {' '}
      <Accordian title="Sign In Method">
        <div className="flex mt-4">
          <p className="w-3/12 text-sm my-auto">
            Email <span className="text-red-600">*</span>{' '}
          </p>
          <input className="px-3 py-3 bg-gray-200 rounded-md w-1/2" type="email" value={'ssinge123@gmail.com'} />

          <button className="w-40 cursor-pointer px-4 py-2 bg-red-400 ml-auto text-white rounded-lg">
            Change Email
          </button>
        </div>

        <div className="flex mt-4">
          <p className="w-3/12 text-sm my-auto">
            Password <span className="text-red-600">*</span>{' '}
          </p>
          <input className="px-3 py-3 bg-gray-200 rounded-md w-1/2" type="password" value={'********'} />

          <button className="w-40 cursor-pointer px-4 py-2 bg-red-400 ml-auto text-white rounded-lg">
            Change Password
          </button>
        </div>
      </Accordian>
    </div>
  );
};

export default SignMethod;
