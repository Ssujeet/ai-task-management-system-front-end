import React from 'react';
import { Accordian } from './Components/Accordian';
import { useFormik } from 'formik';
import { useGetCurrentUserProfileQuery } from 'redux/reducers/user-profile';
import { SettingChildComponentCommonInterface } from './Settings';
import { string as YupString, object as YupObject, number as YupNumber } from 'yup';

export const userProfileValidationSchema = YupObject().shape({
  fullName: YupString().required('Full Name is required'),
  company: YupString(),
  phoneNumber: YupNumber(),
  country: YupString(),
});

export const userProfileName = [
  {
    title: 'Full Name',
    value: 'Max Smith',
    name: 'fullName',
  },

  {
    title: 'Company',
    value: 'Keenthemes',
    name: 'company',
  },
  {
    title: 'Contact Phone',
    value: '044 3276 454 935',
    name: 'phoneNumber',
  },
  {
    title: 'Country',
    value: 'Germany',
    name: 'country',
  },
];

interface IUserProfileForms extends SettingChildComponentCommonInterface {}
const UserProfileForms = (props: IUserProfileForms) => {
  const { onSubmit } = props;

  const initialValues = {
    fullName: '',
    phoneNumber: '',
    country: '',
    company: '',
  };

  const { data: userProfile } = useGetCurrentUserProfileQuery();
  const formik = useFormik({
    initialValues: userProfile?.data ? userProfile?.data : initialValues,
    enableReinitialize: true,
    validationSchema: userProfileValidationSchema,
    onSubmit: (values) => {
      const { fullName, phoneNumber, country, company } = values;
      onSubmit({ fullName, phoneNumber, country, company });
    },
  });

  const { values, handleBlur, handleChange, touched, errors, handleSubmit, resetForm } = formik;

  return (
    <div>
      <Accordian title="Profile Detail">
        <form className="  " onSubmit={handleSubmit}>
          <div className="mt-10">
            {userProfileName.map((item, index) => {
              return (
                <>
                  <div className="flex mt-4" key={index}>
                    <p className="w-3/12 text-sm my-auto">
                      {item.title} <span className="text-red-600">*</span>{' '}
                    </p>
                    <input
                      className="px-3 py-3 bg-gray-200 rounded-md w-1/2"
                      onBlur={handleBlur}
                      value={values[item.name]}
                      onChange={handleChange}
                      name={item.name}
                    />
                    {Boolean(touched[item.name] && errors[item.name]) && (
                      <span className="text-sm text-red-500">{touched[item.name] && errors[item.name]}</span>
                    )}
                  </div>
                </>
              );
            })}
            <div className="flex flex-row justify-end w-full mt-8 border-t-2 pt-5">
              <button
                className="w-40 cursor-pointer px-4 py-2 text-gray-700 hover:text-blue-700 hover:border-2 hover:border-blue-700 rounded-lg"
                onClick={() => resetForm()}
                type="button"
              >
                Reset
              </button>
              <button className="ml-4 w-40 cursor-pointer px-4 py-2 bg-red-400 text-white rounded-lg" type="submit">
                Save Changes
              </button>
            </div>
          </div>
        </form>
      </Accordian>
    </div>
  );
};

export default UserProfileForms;
