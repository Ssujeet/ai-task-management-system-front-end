import React from 'react';
import { connect } from 'react-redux';
import { RootState } from 'redux/store';
import Overview from './Overview';
import classNames from 'classnames';
import { act } from '@testing-library/react';
import Settings from './Settings';
import EditIcon from '@material-ui/icons/Edit';
import { useGetCurrentUserProfileQuery, useAddUserProfilePictureMutation } from 'redux/reducers/user-profile';
import { addSideBarNav } from 'redux/reducers/sidebar';
import { useAppDispatch } from 'hooks';
import { toast } from 'react-toastify';

enum ProfileTab {
  Overview,
  Setting,
}
const Profile = (props) => {
  const fileRef = React.useRef<any>();
  const [activeTab, setActiveTab] = React.useState<ProfileTab>(ProfileTab.Overview);
  const { data: userProfile } = useGetCurrentUserProfileQuery();
  const [addUserProfilePicture] = useAddUserProfilePictureMutation();
  const dispatcher = useAppDispatch();
  dispatcher(addSideBarNav([]));

  const handleChange = (e) => {
    const [file] = e.target.files;
    const id = toast.loading('Your profile is being uploaded');

    addUserProfilePicture({ file: file })
      .unwrap()
      .then(() => {
        toast.update(id, {
          render: 'Profile uploaded successfully',
          type: 'success',
          isLoading: false,
          autoClose: 800,
        });
      })
      .catch((e) => {
        toast.update(id, { render: 'Profile upload failed', type: 'error', isLoading: false, autoClose: 800 });
      });
  };

  return (
    <div className="px-20 mt-5">
      <div className="bg-white p-10 ">
        <div className="flex">
          <div className="w-2/12 relative">
            <button
              onClick={() => fileRef?.current?.click()}
              className=" text-white px-1  py-1  rounded-[50%] absolute "
              style={{ backgroundColor: 'rgba(0, 0, 0, 0.5)' }}
            >
              <input
                ref={fileRef}
                className="hidden"
                onChange={handleChange}
                multiple={false}
                type="file"
                hidden
                accept="image/png, image/gif, image/jpeg"
              />
              <EditIcon />
            </button>
            <img
              src={
                userProfile?.data?.userPictureUrl ??
                'https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/User-avatar.svg/1024px-User-avatar.svg.png'
              }
              className="w-full h-full rounded-lg"
            />
          </div>

          <div className="ml-4">
            <p className="text-2xl font-bold">{userProfile?.data?.fullName || userProfile?.data?.user?.name}</p>

            <div>
              {/* <span className="text-gray-600">Developer</span>
              <span className=" ml-4 text-gray-600">Sf Bay Area</span> */}
              <span className=" text-gray-600">{userProfile?.data?.user?.email}</span>
            </div>
            <div className="mt-5">
              <div className="flex justify-between">
                <p>Profile Completion</p>
                <p>50%</p>
              </div>
              <div className="relative mt-2">
                <div className="absolute h-2 w-40 bg-green-400 z-100  rounded-2xl"></div>
                <div className="w-80 h-2 bg-gray-200 rounded-2xl"></div>
              </div>
            </div>
          </div>
        </div>

        <div className="flex mt-8">
          <p
            className={classNames('p-2 cursor-pointer hover:text-blue-700 ', {
              'border-b-2 text-blue-700 border-blue-700': activeTab === ProfileTab.Overview,
            })}
            onClick={() => setActiveTab(ProfileTab.Overview)}
          >
            Overview
          </p>

          <p
            className={classNames('ml-4 p-2 cursor-pointer  hover:text-blue-700 ', {
              'border-b-2 text-blue-700 border-blue-700': activeTab === ProfileTab.Setting,
            })}
            onClick={() => setActiveTab(ProfileTab.Setting)}
          >
            Settings
          </p>
        </div>
      </div>

      <div className="mt-4">
        {activeTab === ProfileTab.Overview && <Overview />}
        {activeTab === ProfileTab.Setting && <Settings />}
      </div>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
