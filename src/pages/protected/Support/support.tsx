import AddButton from 'components/Buttons/AddButton';
import React from 'react';
import { useGetListsOfSupportTicketsQuery, useAddSupportTicketsMutation } from 'redux/reducers/support-tickets';
import MaxWidthDialog from 'components/Modal';
import useToggle from 'hooks/useToggle';
import CrossIcon from 'assets/icons-svg/CrosIcon';
import MyEditor from 'components/Editor';
import { DialogContent, DialogActions, CircularProgress } from '@material-ui/core';
import { useGetCurrentUserQuery } from 'redux/reducers/user';
import classNames from 'classnames';
import { IRoutes } from 'routes/routesLists';
import PrivateRoutes from 'routes';
import { useHistory } from 'react-router-dom';
import { addSideBarNav } from 'redux/reducers/sidebar';
import { useAppDispatch } from 'hooks';

interface ISupport {
  childrenRoutes: IRoutes[];
  computedMatch: any;
}
const Support = (props: ISupport) => {
  const { childrenRoutes, computedMatch } = props;
  const { data } = useGetListsOfSupportTicketsQuery();
  const [editorValue, setEditorValue] = React.useState('');
  const [title, setTitle] = React.useState('');
  const history = useHistory();

  const [activeTab, setActiveTab] = React.useState(1);

  const { data: currentUser } = useGetCurrentUserQuery();

  const [addSupportTickets, { error, isLoading }] = useAddSupportTicketsMutation();

  const [supportTicketModal, toggleSupportTicketModal] = useToggle(false);

  const dispatcher = useAppDispatch();
  dispatcher(addSideBarNav([]));

  const onTicketSubmit = () => {
    addSupportTickets({ question: editorValue, title })
      .unwrap()
      .then(() => {
        setEditorValue('');
        toggleSupportTicketModal();
        setTitle('');
      });
  };

  const supportTickets = data
    ? activeTab === 1
      ? data.data
      : data?.data?.filter((item) => item?.createdUser?.id === currentUser?.data?.id)
    : [];

  if (computedMatch.isExact) {
    return (
      <>
        <MaxWidthDialog
          isModalOpen={supportTicketModal}
          toggleModal={toggleSupportTicketModal}
          renderTitle={() => {
            return (
              <div className="flex justify-between">
                <p className="text-[18px] font-[600]">Add Ticket</p>
                <div
                  className="bg-[#E5E5E5] cursor-pointer w-[24px] h-[24px] flex justify-between group rounded-[4px]"
                  onClick={() => toggleSupportTicketModal()}
                >
                  <CrossIcon />
                </div>
              </div>
            );
          }}
          submitLabel={'Save'}
          modalSize={'sm'}
          buttonType={'submit'}
        >
          <DialogContent>
            <div className="flex flex-col w-full">
              <label htmlFor="component-filled" className="text-[rgba(52, 52, 52, 0.6)]">
                Title *
              </label>
              <input
                id="component-filled"
                className="border rounded-[8px] px-[24px] py-[12px] mt-[7px] mb-[12px]"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
                aria-describedby="component-error-text"
                name="folderName"
                type="text"
              />
              <label htmlFor="component-filled" className="text-[rgba(52, 52, 52, 0.6)] mb-[10px]">
                Descriptions
              </label>

              <MyEditor onChangeValue={setEditorValue} editorValue={editorValue} />
            </div>
          </DialogContent>
          <DialogActions>
            <div className="flex justify-between w-full mt-[58px] mb-[35px] px-[10px]">
              <button
                color="primary"
                type={'button'}
                className="bg-[#F85365] text-white px-[57px] py-[12px] rounded-[4px]"
                onClick={onTicketSubmit}
              >
                {isLoading ? <CircularProgress /> : 'Submit'}
              </button>
              <button
                onClick={() => toggleSupportTicketModal()}
                type={'button'}
                color="secondary"
                style={{ textTransform: 'none' }}
              >
                Close
              </button>
            </div>
          </DialogActions>
        </MaxWidthDialog>
        <div className="flex justify-between mt-[41px] ">
          <p className="text-[24px] font-[500]">Support Tickets</p>
          <AddButton name="Add Tickets" onClick={toggleSupportTicketModal} />
        </div>

        <div className="flex">
          <p
            className={classNames('bg-red-100 p-2 rounded-sm cursor-pointer', { 'bg-red-200': activeTab === 1 })}
            onClick={() => setActiveTab(1)}
          >
            All Tickets
          </p>
          <p
            className={classNames('bg-red-100 p-2 ml-2 rounded-sm cursor-pointer', { 'bg-red-200': activeTab === 2 })}
            onClick={() => setActiveTab(2)}
          >
            Your Tickets
          </p>
        </div>

        <div className="flex">
          {supportTickets?.map((item) => {
            return (
              <div
                className="p-[20px] shadow-lg cursor-pointer"
                onClick={() => history.push('support-ticket/' + item.id)}
              >
                <p>{item.title}</p>
              </div>
            );
          })}
        </div>
      </>
    );
  }

  return <PrivateRoutes routes={childrenRoutes} />;
};

export default Support;
