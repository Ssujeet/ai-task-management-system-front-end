import React from 'react';
import { IRoutes, routes } from 'routes/routesLists';
import MiniDrawer from 'pages/protected/Dasboard';
import FileViewerListsWithModal from 'components/FileViewerListsWithModal';

interface IProtected {
  childrenRoutes: IRoutes[];
}

const Protected = (props: IProtected) => {
  const { childrenRoutes } = props;

  return (
    <>
      <FileViewerListsWithModal />
      <MiniDrawer routes={childrenRoutes} />
    </>
  );
};

export default Protected;
