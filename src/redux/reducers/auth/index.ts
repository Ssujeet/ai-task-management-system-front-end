// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery, convertJsonToFormData } from 'redux/reducers/settings';
import { BaseResponse } from 'common/types';
import { ROLE } from 'common/enum';

export interface LoginInterface {
  username: string;
  password: string;
}

export interface SignUpInterface {
  name: string;
  username: string;
  password: string;
  email: string;
  fullName: string;
}

export interface AuthResponse {
  accessToken: string;
  refreshToken: string;
}

export interface SignUpResponse {
  id: number;
  name: string;
  username: string;
  roles: ROLE[];
  email: string;
  isAccountDisabled: true;
  createdAt: string;
  updatedAt: string;
}

interface LoginWithGoogleInterface {
  token: string;
}

export const authenticationApi = createApi({
  reducerPath: 'authenticationApi',
  baseQuery: baseQuery,
  tagTypes: ['Posts'],
  endpoints: (build) => ({
    login: build.mutation<BaseResponse<AuthResponse>, LoginInterface>({
      query(body) {
        return {
          url: `auth/login`,
          method: 'POST',
          body,
        };
      },
    }),
    forgotPassword: build.mutation<BaseResponse<{ message: string }>, { email: string }>({
      query(body) {
        return {
          url: `auth/forgot-password`,
          method: 'POST',
          body,
        };
      },
    }),

    verifyEmail: build.mutation<BaseResponse<{ message: string }>, { token: string }>({
      query(body) {
        return {
          url: `auth/verify-email`,
          method: 'POST',
          body,
        };
      },
    }),

    resetPassword: build.mutation<BaseResponse<{ message: string }>, { password: string; token: string }>({
      query(body) {
        return {
          url: `auth/reset-password`,
          method: 'POST',
          body,
        };
      },
    }),
    loginWithGoogle: build.mutation<AuthResponse, LoginWithGoogleInterface>({
      query(body) {
        return {
          url: `google-authentication`,
          method: 'POST',
          body,
        };
      },
    }),

    signUp: build.mutation<BaseResponse<SignUpResponse>, SignUpInterface>({
      query(body) {
        return {
          url: `auth/register`,
          method: 'POST',
          body,
        };
      },
    }),
  }),
});

export const {
  useLoginMutation,
  useSignUpMutation,
  useLoginWithGoogleMutation,
  useForgotPasswordMutation,
  useResetPasswordMutation,
  useVerifyEmailMutation,
} = authenticationApi;
