// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { AbstractInterface, BaseResponse } from 'common/types';
import { UserResponse } from '../user';
import { ProjectInterface } from '../projects';

export interface IChatMessageInput {
  projectId: number;
  receiverId: number;
  isGroupChat: boolean;
}

export interface ChatMessagesResponse extends AbstractInterface {
  sender: UserResponse;
  message: string;
  receiver: UserResponse;
  isGroupChat: boolean;
  project: ProjectInterface;
  fileUrl: string;
  isMessageSeen: boolean;
  isDelivered: boolean;
}

export interface IchatMessagesPicture {
  file: File;
}

export const chatMessagesApi = createApi({
  reducerPath: 'chatMessagesApi',
  baseQuery: baseQuery,
  tagTypes: ['chatMessages'],
  endpoints: (build) => ({
    getMessagesListBetweenUser: build.mutation<BaseResponse<ChatMessagesResponse[]>, IChatMessageInput>({
      query(body) {
        return {
          url: 'message-box/messages-lists',
          method: 'POST',
          body,
        };
      },
    }),
    getCurrentUserUnSeenMessageLists: build.query<BaseResponse<ChatMessagesResponse[]>, void>({
      query: () => `message-box/current-user-unseen-messages`,
      providesTags: [{ type: 'chatMessages', id: 'LIST' }],
    }),
    getCurrentUserUnSeenMessageListsForAProject: build.query<BaseResponse<ChatMessagesResponse[]>, string>({
      query: (projectId) => `message-box/current-user-unseen-messages/${projectId}`,
      providesTags: [{ type: 'chatMessages', id: 'LIST' }],
    }),
    makeMessageSeenForGivenUserId: build.mutation<BaseResponse<ChatMessagesResponse>, string>({
      query(userId) {
        return {
          url: `message-box/see-messages/${userId}`,
          method: 'POST',
        };
      },
      invalidatesTags: [{ type: 'chatMessages', id: 'LIST' }],
    }),
  }),
});

export const {
  useGetMessagesListBetweenUserMutation,
  useGetCurrentUserUnSeenMessageListsQuery,
  useMakeMessageSeenForGivenUserIdMutation,
  useGetCurrentUserUnSeenMessageListsForAProjectQuery,
} = chatMessagesApi;
