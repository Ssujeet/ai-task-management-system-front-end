import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const initialState = {
  showFileViewer: false,
  filesLists: [],
  currentActiveFileIndex: 0,
};

export const fileViewerSlice = createSlice({
  name: 'fileViewer',
  initialState,
  reducers: {
    addFilesInViewer: (state, action: PayloadAction<{ activeIndex: number; fileLists: any }>) => {
      state.showFileViewer = true;
      state.currentActiveFileIndex = action.payload.activeIndex;
      state.filesLists = action.payload.fileLists;
    },
    closeFilesViewer: (state) => {
      state.showFileViewer = false;
      state.currentActiveFileIndex = 0;
      state.filesLists = [];
    },
  },
});
export const { addFilesInViewer, closeFilesViewer } = fileViewerSlice.actions;

export default fileViewerSlice.reducer;
