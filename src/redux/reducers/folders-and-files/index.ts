import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery, convertJsonToFormData } from 'redux/reducers/settings';
import { BaseResponse } from 'common/types';
import { ProjectOutputInterface } from 'redux/reducers/projects/index';

import { UserResponse } from '../user';

export interface IFolderInput {
  folderName: string;
  parentFolderId: number;
  projectId: number;
  id?: string;
}

export interface IFilesInput {
  parentFolderId: number;
  files: File[];
}
export interface IFilesResponse {
  id?: number;
  fileName: string;
  mimeType: string;
  size: string;
  url: string;
  folder: IFolderInput;
  uploadedBy: UserResponse;
  key: string;
}

export interface DeleteFilesIdI {
  id: number[];
}

export const folderAndFilesApi = createApi({
  reducerPath: 'folderAndFilesApi',
  baseQuery: baseQuery,
  tagTypes: ['folders', 'files'],
  endpoints: (build) => ({
    getFoldersListsByParentId: build.query<BaseResponse<IFolderInput[]>, string>({
      query: (parentId) => `projects-folders/folders-lists/parent-folder/${parentId}`,
      providesTags: (result) =>
        result?.data
          ? [...result?.data?.map(({ id }) => ({ type: 'folders', id } as const)), { type: 'folders', id: 'LIST' }]
          : [{ type: 'folders', id: 'LIST' }],
    }),

    addFolder: build.mutation<BaseResponse<IFolderInput>, IFolderInput>({
      query(body) {
        return {
          url: `projects-folders`,
          method: 'POST',
          body,
        };
      },
      invalidatesTags: [{ type: 'folders', id: 'LIST' }],
    }),

    getRootFoldersOfProjectById: build.query<BaseResponse<IFolderInput>, string>({
      query: (projectId) => `/projects-folders/root-folder/project/${projectId}`,
    }),

    updateFolder: build.mutation<BaseResponse<IFolderInput>, { folderName: string; id: number }>({
      query: (body) => {
        return {
          url: `/projects-folders/update`,
          method: 'PUT',
          body,
        };
      },
      invalidatesTags: [{ type: 'folders', id: 'LIST' }],
    }),

    deleteFolderById: build.mutation<BaseResponse<IFolderInput>, string>({
      query: (projectId) => {
        return {
          url: `/projects-folders/${projectId}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: [{ type: 'folders', id: 'LIST' }],
    }),

    getCurrentFoldersParentLists: build.query<BaseResponse<IFolderInput[]>, string>({
      query: (currentFolderId) => `projects-folders/folders-lists/parent-folder-lists/${currentFolderId}`,
    }),

    getFilesListsByParentId: build.query<BaseResponse<IFilesResponse[]>, string>({
      query: (parentId) => `project-files/parent-folder/${parentId}`,
      providesTags: (result) =>
        result?.data
          ? [...result?.data?.map(({ id }) => ({ type: 'files', id } as const)), { type: 'files', id: 'LIST' }]
          : [{ type: 'files', id: 'LIST' }],
    }),

    addFiles: build.mutation<BaseResponse<IFilesResponse[]>, IFilesInput>({
      query(body) {
        const { parentFolderId, files } = body;

        return {
          url: `project-files/upload/parent-folder/${parentFolderId}`,
          method: 'POST',
          body: convertJsonToFormData({ files }),
        };
      },
      invalidatesTags: [{ type: 'files', id: 'LIST' }],
    }),
    deleteFiles: build.mutation<BaseResponse<IFilesResponse[]>, DeleteFilesIdI>({
      query(body) {
        return {
          url: `project-files/delete-files`,
          method: 'DELETE',
          body,
        };
      },
      invalidatesTags: [{ type: 'files', id: 'LIST' }],
    }),
  }),
});

export const {
  useGetFoldersListsByParentIdQuery,
  useAddFolderMutation,
  useGetRootFoldersOfProjectByIdQuery,
  useAddFilesMutation,
  useGetFilesListsByParentIdQuery,
  useGetCurrentFoldersParentListsQuery,
  useDeleteFolderByIdMutation,
  useUpdateFolderMutation,
  useDeleteFilesMutation,
} = folderAndFilesApi;
