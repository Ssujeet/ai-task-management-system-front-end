import { userApi } from './user';
import { taskApi } from './tasks';
import SideBarReducer from './sidebar';
import { standUpApi } from './standup';
import { combineReducers } from 'redux';
import { projectsApi } from './projects';
import { authenticationApi } from './auth';
import { tasksLabelApi } from './tasks-label';
import FileViewerReducer from './file-viewer';
import { postApi } from 'redux/reducers/posts';
import { userProfileApi } from './user-profile';
import { chatMessagesApi } from './chat-messages';
import socketReducer from 'redux/reducers/socket';
import { notificationsApi } from './notifications';
import { tasksCommentApi } from './tasks-comments';
import { projectMemberApi } from './projectMembers';
import { supportTicketsApi } from './support-tickets';
import { sprintPlanningApi } from './sprint-planning';
import { taskAttachmentApi } from './task-attachments';
import { taskChecklistsApi } from './tasks-checklists';
import { folderAndFilesApi } from './folders-and-files';
import { userRolesScreens } from './user-roles-screens';
import { taskTimeTrackerApi } from './task-time-tracker';
import counterReducer from 'redux/reducers/counter/counterSlice';

const reducers = combineReducers({
  socket: socketReducer,
  counter: counterReducer,
  sideBar: SideBarReducer,
  fileViewer: FileViewerReducer,
  [postApi.reducerPath]: postApi.reducer,
  [userApi.reducerPath]: userApi.reducer,
  [taskApi.reducerPath]: taskApi.reducer,
  [standUpApi.reducerPath]: standUpApi.reducer,
  [projectsApi.reducerPath]: projectsApi.reducer,
  [userProfileApi.reducerPath]: userProfileApi.reducer,
  [chatMessagesApi.reducerPath]: chatMessagesApi.reducer,
  [projectMemberApi.reducerPath]: projectMemberApi.reducer,
  [userRolesScreens.reducerPath]: userRolesScreens.reducer,
  [authenticationApi.reducerPath]: authenticationApi.reducer,
  [folderAndFilesApi.reducerPath]: folderAndFilesApi.reducer,
  [taskAttachmentApi.reducerPath]: taskAttachmentApi.reducer,
  [taskChecklistsApi.reducerPath]: taskChecklistsApi.reducer,
  [supportTicketsApi.reducerPath]: supportTicketsApi.reducer,
  [taskTimeTrackerApi.reducerPath]: taskTimeTrackerApi.reducer,
  [notificationsApi.reducerPath]: notificationsApi.reducer,
  [tasksCommentApi.reducerPath]: tasksCommentApi.reducer,
  [tasksLabelApi.reducerPath]: tasksLabelApi.reducer,
  [sprintPlanningApi.reducerPath]: sprintPlanningApi.reducer,
});

export const middleware = (getDefaultMiddleware) =>
  getDefaultMiddleware()
    .concat(postApi.middleware)
    .concat(authenticationApi.middleware)
    .concat(userApi.middleware)
    .concat(projectsApi.middleware)
    .concat(taskApi.middleware)
    .concat(projectMemberApi.middleware)
    .concat(standUpApi.middleware)
    .concat(folderAndFilesApi.middleware)
    .concat(userProfileApi.middleware)
    .concat(chatMessagesApi.middleware)
    .concat(userRolesScreens.middleware)
    .concat(taskAttachmentApi.middleware)
    .concat(taskChecklistsApi.middleware)
    .concat(supportTicketsApi.middleware)
    .concat(taskTimeTrackerApi.middleware)
    .concat(notificationsApi.middleware)
    .concat(tasksCommentApi.middleware)
    .concat(tasksLabelApi.middleware)
    .concat(sprintPlanningApi.middleware);

export default reducers;
