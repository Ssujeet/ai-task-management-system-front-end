import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery, convertJsonToFormData } from 'redux/reducers/settings';
import { BaseResponse } from 'common/types';
import { ProjectOutputInterface } from 'redux/reducers/projects/index';

import { UserResponse } from '../user';
import { NOTIFICATION_STATUS, PROJECTTYPENOTIFICATIONS } from 'common/enum';

export interface INotifications {
  id: number;
  message: string;
  project: ProjectOutputInterface;
  actionBy: UserResponse;
  projectTypeNotification: PROJECTTYPENOTIFICATIONS;
}

export interface INotificationNotifier {
  id: string;
  concernedUser: UserResponse;
  notificationStatus: NOTIFICATION_STATUS;
  notification: INotifications;
}

export const notificationsApi = createApi({
  reducerPath: 'notificationsApi',
  baseQuery: baseQuery,
  tagTypes: ['notifications', 'count'],
  endpoints: (build) => ({
    getCurrentUserNotificationsLists: build.query<BaseResponse<INotificationNotifier[]>, void>({
      query: () => `notication-notifier/current-user/notifications`,
      providesTags: (result) =>
        result?.data
          ? [
              ...result?.data?.map(({ id }) => ({ type: 'notifications', id } as const)),
              { type: 'notifications', id: 'LIST' },
            ]
          : [{ type: 'notifications', id: 'LIST' }],
    }),
    getCurrentUserNotificationsListsCount: build.query<BaseResponse<any>, void>({
      query: () => `notication-notifier/current-user/notifications-count`,
      providesTags: [{ type: 'count', id: 'LIST' }],
    }),

    updateNotificationWithId: build.mutation<BaseResponse<INotificationNotifier>, string>({
      query(id) {
        return {
          url: `notication-notifier/${id}`,
          method: 'PUT',
        };
      },
      invalidatesTags: [
        { type: 'notifications', id: 'LIST' },
        { type: 'count', id: 'LIST' },
      ],
    }),
    markAsRead: build.mutation<BaseResponse<INotificationNotifier>, void>({
      query() {
        return {
          url: `notication-notifier/mark-as-read`,
          method: 'POST',
        };
      },
      invalidatesTags: [
        { type: 'notifications', id: 'LIST' },
        { type: 'count', id: 'LIST' },
      ],
    }),
  }),
});

export const {
  useGetCurrentUserNotificationsListsQuery,
  useGetCurrentUserNotificationsListsCountQuery,
  useUpdateNotificationWithIdMutation,
  useMarkAsReadMutation,
} = notificationsApi;
