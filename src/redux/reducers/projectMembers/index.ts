import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery, convertJsonToFormData } from 'redux/reducers/settings';
import { BaseResponse } from 'common/types';
import { PROJECTSTATUS, PROJECTMEMBERROLE, USERSTATUSINPROJECT } from 'common/enum';
import { ProjectOutputInterface } from 'redux/reducers/projects/index';

export interface ProjectInterface {
  projectName: string;
  startDate: string;
  endDate: string;
  descriptions: string;
  createdAt?: string;
  updatedAt?: string;
  id?: number;
  projectStatus?: PROJECTSTATUS;
}

export interface ProjectMemberInput {
  userId?: number;
  projectId?: number;
  role?: PROJECTMEMBERROLE;
  userStatus?: USERSTATUSINPROJECT;
  userRoleIdInProject?: number;
  id?: number;
}

export interface ToggleProjectMemberStatusInput {
  id: number;
  userStatus: USERSTATUSINPROJECT;
}

export const projectMemberApi = createApi({
  reducerPath: 'projectMemberApi',
  baseQuery: baseQuery,
  tagTypes: ['ProjectMember'],
  endpoints: (build) => ({
    getProjectMembersLists: build.query<BaseResponse<ProjectOutputInterface[]>, string>({
      query: (projectID) => `project-member/lists/${projectID}`,
      providesTags: (result) =>
        result?.data
          ? [
              ...result?.data?.map(({ id }) => ({ type: 'ProjectMember', id } as const)),
              { type: 'ProjectMember', id: 'LIST' },
            ]
          : [{ type: 'ProjectMember', id: 'LIST' }],
    }),
    getCurrentUserDetailsInProject: build.query<BaseResponse<ProjectOutputInterface>, string>({
      query: (projectID) => `project-member/currentUser/${projectID}`,
    }),
    addProjectsMembers: build.mutation<BaseResponse<ProjectMemberInput>, ProjectMemberInput>({
      query(body) {
        return {
          url: `project-member`,
          method: 'POST',
          body,
        };
      },
      invalidatesTags: [{ type: 'ProjectMember', id: 'LIST' }],
    }),
    updateProjectMembersDetails: build.mutation<BaseResponse<ProjectMemberInput>, Partial<ProjectMemberInput>>({
      query(body) {
        return {
          url: `project-member`,
          method: 'PUT',
          body,
        };
      },
      invalidatesTags: [{ type: 'ProjectMember', id: 'LIST' }],
    }),
    toogleProjectMemberStatus: build.mutation<BaseResponse<ToggleProjectMemberStatusInput>, ProjectMemberInput>({
      query(body) {
        return {
          url: `project-member/toogleUserStatus`,
          method: 'POST',
          body,
        };
      },
      invalidatesTags: [{ type: 'ProjectMember', id: 'LIST' }],
    }),
  }),
});

export const {
  useGetProjectMembersListsQuery,
  useGetCurrentUserDetailsInProjectQuery,
  useAddProjectsMembersMutation,
  useToogleProjectMemberStatusMutation,
  useUpdateProjectMembersDetailsMutation,
} = projectMemberApi;
