import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery, convertJsonToFormData } from 'redux/reducers/settings';
import { BaseResponse } from 'common/types';
import { PROJECTSTATUS, PROJECTMEMBERROLE, USERSTATUSINPROJECT } from 'common/enum';
import { UserResponse } from 'redux/reducers/user/index';
import { UserRolesResponse } from '../user-roles-screens';

export interface ProjectInterface {
  projectName: string;
  avatarKey?: string;
  descriptions: string;
  createdAt?: string;
  updatedAt?: string;
  id?: number;
  projectStatus?: PROJECTSTATUS;
  color: string;
  folders?: Array<any>;
  projectMember: { user: UserResponse }[];
  avatarUrl?: string;
}

export interface ProjectOutputInterface {
  id: number;
  createdAt: string;
  updatedAt: string;
  role: PROJECTMEMBERROLE;
  userStatus: USERSTATUSINPROJECT;
  project: ProjectInterface;
  user: UserResponse;
  projectMemberRole: UserRolesResponse;
  avatarUrl: string;
  color: string;
  projectName: string;
}

export interface IChangeProjectAvatar {
  file: File;
  projectId: number;
}

export const projectsApi = createApi({
  reducerPath: 'projectsApi',
  baseQuery: baseQuery,
  tagTypes: ['Projects'],
  endpoints: (build) => ({
    getCurrentUserProjectLists: build.query<BaseResponse<ProjectOutputInterface[]>, void>({
      query: () => 'project-member/projects-lists',
      providesTags: (result) =>
        result?.data
          ? [...result?.data?.map(({ id }) => ({ type: 'Projects', id } as const)), { type: 'Projects', id: 'LIST' }]
          : [{ type: 'Projects', id: 'LIST' }],
    }),
    getProjectsDetailById: build.query<BaseResponse<ProjectOutputInterface>, string>({
      query: (id) => `projects/${id}`,
      providesTags: [{ type: 'Projects', id: 'LIST' }],
    }),
    changeProjectAvatar: build.mutation<BaseResponse<ProjectInterface>, IChangeProjectAvatar>({
      query(body) {
        const { file, projectId } = body;

        return {
          url: `projects/change-avatar/project/${projectId}`,
          method: 'PUT',
          body: convertJsonToFormData({ file }),
        };
      },
      invalidatesTags: [{ type: 'Projects', id: 'LIST' }],
    }),

    addProjects: build.mutation<BaseResponse<ProjectInterface>, ProjectInterface>({
      query(body) {
        return {
          url: `projects`,
          method: 'POST',
          body: convertJsonToFormData(body),
        };
      },
      invalidatesTags: [{ type: 'Projects', id: 'LIST' }],
    }),
    updateProject: build.mutation<BaseResponse<ProjectInterface>, ProjectInterface>({
      query(body) {
        return {
          url: `projects`,
          method: 'PUT',
          body,
        };
      },
      invalidatesTags: [{ type: 'Projects', id: 'LIST' }],
    }),
  }),
});

export const {
  useGetCurrentUserProjectListsQuery,
  useAddProjectsMutation,
  useGetProjectsDetailByIdQuery,
  useUpdateProjectMutation,
  useChangeProjectAvatarMutation,
} = projectsApi;
