import { fetchBaseQuery } from '@reduxjs/toolkit/query';
import { Mutex } from 'async-mutex';

const tokenRef = 'token';
const refreshTokenRef = 'refreshToken';

const baseQueryForUrl = fetchBaseQuery({
  baseUrl: process.env.REACT_APP_API_URL,
  prepareHeaders: (headers, { getState }) => {
    const token = window.localStorage.token;
    if (token) {
      headers.set('Authorization', `Bearer ${JSON.parse(token)}`);
    }

    return headers;
  },
});

const baseQueryForRefresh = fetchBaseQuery({
  baseUrl: process.env.REACT_APP_API_URL,
  prepareHeaders: (headers) => {
    const token = window.localStorage.refreshToken;
    if (token) {
      headers.set('Authorization', `Bearer ${JSON.parse(token)}`);
    }

    return headers;
  },
});

const mutex = new Mutex();

export const baseQuery = async (args: any, api: any, extraOptions: any) => {
  await mutex.waitForUnlock();
  let result: any = await baseQueryForUrl(args, api, extraOptions);
  if (result.error && result.error.status === 401) {
    if (!mutex.isLocked()) {
      const release = await mutex.acquire();
      try {
        const token = window.localStorage.refreshToken;

        const refreshResult: any = await baseQueryForRefresh('/auth/refresh-token', api, extraOptions);
        console.log(refreshResult, 'refreshResult');
        if (refreshResult.data) {
          localStorage.setItem(tokenRef, JSON.stringify(refreshResult.data.data.accessToken));
          localStorage.setItem(refreshTokenRef, JSON.stringify(refreshResult.data.data.refreshToken));
          result = await baseQueryForUrl(args, api, extraOptions);
        } else {
          localStorage.removeItem(tokenRef);
          localStorage.removeItem(refreshTokenRef);
          window.location.href = '/';
        }
      } finally {
        release();
      }
    } else {
      await mutex.waitForUnlock();
      result = await baseQuery(args, api, extraOptions);
    }
  }

  return result;
};

export function convertJsonToFormData(requestData) {
  let formData = new FormData();
  for (let data in requestData) {
    if (requestData[data] instanceof Array) {
      requestData[data].forEach((dataEl: any, index: number) => {
        if (dataEl instanceof Object && !(dataEl instanceof File)) {
          Object.keys(dataEl).forEach((elKey) => formData.append(`${data}[${index}].${elKey}`, dataEl[elKey]));
        } else if (dataEl instanceof File) {
          formData.append(`${data}[${index}]`, dataEl);
        } else if (typeof dataEl === 'number' || typeof dataEl === 'string') {
          formData.append(`${data}[${index}]`, dataEl.toString());
        }
      });
    } else if (requestData[data] instanceof Object && !(requestData[data] instanceof File)) {
      Object.entries(requestData[data]).forEach(([key, value]: [string, any]) =>
        formData.append(`${data}.${key}`, value)
      );
    } else {
      formData.append(data, requestData[data]);
    }
  }

  return formData;
}
