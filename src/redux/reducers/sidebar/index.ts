import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState, AppThunk } from 'redux/store';
import { IRoutes } from 'routes/routesLists';
import { privateRoutes } from 'routes/private';

const initialState = {
  routes: [] as IRoutes[],
};

export const sideBarSlice = createSlice({
  name: 'sidebar',
  initialState,
  reducers: {
    addSideBarNav: (state, action: PayloadAction<IRoutes[]>) => {
      state.routes = action.payload;
    },
  },
});
export const { addSideBarNav } = sideBarSlice.actions;

export default sideBarSlice.reducer;
