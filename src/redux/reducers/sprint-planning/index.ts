import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { AbstractInterface, BaseResponse } from 'common/types';

import { ProjectInterface } from '../projects';

export interface SprintPlanningInput {
  startDate: Date;
  endDate: Date;
  title: string;
  descriptions: string;
  isActive: boolean;
  projectId: number;
  taskId?: number[];
}

export interface SprintPlanningResponse extends SprintPlanningInput, AbstractInterface {
  project: ProjectInterface;
}

export const sprintPlanningApi = createApi({
  reducerPath: 'sprintPlanningApi',
  baseQuery: baseQuery,
  tagTypes: ['SprintPlanning'],
  endpoints: (build) => ({
    getSprintListsOfProjectById: build.query<BaseResponse<SprintPlanningResponse[]>, string>({
      query: (projectId) => `sprint-planning/project-sprint-plannings/${projectId}`,
      providesTags: (result) =>
        result?.data
          ? [
              ...result?.data?.map(({ id }) => ({ type: 'SprintPlanning', id } as const)),
              { type: 'SprintPlanning', id: 'LIST' },
            ]
          : [{ type: 'SprintPlanning', id: 'LIST' }],
    }),

    addSprint: build.mutation<BaseResponse<SprintPlanningInput>, SprintPlanningResponse>({
      query(body) {
        return {
          url: `sprint-planning`,
          method: 'POST',
          body,
        };
      },
      invalidatesTags: [{ type: 'SprintPlanning', id: 'LIST' }],
    }),
    updateSprint: build.mutation<BaseResponse<SprintPlanningInput>, SprintPlanningResponse>({
      query(body) {
        return {
          url: `sprint-planning`,
          method: 'PUT',
          body,
        };
      },
      invalidatesTags: [{ type: 'SprintPlanning', id: 'LIST' }],
    }),
  }),
});

export const { useGetSprintListsOfProjectByIdQuery, useAddSprintMutation, useUpdateSprintMutation } = sprintPlanningApi;
