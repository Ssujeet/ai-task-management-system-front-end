import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery, convertJsonToFormData } from 'redux/reducers/settings';
import { BaseResponse } from 'common/types';
import { STANDUPNOTIFICATIONREPEATSSCHUDULE } from 'common/enum';
import { UserResponse } from '../user';

export interface StandUpInput {
  id?: string;
  title: string;
  descriptions: string;
  projectId: number;
  alertTime: string;
  alertDays: string;
  notificationAlertSchdule: STANDUPNOTIFICATIONREPEATSSCHUDULE;
}

export interface StandUpDetailsOutput {
  id: string;
  details: string;
  standUp: StandUpInput;
  user: UserResponse;
}

export interface StandUpDetailsInput {
  details: string;
  standUpId: number;
}

export const standUpApi = createApi({
  reducerPath: 'standUpApi',
  baseQuery: baseQuery,
  tagTypes: ['StandUp', 'StandUpDetails'],
  endpoints: (build) => ({
    getStandUpListOfProjectById: build.query<BaseResponse<StandUpInput[]>, string>({
      query: (projectId) => `stand-up/projectId/${projectId}`,
      providesTags: (result) =>
        result?.data
          ? [...result?.data?.map(({ id }) => ({ type: 'StandUp', id } as const)), { type: 'StandUp', id: 'LIST' }]
          : [{ type: 'StandUp', id: 'LIST' }],
    }),
    getStandUpDetailsByStandUpId: build.query<BaseResponse<StandUpInput>, string>({
      query: (standUpId) => `stand-up/${standUpId}`,
    }),
    getStandUpDetailsListByStandUpId: build.query<BaseResponse<StandUpDetailsOutput[]>, string>({
      query: (standUpId) => `stand-up-details/lists/${standUpId}`,
      providesTags: (result) =>
        result?.data
          ? [
              ...result?.data?.map(({ id }) => ({ type: 'StandUpDetails', id } as const)),
              { type: 'StandUpDetails', id: 'LIST' },
            ]
          : [{ type: 'StandUpDetails', id: 'LIST' }],
    }),
    addStandUpDetails: build.mutation<BaseResponse<StandUpDetailsInput>, StandUpDetailsInput>({
      query(body) {
        return {
          url: `stand-up-details`,
          method: 'POST',
          body,
        };
      },
      invalidatesTags: [{ type: 'StandUpDetails', id: 'LIST' }],
    }),
    addStandUp: build.mutation<BaseResponse<StandUpInput>, StandUpInput>({
      query(body) {
        return {
          url: `stand-up`,
          method: 'POST',
          body,
        };
      },
      invalidatesTags: [{ type: 'StandUp', id: 'LIST' }],
    }),
    updateStandUp: build.mutation<BaseResponse<StandUpInput>, StandUpInput>({
      query(body) {
        return {
          url: `stand-up`,
          method: 'PUT',
          body,
        };
      },
      invalidatesTags: [{ type: 'StandUp', id: 'LIST' }],
    }),
  }),
});

export const {
  useGetStandUpListOfProjectByIdQuery,
  useAddStandUpMutation,
  useGetStandUpDetailsListByStandUpIdQuery,
  useAddStandUpDetailsMutation,
  useGetStandUpDetailsByStandUpIdQuery,
  useUpdateStandUpMutation,
} = standUpApi;
