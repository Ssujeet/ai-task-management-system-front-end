import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { BaseResponse } from 'common/types';

import { UserResponse } from '../user';

export interface ISupportTicket {
  id?: string;
  question: string;
  createdUser?: UserResponse;
  title: string;
}

export interface ISupportTicketInput extends ISupportTicket {}

export const supportTicketsApi = createApi({
  reducerPath: 'supportTicketsApi',
  baseQuery: baseQuery,
  tagTypes: ['SupportTicket'],
  endpoints: (build) => ({
    getListsOfSupportTickets: build.query<BaseResponse<ISupportTicket[]>, void>({
      query: () => `support-tickets`,
      providesTags: (result) =>
        result?.data
          ? [
              ...result?.data?.map(({ id }) => ({ type: 'SupportTicket', id } as const)),
              { type: 'SupportTicket', id: 'LIST' },
            ]
          : [{ type: 'SupportTicket', id: 'LIST' }],
    }),
    getDetailsOfSupportTickets: build.query<BaseResponse<ISupportTicket>, string>({
      query: (id) => `support-tickets/${id}`,
    }),

    addSupportTickets: build.mutation<BaseResponse<ISupportTicket>, ISupportTicketInput>({
      query(body) {
        return {
          url: `support-tickets`,
          method: 'POST',
          body,
        };
      },
      invalidatesTags: [{ type: 'SupportTicket', id: 'LIST' }],
    }),
    updateSupportTicket: build.mutation<BaseResponse<ISupportTicket>, ISupportTicketInput>({
      query(body) {
        return {
          url: `support-tickets`,
          method: 'PUT',
          body,
        };
      },
      invalidatesTags: [{ type: 'SupportTicket', id: 'LIST' }],
    }),
    deleteSupportTickets: build.mutation<BaseResponse<ISupportTicket>, string>({
      query(id) {
        return {
          url: `support-tickets/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: [{ type: 'SupportTicket', id: 'LIST' }],
    }),
  }),
});

export const {
  useGetListsOfSupportTicketsQuery,
  useAddSupportTicketsMutation,
  useUpdateSupportTicketMutation,
  useDeleteSupportTicketsMutation,
} = supportTicketsApi;
