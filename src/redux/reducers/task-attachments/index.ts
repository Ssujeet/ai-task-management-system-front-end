// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery, convertJsonToFormData } from 'redux/reducers/settings';
import { BaseResponse } from 'common/types';

import { TaskInterface } from '../tasks';
import { IFilesResponse } from '../folders-and-files';

export interface ITaskAttachments extends IFilesResponse {
  task: TaskInterface;
}

export interface ITaskAttachmentsInput {
  files: File[];
  taskId: number;
}

export interface ITaskAttachmentsInputForUpdate extends Partial<ITaskAttachmentsInput> {
  id: string;
  isCompleted?: boolean;
}

export const taskAttachmentApi = createApi({
  reducerPath: 'taskAttachmentApi',
  baseQuery: baseQuery,
  tagTypes: ['TaskAttachment'],
  endpoints: (build) => ({
    getTaskAttachmentByTaskId: build.query<BaseResponse<ITaskAttachments[]>, string>({
      query: (taskId) => `task-attachment/task/${taskId}`,
      providesTags: (result) =>
        result?.data
          ? [
              ...result?.data?.map(({ id }) => ({ type: 'TaskAttachment', id } as const)),
              { type: 'TaskAttachment', id: 'LIST' },
            ]
          : [{ type: 'TaskAttachment', id: 'LIST' }],
    }),

    addTaskAttachments: build.mutation<BaseResponse<ITaskAttachments>, ITaskAttachmentsInput>({
      query(body) {
        const { taskId, files } = body;

        return {
          url: `task-attachment/task/${taskId}`,
          method: 'POST',
          body: convertJsonToFormData({ files }),
        };
      },
      invalidatesTags: [{ type: 'TaskAttachment', id: 'LIST' }],
    }),

    deleteFiles: build.mutation<BaseResponse<ITaskAttachments[]>, string>({
      query(id) {
        return {
          url: `task-attachment/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: [{ type: 'TaskAttachment', id: 'LIST' }],
    }),
  }),
});

export const { useGetTaskAttachmentByTaskIdQuery, useAddTaskAttachmentsMutation, useDeleteFilesMutation } =
  taskAttachmentApi;
