import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { BaseResponse } from 'common/types';

import { UserResponse } from '../user';
import { TaskInterface } from '../tasks';
import { TASKTIMETRACKER } from 'common/enum';

export interface ITaskTimeTrackerResponse {
  id: string;
  task: TaskInterface;
  user: UserResponse;
  isStopped: boolean;
  manualTime: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface ITaskTimeTrackerInput {
  taskId: number;
  taskStatus: TASKTIMETRACKER;
}

export interface ITaskTimeTrackerManualInput {
  taskId: number;
  timeInHours: string;
}

export const taskTimeTrackerApi = createApi({
  reducerPath: 'taskTimeTrackerApi',
  baseQuery: baseQuery,
  tagTypes: ['TaskTimeTracker'],
  endpoints: (build) => ({
    getListsOfTaskTrackerTimeByTaskId: build.query<BaseResponse<ITaskTimeTrackerResponse[]>, string>({
      query: (taskId) => `task-time-tracker/task/${taskId}`,
      providesTags: (result) =>
        result?.data
          ? [
              ...result?.data?.map(({ id }) => ({ type: 'TaskTimeTracker', id } as const)),
              { type: 'TaskTimeTracker', id: 'LIST' },
            ]
          : [{ type: 'TaskTimeTracker', id: 'LIST' }],
    }),

    getCurrentUserTimeTracker: build.query<BaseResponse<ITaskTimeTrackerResponse>, void>({
      query: () => `task-time-tracker/current-user-time-tracker`,
      providesTags: [{ type: 'TaskTimeTracker', id: 'LIST' }],
    }),

    addTaskTimeTrackers: build.mutation<BaseResponse<ITaskTimeTrackerResponse>, ITaskTimeTrackerInput>({
      query(body) {
        return {
          url: `task-time-tracker`,
          method: 'POST',
          body,
        };
      },
      invalidatesTags: [{ type: 'TaskTimeTracker', id: 'LIST' }],
    }),

    addManualTime: build.mutation<BaseResponse<ITaskTimeTrackerResponse>, ITaskTimeTrackerManualInput>({
      query(body) {
        return {
          url: `task-time-tracker/manual-time`,
          method: 'POST',
          body,
        };
      },
      invalidatesTags: [{ type: 'TaskTimeTracker', id: 'LIST' }],
    }),

    deleteTaskTimeTrackers: build.mutation<BaseResponse<ITaskTimeTrackerResponse>, string>({
      query(id) {
        return {
          url: `task-time-tracker/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: [{ type: 'TaskTimeTracker', id: 'LIST' }],
    }),
  }),
});

export const {
  useGetListsOfTaskTrackerTimeByTaskIdQuery,
  useAddTaskTimeTrackersMutation,
  useAddManualTimeMutation,
  useDeleteTaskTimeTrackersMutation,
  useGetCurrentUserTimeTrackerQuery,
} = taskTimeTrackerApi;
