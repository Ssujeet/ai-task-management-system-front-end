// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery, convertJsonToFormData } from 'redux/reducers/settings';
import { BaseResponse } from 'common/types';
import { TaskInterface } from '../tasks';

export interface ITaskChecklists {
  note: string;
  task: TaskInterface;
  isCompleted: boolean;
  id: string;
}

export interface ITaskCheckListsInput {
  note: string;
  taskId: number;
}

export interface ITaskCheckListsInputForUpdate extends Partial<ITaskCheckListsInput> {
  id: string;
  isCompleted?: boolean;
  note?: string;
}

export const taskChecklistsApi = createApi({
  reducerPath: 'taskChecklistsApi',
  baseQuery: baseQuery,
  tagTypes: ['TaskChecklist'],
  endpoints: (build) => ({
    getTaskChecklistByTaskId: build.query<BaseResponse<ITaskChecklists[]>, string>({
      query: (taskId) => `task-checklists/task/${taskId}`,
      providesTags: (result) =>
        result?.data
          ? [
              ...result?.data?.map(({ id }) => ({ type: 'TaskChecklist', id } as const)),
              { type: 'TaskChecklist', id: 'LIST' },
            ]
          : [{ type: 'TaskChecklist', id: 'LIST' }],
    }),

    addTaskCheckLists: build.mutation<BaseResponse<ITaskChecklists>, ITaskCheckListsInput>({
      query(body) {
        return {
          url: `task-checklists`,
          method: 'POST',
          body,
        };
      },
      invalidatesTags: [{ type: 'TaskChecklist', id: 'LIST' }],
    }),

    deleteTaskCheckLists: build.mutation<BaseResponse<ITaskChecklists>, string>({
      query(id) {
        return {
          url: `task-checklists/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: [{ type: 'TaskChecklist', id: 'LIST' }],
    }),

    updateTaskChecklist: build.mutation<BaseResponse<ITaskChecklists>, ITaskCheckListsInputForUpdate>({
      query(body) {
        return {
          url: `task-checklists`,
          method: 'PUT',
          body,
        };
      },
      invalidatesTags: [{ type: 'TaskChecklist', id: 'LIST' }],
    }),
  }),
});

export const {
  useGetTaskChecklistByTaskIdQuery,
  useAddTaskCheckListsMutation,
  useUpdateTaskChecklistMutation,
  useDeleteTaskCheckListsMutation,
} = taskChecklistsApi;
