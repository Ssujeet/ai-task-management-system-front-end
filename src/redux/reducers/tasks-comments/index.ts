// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery, convertJsonToFormData } from 'redux/reducers/settings';
import { AbstractInterface, BaseResponse } from 'common/types';

import { TaskInterface } from '../tasks';
import { IFilesResponse } from '../folders-and-files';
import { UserResponse } from '../user';

export interface ITaskComments extends AbstractInterface {
  task: TaskInterface;
  message: string;
  commenter: UserResponse;

  file: IFilesResponse[];
  isEdited: boolean;
}

export interface ITaskCommentsInput {
  files?: File[];
  taskId: number;
  message?: string;
}

export interface ITaskCommentsInputForUpdate {
  id: string;
  message: string;
}

export const tasksCommentApi = createApi({
  reducerPath: 'tasksCommentApi',
  baseQuery: baseQuery,
  tagTypes: ['TaskComment'],
  endpoints: (build) => ({
    getTaskCommentsListByTaskId: build.query<BaseResponse<ITaskComments[]>, string>({
      query: (taskId) => `task-comments/task/${taskId}`,
      providesTags: (result) =>
        result?.data
          ? [
              ...result?.data?.map(({ id }) => ({ type: 'TaskComment', id } as const)),
              { type: 'TaskComment', id: 'LIST' },
            ]
          : [{ type: 'TaskComment', id: 'LIST' }],
    }),

    addTaskComments: build.mutation<BaseResponse<ITaskComments>, ITaskCommentsInput>({
      query(body) {
        return {
          url: `task-comments`,
          method: 'POST',
          body: convertJsonToFormData(body),
        };
      },
      invalidatesTags: [{ type: 'TaskComment', id: 'LIST' }],
    }),

    updateComment: build.mutation<BaseResponse<ITaskComments>, ITaskCommentsInputForUpdate>({
      query(body) {
        return {
          url: `task-comments`,
          method: 'PUT',
          body,
        };
      },
      invalidatesTags: [{ type: 'TaskComment', id: 'LIST' }],
    }),

    deleteCommentById: build.mutation<BaseResponse<ITaskComments>, string>({
      query(id) {
        return {
          url: `task-comments/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: [{ type: 'TaskComment', id: 'LIST' }],
    }),

    deleteFiles: build.mutation<BaseResponse<ITaskComments[]>, string>({
      query(id) {
        return {
          url: `files/tasks-comment-file/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: [{ type: 'TaskComment', id: 'LIST' }],
    }),
  }),
});

export const {
  useGetTaskCommentsListByTaskIdQuery,
  useUpdateCommentMutation,
  useDeleteCommentByIdMutation,
  useAddTaskCommentsMutation,
  useDeleteFilesMutation,
} = tasksCommentApi;
