import { createApi } from '@reduxjs/toolkit/query/react';

import { AbstractInterface, BaseResponse } from 'common/types';
import { baseQuery } from '../settings';

export interface ITasksLabel extends AbstractInterface {
  title: string;
  color: string;
}

export interface ITasksLabelUpdateInput {
  id: string;
  title: string;
}

export const tasksLabelApi: { [key: string]: any } = createApi({
  reducerPath: 'tasksLabelApi',
  baseQuery: baseQuery,
  tagTypes: ['TasksLabel'],
  endpoints: (build) => ({
    // getTasksLabelListByProjectId: build.query<BaseResponse<ITasksLabel>, string>({
    //   query: (projectId) => `tasks-label/project/${projectId}`,
    // }),
    deleteTasksLabelById: build.mutation<BaseResponse<ITasksLabel>, string>({
      query(id) {
        return {
          url: `tasks-label'${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: [{ type: 'TasksLabel', id: 'LIST' }],
    }),
    updateTaskLabels: build.mutation<BaseResponse<ITasksLabel>, ITasksLabelUpdateInput>({
      query(body) {
        return {
          url: `task-comments`,
          method: 'PUT',
          body,
        };
      },
      invalidatesTags: [{ type: 'TasksLabel', id: 'LIST' }],
    }),
  }),
});

export const { useUpdateTaskLabelsMutation, useDeleteTasksLabelByIdMutation } = tasksLabelApi;
