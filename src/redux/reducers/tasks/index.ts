import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { BaseResponse } from 'common/types';
import { TASKSPRIORITY, USERSTATUSINTASK } from 'common/enum';

import { UserResponse } from '../user';
import { ITasksLabel } from '../tasks-label';
import { ITaskComments } from '../tasks-comments';
import { ITaskAttachments } from '../task-attachments';

export interface TaskInterface {
  id?: number;
  title?: string;
  priority?: TASKSPRIORITY;
  taskStatusId?: number;
  descriptions?: string;
  taskMemberId?: number[];
  taskMember?: TaskMemberInterface[];
  taskStatus?: TaskStatusInterface;
  createdAt?: string;
  updatedAt?: string;
  createdUser?: UserResponse;
  order?: number;
  labels?: ITasksLabel[];
  deadline?: string;
  comments?: ITaskComments[];
  attachments?: ITaskAttachments[];
}

export interface TaskStatusInterface {
  id?: number;
  taskName: string;
  projectId: number;
  canEdit?: boolean;
}
export interface TaskMemberInterface {
  id: number;
  createdAt: string;
  updatedAt: string;
  userStatus: USERSTATUSINTASK;
  user?: UserResponse;
}

export interface AddMemberTaskInterface {
  taskId: number;
  projectId: number;
  userId: number;
  userStatus?: USERSTATUSINTASK;
}

export interface TaskOrderChangeInputInterface {
  taskId: string;
  order: number;
  previousOrder: number;
  taskStatusId: number;
}

export interface IAddRemoveLabelInTaskInput {
  title?: string;
  projectId?: string;
  taskId?: string;
  newLabelID?: string;
  removedLabelId?: string;
}

export interface ITaskInterfaceInput {
  title?: string;
  priority?: TASKSPRIORITY;
  taskStatusId?: number;
  descriptions?: string;
  id?: number;
  deadline?: string;
}

export const taskApi = createApi({
  reducerPath: 'taskApi',
  baseQuery: baseQuery,
  tagTypes: ['Tasks', 'TasksStatus', 'TasksDetails', 'TasksLabels'],
  endpoints: (build) => ({
    getTaskListsByProjectId: build.query<BaseResponse<TaskInterface[]>, { projectId: string; sprintId: string }>({
      query: ({ projectId, sprintId }) => `tasks/getProjectLists/project/${projectId}/${sprintId}`,
      providesTags: (result) =>
        result?.data
          ? [...result?.data?.map(({ id }) => ({ type: 'Tasks', id } as const)), { type: 'Tasks', id: 'LIST' }]
          : [{ type: 'Tasks', id: 'LIST' }],
    }),
    getTaskDetailsById: build.query<BaseResponse<TaskInterface>, string>({
      query: (taskId) => `tasks/${taskId}`,
      providesTags: [{ type: 'TasksDetails', id: 'LIST' }],
    }),
    getTaskMemberById: build.query<BaseResponse<TaskMemberInterface[]>, string>({
      query: (taskId) => `task-member/${taskId}`,
    }),
    deleteTaskMemberById: build.mutation<BaseResponse<TaskStatusInterface>, String>({
      query(body) {
        return {
          url: `task-member/${body}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: [
        { type: 'Tasks', id: 'LIST' },
        { type: 'TasksDetails', id: 'LIST' },
      ],
    }),
    addTasks: build.mutation<BaseResponse<TaskStatusInterface>, ITaskInterfaceInput>({
      query(body) {
        return {
          url: `tasks`,
          method: 'POST',
          body,
        };
      },
      invalidatesTags: [{ type: 'Tasks', id: 'LIST' }],
    }),
    changeOrderOfTasks: build.mutation<BaseResponse<TaskStatusInterface>, TaskOrderChangeInputInterface>({
      query(body) {
        return {
          url: `tasks/ordering`,
          method: 'POST',
          body,
        };
      },
      invalidatesTags: [{ type: 'Tasks', id: 'LIST' }],
    }),
    addMembersInTasks: build.mutation<BaseResponse<AddMemberTaskInterface>, AddMemberTaskInterface>({
      query(body) {
        return {
          url: `task-member`,
          method: 'POST',
          body,
        };
      },
      invalidatesTags: [
        { type: 'Tasks', id: 'LIST' },
        { type: 'TasksDetails', id: 'LIST' },
      ],
    }),
    updateTasksById: build.mutation<BaseResponse<TaskStatusInterface>, ITaskInterfaceInput>({
      query(body) {
        return {
          url: `tasks`,
          method: 'PUT',
          body,
        };
      },
      invalidatesTags: [
        { type: 'Tasks', id: 'LIST' },
        { type: 'TasksDetails', id: 'LIST' },
      ],
    }),
    getTaskStatusByProjectId: build.query<BaseResponse<TaskStatusInterface[]>, string>({
      query: (projectId) => `task-status/listsByProjectId/${projectId}`,
      providesTags: (result) =>
        result?.data
          ? [
              ...result?.data?.map(({ id }) => ({ type: 'TasksStatus', id } as const)),
              { type: 'TasksStatus', id: 'LIST' },
            ]
          : [{ type: 'TasksStatus', id: 'LIST' }],
    }),
    addTaskStatus: build.mutation<BaseResponse<TaskStatusInterface>, TaskStatusInterface>({
      query(body) {
        return {
          url: `task-status`,
          method: 'POST',
          body,
        };
      },
      invalidatesTags: [{ type: 'TasksStatus', id: 'LIST' }],
    }),

    addRemoveLabelsInTask: build.mutation<BaseResponse<TaskStatusInterface>, IAddRemoveLabelInTaskInput>({
      query(body) {
        return {
          url: `tasks/labels/add-remove-tasks-labels`,
          method: 'POST',
          body,
        };
      },
      invalidatesTags: [
        { type: 'TasksDetails', id: 'LIST' },
        { type: 'Tasks', id: 'LIST' },
        { type: 'TasksLabels', id: 'LIST' },
      ],
    }),
    updateTaskStatus: build.mutation<BaseResponse<TaskStatusInterface>, TaskStatusInterface>({
      query(body) {
        return {
          url: `task-status`,
          method: 'PUT',
          body,
        };
      },
      invalidatesTags: [{ type: 'TasksStatus', id: 'LIST' }],
    }),
    getTasksLabelListByProjectId: build.query<BaseResponse<ITasksLabel[]>, string>({
      query: (projectId) => `tasks-label/project/${projectId}`,
      providesTags: [{ type: 'TasksLabels', id: 'LIST' }],
    }),
  }),
});

export const {
  useGetTaskListsByProjectIdQuery,
  useAddTasksMutation,
  useGetTaskStatusByProjectIdQuery,
  useAddTaskStatusMutation,
  useUpdateTasksByIdMutation,
  useUpdateTaskStatusMutation,
  useGetTaskDetailsByIdQuery,
  useGetTaskMemberByIdQuery,
  useAddMembersInTasksMutation,
  useDeleteTaskMemberByIdMutation,
  useChangeOrderOfTasksMutation,
  useAddRemoveLabelsInTaskMutation,
  useGetTasksLabelListByProjectIdQuery,
} = taskApi;
