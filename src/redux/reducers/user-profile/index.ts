// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery, convertJsonToFormData } from 'redux/reducers/settings';
import { BaseResponse } from 'common/types';
import { UserResponse } from '../user';

export interface UserProfileResponseAndInput {
  id?: string;
  fullName: string;
  phoneNumber: string;
  country: string;
  company: string;
  isAccountDeactivated: boolean;
  isEmailNotificationOn: boolean;
  showAsActive: boolean;
  newsLaterNotification: boolean;
  user?: UserResponse;
  userPictureUrl: string;
  bio?: string;
}

export interface IUserProfilePicture {
  file: File;
}

export const userProfileApi = createApi({
  reducerPath: 'userProfileApi',
  baseQuery: baseQuery,
  tagTypes: ['UserProfile'],
  endpoints: (build) => ({
    getCurrentUserProfile: build.query<BaseResponse<UserProfileResponseAndInput>, void>({
      query: () => 'user-profile/current-user',
    }),
    getCurrentUserProfileByUserId: build.query<BaseResponse<UserProfileResponseAndInput>, string>({
      query: (userId) => `user-profile/user-details/${userId}`,
    }),
    addUserProfileDetails: build.mutation<BaseResponse<UserProfileResponseAndInput>, UserProfileResponseAndInput>({
      query(body) {
        return {
          url: `user-profile`,
          method: 'POST',
          body,
        };
      },
      invalidatesTags: [{ type: 'UserProfile', id: 'LIST' }],
    }),
    addUserProfilePicture: build.mutation<BaseResponse<IUserProfilePicture>, any>({
      query(body) {
        return {
          url: `user-profile/upload/user-picture`,
          method: 'POST',
          body: convertJsonToFormData(body),
        };
      },
      invalidatesTags: [{ type: 'UserProfile', id: 'LIST' }],
    }),
  }),
});

export const {
  useGetCurrentUserProfileQuery,
  useAddUserProfileDetailsMutation,
  useAddUserProfilePictureMutation,
  useGetCurrentUserProfileByUserIdQuery,
} = userProfileApi;
