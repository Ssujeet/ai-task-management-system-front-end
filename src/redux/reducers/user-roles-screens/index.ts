// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery, convertJsonToFormData } from 'redux/reducers/settings';
import { AbstractInterface, BaseResponse } from 'common/types';
import { SignUpResponse } from '../auth';
import { ProjectOutputInterface } from '../projects';
import { UserResponse } from '../user';

export interface UserRolesResponse extends AbstractInterface {
  project: ProjectOutputInterface;
  name: string;
  status: boolean;
  systemGenerated: boolean;
  screenRole: UserRolesScreenResponse[];
  createdBy: UserResponse;
}

export interface UserRolesInput {
  projectId: number;
  name: string;
  id?: number;
  status?: boolean;
}
export interface IScreens {
  name: string;
  route: string;
  status: boolean;
  id: number;
}

export interface IUserRolesScreensInput {
  userRoleId: number;
  accessWithScreens: UserRolesScreenI[];
}

export interface UserRolesScreenI {
  screenId: number;
  canRead: boolean;
  canCreate: boolean;
  canUpdate: boolean;
  canDelete: boolean;
  id?: number;
}

export interface UserRolesScreenResponse {
  screen: IScreens;
  userRole: UserRolesResponse;
  canRead: boolean;
  canCreate: boolean;
  canUpdate: boolean;
  canDelete: boolean;
  id: number;
}

export const userRolesScreens = createApi({
  reducerPath: 'userRolesScreens',
  baseQuery: baseQuery,
  tagTypes: ['UserRoles', 'UserRolesScreen'],
  endpoints: (build) => ({
    getScreens: build.query<IScreens[], void>({
      query: () => `screens`,
    }),
    getUserRolesInProjects: build.query<BaseResponse<UserRolesResponse[]>, string>({
      query: (projectId) => `user-roles-in-projects/project/${projectId}`,
      providesTags: (result) =>
        result?.data
          ? [...result?.data?.map(({ id }) => ({ type: 'UserRoles', id } as const)), { type: 'UserRoles', id: 'LIST' }]
          : [{ type: 'UserRoles', id: 'LIST' }],
    }),
    addUserRolesInProjects: build.mutation<BaseResponse<UserRolesResponse>, UserRolesInput>({
      query(body) {
        return {
          url: `user-roles-in-projects`,
          method: 'POST',
          body,
        };
      },
      invalidatesTags: [{ type: 'UserRoles', id: 'LIST' }],
    }),
    updateUserRolesInProjects: build.mutation<BaseResponse<UserRolesResponse>, UserRolesInput>({
      query(body) {
        return {
          url: `user-roles-in-projects`,
          method: 'PUT',
          body,
        };
      },
      invalidatesTags: [{ type: 'UserRoles', id: 'LIST' }],
    }),
    createUserRolesScreens: build.mutation<BaseResponse<IUserRolesScreensInput>, UserRolesScreenResponse>({
      query(body) {
        return {
          url: `user-roles-screens`,
          method: 'PUT',
          body,
        };
      },
      invalidatesTags: [{ type: 'UserRolesScreen', id: 'LIST' }],
    }),
    updateUserRolesScreens: build.mutation<BaseResponse<IUserRolesScreensInput>, UserRolesScreenResponse>({
      query(body) {
        return {
          url: `user-roles-screens`,
          method: 'PUT',
          body,
        };
      },
      invalidatesTags: [{ type: 'UserRolesScreen', id: 'LIST' }],
    }),
    getUserRolesScreenByRoleId: build.query<BaseResponse<UserRolesScreenResponse[]>, string>({
      query: (userRoleId) => `user-roles-screens/user-roles/${userRoleId}`,
      providesTags: (result) =>
        result?.data
          ? [
              ...result?.data?.map(({ id }) => ({ type: 'UserRolesScreen', id } as const)),
              { type: 'UserRolesScreen', id: 'LIST' },
            ]
          : [{ type: 'UserRolesScreen', id: 'LIST' }],
    }),
  }),
});

export const {
  useGetScreensQuery,
  useGetUserRolesInProjectsQuery,
  useAddUserRolesInProjectsMutation,
  useUpdateUserRolesInProjectsMutation,
  useCreateUserRolesScreensMutation,
  useUpdateUserRolesScreensMutation,
  useGetUserRolesScreenByRoleIdQuery,
} = userRolesScreens;
