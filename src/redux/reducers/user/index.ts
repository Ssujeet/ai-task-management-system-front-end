// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery, convertJsonToFormData } from 'redux/reducers/settings';
import { BaseResponse } from 'common/types';
import { SignUpResponse } from '../auth';

export interface UserResponse {
  id: number;
  createdAt: string;
  updatedAt: string;
  roles: string;
  isAccountDisabled: boolean;
  email: string;
  color: string;
  userProfile: IUserProfile;
  name: string;
  username: string;
}

export interface IUserProfile {
  fullName: string;
  phoneNumber: string;
  country: string;
  company: string;
  isAccountDeactivated: boolean;
  isEmailNotificationOn: boolean;
  showAsActive: boolean;
  newsLaterNotification: boolean;
  userPictureUrl: string;
}

export const userApi = createApi({
  reducerPath: 'userApi',
  baseQuery: baseQuery,
  tagTypes: ['Posts'],
  endpoints: (build) => ({
    getCurrentUser: build.query<BaseResponse<UserResponse>, void>({
      query: () => 'users/me',
    }),
    getUsersLists: build.query<BaseResponse<UserResponse[]>, void>({
      query: () => 'users',
    }),
    searchUsersByEmailOrName: build.mutation<BaseResponse<UserResponse[]>, string>({
      query(key) {
        return {
          url: `users/search-user/${key}`,
          method: 'GET',
        };
      },
      invalidatesTags: [{ type: 'Posts', id: 'LIST' }],
    }),
  }),
});

export const { useGetCurrentUserQuery, useGetUsersListsQuery, useSearchUsersByEmailOrNameMutation } = userApi;
