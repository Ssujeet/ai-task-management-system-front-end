/* eslint-disable react/no-children-prop */
import { IRoutes } from './routesLists';
import PageNotFound from 'components/PageNotFound';
import { useLocalStorage } from 'hooks/useStorage';
import React, { ReactElement, Suspense } from 'react';
import FallBackLoader from 'components/FallBackLoader';
import { Switch, Route, RouteComponentProps, Redirect } from 'react-router-dom';

interface IRoutesProps {
  routes: IRoutes[];
}
interface RenderRouteProps extends RouteComponentProps, IRoutes {}

export const RenderRoute: React.FC<IRoutes> = (props) => {
  const { component } = props;
  const Component: React.ComponentType<RenderRouteProps> = component;

  if (Component) {
    return (
      <Route
        exact={props.exact}
        render={(routerProps: RouteComponentProps) => <Component {...routerProps} {...props} />}
      />
    );
  }

  return <PageNotFound />;
};

const PrivateRoutes: React.FC<IRoutesProps> = ({ routes }): ReactElement => {
  const [token] = useLocalStorage('token');

  const filteredRoutes: IRoutes[] = token
    ? routes.filter((item) => !item.isPublic)
    : routes.filter((item) => item.isPublic);

  return (
    <Suspense fallback={<FallBackLoader />}>
      <Switch>
        {filteredRoutes.map((route, index) => (
          <RenderRoute key={index} {...route} />
        ))}

        {!token && <Redirect exact to={'/sign'} from={'*'} />}
        <Redirect exact to={'/'} from={'/sign'} />
        <Redirect exact to={'/'} from={'/signUp'} />
        {/* <Route path="/page-not-found" component={PageNotFound} />
        <Redirect to="/page-not-found" /> */}
      </Switch>
    </Suspense>
  );
};
export default PrivateRoutes;
