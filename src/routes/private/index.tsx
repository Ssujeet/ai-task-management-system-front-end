/* eslint-disable react/display-name */
import React, { ReactNode } from 'react';
import { GoProject } from 'react-icons/go';
import { IRoutes } from 'routes/routesLists';
import { AiFillProfile } from 'react-icons/ai';
import { GiTeamUpgrade } from 'react-icons/gi';
import { GrStatusInfoSmall } from 'react-icons/gr';
import Protected from '../../../src/pages/protected';
import Home from '../../../src/pages/protected/Home';
import Profile from '../../../src/pages/protected/Profile/Profile';
import Board from '../../../src/pages/protected/Boards/boards';
import { HiViewBoards } from 'react-icons/hi';

import BoardIndexPage from '../../../src/pages/protected/Boards';
import StandUp from '../../../src/pages/protected/Boards/StandUp';
import Members from '../../../src/pages/protected/Boards/Members';
import Roles from '../../../src/pages/protected/Boards/PermissionAndRoles/Roles';
import Permissions from '../../../src/pages/protected/Boards/PermissionAndRoles/Permissions';

import ProjectChats from '../../../src/pages/protected/Boards/Chat';
import Calender from '../../../src/pages/protected/Boards/Calender';
import StandUpDetails from 'pages/protected/Boards/StandUp/StandUpDetails';
import BoardDetails from '../../../src/pages/protected/Boards/BoardsDetails';
import { MdSpaceDashboard, MdDocumentScanner, MdMessage, MdSecurity } from 'react-icons/md';
import ProjectDetails from '../../../src/pages/protected/Boards/ProjectDetails';
import ProjectDocuments from '../../../src/pages/protected/Boards/ProjectDocuments';
import { SCREENS, SCREENS_ROUTES_ENUMS } from 'common/enum';
import BoardIcon from 'assets/icons-svg/BoardsIcon';
import ChatIcon from 'assets/icons-svg/ChatIcon';
import MeetingsIcon from 'assets/icons-svg/MeetingsIcon';
import TeamsIcon from 'assets/icons-svg/TeamsIcon';
import RolesIcon from 'assets/icons-svg/RolesIcon';
import DocumentIcon from 'assets/icons-svg/DocumentIcon';
import Support from 'pages/protected/Support/support';
import SupportTicketIcon from 'assets/icons-svg/SupportTickerIcon';
import SupportTicketDetails from 'pages/protected/Support/SupportTicketDetails';
import Billing from 'pages/protected/Billing/billing';

export const privateRoutes: IRoutes[] = [
  {
    path: SCREENS_ROUTES_ENUMS.DASHBOARD,
    title: 'DashBoard',
    component: Protected,
    exact: true,
    childrenRoutes: [
      {
        path: SCREENS_ROUTES_ENUMS.DASHBOARD,
        title: 'Dashboard',
        component: Home,
        notShowToNavBar: true,
        exact: true,
        icon: (color) => <BoardIcon color={color} />,
      },
      {
        path: SCREENS_ROUTES_ENUMS.WORKSPACE,
        title: 'Workspace',
        component: BoardIndexPage,
        icon: (color) => <BoardIcon color={color} />,
        childrenRoutes: [
          {
            path: SCREENS_ROUTES_ENUMS.BOARDS,
            title: 'Boards',
            component: BoardDetails,
            screenkey: SCREENS.BOARDS,
            icon: (color) => <BoardIcon color={color} />,
          },
          {
            path: SCREENS_ROUTES_ENUMS.Documents,
            title: 'Documents',
            component: ProjectDocuments,
            exact: true,
            screenkey: SCREENS.DOCUMENTS,
            icon: (color) => <DocumentIcon color={color} />,
          },
          {
            path: SCREENS_ROUTES_ENUMS.Chats,
            title: 'Chats',
            component: ProjectChats,
            screenkey: SCREENS.CHAT,
            icon: (color) => <ChatIcon color={color} />,
          },
          // {
          //   path: SCREENS_ROUTES_ENUMS.STANDUP,
          //   title: 'Stand up',
          //   component: StandUp,
          //   screenkey: SCREENS.STANDUP,
          //   icon: (color) => <MeetingsIcon color={color} />,
          //   childrenRoutes: [
          //     {
          //       path: SCREENS_ROUTES_ENUMS.STANDUPDETAILS,
          //       title: 'StandUp Details',
          //       component: StandUpDetails,
          //     },
          //   ],
          // },
          // {
          //   path: 'works-space/details/:id',
          //   title: 'Project Details',
          //   component: ProjectDetails,
          // },
          {
            path: SCREENS_ROUTES_ENUMS.MEMBERS,
            title: 'Members',
            component: Members,
            screenkey: SCREENS.MEMBERS,
            icon: (color) => <TeamsIcon color={color} />,
          },
          {
            path: SCREENS_ROUTES_ENUMS.ROLES,
            title: 'Roles and Permissions',
            component: Roles,
            screenkey: SCREENS.ROLES,
            icon: (color) => <RolesIcon color={color} />,
            childrenRoutes: [
              {
                path: SCREENS_ROUTES_ENUMS.PERMISSIONS,
                title: 'Permissions',
                component: Permissions,
              },
            ],
          },
          // {
          //   path: 'works-space/calender/:id',
          //   title: 'Calender',
          //   component: Calender,
          // },
        ],
      },
      {
        path: SCREENS_ROUTES_ENUMS.PROFILE,
        title: 'Profile',
        component: Profile,
        exact: true,
        icon: (color) => <TeamsIcon color={color} />,
      },
      // {
      //   path: SCREENS_ROUTES_ENUMS.BILLING,
      //   title: 'Billing and Payments',
      //   component: Billing,
      //   exact: true,
      //   icon: (color) => <TeamsIcon color={color} />,
      // },
      // {
      //   path: SCREENS_ROUTES_ENUMS.SUPPORT_TICKET,
      //   title: 'Support Ticket',
      //   component: Support,
      //   exact: true,
      //   icon: (color) => <SupportTicketIcon color={color} />,
      //   childrenRoutes: [
      //     {
      //       path: SCREENS_ROUTES_ENUMS.SUPPORT_TICKET_DETAILS,
      //       title: 'Support Ticket Details',
      //       component: SupportTicketDetails,
      //     },
      //   ],
      // },
    ],
  },
];
