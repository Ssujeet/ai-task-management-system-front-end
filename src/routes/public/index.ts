/* eslint-disable react/display-name */
import React, { ReactNode } from 'react';
import { IRoutes } from 'routes/routesLists';
import SignIn from 'pages/Public/SigIn/SiginPage';
import SignUp from 'pages/Public/SignUp/SignUpPage';
import ResetPassword from 'pages/Public/ResetPassword';
import ForgotPassword from 'pages/Public/ForgotPassword';
import EmailSuccess from 'pages/Public/EmailSuccess/EmailSuccess';
import VerifyEmail from 'pages/Public/VerifyEmail/VerifyEmail';

// const SignIn = React.lazy(() => import('../../pages/Public/SigIn/SiginPage'));
// const SignUp = React.lazy(() => import('../../pages/Public/SignUp/SignUpPage'));
// const ResetPassword = React.lazy(() => import('../../pages/Public/ResetPassword'));
// const ForgotPassword = React.lazy(() => import('../../pages/Public/ForgotPassword'));

export const publicRoutes: IRoutes[] = [
  {
    path: '/sign',
    title: 'Sign',
    component: SignIn,
    exact: true,
    isPublic: true,
  },
  {
    path: '/signup',
    title: 'signup',
    component: SignUp,
    exact: true,
    isPublic: true,
  },
  {
    path: '/forgot-password',
    title: 'ForgotPassword',
    component: ForgotPassword,
    exact: true,
    isPublic: true,
  },
  {
    path: '/reset-password',
    title: 'Reset Password',
    component: ResetPassword,
    exact: true,
    isPublic: true,
  },
  {
    path: '/success-email-sent',
    title: 'Email Sent',
    component: EmailSuccess,
    exact: true,
    isPublic: true,
  },
  {
    path: '/verify-email',
    title: 'Verify Email',
    component: VerifyEmail,
    exact: true,
    isPublic: true,
  },
];
