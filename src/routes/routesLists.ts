/* eslint-disable react/display-name */
import React, { ReactNode } from 'react';
import { privateRoutes } from './private';
import { publicRoutes } from 'routes/public';
import { SCREENS } from 'common/enum';
export interface IRoutes {
  path: string;
  title: string;
  component: React.ComponentType<any>;
  exact?: boolean;
  notShowToNavBar?: boolean;
  childrenRoutes?: IRoutes[];
  isPublic?: boolean;
  icon?: (color?: string) => ReactNode;
  screenkey?: SCREENS;
}

export const routes: IRoutes[] = [...publicRoutes, ...privateRoutes];
