import { useLocation } from 'react-router-dom';
import React from 'react';

export const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

export function useQueryParams() {
  const { search } = useLocation();

  return React.useMemo(() => new URLSearchParams(search), [search]);
}

export function getFirstLetters(str): string {
  const firstLetters = str
    ?.split(' ')
    .map((word) => word[0])
    .join('')
    ?.toUpperCase();

  return firstLetters;
}

export function getRandomColor() {
  const randomColor = Math.floor(Math.random() * 16777215).toString(16);

  return '#' + randomColor;
}

export function hexToRGBA(hex, opacity) {
  if (hex) {
    return (
      'rgba(' +
      (hex = hex?.replace('#', ''))
        .match(new RegExp('(.{' + hex.length / 3 + '})', 'g'))
        .map(function (l) {
          return parseInt(hex.length % 2 ? l + l : l, 16);
        })
        .concat(isFinite(opacity) ? opacity : 1)
        .join(',') +
      ')'
    );
  }

  return '';
}
