import { io } from 'socket.io-client';

let url: string = process.env.REACT_APP_SOCKET_URL as string;
class Socket {
  url: string;
  socket: any;
  constructor() {
    this.url = url;
    this.socket = null;
  }

  connect = () => {
    // const token = SSOService.getToken();
    const host = this.url;
    this.socket = io(host, {
      reconnection: false,
      reconnectionDelay: 500,
      reconnectionDelayMax: 500,
      randomizationFactor: 0.5,
      auth: {
        token: localStorage.getItem('token') ? JSON.parse(localStorage.getItem('token') || '') : '',
      },
      secure: true,
      transports: ['websocket'],
    });
  };

  checkConnection = () => {
    // debugger
    console.log('Socket Connection');
    socket.connect();
    this.socket.on('connect', () => {
      console.log(this.socket.connected, 'Socket Connection');
    });
  };

  disconnect = () => this.socket.close();
}

const socket: Socket = new Socket();
// debugger
socket.checkConnection();

export default socket;

export const socketEnvironment = {
  reconnection: false,
  reconnectionDelay: 500,
  reconnectionDelayMax: 500,
  randomizationFactor: 0.5,
  auth: {
    token: localStorage.getItem('token') ? JSON.parse(localStorage.getItem('token') || '') : '',
  },
  secure: true,
  transports: ['websocket'],
};
