module.exports = {
  mode: 'jit',
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'gray-light': 'rgba(41, 42, 46, 0.1)',
        'notification-unseen': 'rgba(248, 83, 101, 0.1)',
        'notification-hover-unseen': 'rgba(248, 83, 101, 0.20)',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
